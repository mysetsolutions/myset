# About
This project was designed for integration purposes. Its main objectives are developing, building, deploying, and maintaining a single web application with a minimal set of dependencies.
After deployment within your preferred application server, you can extend it with custom extensions in a few steps.
[![Sample welcome page](myset-welcome.png)](myset-welcome.png) 


# Extensions
- myset-themes - Themes extension. Custom framework themes.
- myset-base - Base extension. Custom composites and services.
#### Extensions that require additional database configurations
- myset-admin - Administration extension. Administrative components and features (Terminal, File Manager)
- myset-reports - Reports extension. BIRT (Business Intelligence Reporting Tools) reports composites and services.
- myset-cms - CMS extension. CMS (Content Management Service) composites and services.
- myset-tests - Tests extension. Framework features tests.

## Application server deployment
In order to create application server archive according with application server specifications, you need set up runtime external java properties or update the following properties defined into pom.xml

```
        <!-- application deployment (external java properties) -->
        <app.name>${project.artifactId}</app.name>
        <app.archiveFor>tomcat</app.archiveFor>
        <app.archiveName>${app.name}##${project.version}</app.archiveName>
```

to your custom needs

```
        <!-- application deployment (external java properties) -->
        <app.name>myapp</app.name>
        <app.archiveFor>wildfly</app.archiveFor>
        <app.archiveName>${app.name}</app.archiveName>

```


## Testing configurations

### Case 1
In order to run java test classes using custom properties, you need to update java system properties defined into pom.xml

```
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <systemPropertyVariables>
                        <log4j.configurationFile>${project.basedir}/src/test/resources/conf/logger.xml</log4j.configurationFile>
                        <myset.home>${project.basedir}/src/test/resources</myset.home>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
```


### Case 2
In order to run java web application you need to define java system property "myset.home". Java system property naming convention is "your custom application deployment name" concatenated with ".home"

```
-Dmyset.home=<fullpath-to-the-resources-folder>
```





