## MYSET-24.0
- Updates scheduled for 2024
***



## MYSET-23.0
- Updates for 2023
***

### 202210
- Update myset: spring 5.3.23

### 202209
- Update maven-owasp-dependency-check for all modules 

### 202205
- Update myset: ignite-2.13.0
- Update myset: spring-5.3.20

### 202204
- myset-test: test-fa-icons.xhtml
- myset: primeflex 3.1.3
- myset: font-awesome free 6.1.1


### 202202
- myset-worker: embeded Apache Tomcat 9.0.54
- Update myset: ignite-2.12.0 with ignite-log4j2 instead of ignite-jcl

### 202201
- myset: Fix font-awesome location for welcome section 
- Update myset: ignite-2.11.1, ant-1.10.12
- Update myset-base: primeflex-3.1.2
- Update myset: include font-awesome.css in templates
- Update myset-themes: based on primefaces 11.0.0
- Update myset: primefaces 11.0.0, primefaces-extensions 11.0.0
- Update all: version 22


## MYSET-22.0
- Updates for 2022
***

### 202112
- Update all: junit 5.8.0, junit-platform 1.8.2
- Update myset-utils: jmh 1.34, log4j 2.17.1
- Update myset: spring 5.3.14
- Update ignite logger: ignite-jcl instead of ignite-log4j2
- Update maven plugins with latest versions


### 202111
- Terminal update extension commands. Initializing AntRunner before each extension command
- extension-manager 1.1

### 202110
- Upgrade myset: ignite 2.11.0

### 202109
- Update: ckeditor.js, ckeditor-cms.js
- Upgrade myset: spring 5.3.9, javax.el-3.0.1-b12, primeface-extensions-10.0.5

### 202105
- myset-tests add cache get put sample
- !!! change ignite cache names and service names with _ istead of .
- filemanager, filemanager-custom maxChunkSize to allow resume-functionality
  maxChunkSize="1000000"

### 202104
- cms messages dataTable change List on selectedMessages
- cms messages layout refactoring
- cms search layout refactoring
- dataTable composites fixes for globalFilter persistence
- FileUpload: supports chunking and FileDownload: supports AJAX
- dataTable - new atributes: rowStyleClass, size, strippedRow, showGridlines
- dataTable - pe:exporter -> p:dataExporter
- Fixes on filemanager, filemanager-custom multiple selection refactoring using List<File>
- dataTable multiple selection refactoring using List<Object> !!!
- Captcha update from h:inputText to p:inputText
- Update common.css
- Upgrade myset: ignite-2.10.0

### 202103
- primeflex: all templates and pages reorganized
- p:resolveWidgetVar now returns the widget var name only, instead of PF('widgetVarName')
- Upgrade font-awesome-5.15.2 and included in myset-themes
- panel-grid - colums mandatory
- all templates files updated, primeflex, no footer, sticky header
- common.css many fixes
- p:layoutUnit discarded and replaced with pure css
- Compilation errors: PrimefaceUtils.download, DefaultStreamedContent
- https://primefaces.github.io/primefaces/10_0_0/#/../migrationguide/10_0_0
- Upgrade myset: primefaces-10.0.0, primefaces-extensions-10.0.0, resources-ckeditor-10.0.0

### 202102
- activate grid data persistence and grid authentication
  application.xml - cluster activation
  application-grid.xml - data persistence activation, authenticationEnabled grid property

### 202101
- Upgrade myset-utils: version 21, junit-5.7.0, junit-platform-1.7.0, jmh-1.27.0, log4-2.14.0, xerces-2.12.1
- Upgrade myset: version 21, myset-utils-21.0, apache-ignite-2.9.1
- Upgrade myset-themes: version 21
- Upgrade myset-base: version 21, myset-21.0
- Upgrade myset-admin: version 21, myset-21.0, myset-base-21.0
- Upgrade myset-reports: version 21, myset-21.0
- Upgrade myset-cms: version 21, myset-21.0, myset-base-21.0, myset-admin-21.0
- Upgrade myset-test: version 21, myset-21.0, myset-base-21.0, myset-admin-21.0, myset-cms-21.0


## MYSET-21.0
- Updates for 2021
***

### 202010
- Add logging mc:log
- myset-base - common.css fixes
- myset-test - cms.css fixes
- myset-base - dataTable, dataTable fix globalFilter value
- myset-cms - search
  cms_search.sql01,.sql02,.sql03
- myset - appcore_Utils new method getString
- myset-cms - refactoring cms composites
  cms-views, cms-rates, cms-rate
- myset-cms - CMSProcessor updates newsletters, remove newsletterType
- application-filter-security.xml - except test-cms


### 202009
- muset-base - remove - preference.lastPage
- myset-base - DBMS_LOB.compare, oracle
  security.sql07,08,11 updated
  menu, login updated
- myset-base - properties.xml, menu.logout.url
- myset-base - Filemanager, FilemanagerCustom
  synchronized void refreshFolderContent
  refreshSelectionFlags
  multipleSelectedFiles
- myset-base - menu.submenu in properties.xml
- myset - appcore_Utils filter using regexp pattern for value
- myset-cms - CMSProcessor updates pages and parts only by name
- myset-cms - CMS stats simplified. Removed statsType, statForId
- myset-cms - CMS comments removed commentForId
- myset-cms - CMSProcessor updates and database-comand with readclob="true"
- myset-base - fix reload blocked request
- myset-base - user-registration for security.login.type:simple,ldap
- myset-base - login add option security.login.type:console,simple,ldap,external
- myset-base - security.sql01 without password,
  security.sql02 with userId,
  security.sql011 -> sql99 with userId
- myset-base - refactor log using appcore_LoggerDatabase
- refactoring - login, users, forgot-passoword, user-registration
- myset-base - forgot password using username, security.sql05, security.sql06
- myset-admin - add without password field, generate random UUID
- properties.xml - new security section
- refactoring menu with all dynamic submenus
- refactoring help in all modules
- myset-base - login-console removed, included in login
- myset-tests - REPORTS, CMS conditional display
- Update myset-base - dataTable, dataTableSimple
  filters session persistence
- Update myset-cms - new calendars
- Update myset-admin - refreshUsers
- refactoring myset-cms

### 202008
- appcore_Utils - checkClass
- New method appcore_Utils.sleep
- Update application.xml - loadToCache value="#{appcore_Utils.sleep(2000,true)}"
- refactoring myset-cms
- Upgrade ignite 2.8.1

### 202005
- myset-base - warn dataTable, dataTableSimple warn datasets.rows.limit
  dictionary datatable.rows.exceeded
- myset-cms - cms_calendars_types management
- myset-cms - Move beans to appcore_CMSApplication, appcore_CMSSession
- refactoring myset-cms
- OpenSans downloadable font: kern: Too large subtable
  replace font with updated version 97k instead of 212k
  update common.css @font (woff2, woff, ttf)
- dataTable, dataTableSimple - Add ajax event on colReorder for persistence
- dataTable, dataTableSimple - global filter session and pageId persistence
- dataTable, dataTableSimple - Add multiViewState="true" and resetState button
- Fix part tags - cms_parts.sql07, cms_parts.sql08
- Fix utils.GenericMap - get ...  && super.containsKey(key)
- Fix pf 8.0 - upload package change UploadedFile
  org.primefaces.file to org.primefaces.model.file
  listener instead of fileUploadListener
- Fix pf 8.0 - download DefaultStreamedContent.builder()
  fix contentType, update conf/application-mime-types.xml
- Fix pf 8.0 - p:calendar - java.util.Date converter add
- primefaces, primefaces-extensions update - 8.0, 8.0.2
- cms_comments - filter using date, user - cms_comments.sql01
- web.editor.config for filemanager and cms.editor.config for parts content
- all - activate dataTable header filters (except id's)
- all - add favicon.png into templates
- myset-cms - datbase.sql small updates
- myset-cms - refactoring some code loggers with uuid
- myset-cms - extension refactoring
- refactoring datatables with rowKey="#{row.hashCode()}"

### 202004
- Fix for ignite-2.8.0 - lazy init, serviceProxy using timeout 10*1000
  for: application.xml, application-tasks.xml, application-filter-log.xml
- Fix for ignite-2.8.0 - TasksProcessorServiceImpl private static fields
- Fix for ignite-2.8.0 - Update all services extended classes with implements Serializable
- Fix for ignite-2.8.0 - Update all services fields NonSerializableException
- Upgrade ignite-2.8.0 - New Binary Marshaler


### 202003
- myset-utils Pool, DatabasePool logging fixes
- Upgrade spring 5.2.4
- View text fixes
- logs filter date validator
- appcore_Application init fixes
- myset-base-extension filemanager.folders.size, for slow storages
- !!! ApplicationErrorsHandlerServlet, ApplicationFilter annotation,
  not works on jdk-11 and tomcat 9, switch to web.xml (declaration, mapping)
- @PostContruct replaced in all classes with InitializingBean @Override public void afterPropertiesSet
- pom.xml java compile 11 fixes

### 202002
- DatabaseUtils with method clobToFile
- Roles page update, common.css (picklist)
- Updates about page using application-packages
- New configurations: application-mime-types, application-packages
- Update composites mc:dataTable, mc:dataTableSimple rowKey, sortBy
  full expression field identification #{row.fieldId}
- Update of m:database-command with readclob attribute
- DatabaseProcessor, DatabaseProcessorCommand field readClob, (default false)

### 202001
- f:convertDateTime fix java 11+, filemanager, filemanager-custom, test database
  add display pattern using fixed format (pattern="#{appcore_Application.property('format.datetime')})
- Update version 20.0 for all projects


## MYSET-20.0
- Updates for 2020
***

### 201912
- Apache POI interference with BIRT 4.6.0 (XLSX accept only POI 3.8-3.13)
- Build using jdk 1.8, run and test with jdk and testing with: 1.8.0_212, 11.0.4, 12.0.2
- Update jsf from mojarra to jakarta 2.3.14
- Cleanup ant dependencies (jsch,net,regexp ...) and keep only necessary
- Cleanup apache-ignite dependencies (web,jta,urideploy ...) and keep only necessary
- Remove ignite JCL, replace wirth log4j2
- Update DatabaseProcessor with transactionExecuteAddBatch, transactionExecuteBatch,
  transactionCommit, transactionRollback for multi-thread safe
- Move annotations from ApplicationErrorsHandlerServlet to web.xml
- Update Spring to 5.1.12
- Update Apache Ignite 2.7.6

### 201911
- Update DatabaseProcessor with transactionExecuteBatch
- Fix Clob reader Clob or String
- m:eval new component replace f:event type="postAddToView"
- m:eval new component
- Refactor m: for myset components and mc: for myset composites

### 201910
- Move common/users.xml into administration/common/users.xml
- Update log with readClob, readClobLimited
- DatabaseUtils various static utilities
- Update DatabaseUtils, readClob, readClobLimited
- Update ApplicationSecurity logout. Logout based on events. Save last accessed page at logout
- Application, Session, Request refresh without attributes
- Issues related to deprecated @None replaced by @Dependent, @Scope("prototype")
- Properties updates application.login
- myset-base, myset-admin extensions

### 201909
- Update myset-utils: XmlValidator refactoring
- Welcome extension
- Updates terminal refresh commands (configuration, extensions)
- Updates Application with refreshExtensions
- Updates AntRunner, TaskAntRunner, AntRunnerFile , TaskAntRunnerFile
- Change application serviceNames
- Extensions management into terminal
- Cleanup framework in order to implement extensions

### 201908
- Update web.xml with version 3.1 for servlet 4.0 with web-fragments <absolute-ordering />
- Update file manager paths and relative paths
- Filemanager file size labels properties into dictionary
- Dictionary updates

### 201907
- Update templates and properties with "preference.enable"
- Create base configuration
- Base resources configurations
- Update myset: cleanup base build
- Update faces-config.xml: Converters and Validators, class discovery does not work

### 201906
- Update: junit 5.4.0,1.4.2
- Update: log4j 2.12.0
- Update: xerces 2.12.0
- Update: javax.mail 1.6.2
- Update: org.springframework 5.1.8
- Update: org.apache.ignite 2.7.5
- Update: commons-fileupload 1.4
- Update: javax.el 3.0.1-b11
- Update: cdi-api 2.0.SP1
- Update: javax.json 1.1.4
- Update: jackson-core 2.9.9
- Update: com.google.code.gson 2.8.5
- Update: ant 1.10.6
- Update: javax.faces 2.3.9
- Update: primefaces 7.0
- Update: primefaces-extensions 7.0.1
- Update: font-awesome 5.9.0
- Request move execute and dialog methods to FacesUtils
- FacesUtils update execute deprecated method with PrimeFaces.current().executeScript
- FacesUtils update deprecated dialog framework with PrimeFaces.current().dialog()
- PrimeFaces 7.0 - changed it by design native url add #{request.contextPath} to each menu


## MYSET-19.0
- Updates for 2019
- Frameworks updates
- Maven plugins updates
- Dependencies updates
- Extensions
- New release notation myset-<major version last two digits of year>.<minor - sequence>
***

###  201801
- Maven dependencies added for ignite: spring-websocket, jackson
- application-websockets.xml configuration added
- Maven dependencies added for ignite: web, ssh, urideploy, jta
- Custom test application sample

## MYSET-18.0
- Updates for 2018
***

### 201712
- Maven dependencies added for camel: xml, http, http4, ahc, servlet, rss, websocket, atmosphere,
- Maven dependencies added for primefaces: atmosphere, charts, barcode, qrcode, feedreader
- MailProcessorService batch bcc - distinct recipients
- Sample design.css fixes
- MailPoolService new property maxRecipients removed from properties-cms "cms.email.recipients"
- MailProcessorService new method sendBatch many messages using Bcc, splited by maxRecipients
- CMSProcessor newsletter using MailProcessorService sendBatch method
- Added composites: mail-async, mail-batch, mail-batch-async
- ApplicationFilter fixes.
- ApplicationFilter: securityFilter evaluate all matching rules
- Balance security check between myset:security-check and application-filters-security.xml for all pages
- Update all templates with any roles=".*"
- CMSMessages fixes

### 201711
- Upgrade spring to 5.0.2
- Default session timeout - application.session.timeout
- Default authenticated session timeout - application.session.timeout.authenticated
- Fix Thread.sleep Interruption Exception: myset-utils, myset
- Fix dependecies for exporters:itext 5.5.12, POI 3.16
- Add application-tasks.xml configuration for tasks handling

### 201710
- Fix CMSSearch for searchTextAndTags, no tags selected throws exception.
- Add apache-camel 2.20.0 support: spring,ignite,exec,jdbc,sql,jms,jpa,
  ssh,ftp,jsch,mail
- Apache Ignite upgrade to 2.2.0

### 201709
- Fix remove from cache after part content update
- Fix sample /resources/default/js/ckeditor.js
- Fix sample /resources/default/css/design.css
- Updated myset-themes package with card and ui-shadow-1...5

### 201708
- Updated common.css, custom.css less !important usages;
- Errors pages fixes
- Updated application.xml with jsf tunning properties
- New listener added ApplicationSessionListener
- New error handler ApplicationErrorsHandlerServlet
- New application-filters-errors.xml
- Updated Application with errorFilters
- Updated logger.xml
- Updated application.xml with: application-filters-errors.xml
- Updated properties.xml with: application.errors, application.session.timeout
- Updated web.xml:servlet, session-timeout, error-pages

### 201708
- Apache Ignite upgrade to 2.1.0 (ML, distributed algebra, K-mean clustering)
- Update grid-default.xml deprecated: gridName, marshaler section, cahe property swapEnabled
- New flag added for preference.lastPage login redirection - loginRedirectPage.concat('?init=true')
- FileManager actions logging to info instead of debug

### 201707
- CMS pages list upgrade with page location link to view page into a
  new browser tab using property "cms.development.context" and page location

### 201706
- Update extensions with myset.extension.excludes property
- Update errors session-expired replaced by view-expired.xhtml
- Update javax.el-3.0.1-b05.jar with javax.el-3.0.1-b08.jar
- Update about.xhtml with "Expression Language"

### 201705
- Update terminal with refresh command (Administration -> Refresh menu link removed)
- Update application.xml create:
  application-filter-log,application-filter-security,application-filter-cache, application-filter-rewrite
- Update refreshable dynamicApplicationContext
- Update tests/test-regexp.xhtml
- Update ApplicationFilter with UrlRewritter
- Update appcore_Utils.regexpMatcher
- Upgrade about page
- Upgrade myset-reports with birt to 4.6.0
- Upgrade primefaces-extensions to 6.1.0 - patch codemirror 6.1.1
- Update ckeditor.js, common.js with - applicationContextPath

### 201704
- Update Terminal.java - deleted pre tags (terminal already preformated and escaped)
- Update common.css - fix code-mirror height
- Upgrade primefaces-extensions to 6.1.0
- Upgrade primefaces to 6.1
- Updates common.css, custom.css
- Updates on ckeditor.js to include sample components.css, common.css, custom.css
- Fix CMS database commands without setLog
- Fix on cms_search.sql02
- Update cms/search/main.xhtml to receive text from dictionaries
- Update application terminal with encryptMD5, encrypt, decrypt
- Add new managed bean for security appcore_SecurityUtils
- Update myset-utils with EncryptDecryptUtils

### 201703
- Updates on ckeditor.js to include sample custom.css and ckeditor-styles.js
- Add sample page for EL - tests/test-el.xhtml
- Fix logger write into console.
- Update default template-error with lastPage redirect on /resources/myset/main.xhtml
- Rename cms, reports inner test directory with tests
- Update test-revisions
- Fix for user add exists (username, null)
- Update errors pages
- Fix logout error on session expired
- Update of dataTable and dataTableSimple composite with reflow attribute
- Update of common.css with new style class "datatable-auto-columns"
- Update of all dataTables with "datatable-auto-columns"

### 201702
- Update registration check user_name or user_email - security.sql09
- Update dictionary.xml - user-registration.user.exists
- Fixes for filemanager, filemanager-custom
- Logger RegexFilter applied
- Delete columns from users: USER_CERT, USER_LDAP
- Delete column from groups: GROUP_LDAP
- Update configurations: conf/mysql|conf/oracle
- Customizations
- Delete fields and update: administration/users
- Update froms: login.xhtml, login-console.xhtml


## MYSET-17.0
- Updates for 2017
***


## MYSET-16.0
- Updates for 2016
- CMS initial implementation
- Frameworks updates
***


## MYSET-15.0
- Updates for 2015
- BIRT Reports integration
- Frameworks updates
***


## MYSET-14.0
- First release
***
