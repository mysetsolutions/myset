/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import ro.myset.reports.Reports;

import java.util.Map;
import java.util.Properties;

/**
 * ReportsProcessorService interface.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public interface ReportsProcessorService {

    /**
     * Gets reports properties
     */
    public Properties getProperties();

    /**
     * Sets reports properties
     */
    public void setProperties(Properties properties);

    /**
     * Gets reports engine
     */
    public Reports getReports();

    /**
     * Process report
     */
    public void process(String report,
                        Map reportOptions,
                        Map reportParameters
    ) throws Exception;

    /**
     * Process report asynchronous
     */
    public void processAsync(String report,
                             Map reportOptions,
                             Map reportParameters
    ) throws Exception;


}
