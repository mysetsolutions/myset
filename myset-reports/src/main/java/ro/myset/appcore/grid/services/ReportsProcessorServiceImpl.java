/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.reports.Reports;
import ro.myset.reports.common.Constants;

import java.util.Map;
import java.util.Properties;


/**
 * DatabaseProcessorServiceImpl class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class ReportsProcessorServiceImpl implements Service, ReportsProcessorService {
    private static final Logger LOG = LogManager.getFormatterLogger(ReportsProcessorServiceImpl.class);

    private Properties properties = null;
    private Reports reports = null;

    public ReportsProcessorServiceImpl() {
        LOG.debug(this);
    }

    @IgniteInstanceResource
    private Ignite ignite;

    @Override
    public void cancel(ServiceContext serviceContext) {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug("%s, %s", this, reports);
        try {
            reports.shutdown();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void init(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        reports = new Reports(properties);
        reports.startup();
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void execute(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug("%s, %s", this, reports);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public Properties getProperties() {
        return properties;
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public Reports getReports() {
        return this.reports;
    }

    @Override
    public final void process(String report,
                              Map reportOptions,
                              Map reportParameters) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        reports.process(report, reportOptions, reportParameters);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public final void processAsync(final String report,
                                   final Map reportOptions,
                                   final Map reportParameters) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        final Reports reportsFinal = reports;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    reportsFinal.process(report, reportOptions, reportParameters);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.setName(ReportsProcessorServiceImpl.class.getName().concat("-processAsync"));
        t.start();
        LOG.debug(Constants.EVENT_COMPLETED);
    }


}
