/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.reports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.web.Application;
import ro.myset.reports.common.Constants;
import ro.myset.appcore.grid.services.ReportsProcessorService;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Reports class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_Reports")
@Dependent
@Scope("prototype")

public class Reports implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LogManager.getFormatterLogger(Reports.class);

    @Autowired
    private Application appcore_Application;

    private final String OUTPUT_FORMAT = "Format";

    public Reports() {
        LOG.debug(this);
    }

    public final void process(ReportsProcessorService reportsProcessorService,
                              String report,
                              String reportFormat,
                              boolean reportOutputInline,
                              String reportOutputFileName,
                              Map reportOptions,
                              Map reportParameters) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        if (reportsProcessorService != null) {
            // responseReset
            FacesContext.getCurrentInstance().getExternalContext().responseReset();

            // reportHeaders
            FacesContext.getCurrentInstance().getExternalContext().addResponseHeader("Content-Type", appcore_Application.getMimeTypes().getString(reportFormat));

            StringBuffer contentDisposition = new StringBuffer();
            if (reportOutputInline)
                contentDisposition.append("inline");
            else
                contentDisposition.append("attachment");
            contentDisposition.append("; filename=\"");
            if (reportOutputFileName == null || Constants.EMPTY.equals(reportOutputFileName)) {
                contentDisposition.append("report.");
                contentDisposition.append(reportFormat.toLowerCase());
            } else
                contentDisposition.append(reportOutputFileName);
            contentDisposition.append("\"");
            FacesContext.getCurrentInstance().getExternalContext().addResponseHeader("Content-Disposition", contentDisposition.toString());

            // reportOptions
            if (reportOptions == null)
                reportOptions = new HashMap();

            // reportOutputFormat
            reportOptions.put(OUTPUT_FORMAT, reportFormat);

            // reportOutputStream
            reportOptions.put("outputStream", FacesContext.getCurrentInstance().getExternalContext().getResponseOutputStream());

            // process
            reportsProcessorService.process(report, reportOptions, reportParameters);

            // responseComplete
            FacesContext.getCurrentInstance().responseComplete();
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void setAppcore_Application(Application appcore_Application) {
        this.appcore_Application = appcore_Application;
    }
}
