/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.reports;

/**
 * ApplicationReports bean.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

import org.apache.ignite.Ignite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.grid.services.ReportsProcessorService;
import ro.myset.appcore.web.Application;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;


@Named("appcore_ApplicationReports")
@ApplicationScoped
@Scope("singleton")

public class ApplicationReports implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationReports.class);

    @Autowired
    private Application appcore_Application;

    private static final String REPORTS_PROCESSOR_SERVICE_NAME_PROPERTY = "reports.processor.service.name";


    public ApplicationReports() throws Exception {
        LOG.debug(this);
    }

    //=======================================
    //          ReportsProcessorService
    //=======================================

    /**
     * Gets reports processor service from application grid.
     */
    public final ReportsProcessorService reportsProcessorService(String serviceName) {
        if (appcore_Application.getGrid() != null)
            return appcore_Application.getGrid().services().serviceProxy(serviceName, ReportsProcessorService.class, false);
        return null;
    }

    /**
     * Gets application tasks processor service from application grid.
     */
    public final ReportsProcessorService getReportsProcessorService() {
        return reportsProcessorService(appcore_Application.getServicesNames().getProperty(REPORTS_PROCESSOR_SERVICE_NAME_PROPERTY));
    }

    /**
     * Gets tasks processor service from custom grid.
     */
    public final ReportsProcessorService reportsProcessorService(Ignite customGrid, String serviceName) {
        if (customGrid != null)
            return customGrid.services().serviceProxy(serviceName, ReportsProcessorService.class, false);
        return null;
    }


}
