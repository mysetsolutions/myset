/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.reports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;

import java.text.MessageFormat;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * ReportsLogHandler java logging bridge to log4j2
 */
public class ReportsLogHandler extends Handler {
    private static final String LOGGER_UNKNOWN="UNKNOWN";

    public void publish(LogRecord record) {
        // Logger name
        String loggerName = record.getLoggerName();
        if (loggerName == null)
            loggerName = record.getSourceClassName();
        if (loggerName == null)
            loggerName = LOGGER_UNKNOWN;

        // Logger level
        Logger LOG = LogManager.getFormatterLogger(loggerName);
        Level level = record.getLevel();

        // Logger message
        String message =record.getMessage();
        if (record.getParameters() != null && record.getParameters().length != 0) {
            MessageFormat messageFormat = new MessageFormat(record.getMessage());
            message = messageFormat.format(record.getParameters());
        }

        // Logger write
        if (Level.SEVERE.equals(level)) {
            LOG.error(message);
        } else if (Level.WARNING.equals(level)) {
            LOG.warn(message);
        } else if (Level.INFO.equals(level)) {
            LOG.info(message);
        } else if (Level.FINE.equals(level)) {
            LOG.debug(message);
        } else if (Level.FINER.equals(level)) {
            LOG.trace(message);
        } else if (Level.FINEST.equals(level)) {
            LOG.trace(message);
        } else if (Level.ALL.equals(level)) {
            LOG.trace(message);
        }
    }

    @Override
    public void flush() {
        // nothing to do
    }

    @Override
    public void close() throws SecurityException {
        // nothing to do
    }
}
