/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.reports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.birt.core.data.DataTypeUtil;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.*;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.core.internal.registry.RegistryProviderFactory;
import ro.myset.reports.common.Constants;

import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.Level;

/**
 * ReportsEngine class
 */
public class Reports {
    private static final Logger LOG = LogManager.getFormatterLogger(Reports.class);

    private Properties properties = null;
    private EngineConfig config = null;
    private IReportEngineFactory factory = null;
    private IReportEngine reportEngine = null;

    // common
    private final String BIRT_ROOT_LOGGER = "org.eclipse.birt.report";

    // output formats
    private final String OUTPUT_HTML = "html";
    private final String OUTPUT_PDF = "pdf";
    private final String OUTPUT_ODT = "odt";
    private final String OUTPUT_DOC = "doc";
    private final String OUTPUT_DOCX = "docx";
    private final String OUTPUT_ODS = "ods";
    private final String OUTPUT_XLS = "xls";
    private final String OUTPUT_XLSX = "xlsx";
    private final String OUTPUT_ODP = "odp";
    private final String OUTPUT_PPT = "ppt";
    private final String OUTPUT_PPTX = "pptx";
    private final String OUTPUT_POSTSCRIPT = "postscript";


    // reports engine configuration params
    private final String REPORTS_LOGGER_LEVEL = "reports.logger.level";
    private final String REPORTS_RESOURCES_PATH = "reports.resources.path";
    private final String REPORTS_TEMP_PATH = "reports.temp.path";
    private final String REPORTS_FONTS_URL = "reports.fonts.url";

    // reports engine messages
    private final String MESSAGE_01 = "Invalid configuration properties";
    private final String MESSAGE_02 = "Invalid 'reports.resources.path' property value";
    private final String MESSAGE_03 = "Invalid 'reports.temp.path' property value";

    public Reports() {
    }


    public Reports(Properties properties) {
        this.properties = properties;
    }


    public final void startup() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        if (properties == null)
            throw new NullPointerException(MESSAGE_01);

        config = new EngineConfig();

        // jul to log4j
        java.util.logging.Logger reportsLogger = java.util.logging.Logger.getLogger(BIRT_ROOT_LOGGER);
        reportsLogger.setUseParentHandlers(false);
        for (Handler handler : reportsLogger.getHandlers()) {
            handler.setLevel(Level.OFF);
        }
        reportsLogger.addHandler(new ReportsLogHandler());
        if (properties.getProperty(REPORTS_LOGGER_LEVEL) != null) {
            reportsLogger.setLevel(Level.parse(properties.getProperty(REPORTS_LOGGER_LEVEL)));
        }
        config.setLogger(reportsLogger);

        //resources
        if (properties.getProperty(REPORTS_RESOURCES_PATH) == null)
            throw new NullPointerException(MESSAGE_02);
        config.setResourcePath(properties.getProperty(REPORTS_RESOURCES_PATH));

        // temp
        if (properties.getProperty(REPORTS_TEMP_PATH) == null)
            throw new NullPointerException(MESSAGE_03);
        config.setTempDir(properties.getProperty(REPORTS_TEMP_PATH));

        // fonts
        if (properties.getProperty(REPORTS_FONTS_URL) != null)
            config.setFontConfig(new URL(properties.getProperty(REPORTS_FONTS_URL)));

        // startup
        Platform.startup(config);
        factory = (IReportEngineFactory) Platform.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
        reportEngine = factory.createReportEngine(config);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void shutdown() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        reportEngine.destroy();
        Platform.shutdown();
        RegistryProviderFactory.releaseDefault();
        LOG.debug(Constants.EVENT_COMPLETED);
    }


    /**
     * Process report
     */
    public void process(String report,
                        Map reportOptions,
                        Map reportParameters
    ) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.info("report - %s, reportOptions - %s,reportParameters - %s", report, reportOptions, reportParameters);

        // init report execution
        IReportRunnable reportDesign = null;
        IRunAndRenderTask reportTask = null;
        try {
            reportDesign = reportEngine.openReportDesign(report);
            reportTask = reportEngine.createRunAndRenderTask(reportDesign);

            //report options
            IRenderOption reportRenderOption = new RenderOption(reportOptions);

            if (reportRenderOption.getOption(IRenderOption.OUTPUT_FORMAT).equals(OUTPUT_HTML))
                reportRenderOption = new HTMLRenderOption(reportRenderOption);
            if (reportRenderOption.getOption(IRenderOption.OUTPUT_FORMAT).equals(OUTPUT_PDF))
                reportRenderOption = new PDFRenderOption(reportRenderOption);
            if (reportRenderOption.getOption(IRenderOption.OUTPUT_FORMAT).equals(OUTPUT_XLS) ||
                    reportRenderOption.getOption(IRenderOption.OUTPUT_FORMAT).equals(OUTPUT_XLSX))
                reportRenderOption = new EXCELRenderOption(reportRenderOption);

            // report parameters
            if (reportParameters != null) {
                Iterator parametersNames = reportParameters.keySet().iterator();
                String parameterName = null;
                while (parametersNames.hasNext()) {
                    parameterName = (String) parametersNames.next();
                    reportTask.setParameterValue(parameterName, convertParameterType(reportParameters.get(parameterName)));
                }
                reportTask.validateParameters();
            }

            // report task run
            reportTask.setRenderOption(reportRenderOption);
            reportTask.run();
        } catch (Exception e) {
            throw e;
        } finally {
            if (reportTask != null)
                reportTask.close();
        }

        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Convert report parameters
     */
    private final static Object convertParameterType(Object value) throws BirtException {
        if (value == null)
            return null;
        String type = value.getClass().getSimpleName();
        if (DesignChoiceConstants.PARAM_TYPE_STRING.equals(type)) {
            return DataTypeUtil.toString(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_INTEGER.equals(type)) {
            return DataTypeUtil.toInteger(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_DECIMAL.equals(type)) {
            return DataTypeUtil.toBigDecimal(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_FLOAT.equals(type)) {
            return DataTypeUtil.toDouble(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_DATETIME.equals(type)) {
            return DataTypeUtil.toDate(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_DATE.equals(type)) {
            return DataTypeUtil.toSqlDate(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_TIME.equals(type)) {
            return DataTypeUtil.toSqlTime(value);
        } else if (DesignChoiceConstants.PARAM_TYPE_BOOLEAN.equals(type)) {
            return DataTypeUtil.toBoolean(value);
        }
        return value;
    }


    /**
     * Sets reports properties
     */
    public final void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * Gets reports properties
     */
    public final Properties getProperties() {
        return properties;
    }

    public IReportEngine getReportEngine() {
        return reportEngine;
    }
}
