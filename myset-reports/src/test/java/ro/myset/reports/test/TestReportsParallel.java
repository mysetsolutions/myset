/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.reports.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;
import ro.myset.reports.Reports;
import ro.myset.reports.common.Constants;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * TestReports class
 */
public class TestReportsParallel {
    private static final Logger LOG = LogManager.getFormatterLogger(TestReportsParallel.class);

    public TestReportsParallel() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            Class[] classes = { TestReports.class,TestReports.class,TestReports.class,TestReports.class};
            JUnitCore.runClasses(new ParallelComputer(true, true), classes);
            LOG.debug(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(ro.myset.utils.common.Constants.EMPTY, e);
        }
    }
}
