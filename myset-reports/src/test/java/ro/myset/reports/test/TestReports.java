/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.reports.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.RepeatedTest;
import ro.myset.reports.Reports;
import ro.myset.reports.common.Constants;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * TestReports class
 */
public class TestReports {
    private static final Logger LOG = LogManager.getFormatterLogger(TestReports.class);

    public TestReports() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            long mm = Runtime.getRuntime().maxMemory() / (1024 * 1024);
            long um = 0;

            // memory usage
            um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
            LOG.debug("Memory usage: %s of %s Mb", um, mm);

            //--------------------------
            // run test report
            //--------------------------
            Reports reports = null;

            try {
                //--------------------------
                // reports startup
                //--------------------------
                reports = new Reports();
                Properties reportsServerProperties = new Properties();
                // ALL,FINEST,FINNER,FINE,INFO,WARNING,OFF
                reportsServerProperties.setProperty("reports.logger.level", "OFF");
                reportsServerProperties.setProperty("reports.resources.path", System.getProperty("myset.reports.home").concat("/reports/"));
                reportsServerProperties.setProperty("reports.temp.path", System.getProperty("myset.reports.home").concat("/temp/"));
                reportsServerProperties.setProperty("reports.fonts.url", "file://".concat(System.getProperty("myset.reports.home").concat("/fonts/fonts.xml")));
                reports.setProperties(reportsServerProperties);
                reports.startup();

                String report = reportsServerProperties.getProperty("reports.resources.path").concat("test/test.rptdesign");

                Map reportOptions = null;
                Map reportParameters = null;
                OutputStream reportOutputStream = System.out;
                for (int j = 1; j <= 4; j++) {
                    //reportOptions
                    reportOptions = new HashMap();
                    reportOptions.put("Format", "pdf");
                    //reportOptions.put("outputStream", reportOutputStream);
                    reportOptions.put("outputFile",System.getProperty("myset.reports.home").concat("/temp/report.").concat(reportOptions.get("Format").toString()));

                    reportOptions.put("myset.option1", "test");

                    // report parameters
                    reportParameters = new HashMap();
                    reportParameters.put("parameter1", "v1");
                    reportParameters.put("parameter2", "v2");

                    // process report
                    reports.process(report, reportOptions, reportParameters);
                    LOG.debug("%s - %s",Thread.currentThread().getName(),j);

                }

            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            } finally {
                try {
                    // Sleep before shutdown
                    Thread.sleep(5000);
                    if (reports != null)
                        reports.shutdown();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            // memory usage
            um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
            LOG.debug("Memory usage: %s of %s Mb", um, mm);


            LOG.debug(Constants.EVENT_COMPLETED);


        } catch (Exception e) {
            LOG.error(ro.myset.utils.common.Constants.EMPTY, e);
        }
    }
}
