/*----------------------------------------------------  
 *  CKEditor configuration
 *  Language: Java Script 
 *  Version: 1.0
 *  Author: Cristian Gheorghe Florescu
 ----------------------------------------------------*/

//===============================================
// Common
//===============================================
function applicationContextPath() {
    if (location.pathname.split('/').length > 1 &&
        location.pathname.split('/')[1] != 'resources') {
        return "/" + location.pathname.split('/')[1];
    }
    return "";
}
CKEDITOR.applicationContextPath = applicationContextPath();
//alert(CKEDITOR.applicationContextPath);
CKEDITOR.config.enterMode = CKEDITOR.ENTER_P;
CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_BR;

//===============================================
// Styles
//===============================================
CKEDITOR.config.contentsCss = [CKEDITOR.applicationContextPath + '/javax.faces.resource/components.css.xhtml?ln=primefaces',
    CKEDITOR.applicationContextPath + '/javax.faces.resource/theme.css.xhtml?ln=primefaces-myset-vision-light',
    CKEDITOR.applicationContextPath + '/javax.faces.resource/common/css/primeflex.css.xhtml?ln=myset',
    CKEDITOR.applicationContextPath + '/javax.faces.resource/common/css/common.css.xhtml?ln=myset',
    CKEDITOR.applicationContextPath + '/javax.faces.resource/common/css/cms.css.xhtml?ln=myset'];
CKEDITOR.scriptLoader.load(CKEDITOR.applicationContextPath + '/resources/myset/common/js/ckeditor-styles.js');
CKEDITOR.config.stylesSet = 'myStyles';


//===============================================
// Toolbar configuration
//===============================================
CKEDITOR.config.toolbar = [
    ['Source', 'Preview', 'Maximize', 'ShowBlocks'],
    ['NumberedList', 'BulletedList', 'Outdent', 'Indent', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl'],
    ['HorizontalRule', 'PageBreak', 'CreateDiv', 'Blockquote', 'Link', 'Unlink', 'Anchor', 'CreatePlaceholder', 'Image', 'Table', '-', 'Smiley', 'SpecialChar', 'Iframe', 'InsertPre', 'UIColor', 'Flash'],
    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
    '/',
    ['Undo', 'Redo'],
    ['Styles', 'Format', 'Font', 'FontSize'],
    ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', "-", 'RemoveFormat'],
    ['TextColor', 'BGColor'],
    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
    ['Find', 'Replace', 'SelectAll', 'Scayt'],
    ['About']
];


//===============================================
// Image upload configuration
//===============================================
//CKEDITOR.editorConfig = function (config) {
//    var contextPath = $('.contextPath').html();
//    config.filebrowserImageUploadUrl = contextPath+'/upload/';
//};

//===============================================
// Inline editing
//===============================================
//CKEDITOR.disableAutoInline = true;
//CKEDITOR.inlineAll();


//===============================================
// Writer formatting
//===============================================
CKEDITOR.on('instanceReady', function (ev) {
    // editor tags
    var editorTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'li', 'td'];
    for (var i = 0; i < editorTags.length; i++) {
        ev.editor.dataProcessor.writer.setRules(editorTags[i], {
            indent: true,
            breakBeforeOpen: true,
            breakAfterOpen: false,
            breakBeforeClose: false,
            breakAfterClose: false
        });
    }
    // block tags
    var blockTags = ['div', 'pre', 'blockquote', 'ul', 'ol', 'table', 'thead', 'tbody', 'tfoot', 'tr', 'th',];
    for (var i = 0; i < blockTags.length; i++) {
        ev.editor.dataProcessor.writer.setRules(blockTags[i], {
            indent: true,
            breakBeforeOpen: true,
            breakAfterOpen: true,
            breakBeforeClose: true,
            breakAfterClose: false
        });
    }
});



