<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ page import="org.apache.ignite.Ignite" %>
<%@ page import="ro.myset.appcore.web.Application" %>
<%@ page import="org.apache.ignite.IgniteQueue" %>
<%@ page import="org.apache.ignite.configuration.CollectionConfiguration" %>
<%@ page import="java.util.Date" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%
    ApplicationContext springContext = RequestContextUtils.findWebApplicationContext(request);
    out.println("<h1>Queue add ...</h1>");
    Application appcore_Application=((Application)springContext.getBean("appcore_Application"));

    // Init queue
    CollectionConfiguration collectionConfiguration = new CollectionConfiguration();
    collectionConfiguration.setCollocated(true);

    IgniteQueue<Object> queue = appcore_Application.getGrid().queue("queue-test", 0, collectionConfiguration);

    // Add queue elements.
    for (int i = 1; i <= 10; i++) {
        queue.add("Value "+i);
    }
%>

