<%@ page import="org.springframework.context.ApplicationContext"%>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
ApplicationContext springContext = RequestContextUtils.findWebApplicationContext(request);
out.println("<h2>"+springContext.getApplicationName()+"</h2>");
out.println("springContext - "+springContext);
out.println("<br/>beanDefinitionCount - "+springContext.getBeanDefinitionCount());
out.println("<br/>appcore_Application - "+springContext.getBean("appcore_Application"));
out.println("<br/>appcore_Session - "+springContext.getBean("appcore_Session"));
out.println("<br/>appcore_Request - "+springContext.getBean("appcore_Request"));
%>
<h2>Expression language</h2>
<p>\${1+4+5}: ${1+4+5}</p>

<h2>sessionScope</h2>
<p>${sessionScope}</p>

<h2>requestScope</h2>
<p>${requestScope}</p>



