import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ooxml.util.DocumentHelper;
import ro.myset.utils.security.UUIDUtils;

import javax.script.*;
import java.util.ArrayList;
import java.util.List;

public class TestScripting {
    private static final Logger LOG = LogManager.getFormatterLogger(TestScripting.class);

    @org.junit.Test
    public void test(){
        //--------------------------------
        // Check JavaScript engines
        //--------------------------------
        ScriptEngineManager mgr = new ScriptEngineManager();
        List<ScriptEngineFactory> factories = mgr.getEngineFactories();
        for (ScriptEngineFactory factory: factories) {
            String engName = factory.getEngineName();
            String engVersion = factory.getEngineVersion();
            String langName = factory.getLanguageName();
            String langVersion = factory.getLanguageVersion();
            LOG.debug("Script Engine - Name: %s, Version: %s", engName, engVersion);
            List<String> engNames = factory.getNames();
            for(String name: engNames) {
                LOG.debug("Engine Alias: %s", name);
            }
            LOG.debug("Language - Name: %s , Version: %s", langName, langVersion);

        }

        //--------------------------------
        // Sample JavaScript
        //--------------------------------
        ScriptEngine jsEngine = mgr.getEngineByExtension("js");
        try {
            //InputStream is = this.getClass().getResourceAsStream("/scripts/myscript.js");
            //Reader reader = new InputStreamReader(is);
            //jsengine.eval(reader);

            // Simple evaluation
            jsEngine.eval("print(3+7)");

            // List
            List<String> namesList = new ArrayList<>();
            namesList.add("Nico");
            namesList.add("Ana");
            namesList.add("Vlad");
            jsEngine.put("namesListKey", namesList);
            jsEngine.eval("namesListKey.add(\"Ioana\");");
            jsEngine.eval("function printNames(namesList) {" +
                    "  var x;" +
                    "  var names = namesList.toArray();" +
                    "  for(x in names) {" +
                    "    print(names[x]);" +
                    "  }" +
                    "}" +

                    "function addName(namesList, name) {" +
                    "  namesList.add(name);" +
                    "}");

            Invocable jsiEngine = (Invocable) jsEngine;
            jsiEngine.invokeFunction("addName",namesList,"Stefan");
            jsiEngine.invokeFunction("printNames",namesList);

            // java
            for (String name: namesList) {
                LOG.debug(name);
            }


        } catch (Exception e) {
            LOG.error(e);
        }
    }
}
