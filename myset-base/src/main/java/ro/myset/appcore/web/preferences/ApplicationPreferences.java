/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorServiceCommand;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Application user preferences class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ApplicationPreferences")
@SessionScoped
@Scope("session")
public class ApplicationPreferences implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationPreferences.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private Session appcore_Session;


    private String user = null;

    public ApplicationPreferences() throws Exception {
        LOG.debug(this);
    }

    private final void refresh() {
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            if (user != null) {
                String theme = this.preference("preference.theme");
                if (theme != null)
                    appcore_Session.setTheme(theme);
                String locale = this.preference("preference.locale");
                if (locale != null)
                    appcore_Session.setLocale(locale);
                String timezone = this.preference("preference.timezone");
                if (timezone != null)
                    appcore_Session.setTimezone(timezone);
                appcore_Session.refresh();
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }

    public final String preference(String key) {
        if (user == null || key == null)
            return null;
        StringBuffer cacheKeyBuffer = new StringBuffer();
        cacheKeyBuffer.append(user);
        cacheKeyBuffer.append(ro.myset.utils.common.Constants.DOT);
        cacheKeyBuffer.append(key);
        return (String) appcore_Application.getDataCache().get(cacheKeyBuffer.toString());
    }

    public final ArrayList<GenericBean> preferenceValues(String key) {
        if (user == null || key == null)
            return null;
        StringBuffer cacheKeyBuffer = new StringBuffer();
        cacheKeyBuffer.append(user);
        cacheKeyBuffer.append(ro.myset.utils.common.Constants.DOT);
        cacheKeyBuffer.append(key);
        return (ArrayList<GenericBean>) appcore_Application.getDataCache().get(cacheKeyBuffer.toString());
    }

    public final void updatePreference(String key, String value) {
        if (user == null || key == null)
            return;
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            // Delete user preferences
            databaseCommandId = "users_preferences.sql04";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(user);
            databaseCommandParams.add(key);
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommands.add(databaseCommand);

            // Add user preference
            databaseCommandId = "users_preferences.sql05";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(user);
            databaseCommandParams.add(key);
            databaseCommandParams.add(value);
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommands.add(databaseCommand);

            // transaction
            appcore_Application.getDatabaseProcessorService().executeCommandsAsync(databaseCommands);

            // refresh cache
            StringBuffer cacheKeyBuffer = new StringBuffer();
            cacheKeyBuffer.append(user);
            cacheKeyBuffer.append(ro.myset.utils.common.Constants.DOT);
            cacheKeyBuffer.append(key);
            appcore_Application.getDataCache().put(cacheKeyBuffer.toString(), value);


        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }


    public final void updatePreferences(String key, ArrayList<GenericBean> value, String keyFieldName, String valueFieldName) {
        if (user == null || key == null || keyFieldName == null || valueFieldName == null)
            return;
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            // Delete user preferences
            databaseCommandId = "users_preferences.sql04";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(user);
            databaseCommandParams.add(key);
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommands.add(databaseCommand);

            // Add user preferences
            for (int i = 0; i < value.size(); i++) {
                GenericBean userPreferenceBean = value.get(i);
                databaseCommandId = "users_preferences.sql05";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(user);
                databaseCommandParams.add(userPreferenceBean.getFields().get(keyFieldName));
                databaseCommandParams.add(userPreferenceBean.getFields().get(valueFieldName));
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);
            }

            // transaction
            appcore_Application.getDatabaseProcessorService().executeCommandsAsync(databaseCommands);

            // refresh cache
            StringBuffer cacheKeyBuffer = new StringBuffer();
            cacheKeyBuffer.append(user);
            cacheKeyBuffer.append(ro.myset.utils.common.Constants.DOT);
            cacheKeyBuffer.append(key);
            appcore_Application.getDataCache().put(cacheKeyBuffer.toString(), value);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }

    public final String getUser() {
        return user;
    }

    public final void setUser(String user) {
        this.user = user;
        this.refresh();
    }


}
