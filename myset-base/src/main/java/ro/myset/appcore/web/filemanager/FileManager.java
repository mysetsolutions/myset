/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.filemanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.*;
import org.primefaces.model.*;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.model.file.UploadedFiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.utils.disk.DiskUtils;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;
import ro.myset.utils.zip.ZipUtils;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * FileManager class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
@Named("appcore_FileManager")
@SessionScoped
@Scope("session")
public class FileManager implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(FileManager.class);
    private static final String ROOT_TYPE = "root";
    private static final String LOCAL_TYPE = "local";
    private static final String FOLDER_TYPE = "folder";
    private static final String NODE_FIELD_ID = "id";

    @Autowired
    private Application appcore_Application;

    @Autowired
    private FacesUtils appcore_FacesUtils;

    @Autowired
    private Session appcore_Session;

    // filemanager properties
    private boolean showFolderFirst = true;
    private boolean showFoldersSize = true;
    private int navivationHistoryMaxSize = 0;
    private ArrayList<String> nodeBeanFieldsNames;


    private ArrayList<GenericBean> paths;
    private TreeNode root;
    private TreeNode selectedTreeNode;
    private File currentFolder;
    private ArrayList<File> currentFolderContent;
    private File selectedFile;
    private List<File> selectedFiles;
    private List<File> actionFiles;

    private int uploadThresholdSize = 1048576;
    private String uploadTemporaryPath;
    private int readerBufferSize = 4096;

    // navigation history
    private ArrayList<File> navigationHistory;
    private int navigationHistoryPosition = 0;

    // buttons states
    private boolean upDisabled = false;
    private boolean backDisabled = false;
    private boolean forwardDisabled = false;
    private boolean copyPressed = false;
    private boolean movePressed = false;
    private boolean compressionPressed = false;

    // selection
    private boolean nothingSelected = false;
    private boolean singleSelected = false;
    private boolean singleSelectedFile = false;
    private boolean multipleSelected = false;
    private boolean multipleSelectedFiles = false;

    // actions fields
    private String newFileName;
    private String newFolderName;
    private String renameFileName;
    private String archiveFileName;

    //editor
    private GenericMap<String, File> editedFiles;


    public FileManager() {
        LOG.debug(this);
    }


    public void initialize() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        this.showFolderFirst = Boolean.parseBoolean(appcore_Application.propertyValue("filemanager.folders.first").getValueString());
        this.showFoldersSize = Boolean.parseBoolean(appcore_Application.propertyValue("filemanager.folders.size").getValueString());
        this.navivationHistoryMaxSize = Integer.parseInt(appcore_Application.propertyValue("filemanager.navigation.history.maxsize").getValueString());
        this.uploadThresholdSize = Integer.parseInt(appcore_Application.propertyValue("filemanager.upload.threshold").getValueString());
        this.uploadTemporaryPath = appcore_Application.propertyValue("filemanager.upload.temporary-path").getValueString();
        this.readerBufferSize = Integer.parseInt(appcore_Application.propertyValue("filemanager.reader.buffer").getValueString());
        navigationHistory = new ArrayList<File>();
        navigationHistoryPosition = 0;
        nodeBeanFieldsNames = new ArrayList<String>();
        nodeBeanFieldsNames.add(NODE_FIELD_ID);
        this.createTree();
        editedFiles = new GenericMap<String, File>();
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void reset() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        // paths
        paths = null;
        root = null;
        selectedTreeNode = null;
        currentFolder = null;
        currentFolderContent = null;
        selectedFile = null;
        selectedFiles = new ArrayList<File>();

        // navigation history
        navigationHistory = null;
        navigationHistoryPosition = 0;

        // buttons states
        upDisabled = false;
        backDisabled = false;
        forwardDisabled = false;
        actionFiles = null;
        copyPressed = false;
        movePressed = false;
        compressionPressed = false;

        // selection
        nothingSelected = false;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;

        // actions fields
        newFileName = null;
        newFolderName = null;
        renameFileName = null;
        archiveFileName = null;

        // editor
        editedFiles = new GenericMap<String, File>();

        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void resetAfterAction() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        // paths
        selectedFile = null;
        selectedFiles = null;

        // buttons states
        actionFiles = null;
        copyPressed = false;
        movePressed = false;
        compressionPressed = false;

        // selection
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;

        // actions fields
        newFileName = null;
        newFolderName = null;
        renameFileName = null;
        archiveFileName = null;


        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void refreshSelectionFlags() throws Exception{
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;
        // check single file
        if (selectedFiles!=null && selectedFiles.size() == 1) {
            nothingSelected = false;
            singleSelected = true;
            if (selectedFile!=null && selectedFile.isFile())
                singleSelectedFile = true;
        }
        // check multiple files
        if (selectedFiles!=null && selectedFiles.size() > 1) {
            nothingSelected = false;
            singleSelected = false;
            multipleSelected = true;
            for (int i = 0; i <selectedFiles.size() ; i++) {
                if(selectedFiles.get(i).isDirectory()){
                    multipleSelectedFiles=false;
                    break;
                } else
                    multipleSelectedFiles=true;
            }
        }
    }

    private final void createTree() throws Exception {
        root = new DefaultTreeNode(null, null);
        TreeNode firstNode = new DefaultTreeNode(ROOT_TYPE, null, root);
        firstNode.setSelected(true);
        firstNode.setExpanded(true);
        if (paths != null) {
            for (int i = 0; i < paths.size(); i++) {
                File f = new File(paths.get(i).getValueString());
                if (f.isDirectory()) {
                    createTreeNode(LOCAL_TYPE, firstNode, f, paths.get(i).getFields().get(NODE_FIELD_ID).toString());
                }
            }
        }

        //selection flags
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;

        this.selectCurrentTreeNode(root, currentFolder);
        this.resetSelection();
        this.refreshFolderContent();
    }

    private final void createTreeNode(String nodeType, TreeNode parentNode, File folder, String name) throws Exception {
        if (folder.isDirectory()) {
            // create generic bean
            GenericBean nodeBean = new GenericBean();
            nodeBean.setFields(new GenericMap<String, String>());
            nodeBean.setFieldsNames(nodeBeanFieldsNames);
            nodeBean.getFields().put(NODE_FIELD_ID, name);
            nodeBean.setValue(folder);
            // create tree node
            TreeNode currentNode = new DefaultTreeNode(nodeType, nodeBean, parentNode);
            parentNode.getChildren().add(currentNode);
            File[] childsAll = folder.listFiles();
            Arrays.sort(childsAll);
            for (int i = 0; i < childsAll.length; i++) {
                if (childsAll[i].isDirectory()) {
                    createTreeNode(FOLDER_TYPE, currentNode, childsAll[i], childsAll[i].getName());
                }
            }
        }
    }

    private final void selectCurrentTreeNode(TreeNode treeNode, File file) throws Exception {
        if (file != null && file.exists() && file.isDirectory()) {
            treeNode.setSelected(false);
            treeNode.setExpanded(false);
            List<TreeNode> children = treeNode.getChildren();
            File childrenFile = null;
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i).getData() != null) {
                    children.get(i).setSelected(false);
                    children.get(i).setExpanded(false);
                    childrenFile = ((File) ((GenericBean) children.get(i).getData()).getValue());
                    if (file.getCanonicalPath().equals(childrenFile.getCanonicalPath())) {
                        //LOG.debug("%s,%s",children.get(i),childrenFile);
                        selectedTreeNode = children.get(i);
                        children.get(i).setSelected(true);
                        children.get(i).setExpanded(true);
                        expandParents(selectedTreeNode);
                        return;
                    }
                }
                //recurse
                selectCurrentTreeNode(children.get(i), file);
            }
        }
    }

    private final void expandParents(TreeNode treeNode) {
        if (treeNode.getParent() != null) {
            treeNode.getParent().setExpanded(true);
            expandParents(treeNode.getParent());
        }
    }

    private final void removeTreeNode(TreeNode treeNode) throws Exception {
        //LOG.debug(((GenericBean)treeNode.getData()).getValue());
        selectedTreeNode = treeNode.getParent();
        List<TreeNode> children = treeNode.getChildren();
        for (int i = 0; i < children.size(); i++) {
            removeTreeNode(children.get(i));
        }
        treeNode.getParent().getChildren().remove(treeNode);
    }

    private final void removeTreeNodeChild(TreeNode treeNode, File child) throws Exception {
        //LOG.debug(((GenericBean)treeNode.getData()).getValue());
        List<TreeNode> children = treeNode.getChildren();
        File childrenFile = null;
        for (int i = 0; i < children.size(); i++) {
            childrenFile = ((File) ((GenericBean) children.get(i).getData()).getValue());
            if (child.isDirectory() && child.getCanonicalPath().equals(childrenFile.getCanonicalPath()))
                removeTreeNode(children.get(i));
        }
    }

    public final void treeNodeExpand(NodeExpandEvent event) throws Exception {
        selectedTreeNode.setSelected(false);
        selectedTreeNode = event.getTreeNode();
        selectedTreeNode.setSelected(true);
        currentFolder = ((File) ((GenericBean) event.getTreeNode().getData()).getValue());
        this.refreshFolderContent();
    }


    public final void treeNodeSelect(NodeSelectEvent event) throws Exception {
        // expand/collapse selected tree node
        if (selectedTreeNode.isExpanded())
            selectedTreeNode.setExpanded(false);
        else
            selectedTreeNode.setExpanded(true);
        if (event.getTreeNode().getData() != null) {
            currentFolder = ((File) ((GenericBean) event.getTreeNode().getData()).getValue());
            selectedFile = currentFolder;
        }
        //selection flags
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;

        this.addToNavigationHistory();
        this.refreshFolderContent();
    }


    public final void toggleSelect(ToggleSelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getSource() != null) {
            if (event.isSelected()) {
                selectedFiles = (ArrayList<File>) ((DataTable) event.getSource()).getValue();
            } else
                selectedFiles = null;
            // refresh
            this.refreshSelectionFlags();
        }
    }

    public final void contentNodeSelect(SelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            selectedFile = (File) event.getObject();
            // refresh
            this.refreshSelectionFlags();
        }

    }


    public final void contentNodeUnselect(UnselectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            // refresh
            this.refreshSelectionFlags();
        }

    }


    public final void contentNodeDbSelect(SelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            File selection = (File) event.getObject();
            if (selection.isDirectory()) {
                currentFolder = selection;
                this.addToNavigationHistory();
                this.refreshFolderContent();
            }
        }
    }

    private final void addToNavigationHistory() throws Exception {
        if (currentFolder == null)
            return;
        if (navigationHistory.size() == 0 || !navigationHistory.get(navigationHistory.size() - 1).getCanonicalPath().equals(currentFolder.getCanonicalPath())) {
            navigationHistory.add(currentFolder);
            if (navigationHistory.size() > navivationHistoryMaxSize)
                navigationHistory.remove(0);
            navigationHistoryPosition = navigationHistory.size() - 1;
        }
    }

    public final synchronized void refreshFolderContent() throws Exception {
        if (currentFolder == null)
            return;
        currentFolderContent = new ArrayList<File>();
        File[] currentFolders = currentFolder.listFiles();
        Arrays.sort(currentFolders);
        if (currentFolders != null) {
            if (showFolderFirst) {
                // folders
                for (int i = 0; i < currentFolders.length; i++) {
                    if (currentFolders[i].isDirectory())
                        currentFolderContent.add(currentFolders[i]);
                }
                // files
                for (int i = 0; i < currentFolders.length; i++) {
                    if (currentFolders[i].isFile())
                        currentFolderContent.add(currentFolders[i]);
                }
            } else {
                // all
                for (int i = 0; i < currentFolders.length; i++) {
                    currentFolderContent.add(currentFolders[i]);
                }
            }
        }
        // activate buttons states
        if (FOLDER_TYPE.equals(selectedTreeNode.getType())) {
            upDisabled = false;
        } else
            upDisabled = true;
        if (navigationHistoryPosition > 0)
            backDisabled = false;
        else
            backDisabled = true;
        if (navigationHistoryPosition < navigationHistory.size() - 1)
            forwardDisabled = false;
        else
            forwardDisabled = true;

    }

    //==========================================
    //              Buttons bar
    //==========================================
    private final void resetSelection() throws Exception {
        copyPressed = false;
        movePressed = false;
        compressionPressed = false;
    }

    public final void refreshTree() throws Exception {
        this.resetSelection();
        this.createTree();
    }

    public final void up() throws Exception {
        selectedTreeNode.setSelected(false);
        if (FOLDER_TYPE.equals(selectedTreeNode.getType())) {
            currentFolder = currentFolder.getParentFile();
            if (selectedTreeNode.getParent() != null) {
                selectedTreeNode.getParent().setSelected(true);
                selectedTreeNode = selectedTreeNode.getParent();
            }
        }
        this.refreshFolderContent();
    }

    public final void back() throws Exception {
        if (navigationHistoryPosition > 0) {
            navigationHistoryPosition--;
            currentFolder = navigationHistory.get(navigationHistoryPosition);
            if (!currentFolder.exists()) {
                navigationHistory.remove(navigationHistoryPosition);
                back();
            }
        }
        selectedTreeNode.setSelected(false);
        this.refreshFolderContent();
    }

    public final void forward() throws Exception {
        if (navigationHistoryPosition < navigationHistory.size() - 1) {
            navigationHistoryPosition++;
            currentFolder = navigationHistory.get(navigationHistoryPosition);
            if (!currentFolder.exists()) {
                navigationHistory.remove(navigationHistoryPosition);
                forward();
            }
        }
        selectedTreeNode.setSelected(false);
        this.refreshFolderContent();
    }

    public final StreamedContent download(File file) throws Exception {
        if (file != null && file.isFile()) {
            LOG.info("%s,%s", file.getCanonicalPath(), file.length());
            InputStream fis = new FileInputStream(file);
            return DefaultStreamedContent.builder().contentType(appcore_Application.getMimeTypes().getString(DiskUtils.getFileExtension(file.getName()))).name(file.getName()).stream(() -> fis).build();
        }
        return null;
    }

    public final void upload(FileUploadEvent event) {
        try {
            UploadedFile uploadedFile = event.getFile();
            InputStream is = uploadedFile.getInputStream();
            byte[] buffer = new byte[readerBufferSize];
            File file = new File(currentFolder, uploadedFile.getFileName());
            LOG.info("%s,%s", file.getCanonicalPath(), uploadedFile.getSize());
            FileOutputStream fos = new FileOutputStream(file);
            int bufferLength = 0;
            while ((bufferLength = is.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
                fos.flush();
            }
            is.close();
            fos.close();
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }

    }

    public final void delete() {
        try {
            if (selectedFiles != null && selectedFiles.size() > 0) {
                for (int i = 0; i < selectedFiles.size(); i++) {
                    LOG.info(selectedFiles.get(i));
                    // delete file
                    if (selectedFiles.get(i) != null && selectedFiles.get(i).exists() && selectedFiles.get(i).isFile()) {
                        DiskUtils.delete(selectedFiles.get(i));
                    }
                    //delete directory
                    if (selectedFiles.get(i) != null && selectedFiles.get(i).exists() && selectedFiles.get(i).isDirectory()) {
                        DiskUtils.deleteDirectory(selectedFiles.get(i));
                        refreshTree();
                    }
                }
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void newFile() {
        try {
            LOG.info("%s,%s", currentFolder, newFileName);
            if (currentFolder != null && currentFolder.exists()) {
                File newFile = new File(currentFolder, newFileName);
                if (!(newFile.exists() && newFile.isFile())) {
                    newFile.createNewFile();
                } else
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.file-exist"));
                resetAfterAction();
                refreshFolderContent();
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void newFolder() {
        try {
            LOG.info("%s,%s", currentFolder, newFolderName);
            if (currentFolder != null && currentFolder.exists()) {
                File newFolder = new File(currentFolder, newFolderName);
                if (!(newFolder.exists() && newFolder.isDirectory())) {
                    newFolder.mkdir();
                } else
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.folder-exist"));
                refreshTree();
                resetAfterAction();
                refreshFolderContent();
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void rename() {
        try {
            LOG.info("%s,%s", selectedFile, renameFileName);
            // rename file
            if (selectedFile != null && selectedFile.exists() && selectedFile.isFile()) {
                File renamedFile = new File(currentFolder, renameFileName);
                if (renamedFile.exists() && renamedFile.isFile()) {
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.file-exist"));
                    return;
                }
                selectedFile.renameTo(renamedFile);
            }
            // rename folder
            if (selectedFile != null && selectedFile.exists() && selectedFile.isDirectory()) {
                File renamedFile = new File(selectedFile.getParentFile(), renameFileName);
                if (renamedFile.exists() && renamedFile.isDirectory()) {
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.folder-exist"));
                    return;
                }
                renamedFile.mkdir();
                selectedFile.renameTo(renamedFile);
                currentFolder = selectedFile.getParentFile();
                selectedFile = currentFolder;
                refreshTree();
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void copy() {
        try {
            resetSelection();
            copyPressed = true;
            actionFiles=selectedFiles;

        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void move() {
        try {
            resetSelection();
            movePressed = true;
            actionFiles=selectedFiles;

        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void paste() {
        try {
            if (actionFiles != null) {
                //copy
                if (copyPressed) {
                    for (int i = 0; i < actionFiles.size(); i++) {
                        LOG.debug("Copy %s to %s", actionFiles.get(i), currentFolder);
                        DiskUtils.copy(actionFiles.get(i), currentFolder, true);
                        LOG.info("Copied %s to %s", actionFiles.get(i), currentFolder);
                    }
                }
                //move
                if (movePressed) {
                    for (int i = 0; i < actionFiles.size(); i++) {
                        LOG.debug("Move %s to %s", actionFiles.get(i), currentFolder);
                        DiskUtils.move(actionFiles.get(i), currentFolder, true);
                        LOG.info("Moved %s to %s", actionFiles.get(i), currentFolder);
                    }
                    resetSelection();
                }
            }
            refreshTree();
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }


    public final void compress() {
        try {
            LOG.info("%s,%s", currentFolder, archiveFileName);
            if (selectedFiles != null && selectedFiles.size() > 0) {
                File archiveFile = new File(currentFolder, archiveFileName);
                ZipUtils zipUtils = new ZipUtils();
                String[] files = new String[selectedFiles.size()];
                for (int i = 0; i < selectedFiles.size(); i++) {
                    files[i] = selectedFiles.get(i).getCanonicalPath();
                    LOG.info("Add %s to %s", files[i], archiveFileName);
                }
                zipUtils.createArchive(files, archiveFile.getCanonicalPath(), true);
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void extract() {
        try {
            LOG.info("%s,%s", currentFolder, selectedFile);
            if (selectedFile != null && selectedFile.isFile()) {
                File dest = currentFolder;
                if (archiveFileName != null)
                    dest = new File(currentFolder, archiveFileName);
                ZipUtils.extractAll(selectedFile, dest, true);
            }
            refreshTree();
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    //==========================================
    //              Fields
    //==========================================

    public final ArrayList<GenericBean> getPaths() {
        return paths;
    }

    public final void setPaths(ArrayList<GenericBean> paths) {
        this.paths = paths;
    }

    public final boolean isShowFoldersSize() {
        return showFoldersSize;
    }

    public final void setShowFoldersSize(boolean showFoldersSize) {
        this.showFoldersSize = showFoldersSize;
    }

    public final double fileSizeValue(File f, double sizeUnit) throws Exception {
        if(f.isDirectory() && !showFoldersSize)
            return -1;
        return DiskUtils.fileSizeValue(f, sizeUnit);
    }

    public final String fileSizeUnit(File f, double sizeUnit, String format) throws Exception {
        if(f.isDirectory() && !showFoldersSize)
            return Constants.EMPTY;
        return DiskUtils.fileSizeUnit(f, sizeUnit, format);
    }

    public final TreeNode getRoot() {
        return root;
    }

    public final TreeNode getSelectedTreeNode() {
        return selectedTreeNode;
    }

    public final void setSelectedTreeNode(TreeNode selectedTreeNode) {
        if (this.selectedTreeNode != null)
            this.selectedTreeNode.setSelected(false);
        this.selectedTreeNode = selectedTreeNode;
        this.selectedTreeNode.setSelected(true);
    }

    public final File getCurrentFolder() {
        return currentFolder;
    }

    public final void setCurrentFolder(File currentFolder) {
        this.currentFolder = currentFolder;
    }

    public final File getSelectedFile() {
        return selectedFile;
    }

    public final void setSelectedFile(File selectedFile) {
        this.selectedFile = selectedFile;
    }

    public final List<File> getSelectedFiles() {
        return selectedFiles;
    }

    public final void setSelectedFiles(List<File> selectedFiles) {
        this.selectedFiles = selectedFiles;
    }

    public final ArrayList<File> getCurrentFolderContent() {
        return currentFolderContent;
    }

    public final boolean isUpDisabled() {
        return upDisabled;
    }

    public final boolean isBackDisabled() {
        return backDisabled;
    }

    public final boolean isForwardDisabled() {
        return forwardDisabled;
    }

    public final boolean isNothingSelected() {
        return nothingSelected;
    }

    public final boolean isSingleSelected() {
        return singleSelected;
    }

    public final boolean isSingleSelectedFile() {
        return singleSelectedFile;
    }

    public final boolean isMultipleSelected() {
        return multipleSelected;
    }

    public final boolean isMultipleSelectedFiles() {
        return multipleSelectedFiles;
    }

    public final int getUploadThresholdSize() {
        return uploadThresholdSize;
    }

    public final String getUploadTemporaryPath() {
        return uploadTemporaryPath;
    }

    public final String getNewFileName() {
        return newFileName;
    }

    public final void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public final String getNewFolderName() {
        return newFolderName;
    }

    public final void setNewFolderName(String newFolderName) {
        this.newFolderName = newFolderName;
    }

    public final String getRenameFileName() {
        return renameFileName;
    }

    public final void setRenameFileName(String renameFileName) {
        this.renameFileName = renameFileName;
    }

    public final String getArchiveFileName() {
        return archiveFileName;
    }

    public final void setArchiveFileName(String archiveFileName) {
        this.archiveFileName = archiveFileName;
    }

    public final boolean isCopyPressed() {
        return copyPressed;
    }

    public final void setCopyPressed(boolean copyPressed) {
        this.copyPressed = copyPressed;
    }

    public final boolean isMovePressed() {
        return movePressed;
    }

    public final void setMovePressed(boolean movePressed) {
        this.movePressed = movePressed;
    }

    public final boolean isCompressionPressed() {
        return compressionPressed;
    }

    public final void setCompressionPressed(boolean compressionPressed) {
        this.compressionPressed = compressionPressed;
    }

    public final GenericMap<String, File> getEditedFiles() {
        return editedFiles;
    }

    public final void setEditedFiles(GenericMap<String, File> editedFiles) {
        this.editedFiles = editedFiles;
    }
}
