/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.preferences.ApplicationPreferences;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.appcore.web.utils.LoggerDatabase;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.security.MD5Utils;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Application security base.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ApplicationSecurityBase")
@RequestScoped
@Scope("request")
public class ApplicationSecurityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    // constants
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationSecurityBase.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private ApplicationPreferences appcore_ApplicationPreferences;


    public ApplicationSecurityBase() {
        LOG.debug(this);
    }


    /**
     * Get user attributes from application database
     *
     * @param userName - User name
     */
    protected final GenericBean getUserGenericBean(String uuid, String userName) throws Exception {
        GenericBean userGenericBean = null;
        try {
            LOG.debug("[%s] userName: %s", uuid, userName);
            DatabaseCommand databaseCommand = new DatabaseCommand();
            String databaseCommandId = "security.sql01";
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(userName);
            databaseCommand.setParameters(databaseCommandParams);
            ArrayList<GenericBean> userGenericBeans = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
            if (userGenericBeans != null && userGenericBeans.size() == 1)
                userGenericBean = userGenericBeans.get(0);
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            return userGenericBean;
        }
    }


    /**
     * Get user roles from application database
     *
     * @param userGenericBean - User attributes
     */
    protected final ArrayList <GenericBean> getUserRolesGenericBean(String uuid, GenericBean userGenericBean) throws Exception {
        ArrayList <GenericBean> userRolesGenericBeans = null;
        try {
            LOG.debug("[%s] userId: %s", uuid, userGenericBean.getFields().get("userId").toString());
            if (userGenericBean != null) {
                DatabaseCommand databaseCommand = new DatabaseCommand();
                String databaseCommandId = "security.sql02";
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(userGenericBean.getFields().get("userId").toString());
                databaseCommand.setParameters(databaseCommandParams);
                userRolesGenericBeans = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
            }

        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            return userRolesGenericBeans;
        }
    }


    /**
     * After logon actions
     *
     * @param userGenericBean - User attributes
     */
    protected final void afterLogon(String uuid, GenericBean userGenericBean) throws Exception {
        try {
            LOG.debug("[%s] userId: %s", uuid, userGenericBean.getFields().get("userId").toString());
            if (userGenericBean != null) {
                DatabaseCommand databaseCommand = new DatabaseCommand();
                String databaseCommandId = "security.sql99";
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandUpdateParams = new ArrayList<Object>();
                databaseCommandUpdateParams.add(userGenericBean.getFields().get("userId").toString());
                databaseCommand.setParameters(databaseCommandUpdateParams);
                appcore_Application.getDatabaseProcessorService().executeCommandAsync(databaseCommand);

                // ApplicationPreferences
                appcore_ApplicationPreferences.setUser(userGenericBean.getFields().get("userName").toString());

            }

        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            throw e;
        }
    }




}
