/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.database.DatabaseCommand;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * LoggerDatabase class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_LoggerDatabase")
@ApplicationScoped
@Scope("singleton")
public class LoggerDatabase implements Serializable {
    private static final org.apache.logging.log4j.Logger LOG = LogManager.getFormatterLogger(LoggerDatabase.class);

    private static String LOGGER_NAME = "logs";

    @Autowired
    private Application appcore_Application;

    @Autowired
    private Utils appcore_Utils;

    @Autowired
    private HttpServletRequest request;

    public LoggerDatabase() {
        LOG.debug(this);
    }


    /**
     * Log into database
     */
    public final void log(DatabaseProcessorService databaseProcessorService,
                          String name,
                          String user,
                          String host,
                          String source,
                          String level,
                          String message) {

        try {
            if (Boolean.parseBoolean(appcore_Application.property("log.enable"))) {
                // default values
                if (databaseProcessorService == null)
                    databaseProcessorService = appcore_Application.getDatabaseProcessorService();
                if (name == null || Constants.EMPTY.equalsIgnoreCase(name))
                    name = LOGGER_NAME;
                HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                if ((user == null || Constants.EMPTY.equalsIgnoreCase(user))
                        && httpSession != null
                        && httpSession.getAttribute("user") != null)
                    user = httpSession.getAttribute("user").toString();
                if (host == null || Constants.EMPTY.equalsIgnoreCase(host))
                    host = request.getRemoteAddr();
                if (source == null || Constants.EMPTY.equalsIgnoreCase(source))
                    source = request.getRequestURI();
                if (level == null || Constants.EMPTY.equalsIgnoreCase(level))
                    level = Constants.LOGGER_INFO;

                // logger
                if (Constants.LOGGER_FATAL.equalsIgnoreCase(level)) {
                    LOG.fatal("[%s] %s - %s", user, source, message);
                } else if (Constants.LOGGER_ERROR.equalsIgnoreCase(level)) {
                    LOG.error("[%s] %s - %s", user, source, message);
                } else if (Constants.LOGGER_WARN.equalsIgnoreCase(level)) {
                    LOG.warn("[%s] %s - %s", user, source, message);
                } else if (Constants.LOGGER_INFO.equalsIgnoreCase(level)) {
                    LOG.info("[%s] %s - %s", user, source, message);
                } else {
                    LOG.debug("[%s] %s - %s", user, source, message);
                }

                // database
                DatabaseCommand databaseCommand = new DatabaseCommand();
                String databaseCommandId = "logs.sql";
                databaseCommand.setSqlCommand(appcore_Utils.format(appcore_Application.database("logs.sql"), name));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(appcore_Utils.truncate(user, 500));
                databaseCommandParams.add(appcore_Utils.truncate(host, 500));
                databaseCommandParams.add(appcore_Utils.truncate(source, 4000));
                databaseCommandParams.add(appcore_Utils.requestHeaders());
                databaseCommandParams.add(appcore_Utils.truncate(level, 100));
                databaseCommandParams.add(message);
                databaseCommand.setParameters(databaseCommandParams);
                databaseProcessorService.executeCommandAsync(databaseCommand);

            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Log into database
     */
    public final void log(String level,
                          String message) {
        this.log(null, null, null, null, null, level, message);
    }


}