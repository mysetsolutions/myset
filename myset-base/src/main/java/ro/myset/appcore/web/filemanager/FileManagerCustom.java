/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.filemanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.utils.disk.DiskUtils;
import ro.myset.utils.generic.GenericMap;
import ro.myset.utils.zip.ZipUtils;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * FileManagerSimple class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
@Named("appcore_FileManagerCustom")
@SessionScoped
@Scope("session")
public class FileManagerCustom implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(FileManagerCustom.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private FacesUtils appcore_FacesUtils;
    
    @Autowired
    private Session appcore_Session;

    // filemanager properties
    private boolean showFolderFirst = true;
    private boolean showFoldersSize = true;

    private File rootFolder;
    private File currentFolder;
    private ArrayList<File> currentFolderContent;
    private File selectedFile;
    private List<File> selectedFiles;
    private List<File> actionFiles;

    private int uploadThresholdSize = 1048576;
    private String uploadTemporaryPath;
    private int readerBufferSize = 4096;


    // buttons states
    private boolean upDisabled = false;
    private boolean copyPressed = false;
    private boolean movePressed = false;
    private boolean compressionPressed = false;

    // selection
    private boolean nothingSelected = true;
    private boolean singleSelected = false;
    private boolean singleSelectedFile = false;
    private boolean multipleSelected = false;
    private boolean multipleSelectedFiles = false;

    // actions fields
    private String newFileName;
    private String newFolderName;
    private String renameFileName;
    private String archiveFileName;

    //editor
    private GenericMap<String, File> editedFiles;


    public FileManagerCustom() {
        LOG.debug(this);
    }


    public void initialize() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        this.showFolderFirst = Boolean.parseBoolean(appcore_Application.propertyValue("filemanager.folders.first").getValueString());
        this.showFoldersSize = Boolean.parseBoolean(appcore_Application.propertyValue("filemanager.folders.size").getValueString());
        this.uploadThresholdSize = Integer.parseInt(appcore_Application.propertyValue("filemanager.upload.threshold").getValueString());
        this.uploadTemporaryPath = appcore_Application.propertyValue("filemanager.upload.temporary-path").getValueString();
        this.readerBufferSize = Integer.parseInt(appcore_Application.propertyValue("filemanager.reader.buffer").getValueString());
        currentFolder = rootFolder;
        selectedFiles = new ArrayList<File>();
        this.refreshFolderContent();
        editedFiles = new GenericMap<String, File>();
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void reset() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        // paths
        rootFolder = null;
        currentFolder = null;
        currentFolderContent = null;
        selectedFile = null;
        selectedFiles = new ArrayList<File>();

        // buttons states
        actionFiles = null;
        copyPressed = false;
        movePressed = false;
        compressionPressed = false;

        // selection
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;

        // actions fields
        newFileName = null;
        newFolderName = null;
        renameFileName = null;
        archiveFileName = null;

        //editor
        editedFiles = new GenericMap<String, File>();

        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void resetAfterAction() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        // paths
        selectedFile = null;
        selectedFiles = null;

        // buttons states
        actionFiles = null;
        copyPressed = false;
        movePressed = false;
        compressionPressed = false;

        // selection
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;

        // actions fields
        newFileName = null;
        newFolderName = null;
        renameFileName = null;
        archiveFileName = null;


        LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void refreshSelectionFlags() throws Exception{
        nothingSelected = true;
        singleSelected = false;
        singleSelectedFile = false;
        multipleSelected = false;
        multipleSelectedFiles = false;
        // check single file
        if (selectedFiles!=null && selectedFiles.size() == 1) {
            nothingSelected = false;
            singleSelected = true;
            if (selectedFile!=null && selectedFile.isFile())
                singleSelectedFile = true;
        }
        // check multiple files
        if (selectedFiles!=null && selectedFiles.size() > 1) {
            nothingSelected = false;
            singleSelected = false;
            multipleSelected = true;
            //LOG.debug("%s , %s",selectedFiles.size(),selectedFiles);
            for (int i = 0; i < selectedFiles.size() ; i++) {
                //LOG.debug("%s , %s",i, selectedFiles.get(i));
                if(selectedFiles.get(i).isDirectory()){
                    multipleSelectedFiles=false;
                    break;
                } else
                    multipleSelectedFiles=true;
            }
        }
    }

    public final void toggleSelect(ToggleSelectEvent event) throws Exception {
        //LOG.debug("%s", event.getSource());
        if (event.getSource() != null) {
            if (event.isSelected()) {
                selectedFiles = (ArrayList<File>) ((DataTable) event.getSource()).getValue();
            } else
                selectedFiles = null;
            // refresh
            this.refreshSelectionFlags();
        }

    }

    public final void contentNodeSelect(SelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            selectedFile = (File) event.getObject();
            // refresh
            this.refreshSelectionFlags();
        }

    }

    public final void contentNodeUnselect(UnselectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            // refresh
            this.refreshSelectionFlags();

        }

    }

    public final void contentNodeDbSelect(SelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            File selection = (File) event.getObject();
            if (selection.isDirectory()) {
                currentFolder = selection;
                this.refreshFolderContent();
            }
        }
    }

    public final void up() throws Exception {
        currentFolder = currentFolder.getParentFile();
        this.refreshFolderContent();
    }


    public final synchronized void refreshFolderContent() throws Exception {
        if (currentFolder == null)
            return;
        currentFolderContent = new ArrayList<File>();
        File[] currentFolders = currentFolder.listFiles();
        Arrays.sort(currentFolders);
        if (currentFolders != null) {
            if (showFolderFirst) {
                // folders
                for (int i = 0; i < currentFolders.length; i++) {
                    if (currentFolders[i].isDirectory())
                        currentFolderContent.add(currentFolders[i]);
                }
                // files
                for (int i = 0; i < currentFolders.length; i++) {
                    if (currentFolders[i].isFile())
                        currentFolderContent.add(currentFolders[i]);
                }
            } else {
                // all
                for (int i = 0; i < currentFolders.length; i++) {
                    currentFolderContent.add(currentFolders[i]);
                }
            }
        }
        // activate buttons states
        if (!rootFolder.getCanonicalPath().equalsIgnoreCase(currentFolder.getCanonicalPath())) {
            upDisabled = false;
        } else
            upDisabled = true;

        // refresh
        this.refreshSelectionFlags();


    }

    //==========================================
    //              Buttons bar
    //==========================================
    private final void resetSelection() throws Exception {
        copyPressed = false;
        movePressed = false;
        compressionPressed = false;
    }


    public final StreamedContent download(File file) throws Exception {
        if (file != null && file.isFile()) {
            LOG.info("%s,%s", file.getCanonicalPath(), file.length());
            InputStream fis = new FileInputStream(file);
            return DefaultStreamedContent.builder().contentType(appcore_Application.getMimeTypes().getString(DiskUtils.getFileExtension(file.getName()))).name(file.getName()).stream(() -> fis).build();
        }
        return null;
    }

    public void upload(FileUploadEvent event) {
        try {
            UploadedFile uploadedFile = event.getFile();
            InputStream is = uploadedFile.getInputStream();
            byte[] buffer = new byte[readerBufferSize];
            File file = new File(currentFolder, uploadedFile.getFileName());
            LOG.info("%s,%s", file.getCanonicalPath(), uploadedFile.getSize());
            FileOutputStream fos = new FileOutputStream(file);
            int bufferLength = 0;
            while ((bufferLength = is.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
                fos.flush();
            }
            is.close();
            fos.close();
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }

    }

    public final void delete() {
        try {
            if (selectedFiles != null && selectedFiles.size() > 0) {
                for (int i = 0; i < selectedFiles.size(); i++) {
                    LOG.info(selectedFiles.get(i));
                    // delete file
                    if (selectedFiles.get(i) != null && selectedFiles.get(i).exists() && selectedFiles.get(i).isFile()) {
                        DiskUtils.delete(selectedFiles.get(i));
                    }
                    //delete directory
                    if (selectedFiles.get(i) != null && selectedFiles.get(i).exists() && selectedFiles.get(i).isDirectory()) {
                        DiskUtils.deleteDirectory(selectedFiles.get(i));
                    }
                }
            }
            resetAfterAction();
            this.refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void newFile() {
        try {
            LOG.info("%s,%s", currentFolder, newFileName);
            if (currentFolder != null && currentFolder.exists()) {
                File newFile = new File(currentFolder, newFileName);
                if (!(newFile.exists() && newFile.isFile())) {
                    newFile.createNewFile();
                } else
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.file-exist"));
                resetAfterAction();
                this.refreshFolderContent();
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void newFolder() {
        try {
            LOG.info("%s,%s", currentFolder, newFolderName);
            if (currentFolder != null && currentFolder.exists()) {
                File newFolder = new File(currentFolder, newFolderName);
                if (!(newFolder.exists() && newFolder.isDirectory())) {
                    newFolder.mkdir();
                } else
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.folder-exist"));
                resetAfterAction();
                this.refreshFolderContent();
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void rename() {
        try {
            LOG.info("%s,%s", selectedFile, renameFileName);
            // rename file
            if (selectedFile != null && selectedFile.exists() && selectedFile.isFile()) {
                File renamedFile = new File(currentFolder, renameFileName);
                if (renamedFile.exists() && renamedFile.isFile()) {
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.file-exist"));
                    return;
                }
                selectedFile.renameTo(renamedFile);
            }
            // rename folder
            if (selectedFile != null && selectedFile.exists() && selectedFile.isDirectory()) {
                File renamedFile = new File(selectedFile.getParentFile(), renameFileName);
                if (renamedFile.exists() && renamedFile.isDirectory()) {
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, appcore_Session.dictionary("file-manager.message.folder-exist"));
                    return;
                }
                renamedFile.mkdir();
                selectedFile.renameTo(renamedFile);
                currentFolder = selectedFile.getParentFile();
                selectedFile = currentFolder;
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void copy() {
        try {
            resetSelection();
            copyPressed = true;
            actionFiles=selectedFiles;

        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void move() {
        try {
            resetSelection();
            movePressed = true;
            actionFiles=selectedFiles;

        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void paste() {
        try {
            if (actionFiles != null) {
                //copy
                if (copyPressed) {
                    for (int i = 0; i < actionFiles.size(); i++) {
                        LOG.debug("Copy %s to %s", actionFiles.get(i), currentFolder);
                        DiskUtils.copy(actionFiles.get(i), currentFolder, true);
                        LOG.info("Copied %s to %s", actionFiles.get(i), currentFolder);
                    }
                }
                //move
                if (movePressed) {
                    for (int i = 0; i < actionFiles.size(); i++) {
                        LOG.debug("Move %s to %s", actionFiles.get(i), currentFolder);
                        DiskUtils.move(actionFiles.get(i), currentFolder, true);
                        LOG.info("Moved %s to %s", actionFiles.get(i), currentFolder);
                    }
                    resetSelection();
                }
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }


    public final void compress() {
        try {
            LOG.info("%s,%s", currentFolder, archiveFileName);
            if (selectedFiles != null && selectedFiles.size() > 0) {
                File archiveFile = new File(currentFolder, archiveFileName);
                ZipUtils zipUtils = new ZipUtils();
                String[] files = new String[selectedFiles.size()];
                for (int i = 0; i < selectedFiles.size(); i++) {
                    files[i] = selectedFiles.get(i).getCanonicalPath();
                    LOG.info("Add %s to %s", files[i], archiveFileName);
                }
                zipUtils.createArchive(files, archiveFile.getCanonicalPath(), true);
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void extract() {
        try {
            LOG.info("%s,%s", currentFolder, selectedFile);
            if (selectedFile != null && selectedFile.isFile()) {
                File dest = currentFolder;
                if (archiveFileName != null)
                    dest = new File(currentFolder, archiveFileName);
                ZipUtils.extractAll(selectedFile, dest, true);
            }
            resetAfterAction();
            refreshFolderContent();
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    //==========================================
    //              Fields
    //==========================================

    public final boolean isShowFoldersSize() {
        return showFoldersSize;
    }

    public final void setShowFoldersSize(boolean showFoldersSize) {
        this.showFoldersSize = showFoldersSize;
    }

    public final double fileSizeValue(File f, double sizeUnit) throws Exception {
        if(f.isDirectory() && !showFoldersSize)
            return -1;
        return DiskUtils.fileSizeValue(f, sizeUnit);
    }

    public final String fileSizeUnit(File f, double sizeUnit, String format) throws Exception {
        if(f.isDirectory() && !showFoldersSize)
            return Constants.EMPTY;
        return DiskUtils.fileSizeUnit(f, sizeUnit, format);
    }

    public final File getRootFolder() {
        return rootFolder;
    }

    public final void setRootFolder(File rootFolder) {
        this.rootFolder = rootFolder;
    }

    public final File getCurrentFolder() {
        return currentFolder;
    }

    public final void setCurrentFolder(File currentFolder) {
        this.currentFolder = currentFolder;
    }

    public final File getSelectedFile() {
        return selectedFile;
    }

    public final void setSelectedFile(File selectedFile) {
        this.selectedFile = selectedFile;
    }

    public final List<File> getSelectedFiles() {
        return selectedFiles;
    }

    public final void setSelectedFiles(List<File> selectedFiles) {
        this.selectedFiles = selectedFiles;
    }

    public final ArrayList<File> getCurrentFolderContent() {
        return currentFolderContent;
    }

    public final boolean isNothingSelected() {
        return nothingSelected;
    }

    public final boolean isSingleSelected() {
        return singleSelected;
    }

    public final boolean isSingleSelectedFile() {
        return singleSelectedFile;
    }

    public final boolean isMultipleSelected() {
        return multipleSelected;
    }

    public final boolean isMultipleSelectedFiles() {
        return multipleSelectedFiles;
    }

    public final boolean isUpDisabled() {
        return upDisabled;
    }

    public final int getUploadThresholdSize() {
        return uploadThresholdSize;
    }

    public final String getUploadTemporaryPath() {
        return uploadTemporaryPath;
    }

    public final String getNewFileName() {
        return newFileName;
    }

    public final void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public final String getNewFolderName() {
        return newFolderName;
    }

    public final void setNewFolderName(String newFolderName) {
        this.newFolderName = newFolderName;
    }

    public final String getRenameFileName() {
        return renameFileName;
    }

    public final void setRenameFileName(String renameFileName) {
        this.renameFileName = renameFileName;
    }

    public final String getArchiveFileName() {
        return archiveFileName;
    }

    public final void setArchiveFileName(String archiveFileName) {
        this.archiveFileName = archiveFileName;
    }

    public final boolean isCopyPressed() {
        return copyPressed;
    }

    public final void setCopyPressed(boolean copyPressed) {
        this.copyPressed = copyPressed;
    }

    public final boolean isMovePressed() {
        return movePressed;
    }

    public final void setMovePressed(boolean movePressed) {
        this.movePressed = movePressed;
    }

    public final boolean isCompressionPressed() {
        return compressionPressed;
    }

    public final void setCompressionPressed(boolean compressionPressed) {
        this.compressionPressed = compressionPressed;
    }

    public final GenericMap<String, File> getEditedFiles() {
        return editedFiles;
    }

    public final void setEditedFiles(GenericMap<String, File> editedFiles) {
        this.editedFiles = editedFiles;
    }
}
