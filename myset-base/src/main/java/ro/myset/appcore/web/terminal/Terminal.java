/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.terminal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.grid.ant.AntRunner;
import ro.myset.appcore.web.extensions.Extension;
import ro.myset.appcore.web.utils.SecurityUtils;
import ro.myset.appcore.web.utils.Utils;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Named("appcore_Terminal")
@ApplicationScoped
@Scope("singleton")

/**
 *
 * Terminal class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class Terminal implements Serializable {
    public static final String PARAMS_REGEXP = "\"([^\"]*)\"|\\s*([^\"\\s]+)";
    public static final String SCRIPT_START_TAG = "<script>";
    public static final String SCRIPT_END_TAG = "</script>";
    public static final String PRE_START_TAG = "<pre>";
    public static final String PRE_END_TAG = "</pre>";
    public static final String BR = "<br/>";
    public static final String HR = "<hr/>";
    public static final String INVALID_COMMAND = "Invalid command  - Check command syntax";
    // Commands
    public static final String CMD = "";
    public static final String CMD_HELP = "?";
    public static final String CMD_CLEAR = "clear";
    public static final String CMD_GET_ENVS = "get-envs";
    public static final String CMD_GET_ENV = "get-env";
    public static final String CMD_GET_PROPERTIES = "get-properties";
    public static final String CMD_GET_PROPERTY = "get-property";
    public static final String CMD_SET_PROPERTY = "set-property";
    public static final String CMD_REFRESH = "refresh";
    public static final String CMD_REFRESH_ALL = "all";
    public static final String CMD_REFRESH_CONFIGURATION = "configuration";
    public static final String CMD_REFRESH_CONFIGURATIONS = "configurations";
    public static final String CMD_REFRESH_CONTEXT = "context";
    public static final String CMD_EXTENSIONS = "extensions";
    public static final String CMD_EXTENSION = "extension";

    public static final String CMD_DATE = "date";
    public static final String CMD_PWD = "pwd";
    public static final String CMD_HOST = "host";
    public static final String CMD_GUEST = "guest";

    public static final String CMD_ANT = "ant";
    public static final String CMD_ENCRYPTMD5 = "encryptMD5";
    public static final String CMD_ENCRYPT = "encrypt";
    public static final String CMD_DECRYPT = "decrypt";
    public static final String CMD_DO_WAIT = "do-wait";


    private static final String EXTENSION_ID = "myset.extension.id";
    private static final String EXTENSION_INSTALLED = "myset.extension.installed";
    private static final String EXTENSION_CHECK = "CHECK";


    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(Terminal.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private Utils appcore_Utils;

    public Terminal() {
        LOG.debug(this);
    }

    public String handleCommand(String command, String[] params) {
        StringBuffer sb = new StringBuffer();
        //sb.append(PRE_START_TAG);
        LOG.info("%s %s", command, Arrays.toString(params));
        try {
            params = this.parseParams(params);
            if (CMD.equalsIgnoreCase(command)) {
                // nothing to do
            } else if (CMD_HELP.equalsIgnoreCase(command)) {
                sb.append("Help  - Available commands");
                help(null, sb);
            } else if (CMD_CLEAR.equalsIgnoreCase(command)) {
                PrimeFaces.current().executeScript("PF('term').clear();PF('term').focus()");
            } else if (CMD_DATE.equalsIgnoreCase(command)) {
                sb.append(new Date());
            } else if (CMD_HOST.equalsIgnoreCase(command)) {
                HttpServletRequest httpRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                sb.append(httpRequest.getServerName());
                sb.append(Constants.CRLF);
                sb.append(httpRequest.getLocalAddr());
            } else if (CMD_PWD.equalsIgnoreCase(command)) {
                sb.append(appcore_Application.getApplicationPath().getAbsolutePath());
            } else if (CMD_GUEST.equalsIgnoreCase(command)) {
                HttpServletRequest httpRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                sb.append(httpRequest.getRemoteHost());
                sb.append(Constants.CRLF);
                sb.append(httpRequest.getRemoteAddr());
            } else if (CMD_DO_WAIT.equalsIgnoreCase(command)) {
                if (params == null || params.length != 1 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_DO_WAIT, sb);
                } else {
                    try {
                        Thread.sleep(Long.decode(params[0]));
                    } catch (InterruptedException e) {
                        //nothing to do
                    }
                }
            } else if (CMD_EXTENSIONS.equalsIgnoreCase(command)) {
                if (params != null && params.length > 1) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_EXTENSIONS, sb);
                } else {
                    List<Extension> extensions = appcore_Application.getExtensions();
                    for (int i = 0; i < extensions.size(); i++) {
                        Extension extension = extensions.get(i);
                        if (params == null) {
                            extension.getExtensionAntRunner().initialize();
                            extension.getExtensionAntRunner().runTarget(EXTENSION_CHECK);
                            sb.append(extension.getExtensionProperties().getProperty(EXTENSION_ID));
                            if (Boolean.parseBoolean(extension.getExtensionAntRunner().getProject().getProperty(EXTENSION_INSTALLED)))
                                sb.append("    [installed]");
                            sb.append(Constants.CRLF);
                        } else {
                            sb.append("-------------------------------------------------------------");
                            sb.append(Constants.CRLF);
                            sb.append(CMD_EXTENSION);
                            sb.append(Constants.SPACE);
                            sb.append(extension.getExtensionProperties().getProperty(EXTENSION_ID));
                            sb.append(Constants.SPACE);
                            sb.append(params[0]);
                            sb.append(Constants.CRLF);
                            sb.append(extension.getExtensionAntRunner().runTarget(params[0]));
                            sb.append("-------------------------------------------------------------");
                        }

                    }

                }
            } else if (CMD_EXTENSION.equalsIgnoreCase(command)) {
                if (params == null || params.length != 2 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_EXTENSION, sb);
                } else {
                    appcore_Application.getExtension(params[0]).getExtensionAntRunner().initialize();
                    sb.append(appcore_Application.getExtension(params[0]).getExtensionAntRunner().runTarget(params[1]));
                }
            } else if (CMD_GET_ENVS.equalsIgnoreCase(command)) {
                Map<String, String> envs = System.getenv();
                Set<String> envKeySet = envs.keySet();
                Iterator<String> envKeySetIterator = envKeySet.iterator();
                String key = null;
                while (envKeySetIterator.hasNext()) {
                    key = envKeySetIterator.next();
                    sb.append(key);
                    sb.append(Constants.EQUAL);
                    sb.append(envs.get(key));
                    sb.append(Constants.CRLF);
                }
            } else if (CMD_GET_ENV.equalsIgnoreCase(command)) {
                if (params == null || params.length != 1 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_GET_ENV, sb);
                } else {
                    sb.append(System.getenv(params[0]));
                }
            } else if (CMD_GET_PROPERTIES.equalsIgnoreCase(command)) {
                Properties properties = System.getProperties();
                Set<Object> propertiesKeySet = properties.keySet();
                Iterator<Object> propertiesKeySetIterator = propertiesKeySet.iterator();
                String key = null;
                while (propertiesKeySetIterator.hasNext()) {
                    key = propertiesKeySetIterator.next().toString();
                    sb.append(key);
                    sb.append(Constants.EQUAL);
                    sb.append(properties.get(key));
                    sb.append(Constants.CRLF);
                }
            } else if (CMD_GET_PROPERTY.equalsIgnoreCase(command)) {
                if (params == null || params.length != 1 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_GET_PROPERTY, sb);
                } else {
                    sb.append(System.getProperty(params[0]));
                }
            } else if (CMD_SET_PROPERTY.equalsIgnoreCase(command)) {
                if (params == null || params[0] == null || params[1] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_SET_PROPERTY, sb);
                } else {
                    System.setProperty(params[0], params[1]);
                }
            } else if (CMD_ANT.equalsIgnoreCase(command)) {
                if (params == null || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_ANT, sb);
                } else {
                    AntRunner ar = new AntRunner();
                    ar.setFile(new File(params[0]));
                    ar.initialize();
                    if (params.length >= 2)
                        sb.append(ar.runTarget(params[1]));
                    else
                        ar.runTarget(null);
                }
            } else if (CMD_ENCRYPTMD5.equalsIgnoreCase(command)) {
                if (params == null || params.length != 1 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_ENCRYPTMD5, sb);
                } else {
                    SecurityUtils appcore_SecurityUtils = new SecurityUtils();
                    sb.append(appcore_SecurityUtils.encryptMD5(params[0]));
                }
            } else if (CMD_ENCRYPT.equalsIgnoreCase(command)) {
                if (params == null || params.length != 1 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_ENCRYPT, sb);
                } else {
                    SecurityUtils appcore_SecurityUtils = new SecurityUtils();
                    sb.append(appcore_SecurityUtils.encryptDefault(params[0]));
                }
            } else if (CMD_DECRYPT.equalsIgnoreCase(command)) {
                if (params == null || params.length != 1 || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_DECRYPT, sb);
                } else {
                    SecurityUtils appcore_SecurityUtils = new SecurityUtils();
                    sb.append(appcore_SecurityUtils.decryptDefault(params[0]));
                }
            } else if (CMD_REFRESH.equalsIgnoreCase(command)) {
                if (params == null || params[0] == null) {
                    sb.append(INVALID_COMMAND);
                    help(CMD_REFRESH, sb);
                } else if (CMD_REFRESH_ALL.equalsIgnoreCase(params[0])) {
                    appcore_Application.refreshConfigurations();
                    sb.append("Refresh configurations ... OK");
                    appcore_Application.refreshDynamicApplicationContext();
                    sb.append(Constants.CRLF);
                    sb.append("Refresh context ... OK");
                    appcore_Application.refreshExtensions();
                    sb.append(Constants.CRLF);
                    sb.append("Refresh extensions ... OK");
                } else if (CMD_REFRESH_CONFIGURATION.equalsIgnoreCase(params[0])) {
                    if (params.length == 2) {
                        appcore_Application.refreshConfiguration(params[1]);
                        sb.append("Refresh configuration '");
                        sb.append(params[1]);
                        sb.append("' ... OK");
                    }
                } else if (CMD_REFRESH_CONFIGURATIONS.equalsIgnoreCase(params[0])) {
                    appcore_Application.refreshConfigurations();
                    sb.append("Refresh configurations ... OK");
                } else if (CMD_REFRESH_CONTEXT.equalsIgnoreCase(params[0])) {
                    appcore_Application.refreshDynamicApplicationContext();
                    sb.append("Refresh context ... OK");
                } else if (CMD_EXTENSIONS.equalsIgnoreCase(params[0])) {
                    appcore_Application.refreshExtensions();
                    sb.append("Refresh extensions ... OK");
                } else {
                    sb.append(INVALID_COMMAND);
                    help(CMD_REFRESH, sb);
                }
            }


            //------------------------------------------------------------
            else {
                sb.append("Invalid command  - Available commands");
                help(null, sb);
            }
            return Constants.EMPTY;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter, true));
            sb.append(stringWriter.toString());
        } finally {
            //sb.append(PRE_END_TAG);
            LOG.info("%s", sb.toString());
            return sb.toString();
        }
    }

    private final void help(String cmd, StringBuffer sb) {
        if (cmd == null || CMD_HELP.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("?  - This help");
        }
        if (cmd == null || CMD_CLEAR.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("clear - Clear terminal output");
        }
        if (cmd == null || CMD_DATE.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("date - Display server date");
        }
        if (cmd == null || CMD_DO_WAIT.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("do-wait <milliseconds> - Generate wait.");
        }
        if (cmd == null || CMD_HOST.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("host - Display host name and ip address");
        }
        if (cmd == null || CMD_PWD.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("pwd - Display server working directory");
        }
        if (cmd == null || CMD_GUEST.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("guest - Display guest name and ip address");
        }
        if (cmd == null || CMD_EXTENSIONS.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("extensions - List extensions");
            sb.append(Constants.CRLF);
            sb.append("extensions <extension manager target> - Execute extension manager target for each extension");
        }
        if (cmd == null || CMD_EXTENSION.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("extension <extension id> <extension manager target> - Execute extension manager target");
        }
        if (cmd == null || CMD_ENCRYPTMD5.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("encryptMD5 <text> - Encrypt text using MD5.");
        }
        if (cmd == null || CMD_ENCRYPT.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("encrypt <text> - Encrypt text using application algorithm and key.");
        }
        if (cmd == null || CMD_DECRYPT.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("decrypt <text> - Decrypt text using application algorithm and key.");
        }
        if (cmd == null || CMD_GET_ENVS.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("get-envs - Gets environment variables.");
        }
        if (cmd == null || CMD_GET_ENV.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("get-env <variable name> - Gets environment variable by name.");
        }
        if (cmd == null || CMD_GET_PROPERTIES.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("get-properties - Gets java properties");
        }
        if (cmd == null || CMD_GET_PROPERTY.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("get-property <property name> - Gets java property by name.");
        }
        if (cmd == null || CMD_SET_PROPERTY.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("set-property <property name> <property value> - Sets java property.");
        }
        if (cmd == null || CMD_ANT.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("ant <ant configuration file full path> <ant target> - Run Apache ant target.");
        }
        if (cmd == null || CMD_REFRESH.equalsIgnoreCase(cmd)) {
            sb.append(Constants.CRLF);
            sb.append("refresh all - Refresh all configurations.");
            sb.append(Constants.CRLF);
            sb.append("refresh configurations - Refresh configurations.");
            sb.append(Constants.CRLF);
            sb.append("refresh configuration <logger|properties|dictionary|database|preferences> - Refresh configuration by type.");
            sb.append(Constants.CRLF);
            sb.append("refresh context - Refresh context configurations.");
            sb.append(Constants.CRLF);
            sb.append("refresh extensions - Refresh extensions.");

        }
        sb.append(Constants.CRLF);
    }

    private String fullCommand(String command, String[] params) {
        StringBuffer fullCommand = new StringBuffer();
        fullCommand.append(command);
        if (params != null && params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                fullCommand.append(Constants.SPACE);
                fullCommand.append(params[i]);
            }
        }
        return fullCommand.toString();
    }

    private String[] parseParams(String[] params) {
        String paramsWork = null;
        String[] paramsInter = null;
        String[] paramsFinal = null;
        if (params != null && params.length > 0) {
            paramsWork = params[0];
            for (int i = 1; i < params.length; i++) {
                paramsWork += Constants.SPACE + params[i];
            }
            Matcher m = Pattern.compile(PARAMS_REGEXP).matcher(paramsWork);
            int j = 0;
            paramsInter = new String[params.length];
            while (m.find()) {
                // LOG.debug("%s",m.group(0));
                if (m.group(0).startsWith(Constants.SPACE))
                    paramsInter[j] = m.group(0).substring(1);
                else if (m.group(0).startsWith("\""))
                    paramsInter[j] = m.group(0).substring(1, m.group(0).length() - 1);
                else
                    paramsInter[j] = m.group(0);
                j++;
            }
            paramsFinal = new String[j];
            System.arraycopy(paramsInter, 0, paramsFinal, 0, j);
        }
        return paramsFinal;
    }

}
