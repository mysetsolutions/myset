/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.appcore.web.utils.LoggerDatabase;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;
import ro.myset.utils.security.MD5Utils;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Application security console
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ApplicationSecurityConsole")
@RequestScoped
@Scope("request")
public class ApplicationSecurityConsole implements Serializable {
    private static final long serialVersionUID = 1L;

    // constants
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationSecurityConsole.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private ApplicationSecurity appcore_ApplicationSecurity;

    @Autowired
    private FacesUtils appcore_FacesUtils;

    @Autowired
    private Session appcore_Session;

    public ApplicationSecurityConsole() {
        LOG.debug(this);
    }


    /**
     * Login
     *
     * @param userName     - User name
     * @param userPassword - User password
     */
    public final boolean login(String userName, String userPassword) throws Exception {
        String uuid = UUIDUtils.generate();
        boolean result = false;
        try {
            LOG.info("[%s] userName: %s", uuid, userName);

            if (userName.equalsIgnoreCase(appcore_Application.property("security.login.console.username")) &&
                    MD5Utils.encrypt(userPassword).equals(appcore_Application.property("security.login.console.password"))) {

                //userGenericBean
                GenericBean userGenericBean = new GenericBean();
                GenericMap<String, String> userGenericMap = new GenericMap<>();
                userGenericMap.put("userId", "1");
                userGenericMap.put("userName", userName);
                userGenericMap.put("userFirstName", "Console");
                userGenericMap.put("userGivenName", null);
                userGenericMap.put("userLastName", "Administrator");
                userGenericMap.put("userBirthDate", null);
                userGenericMap.put("userAddress", null);
                userGenericMap.put("userEmail", null);
                userGenericMap.put("userOrganization", null);
                userGenericMap.put("userOrganizationUnit", null);
                userGenericMap.put("userDescription", "Console Administrator");
                userGenericMap.put("userPhone", null);
                userGenericMap.put("userMobile", null);
                userGenericMap.put("userWorkPhone", null);
                userGenericMap.put("userProtected", Constants.ONE);
                userGenericMap.put("userCreateDate", null);
                userGenericMap.put("userLastUpdate", null);
                userGenericMap.put("userLastLogon", null);
                userGenericBean.setFields(userGenericMap);

                // userRolesGenericBeans
                GenericBean userRoleGenericBean = null;
                GenericMap<String, String> userRoleGenericMap = null;
                ArrayList<GenericBean> userRolesGenericBeans = new ArrayList<>();

                // role 1
                userRoleGenericBean = new GenericBean();
                userRoleGenericMap = new GenericMap<>();
                userRoleGenericMap.put("roleId", "1");
                userRoleGenericMap.put("roleName", "app_admins");
                userRoleGenericMap.put("roleDescription", "Administrators");
                userRoleGenericMap.put("roleProtected", Constants.ONE);
                userRoleGenericBean.setFields(userRoleGenericMap);
                userRolesGenericBeans.add(userRoleGenericBean);

                // role 2
                userRoleGenericBean = new GenericBean();
                userRoleGenericMap = new GenericMap<>();
                userRoleGenericMap.put("roleId", "2");
                userRoleGenericMap.put("roleName", "app_users");
                userRoleGenericMap.put("roleDescription", "Users");
                userRoleGenericMap.put("roleProtected", Constants.ONE);
                userRoleGenericBean.setFields(userRoleGenericMap);
                userRolesGenericBeans.add(userRoleGenericBean);

                // role 3
                userRoleGenericBean = new GenericBean();
                userRoleGenericMap = new GenericMap<>();
                userRoleGenericMap.put("roleId", "3");
                userRoleGenericMap.put("roleName", "app_testers");
                userRoleGenericMap.put("roleDescription", "Testers");
                userRoleGenericMap.put("roleProtected", Constants.ONE);
                userRoleGenericBean.setFields(userRoleGenericMap);
                userRolesGenericBeans.add(userRoleGenericBean);

                // login
                appcore_ApplicationSecurity.login(userName, userGenericBean, userRolesGenericBeans);

                if (appcore_ApplicationSecurity.getSecurity().isAuthenticated()) {
                    // success
                    result = true;
                    return result;
                }
            }
            if (!result) {
                appcore_FacesUtils.errorMessage(appcore_Session.dictionary("login.message.01"));
            }
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            appcore_FacesUtils.errorMessage(appcore_Session.dictionary("login.message.01"));
        } finally {
            return result;
        }
    }

}
