/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.filemanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.utils.disk.DiskUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;

/**
 * FileManager class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_FileManagerTextViewer")
@RequestScoped
@Scope("request")
public class FileManagerTextViewer implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(FileManagerTextViewer.class);
    protected static final String SYSTEM_LINE_SEPARATOR = System.getProperty("line.separator");
    protected static final String WEB_LINE_SEPARATOR = "\r\n";
    protected static final String TREE_DOTS = "...";
    protected static final long VIEW_SIZE = 1048576;

    @Autowired
    private Application appcore_Application;

    @Autowired
    private FacesUtils appcore_FacesUtils;
    
    @Autowired
    private Session appcore_Session;

    // fields
    private String editedFileKey;
    private String editorFileContent;
    private long editorFileSize;
    private int editorRefreshInterval=0;


    public FileManagerTextViewer() {
        //LOG.debug(this);
    }


    public void initialize() throws Exception {
        //LOG.debug(Constants.EVENT_INVOKED);
        //LOG.debug(application);
        //LOG.debug(session);
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void viewInit(File file) {
        LOG.info(file);
        try {
            if (file != null && file.exists()) {
                ByteArrayOutputStream baos = null;
                editorFileSize=file.length();
                if(file.length() > VIEW_SIZE) {
                    baos=DiskUtils.readFileContent(file.getCanonicalPath(),file.length()-VIEW_SIZE);
                    editorFileContent = TREE_DOTS.concat(WEB_LINE_SEPARATOR).concat(new String(baos.toByteArray()));
                }
                else {
                    baos = DiskUtils.readFileContent(file.getCanonicalPath());
                    editorFileContent = new String(baos.toByteArray());
                }
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void viewRefresh(File file, long fromOffset) {
        LOG.info(file);
        try {
            if (file != null && file.exists() && fromOffset>=0) {
                ByteArrayOutputStream baos = DiskUtils.readFileContent(file.getCanonicalPath(),fromOffset);
                editorFileSize=editorFileSize+baos.size();
                editorFileContent = new String(baos.toByteArray());
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }




    //==========================================
    //              Fields
    //==========================================

    public final String getEditedFileKey() {
        return editedFileKey;
    }

    public final void setEditedFileKey(String editedFileKey) {
        this.editedFileKey = editedFileKey;
    }

    public final String getEditorFileContent() {
        return editorFileContent;
    }

    public final void setEditorFileSize(long editorFileSize) {
        this.editorFileSize = editorFileSize;
    }

    public final long getEditorFileSize() {
        return editorFileSize;
    }

    public final int getEditorRefreshInterval() {
        return editorRefreshInterval;
    }

    public final void setEditorRefreshInterval(int editorRefreshInterval) {
        this.editorRefreshInterval = editorRefreshInterval;
    }
}
