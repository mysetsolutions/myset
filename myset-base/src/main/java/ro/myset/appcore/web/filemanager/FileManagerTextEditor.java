/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.filemanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.utils.disk.DiskUtils;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;

/**
 * FileManager class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_FileManagerTextEditor")
@RequestScoped
@Scope("request")
public class FileManagerTextEditor implements Serializable {
    protected static final String SYSTEM_LINE_SEPARATOR = System.getProperty("line.separator");
    protected static final String WEB_LINE_SEPARATOR = "\r\n";
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(FileManagerTextEditor.class);
    @Autowired
    private Application appcore_Application;

    @Autowired
    private Session appcore_Session;


    @Autowired
    private FacesUtils appcore_FacesUtils;


    // fields
    private String editedFileKey;
    private String editorFileContent;


    public FileManagerTextEditor() {
        //LOG.debug(this);
    }


    public void initialize() throws Exception {
        //LOG.debug(Constants.EVENT_INVOKED);
        //LOG.debug(application);
        //LOG.debug(session);
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    public final void editInit(File file) {
        LOG.info(file);
        try {
            if (file != null && file.exists()) {
                ByteArrayOutputStream baos = DiskUtils.readFileContent(file.getCanonicalPath());
                editorFileContent = new String(baos.toByteArray());
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }

    public final void editSave(File file) {
        LOG.info(file);
        try {
            if (file != null && file.exists()) {
                if (editorFileContent != null)
                    DiskUtils.writeIntoFile(file.getCanonicalPath(), editorFileContent, false);
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, appcore_Session.dictionary("messages.action.success"));
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }
    }


    //==========================================
    //              Fields
    //==========================================

    public final String getEditedFileKey() {
        return editedFileKey;
    }

    public final void setEditedFileKey(String editedFileKey) {
        this.editedFileKey = editedFileKey;
    }

    public final String getEditorFileContent() {
        return editorFileContent;
    }

    public final void setEditorFileContent(String editorFileContent) {
        //LOG.debug(editorFileContent);
        // W3C line separator issue
        if (editorFileContent != null && !SYSTEM_LINE_SEPARATOR.equals(WEB_LINE_SEPARATOR))
            this.editorFileContent = editorFileContent.replaceAll(WEB_LINE_SEPARATOR, SYSTEM_LINE_SEPARATOR);
        else
            this.editorFileContent = editorFileContent;
    }


}
