/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.preferences.ApplicationPreferences;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.appcore.web.utils.LoggerDatabase;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.security.MD5Utils;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Application security simple
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ApplicationSecuritySimple")
@RequestScoped
@Scope("request")
public class ApplicationSecuritySimple extends ApplicationSecurityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    // constants
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationSecuritySimple.class);

    @Autowired
    private ApplicationSecurity appcore_ApplicationSecurity;

    @Autowired
    private FacesUtils appcore_FacesUtils;

    @Autowired
    private Session appcore_Session;

    @Autowired
    private LoggerDatabase appcore_LoggerDatabase;


    public ApplicationSecuritySimple() {
        LOG.debug(this);
    }


    /**
     * Login
     *
     * @param userName     - User name
     * @param userPassword - User password
     */
    public final boolean login(String userName, String userPassword) throws Exception {
        String uuid = UUIDUtils.generate();
        boolean result = false;
        try {
            LOG.info("[%s] userName: %s", uuid, userName);

            GenericBean userGenericBean = this.getUserGenericBean(uuid, userName);

            if (userGenericBean != null
                    && MD5Utils.encrypt(userPassword).equalsIgnoreCase(userGenericBean.getFields().get("userPass").toString())) {
                ArrayList<GenericBean> userRolesGenericBeans = this.getUserRolesGenericBean(uuid, userGenericBean);
                if (userGenericBean != null && userRolesGenericBeans != null) {
                    appcore_ApplicationSecurity.login(userName, userGenericBean, userRolesGenericBeans);

                    if (appcore_ApplicationSecurity.getSecurity().isAuthenticated()) {

                        // check disabled
                        if (userGenericBean.getFields().get("userDisabled") != null
                                && userGenericBean.getFields().get("userDisabled").toString().equalsIgnoreCase("1")) {
                            appcore_FacesUtils.errorMessage(appcore_Session.dictionary("login.message.02"));
                            appcore_LoggerDatabase.log(Constants.LOGGER_ERROR, userName.concat(Constants.COMMA).concat(appcore_Session.dictionary("login.message.02")));
                            return result;
                        }

                        // check locked
                        if (userGenericBean.getFields().get("userLocked") != null
                                && userGenericBean.getFields().get("userLocked").toString().equalsIgnoreCase("1")) {
                            appcore_FacesUtils.errorMessage(appcore_Session.dictionary("login.message.03"));
                            appcore_LoggerDatabase.log(Constants.LOGGER_ERROR, userName.concat(Constants.COMMA).concat(appcore_Session.dictionary("login.message.03")));
                            return result;
                        }

                        // after logon
                        this.afterLogon(uuid, userGenericBean);

                        // success
                        appcore_LoggerDatabase.log(Constants.LOGGER_INFO, userName);
                        result = true;
                        return result;
                    }
                }
            }
            if (!result) {
                appcore_FacesUtils.errorMessage(appcore_Session.dictionary("login.message.01"));
                appcore_LoggerDatabase.log(Constants.LOGGER_ERROR, userName.concat(Constants.COMMA).concat(appcore_Session.dictionary("login.message.01")));
            }
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            appcore_FacesUtils.errorMessage(appcore_Session.dictionary("login.message.01"));
            appcore_LoggerDatabase.log(Constants.LOGGER_ERROR, userName.concat(Constants.COMMA).concat(appcore_Session.dictionary("login.message.01")).concat(Constants.COMMA).concat(e.toString()));
        } finally {
            return result;
        }
    }

}
