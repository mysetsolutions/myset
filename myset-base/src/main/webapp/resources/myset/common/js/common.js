/*----------------------------------------------------  
 *  Common functions
 *  Language: Java Script 
 *  Version: 1.0
 *  Author: Cristian Gheorghe Florescu
 ----------------------------------------------------*/

// ===============================================
// JS-OOP functions
// ===============================================

//Class.create()
var Class = {
    create: function () {
        return function () {
            this.initialize.apply(this, arguments);
        }
    }
}

// Object.extend(from,to)
Object.extend = function (source, destination) {
    for (var property in destination) {
        source[property] = destination[property];
    }
    return source;
}


// ===============================================
// Writing functions
// ===============================================
function write(text) {
    document.write(text);
}
function writeLine(text) {
    try {
        write(text);
        write("\r\n");
    } catch (e) {
        write("<br>[ERROR] " + e);
    }
}

// ===============================================
// Formatting functions
// ===============================================

// Parse string date using long date format yyyy-mm-dd hh24:nn:ss
Date.parseDate = function (val) {
    if (val != null && val != "undefined") {
        var year, month, day, hour, minutes, seconds;
        if (val.length >= 4)
            year = val.substr(0, 4);
        if (val.length >= 7)
            month = val.substr(5, 2) - 1;
        if (val.length >= 10)
            day = val.substr(8, 2);
        if (val.length >= 13)
            hour = val.substr(11, 2);
        if (val.length >= 16)
            minutes = val.substr(14, 2);
        if (val.length >= 19)
            seconds = val.substr(17, 2);
        if (isNaN(year) || year < 0)
            year = 1900
        if (isNaN(month) || month < 0 || month > 11)
            month = 0
        if (isNaN(day) || day < 0 || day > 31)
            day = 1
        if (isNaN(hour) || hour < 0 || hour > 23)
            hour = 0
        if (isNaN(minutes) || minutes < 0 || minutes > 59)
            minutes = 0
        if (isNaN(seconds) || seconds < 0 || seconds > 59)
            seconds = 0
        return new Date(year, month, day, hour, minutes, seconds);
    } else
        return null;
}

// Date format
Date.prototype.format = function (f) {
    if (!this.valueOf())
        return '';
    var d = this;
    return f.replace(/(yyyy|yy|mmmm|mmm|mm|m|dddd|ddd|dd|d|hh24|hh|h|nn|n|ss|s|ampm)/gi, function ($1) {
        switch ($1.toLowerCase()) {
            case 'yyyy':
                return d.getFullYear();
            case 'yy':
                return new String(d.getFullYear()).substr(2, 3);
            case 'mmmm':
                return dictionary.common.monthNames[d.getMonth()];
            case 'mmm':
                return dictionary.common.monthNames[d.getMonth()].substr(0, 3);
            case 'mm':
                return new String(d.getMonth() + 1).zf(2);
            case 'm':
                return (d.getMonth() + 1);
            case 'dddd':
                return dictionary.common.dayNames[d.getDay()];
            case 'ddd':
                return dictionary.common.dayNames[d.getDay()].substr(0, 3);
            case 'dd':
                return new String(d.getDate()).zf(2);
            case 'd':
                return d.getDate();
            case 'hh24':
                return new String(d.getHours()).zf(2);
            case 'hh':
                return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case 'h':
                return ((h = d.getHours() % 12) ? h : 12);
            case 'nn':
                return new String(d.getMinutes()).zf(2);
            case 'n':
                return d.getMinutes();
            case 'ss':
                return new String(d.getSeconds()).zf(2);
            case 's':
                return d.getSeconds();
            case 'ampm':
                return d.getHours() < 12 ? 'am' : 'pm';
        }
        return '';
    });
}

// Zero fill String
String.prototype.zf = function (digits) {
    var v = new String(this);
    if (!isNaN(digits) && v.length < digits) {
        var res = this;
        var count = digits - v.length;
        for (var i = 1; i <= count; i++) {
            res = '0' + res;
        }
        return res;
    }
    return this;
};

// parse Boolean
function parseBoolean(val) {
    return (new String(val).toLowerCase() == "true" ? true : false);
}


//===============================================
// Other various functions
//===============================================

//Show error
function showError(e) {
    alert("[ERROR] \r\n" + e);
}

//Sets page title
function setPageTitle(title) {
    document.title = title;
}

//Close window ...
function closeWindow() {
    parent.parent.parent.close();
}
//Open a popup window
function popup(url, wn) {
    h = 450;
    w = 700;
    props = 'height=' + h + ',width=' + w + ',menubar=1,toolbar=1,resizable=1,scrollbars=1,status=1,dependent=0,top=0,left=0';
    obj = window.open(url, wn, props);
    obj.focus();
}

//Open link
function openLink(loc, target) {
    if (target == "_self" || target == null) {
        document.location = loc;
    } else {
        if (target == "_top") {
            top.location = loc;
        } else {
            if (target == "_blank") {
                window.open(loc);
            } else {
                if (target == "_parent") {
                    parent.location = loc;
                } else {
                    window.open(loc, target);
                }
                ;
            }
        }
    }
}

//Show-Hide div
function showHide(name) {
    try {
        var divo = document.getElementById(name);
        if (divo.style.display == 'none')
            divo.style.display = 'block';
        else
            divo.style.display = 'none';
    } catch (e) {
        showError(e);
    }
}

//Show-Hide div
function showHideSummary(suffix) {
    try {
        var divs = document.getElementById(suffix + "_summary");
        var divc = document.getElementById(suffix);
        divs.title = dictionary.common.show;
        divc.title = dictionary.common.hide;
        if (divc.style.display == 'none') {
            divs.style.display = 'none';
            divc.style.display = 'block';
        } else {
            divs.style.display = 'block';
            divc.style.display = 'none';
        }
    } catch (e) {
        showError(e);
    }
}

function setDataTableHeight(objId, panelId) {
    var docHeight = jQuery(window).height();
    var panelHeight = jQuery(panelId).height();
    var scrollHeight = docHeight - panelHeight - 228;
    if (jQuery.browser.msie && jQuery.browser.version == "7.0") {
        var scrollBody = jQuery(objId).find('.ui-datatable-scrollable-body');
        scrollBody.height(scrollHeight);
    } else {
        var scrollBody = jQuery(objId + " .ui-datatable-scrollable-body");
        scrollBody.height(scrollHeight);
    }
}

function scrollContent(obj) {
    try {
        //alert(obj.height());
        if (obj != null)
            obj.scrollTop(obj.height());
    } catch (e) {
        showError(e);
    }
}

function applicationContextPath() {
    if (location.pathname.split('/').length > 1)
        return "/" + location.pathname.split('/')[1];
    else
        return "/";
}


