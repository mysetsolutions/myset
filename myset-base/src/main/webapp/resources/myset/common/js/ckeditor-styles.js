/*----------------------------------------------------  
 *  CKEditor configuration
 *  Language: Java Script 
 *  Version: 1.0
 *  Author: Cristian Gheorghe Florescu
 ----------------------------------------------------*/

CKEDITOR.stylesSet.add('myStyles', [
        // Block-level styles
        {
            name: 'Special Container',
            element: 'div',
            styles: {
                padding: '5px 10px',
                background: '#eee',
                border: '1px solid #ccc'
            }
        },
        { name: 'H1 - Blue', element: 'h1', styles: { 'color': '#0000ff' } },
        { name: 'H2 - Red' , element: 'h2', styles: { 'color': '#ff0000' } },

        // Inline styles
        { name: 'Marker 1', element: 'span', styles: { 'background-color': '#ffff00' } },
        { name: 'Big',				element: 'big' },
        { name: 'Small',			element: 'small' },
        { name: 'Typewriter',		element: 'tt' },

        { name: 'Computer Code',	element: 'code' },
        { name: 'Keyboard Phrase',	element: 'kbd' },
        { name: 'Sample Text',		element: 'samp' },
        { name: 'Variable',			element: 'var' },

        { name: 'Deleted Text',		element: 'del' },
        { name: 'Inserted Text',	element: 'ins' },

        { name: 'Cited Work',		element: 'cite' },
        { name: 'Inline Quotation',	element: 'q' },

        { name: 'Language: RTL',	element: 'span', attributes: { 'dir': 'rtl' } },
        { name: 'Language: LTR',	element: 'span', attributes: { 'dir': 'ltr' } }


] );
