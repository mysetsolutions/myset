/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.cms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.security.ApplicationSecurity;
import ro.myset.appcore.web.utils.Utils;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * CMSSearch class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_CMSSearch")
@SessionScoped
@Scope("session")
public class CMSSearch  implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(CMSSearch.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private Utils appcore_Utils;

    @Autowired
    private ApplicationSecurity appcore_Security;

    // Variables
    private DatabaseProcessorService databaseProcessorService = null;

    private String userId = null;

    // Search
    private String searchText = null;
    private ArrayList<GenericBean> tags = null;
    private String searchTag = null;
    private String[] searchTags = null;
    private ArrayList<GenericBean> pages = null;


    public CMSSearch() {
        LOG.debug(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    private void initialize() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        this.databaseProcessorService = appcore_Application.getDatabaseProcessorService();
        //this.userId = appcore_Security.getSecurity().getUser().getUserId();
        LOG.debug(Constants.EVENT_COMPLETED);
    }


    public final void changeTag(AjaxBehaviorEvent event) {
        //LOG.debug(searchTags);
        this.searchByTextAndTags();
    }

    /**
     * Search pages using text
     */
    public final void search() {
        try {
            // Pages
            String databaseCommandId = "cms_search.sql01";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(searchText);
            databaseCommandParams.add(searchText);
            databaseCommand.setParameters(databaseCommandParams);
            this.pages = databaseProcessorService.executeQuery(databaseCommand);

            // Tags
            databaseCommandId = "cms_search.sql04";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(searchText);
            databaseCommand.setParameters(databaseCommandParams);
            this.tags = databaseProcessorService.executeQuery(databaseCommand);


        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Search pages by tag
     */
    public final void searchByTag() {
        try {
            // Command
            String databaseCommandId = "cms_search.sql03";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(searchTag);
            databaseCommand.setParameters(databaseCommandParams);
            this.pages = databaseProcessorService.executeQuery(databaseCommand);

            // Tags
            databaseCommandId = "cms_search.sql05";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(searchTag);
            databaseCommand.setParameters(databaseCommandParams);
            this.tags = databaseProcessorService.executeQuery(databaseCommand);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Search pages by text and multiple tags
     */
    public final void searchByTextAndTags() {
        try {
            if (searchTags != null && searchTags.length > 0) {
                // Command
                String databaseCommandId = "cms_search.sql02";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(searchText);
                databaseCommandParams.add(searchText);
                databaseCommandParams.add(appcore_Utils.createText(searchTags, Constants.PIPE, null, null));
                databaseCommand.setParameters(databaseCommandParams);
                this.pages = databaseProcessorService.executeQuery(databaseCommand);

                // Tags
                databaseCommandId = "cms_search.sql04";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(searchText);
                databaseCommand.setParameters(databaseCommandParams);
                this.tags = databaseProcessorService.executeQuery(databaseCommand);
            } else
                this.search();

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Fields
     */
    public final String getSearchText() {
        return searchText;
    }

    public final void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public final String getSearchTag() {
        return searchTag;
    }

    public final void setSearchTag(String searchTag) {
        this.searchTag = searchTag;
        this.searchTags = new String[]{searchTag};
    }

    public final String[] getSearchTags() {
        return searchTags;
    }

    public final void setSearchTags(String[] searchTags) {
        this.searchTags = searchTags;
    }

    public final ArrayList<GenericBean> getTags() {
        return tags;
    }

    public final ArrayList<GenericBean> getPages() {
        return pages;
    }
}
