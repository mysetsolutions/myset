/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.cms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorServiceCommand;
import ro.myset.appcore.grid.services.MailSenderService;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.utils.ELUtils;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.appcore.web.utils.Utils;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Map;

/**
 * CMSProcessor class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
@Named("appcore_CMSProcessor")
@ApplicationScoped
@Scope("singleton")

public class CMSProcessor {
    private static final Logger LOG = LogManager.getFormatterLogger(CMSProcessor.class);

    // Constants
    private final static String CACHE_PART_PREFIX = "part.";
    private final static String CACHE_PART_TAGS = ".tags";
    private final static String CACHE_PAGE_PREFIX = "page.";
    private final static String CACHE_PAGE_PARTS = ".parts";
    private final static String CACHE_CALENDAR_PREFIX = "calendar.";
    private final static String CACHE_COMMENTS_PREFIX = "comments.";
    private final static String CACHE_STATS_PREFIX = "stats.";
    private final static String CACHE_NEWSLETTERS = "newsletters";

    // Numbers
    private final static BigDecimal ZERO = new BigDecimal(0);
    private final static BigDecimal ONE = new BigDecimal(1);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private FacesUtils appcore_FacesUtils;

    @Autowired
    private CMSApplication appcore_CMSApplication;

    @Autowired
    private Utils appcore_Utils;


    public CMSProcessor() {
        LOG.debug(this);
    }



    /**
     * Gets page id from name
     */
    private final String pageId(String pageName) {
        String result = Constants.ZERO;
        try {
            ArrayList<GenericBean> pages = (ArrayList<GenericBean>) appcore_Application.getAttributes().get("myset.cms_pages");
            GenericBean pageGenericBean = appcore_Utils.find(pages, "name", pageName);
            if (pageGenericBean != null)
                result = pageGenericBean.getFields().get("id").toString();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Remove page from cache
     */
    public final void removePage(String pageId) {
        String cacheKey = CACHE_PAGE_PREFIX.concat(pageId);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Remove page parts from cache
     */
    public final void removePageParts(String pageId) {
        String cacheKey = CACHE_PAGE_PREFIX.concat(pageId).concat(CACHE_PAGE_PARTS);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Gets part from cache
     */
    private final GenericBean part(String partId) {
        GenericBean result = null;
        String cacheKey = CACHE_PART_PREFIX.concat(partId);
        try {
            if (!appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get entry from database
                String databaseCommandId = "cms_content.sql01";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(partId);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommand.setReadClob(true);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                if (results.size() > 0) {
                    // Put entry into cache
                    appcore_Application.getWebContentCache().put(cacheKey, results.get(0));
                }
            }
            // Get entry from cache
            result = (GenericBean) appcore_Application.getWebContentCache().get(cacheKey);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Remove part from cache
     */
    public final void removePart(String partId) {
        String cacheKey = CACHE_PART_PREFIX.concat(partId);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Gets part tags from cache
     */
    private final ArrayList<GenericBean> partTags(String partId) {
        ArrayList<GenericBean> result = null;
        String cacheKey = CACHE_PART_PREFIX.concat(partId).concat(CACHE_PART_TAGS);
        try {
            if (!appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get from database
                String databaseCommandId = "cms_content.sql02";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database("cms_content.sql02"));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(partId);
                databaseCommand.setParameters(databaseCommandParams);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                // Put into cache
                appcore_Application.getWebContentCache().put(cacheKey, results);
            }
            // Get from cache
            result = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Remove part tags from cache
     */
    public final void removePartTags(String partId) {
        String cacheKey = CACHE_PART_PREFIX.concat(partId).concat(CACHE_PART_TAGS);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Gets comments from cache
     */
    public final ArrayList<GenericBean> comments(String commentFor) {
        ArrayList<GenericBean> result = null;
        String cacheKey = CACHE_COMMENTS_PREFIX.concat(commentFor);
        try {
            if (!appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get from database
                String databaseCommandId = "cms_comments.sql10";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(commentFor);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommand.setReadClob(true);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                // Put into cache
                appcore_Application.getWebContentCache().put(cacheKey, results);
            }
            // Get from cache
            result = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Remove comments from cache
     */
    public final void removeComments(String commentFor) {
        String cacheKey = CACHE_COMMENTS_PREFIX.concat(commentFor);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Add comment
     */
    public final void commentAdd(String commentFor, String userId, String commentSubject, String commentContent, String commentPublic) {
        // Empty String EL issue
        userId = ELUtils.emptyString(userId);
        commentSubject = ELUtils.emptyString(commentSubject);
        commentContent = ELUtils.emptyString(commentContent);

        try {
            // Add new comment
            String databaseCommandId = "cms_comments.sql02";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(commentFor);
            databaseCommandParams.add(userId);
            databaseCommandParams.add(commentSubject);
            databaseCommandParams.add(commentContent);
            databaseCommandParams.add(commentPublic);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // Remove from cache
            this.removeComments(commentFor);
        }
    }

    /**
     * Update comment
     */
    public final void commentUpdate(String commentFor, String commentSubject, String commentContent, String commentPublic, String commentId) {
        // Empty String EL issue
        commentSubject = ELUtils.emptyString(commentSubject);
        commentContent = ELUtils.emptyString(commentContent);

        try {
            // Update comment
            String databaseCommandId = "cms_comments.sql03";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(commentSubject);
            databaseCommandParams.add(commentContent);
            databaseCommandParams.add(commentPublic);
            databaseCommandParams.add(commentId);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // Remove from cache
            this.removeComments(commentFor);
        }
    }

    /**
     * Delete comment
     */
    public final void commentDelete(String commentFor, String commentId) {
        try {
            // Delete comment
            String databaseCommandId = "cms_comments.sql04";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(commentId);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // Remove from cache
            this.removeComments(commentFor);
        }
    }


    /**
     * Gets stats from cache
     */
    public final ArrayList<GenericBean> stats(String statFor) {
        ArrayList<GenericBean> result = null;
        String cacheKey = CACHE_STATS_PREFIX.concat(Constants.DOT).concat(statFor);
        try {
            if (!appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get from database
                String databaseCommandId = "cms_stats.sql10";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(statFor);
                databaseCommand.setParameters(databaseCommandParams);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                // Put into cache
                appcore_Application.getWebContentCache().put(cacheKey, results);
            }
            // Get from cache
            result = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Remove stats from cache
     */
    public final void removeStats(String statFor) {
        String cacheKey = CACHE_STATS_PREFIX.concat(Constants.DOT).concat(statFor);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Gets stat from cache
     */
    public final GenericBean stat(String statFor) {
        GenericBean result = null;
        try {
            result = this.stats(statFor).get(0);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Gets stat from stats cache using userId
     */
    public final GenericBean statByUser(String statFor, String userId) {
        GenericBean result = null;
        try {
            if (userId != null) {
                ArrayList<GenericBean> stats = this.stats(statFor);
                if (stats != null) {
                    result = appcore_Utils.find(stats, "userId", userId);
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Add stat
     */
    public final void statAdd(String statFor, String userId, BigDecimal statValue, String statText) {
        // Empty String EL issue
        userId = ELUtils.emptyString(userId);
        statText = ELUtils.emptyString(statText);

        try {
            // Add new stat
            String databaseCommandId = "cms_stats.sql02";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(statFor);
            databaseCommandParams.add(userId);
            databaseCommandParams.add(statValue);
            databaseCommandParams.add(statText);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // Remove from cache
            this.removeStats(statFor);
        }
    }


    /**
     * Update stat
     */
    public final void statUpdate(String statFor, String userId, BigDecimal statValue, String statText, String statId) {
        // Empty String EL issue
        userId = ELUtils.emptyString(userId);
        statText = ELUtils.emptyString(statText);

        try {
            // Command
            String databaseCommandId = "cms_stats.sql03";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(statFor);
            databaseCommandParams.add(userId);
            databaseCommandParams.add(statValue);
            databaseCommandParams.add(statText);
            databaseCommandParams.add(statId);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Gets stats metrics
     */
    public final GenericBean statsMetrics(String statFor) {
        GenericBean result = null;
        try {
            ArrayList<GenericBean> stats = this.stats(statFor);
            Map<Object, Object> fields = new GenericMap<String, Object>();
            BigDecimal statSize = ZERO;
            BigDecimal statSum = ZERO;
            BigDecimal statAverage = new BigDecimal(0);
            if (stats != null) {
                statSize = new BigDecimal(stats.size());
                result = new GenericBean();
                GenericBean rank = null;
                BigDecimal rankValue = null;
                for (int i = 0; i < stats.size(); i++) {
                    rank = (GenericBean) stats.get(i);
                    rankValue = (BigDecimal) rank.getFields().get("statValue");
                    statSum = statSum.add(rankValue);
                }
                statAverage = statSum.divide(statSize, MathContext.DECIMAL128);
            }
            fields.put("statSize", statSize);
            fields.put("statSum", statSum);
            fields.put("statAverage", statAverage);
            result.setFields(fields);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Gets stats metrics by value
     */
    public final GenericBean statsMetricsByValue(String statFor, double statValue) {
        GenericBean result = null;
        try {
            ArrayList<GenericBean> stats = this.stats(statFor);
            Map<Object, Object> fields = new GenericMap<String, Object>();
            BigDecimal statSize = ZERO;
            BigDecimal statSum = ZERO;
            BigDecimal statAverage = new BigDecimal(statValue);
            if (stats != null) {
                result = new GenericBean();
                GenericBean rank = null;
                BigDecimal rankValue = null;
                for (int i = 0; i < stats.size(); i++) {
                    rank = (GenericBean) stats.get(i);
                    rankValue = (BigDecimal) rank.getFields().get("statValue");
                    if (rankValue.compareTo(new BigDecimal(statValue)) == 0) {
                        statSize = statSize.add(ONE);
                        statSum = statSum.add(rankValue);
                    }
                }
            }
            fields.put("statSize", statSize);
            fields.put("statSum", statSum);
            fields.put("statAverage", statAverage);
            result.setFields(fields);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Stat save
     */
    public final void statSave(String statFor, String userId, BigDecimal statValue, String statText) {
        // Empty String EL issue
        userId = ELUtils.emptyString(userId);
        statText = ELUtils.emptyString(statText);
        LOG.info("%s,%s,%s,%s",statFor,userId,statValue,statText);
        try {
            if (this.stat(statFor) == null) {
                // Add new stat
                this.statAdd(statFor, userId, statValue, statText);
            } else {
                GenericBean stat = this.statByUser(statFor, userId);
                if (stat != null) {
                    String statId = stat.getFields().get("statId").toString();
                    // Update existing stat
                    this.statUpdate(statFor, userId, statValue, statText, statId);
                } else {
                    // Add new stat
                    this.statAdd(statFor, userId, statValue, statText);
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // Remove from cache
            this.removeStats(statFor);
        }
    }

    /**
     * Stat increment value
     */
    public final void statIncrement(String statFor) {
        try {
            if (this.stat(statFor) == null) {
                // Add new stat
                this.statAdd(statFor, null, ONE, null);
            } else {
                // Get existing stat
                GenericBean stat = this.stat(statFor);
                String statId = stat.getFields().get("statId").toString();
                BigDecimal statValue = (BigDecimal) stat.getFields().get("statValue");
                // Update existing stat
                statValue = statValue.add(ONE);
                this.statUpdate(statFor, null, statValue, null, statId);
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // Remove from cache
            this.removeStats(statFor);
        }
    }



    /**
     * Gets calendar
     */
    public final ArrayList<GenericBean> calendar(String calendarTypeId) {
        ArrayList<GenericBean> result = null;
        String cacheKey = CACHE_CALENDAR_PREFIX.concat(calendarTypeId);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get entry from cache
                result = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);
            } else {
                // Get entry from database
                String databaseCommandId = "cms_calendars.sql05";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(calendarTypeId);
                databaseCommand.setParameters(databaseCommandParams);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                // Put entry into cache
                appcore_Application.getWebContentCache().put(cacheKey, results);
                // Get entry from cache
                result = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);
            }

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Remove calendar from cache
     */
    public final void removeCalendar(String calendarTypeId) {
        String cacheKey = CACHE_CALENDAR_PREFIX.concat(calendarTypeId);
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Gets newsletters
     */
    public final ArrayList<GenericBean> newsletters() {
        ArrayList<GenericBean> results = null;
        String cacheKey = CACHE_NEWSLETTERS;
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get entry from cache
                results = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);
            } else {
                // Get entry from database
                String databaseCommandId = "cms_newsletters.sql05";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommand.setParameters(databaseCommandParams);
                results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                // Put entry into cache
                appcore_Application.getWebContentCache().put(cacheKey, results);
                // Get entry from cache
                results = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return results;
        }
    }


    /**
     * Remove newsletters from cache
     */
    public final void removeNewsletters() {
        String cacheKey = CACHE_NEWSLETTERS;
        try {
            if (appcore_Application.getWebContentCache().containsKey(cacheKey))
                appcore_Application.getWebContentCache().remove(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Gets newsletter
     */
    public final GenericBean newsletter(String newsletterId) {
        GenericBean result = null;
        try {
            result = appcore_Utils.find(this.newsletters(), "newsletterId", newsletterId);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Remove newsletters subscriptions
     *
     * @param newsletterSubscriberValue Newletter subsciber value
     * @param infoMessage               Final info message
     * @param warnMessage               Final warn message
     * @param errorMessage              Final error message
     */
    public final void newslettersUnsubscribeAll(String newsletterSubscriberValue, String infoMessage, String warnMessage, String errorMessage) {
        try {
            String databaseCommandId = "cms_newsletters_subscribers.sql05";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(newsletterSubscriberValue);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Remove newsletter subscription
     *
     * @param newsletterId              Newletter id
     * @param newsletterSubscriberValue Newletter subsciber value
     * @param infoMessage               Final info message
     * @param warnMessage               Final warn message
     * @param errorMessage              Final error message
     */
    public final void newsletterUnsubscribe(String newsletterId, String newsletterSubscriberValue, String infoMessage, String warnMessage, String errorMessage) {
        try {
            String databaseCommandId = "cms_newsletters_subscribers.sql06";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(newsletterId);
            databaseCommandParams.add(newsletterSubscriberValue);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Newsletters subscribe
     *
     * @param newsletterSubscriberValue Newletter subscriber value
     * @param selectedNewsletters       ArrayList of selected newsletters id's
     * @param infoMessage               Final info message
     * @param warnMessage               Final warn message
     * @param errorMessage              Final error message
     */
    public final void newslettersSubscribe(String newsletterSubscriberValue, String[] selectedNewsletters, String infoMessage, String warnMessage, String errorMessage) {
        try {
            // Transaction
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            for (int i = 0; i < selectedNewsletters.length; i++) {
                // Subscribe
                String databaseCommandId = "cms_newsletters_subscribers.sql02";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(selectedNewsletters[i]);
                databaseCommandParams.add(newsletterSubscriberValue);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);
            }
            // Commit transaction
            appcore_Application.getDatabaseProcessorService().executeCommands(databaseCommands);

            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Newsletter send emails
     *
     * @param newsletter         Newsletter bean
     * @param messageType        Message type (Ex. text/html)
     * @param messageEncoding    Message encoding (Ex. UTF-8)
     * @param messageAttachments Message attachments (Ex. Full path)
     * @param messageSubject     Message subject
     * @param messageContent     Message from
     * @param infoMessage        Final info message
     * @param warnMessage        Final warn message
     * @param errorMessage       Final error message
     */
    public final void newsletterEmails(GenericBean newsletter,
                                       String messageType,
                                       String messageEncoding,
                                       String messageAttachments,
                                       String messageSubject,
                                       String messageContent,
                                       String infoMessage,
                                       String warnMessage,
                                       String errorMessage) {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, messageSubject);
        try {
            if (newsletter != null) {
                // configs
                MailSenderService mailSenderService = appcore_Application.getMailSenderService();

                // Get newsletter subscribers from database
                String databaseCommandId = "cms_newsletters_subscribers.sql07";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(newsletter.getFields().get("newsletterId"));
                databaseCommand.setParameters(databaseCommandParams);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                StringBuffer recipients = new StringBuffer();
                for (int i = 0; i < results.size(); i++) {
                    if (i < results.size()) {
                        if (i > 0)
                            recipients.append(Constants.COMMA);
                        recipients.append(results.get(i).getFields().get("newsletterSubscriberValue"));
                    }
                }

                LOG.debug("[%s] %s", uuid, recipients.toString());

                //Send batch emails
                mailSenderService.sendBatch(messageType,
                        messageEncoding,
                        newsletter.getFields().get("newsletterFrom").toString(),
                        recipients.toString(),
                        messageAttachments,
                        messageSubject,
                        messageContent);

                if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
                if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
            }
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                LOG.error("[%s]", uuid, e);
        }
    }


    /**
     * Get labels
     */
    public ArrayList<GenericBean> labels(String labelName,
                                         String labelDesc,
                                         String userId) {
        // Empty String EL issue
        labelName = ELUtils.emptyString(labelName);
        labelDesc = ELUtils.emptyString(labelDesc);
        userId = ELUtils.emptyString(userId);

        ArrayList<GenericBean> results = null;
        try {
            // Command
            String databaseCommandId = "cms_labels.sql01";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(labelName);
            databaseCommandParams.add(labelDesc);
            databaseCommandParams.add(userId);
            databaseCommand.setParameters(databaseCommandParams);
            results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return results;
        }
    }


    /**
     * Add label
     */
    public final void labelAdd(String labelName,
                               String labelDesc,
                               String userId) {
        // Empty String EL issue
        labelName = ELUtils.emptyString(labelName);
        labelDesc = ELUtils.emptyString(labelDesc);
        userId = ELUtils.emptyString(userId);
        try {
            // Command
            String databaseCommandId = "cms_labels.sql02";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(labelName);
            databaseCommandParams.add(labelDesc);
            databaseCommandParams.add(userId);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }

    }

    /**
     * Update label
     */
    public final void labelUpdate(String labelName,
                                  String labelDesc,
                                  String userId,
                                  String labelId) {
        // Empty String EL issue
        labelName = ELUtils.emptyString(labelName);
        labelDesc = ELUtils.emptyString(labelDesc);
        userId = ELUtils.emptyString(userId);

        try {
            // Command
            String databaseCommandId = "cms_labels.sql03";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(labelName);
            databaseCommandParams.add(labelDesc);
            databaseCommandParams.add(userId);
            databaseCommandParams.add(labelId);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Delete label
     */
    public final void labelDelete(
            String labelId) {
        try {
            // Command
            String databaseCommandId = "cms_labels.sql04";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(labelId);
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getDatabaseProcessorService().executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    // =========================================================================================
    // =========================================================================================
    // =========================================================================================
    // =========================================================================================


    /**
     * Gets page from cache
     */
    public final GenericBean page(String pageName) {
        GenericBean result = null;
        String pageId = this.pageId(pageName);
        try {
            // get page content
            String cacheKey = CACHE_PAGE_PREFIX.concat(pageId);
            if (!appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get from database
                String databaseCommandId = "cms_content.sql03";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(pageId);
                databaseCommand.setParameters(databaseCommandParams);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                if (results.size() > 0) {
                    // Put into cache
                    appcore_Application.getWebContentCache().put(cacheKey, results.get(0));
                }
            }
            // Get from cache
            result = (GenericBean) appcore_Application.getWebContentCache().get(cacheKey);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Gets page parts from cache
     */
    public final ArrayList<GenericBean> pageParts(String pageName) {
        ArrayList<GenericBean> result = null;
        String pageId = this.pageId(pageName);
        try {
            // get page content
            String cacheKey = CACHE_PAGE_PREFIX.concat(pageId).concat(CACHE_PAGE_PARTS);
            if (!appcore_Application.getWebContentCache().containsKey(cacheKey)) {
                // Get from database
                String databaseCommandId = "cms_content.sql04";
                DatabaseCommand databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(pageId);
                databaseCommand.setParameters(databaseCommandParams);
                ArrayList<GenericBean> results = appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand);
                // Put into cache
                appcore_Application.getWebContentCache().put(cacheKey, results);
            }
            // Get from cache
            result = (ArrayList<GenericBean>) appcore_Application.getWebContentCache().get(cacheKey);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Gets page part from cache
     */
    public final GenericBean pagePart(String pageName, String partName) {
        GenericBean result = null;
        try {
            GenericBean pagePartGenericBean = ((GenericBean) appcore_Utils.find(this.pageParts(pageName), "partName", partName));
            if (pagePartGenericBean != null)
                result = this.part(pagePartGenericBean.getFields().get("partId").toString());
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Gets page part tags from cache
     */
    public final ArrayList<GenericBean> pagePartTags(String pageName, String partName) {
        ArrayList<GenericBean> result = null;
        try {
            GenericBean pagePartGenericBean = this.pagePart(pageName, partName);
            if(pagePartGenericBean!=null)
                result=this.partTags(pagePartGenericBean.getFields().get("partId").toString());
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }








}
