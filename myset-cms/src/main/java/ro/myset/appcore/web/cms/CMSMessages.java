/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.cms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;
import ro.myset.appcore.grid.services.DatabaseProcessorServiceCommand;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.security.ApplicationSecurity;
import ro.myset.appcore.web.utils.ELUtils;
import ro.myset.appcore.web.utils.Utils;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * CMSProcessor class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_CMSMessages")
@SessionScoped
@Scope("session")
public class CMSMessages implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(CMSMessages.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private Utils appcore_Utils;

    @Autowired
    private ApplicationSecurity appcore_Security;

    // Variables
    private DatabaseProcessorService databaseProcessorService = null;

    // selection
    private boolean nothingSelected = false;
    private boolean singleSelected = false;
    private boolean multipleSelected = false;


    private String userId = null;
    private String label = null;
    private ArrayList<GenericBean> messages = null;
    private GenericBean selectedMessage = null;
    private List<GenericBean> selectedMessages = null;
    private GenericBean selectionDbSelect = null;
    private ArrayList<GenericBean> messagesNotReadCount = null;


    public CMSMessages() {
        LOG.debug(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    private void initialize() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        this.databaseProcessorService = appcore_Application.getDatabaseProcessorService();
        LOG.debug(Constants.EVENT_COMPLETED);
    }


    public final void toggleSelect(ToggleSelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getSource() != null) {
            if (event.isSelected())
                selectedMessages = (ArrayList<GenericBean>) ((DataTable) event.getSource()).getValue();
            else
                selectedMessages = null;

            //selection flags
            nothingSelected = true;
            singleSelected = false;
            multipleSelected = false;
            if (selectedMessages != null) {
                if (selectedMessages.size() == 1) {
                    nothingSelected = false;
                    singleSelected = true;
                }
                if (selectedMessages.size() > 1) {
                    nothingSelected = false;
                    singleSelected = false;
                    multipleSelected = true;
                }
            }
        }

    }

    public final void rowSelect(SelectEvent event) throws Exception {
        //LOG.debug("%s", event.getObject());
        if (event.getObject() != null) {
            selectedMessage = (GenericBean) event.getObject();

            //selection flags
            nothingSelected = true;
            singleSelected = false;
            multipleSelected = false;
            if (selectedMessages.size() == 1) {
                nothingSelected = false;
                singleSelected = true;
            }
            if (selectedMessages.size() > 1) {
                nothingSelected = false;
                singleSelected = false;
                multipleSelected = true;
            }
        }

    }

    public final void resetSelection() {
        nothingSelected = true;
        singleSelected = false;
        multipleSelected = false;
        selectedMessage = null;
        selectedMessages = null;
    }


    private final String seq_nextval(String sequenceName) {
        String result = null;
        // Empty String EL issue
        sequenceName = ELUtils.emptyString(sequenceName);

        try {
            // Command
            String databaseCommandId = "common.sql01";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(sequenceName);
            databaseCommand.setParameters(databaseCommandParams);
            ArrayList<GenericBean> results = databaseProcessorService.executeQuery(databaseCommand);
            if (results.size() > 0)
                result = results.get(0).getFields().get("value").toString();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Refresh userId
     */
    private final void refreshUserId() {
        if (appcore_Security != null
                && appcore_Security.getSecurity() != null
                && appcore_Security.getSecurity().getUser() != null)
            this.userId = appcore_Security.getSecurity().getUser().getUserId();
    }

    /**
     * Refresh messages not read count
     */
    public final void refreshMessagesNotReadCount() {
        try {
            this.refreshUserId();

            // Command
            String databaseCommandId = "cms_messages_labels.sql10";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(userId);
            databaseCommand.setParameters(databaseCommandParams);
            messagesNotReadCount = databaseProcessorService.executeQuery(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Read messages not read count
     */
    public final String messagesNotReadCount(String labelId) {
        String result = Constants.EMPTY;
        try {
            // Empty String EL issue
            labelId = ELUtils.emptyString(labelId);

            if (messagesNotReadCount != null) {
                GenericBean count = appcore_Utils.find(messagesNotReadCount, "labelId", labelId);
                if (count != null)
                    result = "(" + count.getFields().get("countValue") + ")";
            }

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }

    /**
     * Refresh messages
     */
    public final void refreshMessages(String labelId) {
        // Empty String EL issue
        labelId = ELUtils.emptyString(labelId);

        this.resetSelection();
        ArrayList<GenericBean> results = null;
        try {
            this.userId = appcore_Security.getSecurity().getUser().getUserId();
            this.label = labelId;

            // Command
            String databaseCommandId = "cms_messages.sql01";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(labelId);
            databaseCommandParams.add(userId);
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommand.setReadClob(true);
            this.messages = databaseProcessorService.executeQuery(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Refresh all messages
     */
    public final void refreshAllMessages(String labelId) {
        // Empty String EL issue
        labelId = ELUtils.emptyString(labelId);

        this.resetSelection();
        ArrayList<GenericBean> results = null;
        try {
            this.userId = appcore_Security.getSecurity().getUser().getUserId();
            this.label = labelId;
            // Command
            String databaseCommandId = "cms_messages.sql02";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommand.setReadClob(true);
            this.messages = databaseProcessorService.executeQuery(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Update message read
     */
    public final void messageUpdateRead(String messageRead) {
        try {
            this.refreshUserId();

            // Command
            String databaseCommandId = "cms_messages_labels.sql02";
            DatabaseCommand databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(messageRead);
            databaseCommandParams.add(selectedMessage.getFields().get("id"));
            databaseCommandParams.add(userId);
            databaseCommand.setParameters(databaseCommandParams);
            databaseProcessorService.executeCommand(databaseCommand);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Message send to many
     */
    public final void messageSendMany(String messageFrom,
                                      GenericBean[] messageTo,
                                      String messageSubject,
                                      String messageContent) {
        // Empty String EL issue
        messageFrom = ELUtils.emptyString(messageFrom);
        messageSubject = ELUtils.emptyString(messageSubject);
        messageContent = ELUtils.emptyString(messageContent);

        try {
            this.refreshUserId();
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            for (int i = 0; i < messageTo.length; i++) {
                // Extract id
                String messageId = seq_nextval("seq_cms_messages_labels");

                // Add to cms_messages
                databaseCommandId = "cms_messages.sql03";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(messageId);
                databaseCommandParams.add(messageFrom);
                databaseCommandParams.add(messageTo[i].getFields().get("userId"));
                databaseCommandParams.add(messageSubject);
                databaseCommandParams.add(messageContent);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);

                // Add to cms_messages_labels - 102
                databaseCommandId = "cms_messages_labels.sql01";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(messageId);
                databaseCommandParams.add("102");
                databaseCommandParams.add(messageFrom);
                databaseCommandParams.add("1");
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);

                // Add to cms_messages_labels - 101
                databaseCommandId = "cms_messages_labels.sql01";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(messageId);
                databaseCommandParams.add("101");
                databaseCommandParams.add(messageTo[i].getFields().get("userId"));
                databaseCommandParams.add(null);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);

            }

            // transaction
            databaseProcessorService.executeCommands(databaseCommands);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Message send
     */
    public final void messageSend(String messageFrom,
                                  String messageTo,
                                  String messageSubject,
                                  String messageContent) {
        // Empty String EL issue
        messageFrom = ELUtils.emptyString(messageFrom);
        messageSubject = ELUtils.emptyString(messageSubject);
        messageContent = ELUtils.emptyString(messageContent);

        try {
            this.refreshUserId();
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            // Extract id
            String messageId = seq_nextval("seq_cms_messages_labels");

            // Add to cms_messages
            databaseCommandId = "cms_messages.sql03";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(messageId);
            databaseCommandParams.add(messageFrom);
            databaseCommandParams.add(messageTo);
            databaseCommandParams.add(messageSubject);
            databaseCommandParams.add(messageContent);
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommands.add(databaseCommand);

            // Add to cms_messages_labels - 102
            databaseCommandId = "cms_messages_labels.sql01";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(messageId);
            databaseCommandParams.add("102");
            databaseCommandParams.add(messageFrom);
            databaseCommandParams.add("1");
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommands.add(databaseCommand);

            // Add to cms_messages_labels - 101
            databaseCommandId = "cms_messages_labels.sql01";
            databaseCommand = new DatabaseCommand();
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            databaseCommandParams = new ArrayList<Object>();
            databaseCommandParams.add(messageId);
            databaseCommandParams.add("101");
            databaseCommandParams.add(messageTo);
            databaseCommandParams.add(null);
            databaseCommand.setParameters(databaseCommandParams);
            databaseCommands.add(databaseCommand);

            // transaction
            databaseProcessorService.executeCommands(databaseCommands);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Messages move into
     */
    public final void messagesMoveInto(String toLabelId) {
        // Empty String EL issue
        toLabelId = ELUtils.emptyString(toLabelId);

        try {
            this.refreshUserId();
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            for (int i = 0; i < selectedMessages.size(); i++) {
                // Command
                databaseCommandId = "cms_messages_labels.sql03";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(toLabelId);
                databaseCommandParams.add(selectedMessages.get(i).getFields().get("id"));
                databaseCommandParams.add(userId);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);
            }

            // transaction
            databaseProcessorService.executeCommands(databaseCommands);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Empty deleted user messages
     */
    public final void messagesEmptyDeleted() {
        // Empty String EL issue

        try {
            this.refreshUserId();
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            for (int i = 0; i < selectedMessages.size(); i++) {
                // Command
                databaseCommandId = "cms_messages_labels.sql04";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(selectedMessages.get(i).getFields().get("id"));
                databaseCommandParams.add(userId);
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);
            }

            // transaction
            databaseProcessorService.executeCommands(databaseCommands);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Delete forever messages
     */
    public final void messagesDeleteForever() {
        // Empty String EL issue

        try {
            this.refreshUserId();
            ArrayList<DatabaseProcessorServiceCommand> databaseCommands = new ArrayList<DatabaseProcessorServiceCommand>();
            String databaseCommandId = null;
            DatabaseCommand databaseCommand = null;
            ArrayList<Object> databaseCommandParams = null;

            for (int i = 0; i < selectedMessages.size(); i++) {
                // Command
                databaseCommandId = "cms_messages_labels.sql05";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(selectedMessages.get(i).getFields().get("messageId"));
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);

                // Command
                databaseCommandId = "cms_messages.sql04";
                databaseCommand = new DatabaseCommand();
                databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
                databaseCommand.setLog(databaseCommandId);
                databaseCommandParams = new ArrayList<Object>();
                databaseCommandParams.add(selectedMessages.get(i).getFields().get("messageId"));
                databaseCommand.setParameters(databaseCommandParams);
                databaseCommands.add(databaseCommand);

            }

            // transaction
            databaseProcessorService.executeCommands(databaseCommands);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public final ArrayList<GenericBean> getMessages() {
        return messages;
    }

    public final ArrayList<GenericBean> getMessagesNotReadCount() {
        return messagesNotReadCount;
    }

    public final boolean isNothingSelected() {
        return nothingSelected;
    }

    public final void setNothingSelected(boolean nothingSelected) {
        this.nothingSelected = nothingSelected;
    }

    public final boolean isSingleSelected() {
        return singleSelected;
    }

    public final void setSingleSelected(boolean singleSelected) {
        this.singleSelected = singleSelected;
    }

    public final boolean isMultipleSelected() {
        return multipleSelected;
    }

    public final void setMultipleSelected(boolean multipleSelected) {
        this.multipleSelected = multipleSelected;
    }

    public GenericBean getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(GenericBean selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public List<GenericBean> getSelectedMessages() {
        return selectedMessages;
    }

    public void setSelectedMessages(List<GenericBean> selectedMessages) {
        this.selectedMessages = selectedMessages;
    }
}
