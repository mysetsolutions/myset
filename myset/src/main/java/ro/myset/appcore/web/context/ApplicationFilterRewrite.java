/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import java.io.Serializable;
import java.util.List;

/**
 * ApplicationFilterRewrite object class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationFilterRewrite implements Serializable {
    private static final long serialVersionUID = 1L;

    private String urlPattern = null;

    private List<ApplicationFilterRewriteCondition> rewriteConditions = null;

    private boolean redirect = false;

    private String rewriteWith = null;

    public final String getUrlPattern() {
        return urlPattern;
    }

    public final void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public final List<ApplicationFilterRewriteCondition> getRewriteConditions() {
        return rewriteConditions;
    }

    public final void setRewriteConditions(List<ApplicationFilterRewriteCondition> rewriteConditions) {
        this.rewriteConditions = rewriteConditions;
    }

    public final boolean isRedirect() {
        return redirect;
    }

    public final void setRedirect(boolean redirect) {
        this.redirect = redirect;
    }

    public final String getRewriteWith() {
        return rewriteWith;
    }

    public final void setRewriteWith(String rewriteWith) {
        this.rewriteWith = rewriteWith;
    }
}
