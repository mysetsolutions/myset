package ro.myset.appcore.web.components.execute;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;

/**
 * Execute component class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

@FacesComponent(value = "ro.myset.appcore.web.components.ExecuteComponent")

public class ExecuteComponent extends UIComponentBase {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ExecuteComponent.class);

    public static final String COMPONENT_FAMILY = "ro.myset.appcore.web.components";
    public static final String RENDERER_TYPE = "ro.myset.appcore.web.components.ExecuteRenderer";
    public static final boolean RENDERS_CHILDREN = false;

    public ExecuteComponent() {
        super();
        LOG.debug(this);
        setRendererType(RENDERER_TYPE);
    }

    @Override
    public final String getFamily() {
        return COMPONENT_FAMILY;
    }

    @Override
    public final boolean getRendersChildren() {
        return RENDERS_CHILDREN;
    }

}
