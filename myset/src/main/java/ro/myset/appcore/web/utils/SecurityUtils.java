/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import ro.myset.utils.security.EncryptionDecryptionUtils;
import ro.myset.utils.security.MD5Utils;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.UUID;

/**
 * Date utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_SecurityUtils")
@ApplicationScoped
@Scope("singleton")

public class SecurityUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(SecurityUtils.class);
    private static final String DEFAULT_ALGORITHM = "AES";
    private static final String DEFAULT_KEY = "MysetDefault-Key"; // 16 bits key


    public SecurityUtils() {
        LOG.debug(this);
    }

    /**
     * Encrypt MD5
     */
    public final String encryptMD5(String text) throws Exception {
        return MD5Utils.encrypt(text);
    }

    /**
     * Generate random MD5
     */
    public final String generateMD5() throws Exception {
        return MD5Utils.generate();
    }

    /**
     * Encrypt text using algorithm and key
     */
    public final String encrypt(String algorithm, String key, String text) throws Exception {
        return EncryptionDecryptionUtils.encrypt(algorithm, key, text);
    }

    /**
     * Decrypt text using algorithm and key
     */
    public final String decrypt(String algorithm, String key, String text) throws Exception {
        return EncryptionDecryptionUtils.decrypt(algorithm, key, text);
    }

    /**
     * Encrypt text using default algorithm and key
     */
    public final String encryptDefault(String text) throws Exception {
        return EncryptionDecryptionUtils.encrypt(DEFAULT_ALGORITHM, DEFAULT_KEY, text);
    }

    /**
     * Decrypt text using default algorithm and key
     */
    public final String decryptDefault(String text) throws Exception {
        return EncryptionDecryptionUtils.decrypt(DEFAULT_ALGORITHM, DEFAULT_KEY, text);
    }

    /**
     * Generate random UUID
     */
    public final String generateUUID() throws Exception {
        return UUIDUtils.generate();
    }

    /**
     * Generate UUID
     */
    public final String generateUUID(String namespace) throws Exception {
        return UUIDUtils.generate(namespace);
    }

    /**
     * Generate UUID using time
     */
    public final String generateUUIDUsingTime(String namespace) throws Exception {
        return UUIDUtils.generateUsingTime(namespace);
    }

    /**
     * parse UUID using time
     */
    public final UUID parseUUID(String uuidString) throws Exception {
        return UUIDUtils.parse(uuidString);
    }


}
