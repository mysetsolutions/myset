/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.web.Application;
import ro.myset.utils.common.Constants;
import ro.myset.utils.disk.DiskUtils;
import ro.myset.utils.disk.DiskUtilsException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Date utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
@Named("appcore_FileUtils")
@ApplicationScoped
@Scope("singleton")

public class FileUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(FileUtils.class);

    @Autowired
    private Application appcore_Application;

    @Autowired
    private FacesUtils appcore_FacesUtils;

    private String uploadTemporaryPath;
    private int readerBufferSize = 4096;

    public FileUtils() {
        LOG.debug(this);
    }





    /**
     * Get fileSeparator
     *
     */
    public final String getFileSeparator() throws Exception {
        return Constants.SYS_FILE_SEPARATOR;
    }



    /**
     * Create file
     *
     * @param path
     */
    public final File createFile(String path) throws Exception {
        return new File(path);
    }


    /**
     * Get file
     *
     * @param path
     */
    public final File file(String path) throws Exception {
        return DiskUtils.getFile(path);
    }

    /**
     * File check if exists
     *
     * @param path
     */
    public final boolean fileCheck(String path) {
        boolean result = false;
        try {
            if (DiskUtils.getFile(path) != null)
                result = true;
        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }
    }

    /**
     * Directory check if exists
     *
     * @param path
     */
    public final boolean directoryCheck(String path) {
        boolean result = false;
        try {
            if (DiskUtils.getDirectory(path) != null)
                result = true;
        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }
    }

    /**
     * Get directory
     *
     * @param path
     */
    public final File directory(String path) throws Exception {
        return DiskUtils.getDirectory(path);
    }


    /**
     * Get file attributes
     *
     * @param file
     */
    public final BasicFileAttributes basicFileAttributes(File file) {
        BasicFileAttributes result = null;
        try {
            if (file != null && file.isFile())
                result = Files.getFileAttributeView(Paths.get(file.getAbsolutePath()), BasicFileAttributeView.class).readAttributes();
        } catch (Exception e) {
            result = null;
        } finally {
            return result;
        }

    }


    /**
     * List all files from folder.
     *
     * @param folder
     */
    public final ArrayList<File> listFiles(File folder) {
        ArrayList<File> result = null;
        if (folder != null && (folder instanceof File)) {
            result = new ArrayList<File>(Arrays.asList(folder.listFiles()));
        }
        return result;
    }

    /**
     * List files from folder using filter regexp.
     *
     * @param folder
     * @param regexp
     */
    public final ArrayList<File> listFilesFiltered(File folder, String regexp) {
        ArrayList<File> result = null;
        if (folder != null && (folder instanceof File)) {
            result = new ArrayList<File>(Arrays.asList(folder.listFiles(new RegexpFileNameFilter(regexp))));
        }
        return result;
    }


    /**
     * Makes directory.
     *
     * @param path      /myfolder/
     * @param overwrite Overwrite if exists without clear content.
     * @throws DiskUtilsException If the execution fail.
     */
    public File mkdir(String path, boolean overwrite) throws DiskUtilsException {
        try {
            File dir = new File(path);
            if (!dir.mkdirs() && !overwrite)
                throw new DiskUtilsException("\"" + path + "\" - Already exist and can not be overwritten.");
            return dir;
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }


    /**
     * Copy file from source to destination.
     *
     * @param src       Source file.
     * @param dest      Destination file.
     * @param overwrite Overwrite file if exists.
     * @throws DiskUtilsException If the execution fail.
     */
    public void copyFile(File src, File dest, boolean overwrite) throws DiskUtilsException {
        try {
            DiskUtils.copyFile(src, dest, overwrite);
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Copy recursive from source to destination.
     *
     * @param src       Source file or directory.
     * @param dest      Destination directory.
     * @param overwrite Overwrite file or directory if exists.
     * @throws DiskUtilsException If the execution fail.
     */
    public void copy(File src, File dest, boolean overwrite) throws DiskUtilsException {
        try {
            DiskUtils.copy(src, dest, overwrite);
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }


    /**
     * Delete file.
     *
     * @param src Source file.
     * @throws DiskUtilsException If the execution fail.
     */
    public void delete(File src) throws DiskUtilsException {
        try {
            DiskUtils.delete(src);
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Delete directory.
     *
     * @param src Source directory.
     * @throws DiskUtilsException If the execution fail.
     */
    public void deleteDirectory(File src) throws DiskUtilsException {
        try {
            DiskUtils.deleteDirectory(src);
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Download file
     *
     * @param file Source directory.
     * @throws Exception If the execution fail.
     */
    public final StreamedContent download(File file) throws Exception {
        if (file != null && file.isFile()) {
            LOG.info("%s,%s", file.getCanonicalPath(), file.length());
            InputStream fis = new FileInputStream(file);
            return DefaultStreamedContent.builder().contentType(appcore_Application.getMimeTypes().getString(DiskUtils.getFileExtension(file.getName()))).name(file.getName()).stream(() -> fis).build();
        }
        return null;
    }


    /**
     * Upload files into folder.
     *
     * @param event Upload events
     * @param folder Target folder
     * @throws Exception If the execution fail.
     */
    public final void upload(FileUploadEvent event, File folder) {
        try {
            this.uploadTemporaryPath = appcore_Application.propertyValue("filemanager.upload.temporary-path").getValueString();
            this.readerBufferSize = Integer.parseInt(appcore_Application.propertyValue("filemanager.reader.buffer").getValueString());

            if (folder != null && folder.isDirectory()) {
                UploadedFile uploadedFile = event.getFile();
                InputStream is = uploadedFile.getInputStream();
                byte[] buffer = new byte[readerBufferSize];
                File file = new File(folder, uploadedFile.getFileName());
                LOG.info("%s,%s", file.getCanonicalPath(), uploadedFile.getSize());
                FileOutputStream fos = new FileOutputStream(file);
                int bufferLength = 0;
                while ((bufferLength = is.read(buffer)) > 0) {
                    fos.write(buffer, 0, bufferLength);
                    fos.flush();
                }
                is.close();
                fos.close();
            }else{
                throw new IOException("Invalid upload folder "+folder);
            }
        } catch (Exception e) {
            appcore_FacesUtils.message(e);
        }

    }



}


class RegexpFileNameFilter implements FilenameFilter {
    private String regexp = null;

    public RegexpFileNameFilter(String regexp) {
        this.regexp = regexp;
    }

    @Override
    public boolean accept(File dir, String name) {
        if (regexp == null || name == null || dir == null)
            return false;
        //check regexp
        return Pattern.compile(regexp).matcher(new File(name).getName()).matches();
    }

}