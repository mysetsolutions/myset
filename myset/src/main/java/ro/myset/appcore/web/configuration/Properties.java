/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.configuration;

import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.xml.XmlReader;

import java.io.File;
import java.util.ArrayList;

/**
 * Configuration parser Properties class for properties.xml
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class Properties implements Configuration {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(Properties.class);

    private File configurationFile = null;
    private IgniteCache cache = null;
    private boolean loadToCache = false;

    public Properties() throws Exception {
        LOG.debug(this);
    }

    /**
     * Refresh objects from configuration file
     */
    public final void refresh() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.info(configurationFile.getAbsolutePath());
        XmlReader xr = new XmlReader(configurationFile.getAbsolutePath());
        ArrayList<String> attrNames = new ArrayList<String>();
        attrNames.add("id");
        attrNames.add("description");

        // property
        ArrayList<GenericBean> valueArrayList = xr.readAttributes("properties/property", attrNames);
        for (int i = 0; i < valueArrayList.size(); i++) {
            //LOG.debug("%s,%s", valueArrayList.get(i).getFields().get("id").toString(), valueArrayList.get(i));
            if (cache != null && !cache.isClosed())
                cache.put(valueArrayList.get(i).getFields().get("id").toString(), valueArrayList.get(i));
        }
        // properties
        ArrayList<GenericBean> valuesArrayList = xr.readAttributes("properties/properties", attrNames);
        for (int i = 0; i < valuesArrayList.size(); i++) {
            xr.resetFilters();
            xr.addFilter("properties");
            xr.addFilter("properties/properties", "id", valuesArrayList.get(i).getFields().get("id").toString());
            xr.addFilter("properties/properties/property", true);
            ArrayList<GenericBean> valuesArray = xr.readAttributes("properties/properties/property", attrNames);
            //LOG.debug("%s,%s", valuesArrayList.get(i).getFields().get("id").toString(), valuesArray);
            if (cache != null && !cache.isClosed())
                cache.put(valuesArrayList.get(i).getFields().get("id").toString(), valuesArray);
        }

        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Gets configuration file
     */
    public File getConfigurationFile() {
        return configurationFile;
    }

    /**
     * Sets configuration file
     */
    public void setConfigurationFile(File configurationFile) {
        this.configurationFile = configurationFile;
    }


    /**
     * Gets cache
     */
    public IgniteCache getCache() {
        return cache;
    }

    /**
     * Sets cache
     */
    public void setCache(IgniteCache cache) {
        this.cache = cache;
    }

    /**
     * Gets load to cache
     */
    public boolean isLoadToCache() {
        return loadToCache;
    }

    /**
     * Sets load to cache
     */
    public void setLoadToCache(boolean loadToCache) throws Exception {
        this.loadToCache = loadToCache;
        if (loadToCache)
            this.refresh();
    }

}
