/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletSecurityElement;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * ApplicationServletRegistration class.
 */
public class ApplicationServletRegistration implements ServletRegistration.Dynamic {
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationServletRegistration.class);

    private ServletRegistration.Dynamic servletRegistration = null;

    public ApplicationServletRegistration(ServletRegistration.Dynamic servletRegistration) {
        LOG.debug(servletRegistration);
        this.servletRegistration = servletRegistration;
    }

    @Override
    public void setLoadOnStartup(int i) {
        servletRegistration.setLoadOnStartup(i);
    }

    @Override
    public Set<String> setServletSecurity(ServletSecurityElement servletSecurityElement) {
        return servletRegistration.setServletSecurity(servletSecurityElement);
    }

    @Override
    public void setMultipartConfig(MultipartConfigElement multipartConfigElement) {
        servletRegistration.setMultipartConfig(multipartConfigElement);
    }

    @Override
    public void setAsyncSupported(boolean b) {
        servletRegistration.setAsyncSupported(b);
    }

    @Override
    public Set<String> addMapping(String... strings) {
        return servletRegistration.addMapping(strings);
    }

    @Override
    public Collection<String> getMappings() {
        return servletRegistration.getMappings();
    }

    @Override
    public String getRunAsRole() {
        return servletRegistration.getRunAsRole();
    }

    @Override
    public void setRunAsRole(String s) {
        servletRegistration.setRunAsRole(s);
    }

    @Override
    public String getName() {
        return servletRegistration.getName();
    }

    @Override
    public String getClassName() {
        return servletRegistration.getClassName();
    }

    @Override
    public boolean setInitParameter(String s, String s1) {
        return servletRegistration.setInitParameter(s, s1);
    }

    @Override
    public String getInitParameter(String s) {
        return servletRegistration.getInitParameter(s);
    }

    @Override
    public Set<String> setInitParameters(Map<String, String> map) {
        return servletRegistration.setInitParameters(map);
    }

    @Override
    public Map<String, String> getInitParameters() {
        return servletRegistration.getInitParameters();
    }
}
