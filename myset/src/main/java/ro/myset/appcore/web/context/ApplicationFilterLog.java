/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.logging.log4j.Level;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;

import java.io.Serializable;

/**
 * ApplicationFilterLog object class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationFilterLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private String urlPattern = null;
    private DatabaseProcessorService databaseProcessorService = null;
    private String name = "logs";
    private String level = "INFO";
    private Level loggerLevel = Level.INFO;
    private String message = Constants.EMPTY;

    public final String getUrlPattern() {
        return urlPattern;
    }

    public final void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public final DatabaseProcessorService getDatabaseProcessorService() {
        return databaseProcessorService;
    }

    public final void setDatabaseProcessorService(DatabaseProcessorService databaseProcessorService) {
        this.databaseProcessorService = databaseProcessorService;
    }

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final String getLevel() {
        return level;
    }

    public final void setLevel(String level) {
        this.level = level;
        this.loggerLevel = Level.getLevel(this.level);
    }

    public Level getLoggerLevel() {
        return loggerLevel;
    }

    public final String getMessage() {
        return message;
    }

    public final void setMessage(String message) {
        this.message = message;
    }
}
