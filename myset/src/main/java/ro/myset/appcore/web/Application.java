/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;
import ro.myset.appcore.grid.services.MailSenderService;
import ro.myset.appcore.grid.services.TasksProcessorService;
import ro.myset.appcore.web.configuration.Configuration;
import ro.myset.appcore.web.context.*;
import ro.myset.appcore.web.extensions.Extension;
import ro.myset.appcore.web.packages.ApplicationPackage;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletRegistration;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Global application bean.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
@Named("appcore_Application")
@ApplicationScoped
@Scope("singleton")

public class Application implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(Application.class);

    @Autowired
    private ApplicationServletContext applicationServletContext;

    // dynamic application context
    private FileSystemXmlApplicationContext dynamicApplicationContext = null;

    // application path
    private File applicationPath = null;

    // attributes
    private GenericMap<String, Object> attributes = null;

    // grid
    private Ignite grid = null;

    //config caches
    private IgniteCache propertiesCache = null;
    private IgniteCache dictionaryCache = null;
    private IgniteCache databaseCache = null;

    // work caches
    private IgniteCache dataCache = null;
    private IgniteCache webContentCache = null;
    private IgniteCache webSessionsCache = null;

    //services
    private Properties servicesNames = null;
    private static final String TASKS_PROCESSOR_SERVICE_NAME_PROPERTY = "tasks.processor.service.name";
    private static final String DATABASE_PROCESSOR_SERVICE_NAME_PROPERTY = "database.processor.service.name";
    private static final String MAIL_PROCESSOR_SERVICE_NAME_PROPERTY = "mail.processor.service.name";

    // configurations
    private List<Configuration> configurations = null;

    // configuration files
    private List<File> configurationFiles = null;

    // filters
    private static final String APPCORE_ERROR_FILTERS = "appcore_ErrorFilters";
    private List<ApplicationFilterError> errorFilters = null;
    private static final String APPCORE_LOG_FILTERS = "appcore_LogFilters";
    private List<ApplicationFilterLog> logFilters = null;
    private static final String APPCORE_SECUTIRY_FILTERS = "appcore_SecurityFilters";
    private List<ApplicationFilterSecurity> securityFilters = null;
    private static final String APPCORE_CACHE_FILTERS = "appcore_CacheFilters";
    private List<ApplicationFilterCache> cacheFilters = null;
    private static final String APPCORE_REWRITE_FILTERS = "appcore_RewriteFilters";
    private List<ApplicationFilterRewrite> rewriteFilters = null;

    // extensions
    private static final String APPCORE_EXTENSIONSMANAGER_PROPERTIES = "appcore_ExtensionsManagerProperties";
    private static final String APPCORE_EXTENSIONS = "appcore_Extensions";
    private static final String APPCORE_EXTENSION_ID = "myset.extension.id";
    private Properties extensionsManagerProperties = null;
    private List<Extension> extensions = null;

    // mimeTypes
    private static final String APPCORE_MIME_TYPES = "appcore_MimeTypes";
    private GenericMap<String,String> mimeTypes = null;

    // packages
    private static final String APPCORE_PACKAGES = "appcore_Packages";
    private List<ApplicationPackage> packages= null;


    public Application() throws Exception {
        LOG.debug(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    public void initialize() {
        try {
            LOG.debug(Constants.EVENT_INVOKED);

            // attributes
            this.attributes = new GenericMap<String, Object>();

            // log configurations
            LOG.debug(configurations);

            // log configuration files
            LOG.debug(configurationFiles);

            // application path
            applicationPath = new File(applicationServletContext.getServletContext().getRealPath("/"));

            // refresh
            this.refreshDynamicApplicationContext();
            this.refreshExtensions();


            LOG.debug(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    //=======================================
    //=======================================
    //=======================================

    /**
     * Refresh configuration
     */
    public final void refreshConfiguration(String configurationSuffix) throws Exception {
        try {
            LOG.info(Constants.EVENT_INVOKED);
            if (configurationSuffix != null && configurations != null) {
                for (int i = 0; i < configurations.size(); i++) {
                    if (configurations.get(i).getClass().getName().toLowerCase().endsWith(configurationSuffix.toLowerCase()))
                        configurations.get(i).refresh();
                }
            }
            LOG.info(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Refresh configurations
     */
    public final void refreshConfigurations() throws Exception {
        try {
            LOG.info(Constants.EVENT_INVOKED);
            // refresh configurations
            if (configurations != null) {
                for (int i = 0; i < configurations.size(); i++) {
                    configurations.get(i).refresh();
                }
            }
            LOG.info(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Refresh dynamic application context
     */
    public final void refreshDynamicApplicationContext() throws Exception {
        try {
            LOG.info(Constants.EVENT_INVOKED);
            if (configurationFiles != null) {
                String[] configurationsArray = new String[configurationFiles.size()];
                for (int i = 0; i < configurationFiles.size(); i++) {
                    LOG.info(configurationFiles.get(i));
                    configurationsArray[i] = "file:" + configurationFiles.get(i).getAbsolutePath();
                }
                dynamicApplicationContext = new FileSystemXmlApplicationContext();
                dynamicApplicationContext.setConfigLocations(configurationsArray);
                dynamicApplicationContext.setParent(this.getApplicationContext());
                dynamicApplicationContext.refresh();
                dynamicApplicationContext.start();

                // filters
                this.errorFilters = dynamicApplicationContext.getBean(APPCORE_ERROR_FILTERS, List.class);
                this.logFilters = dynamicApplicationContext.getBean(APPCORE_LOG_FILTERS, List.class);
                this.securityFilters = dynamicApplicationContext.getBean(APPCORE_SECUTIRY_FILTERS, List.class);
                this.cacheFilters = dynamicApplicationContext.getBean(APPCORE_CACHE_FILTERS, List.class);
                this.rewriteFilters = dynamicApplicationContext.getBean(APPCORE_REWRITE_FILTERS, List.class);

                // mimeTypes
                this.mimeTypes = dynamicApplicationContext.getBean(APPCORE_MIME_TYPES, GenericMap.class);

                // packages
                this.packages = dynamicApplicationContext.getBean(APPCORE_PACKAGES, List.class);
            }
            LOG.info(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Refresh extensions
     */
    public final void refreshExtensions() throws Exception {
        try {
            LOG.info(Constants.EVENT_INVOKED);
            // refresh extensions
            this.extensionsManagerProperties = dynamicApplicationContext.getBean(APPCORE_EXTENSIONSMANAGER_PROPERTIES, Properties.class);
            this.extensions = dynamicApplicationContext.getBean(APPCORE_EXTENSIONS, List.class);
            if (extensions != null) {
                Extension extension = null;
                for (int i = 0; i < extensions.size(); i++) {
                    extension = extensions.get(i);
                    extension.initialize(this);
                }
            }
            LOG.info(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


    /**
     * Gets servlet context instance.
     */
    public final ApplicationServletContext getApplicationServletContext() {
        return applicationServletContext;
    }

    /**
     * Gets spring {@link WebApplicationContext} instance.
     */
    public final WebApplicationContext getContext() {
        if (applicationServletContext != null)
            return WebApplicationContextUtils.getWebApplicationContext(applicationServletContext.getServletContext());
        return null;
    }

    /**
     * Gets spring {@link XmlWebApplicationContext} instance.
     */
    public final XmlWebApplicationContext getApplicationContext() {
        if (applicationServletContext != null)
            return (XmlWebApplicationContext) applicationServletContext.getServletContext().getAttribute(Constants.APP_CONTEXT);
        return null;
    }

    /**
     * Gets spring {@link DispatcherServlet} instance.
     */
    public final DispatcherServlet getSpringDispatcherServlet() {
        if (applicationServletContext != null)
            return (DispatcherServlet) applicationServletContext.getServletContext().getAttribute(Constants.APP_SPRING_DISPATCHER_SERVLET);
        return null;
    }

    /**
     * Gets applicationServletRegistration {@link ApplicationServletRegistration} instance.
     */
    public final ApplicationServletRegistration getApplicationServletRegistration(String name) {
        if (applicationServletContext != null)
            return new ApplicationServletRegistration((ServletRegistration.Dynamic) applicationServletContext.getServletContext().getServletRegistration(name));
        return null;
    }

    /**
     * Gets applicationFilterRegistration {@link ApplicationFilterRegistration} instance.
     */
    public final ApplicationFilterRegistration getApplicationFilterRegistration(String name) {
        if (applicationServletContext != null)
            return new ApplicationFilterRegistration((FilterRegistration.Dynamic) applicationServletContext.getServletContext().getFilterRegistration(name));
        return null;
    }


    /**
     * Gets dynamic application context instance.
     */
    public final FileSystemXmlApplicationContext getDynamicApplicationContext() {
        return dynamicApplicationContext;
    }

    /**
     * Gets spring bean using bean id from {@link WebApplicationContext}.
     */
    public final Object bean(String beanId) {
        if (this.getContext() != null)
            return this.getContext().getBean(beanId);
        return null;
    }

    /**
     * Gets application path.
     */
    public final File getApplicationPath() {
        return applicationPath;
    }

    /**
     * Gets application attributes.
     */
    public final GenericMap<String, Object> getAttributes() {
        return attributes;
    }

    /**
     * Sets application attributes.
     */
    public final void setAttributes(GenericMap<String, Object> attributes) {
        this.attributes = attributes;
    }

    /**
     * Gets application default application grid.
     */
    public Ignite getGrid() {
        return grid;
    }

    /**
     * Sets application default application grid.
     */
    public void setGrid(Ignite grid) {
        this.grid = grid;
    }

    /**
     * Gets {@link Ignite} instance using grid bean id from {@link WebApplicationContext}.
     */

    public final Ignite grid(String gridBeanId) {
        if (this.getContext() != null)
            return this.getContext().getBean(gridBeanId, Ignite.class);
        return null;
    }

    /**
     * Gets {@link IgniteCache} instance of {@link Ignite} instance using bean id and cacheName.
     */
    public final IgniteCache cache(String gridBeanId, String cacheName) {
        if (this.grid(gridBeanId) != null)
            return this.grid(gridBeanId).cache(cacheName);
        return null;
    }

    /**
     * Gets grid service names.
     */
    public final Properties getServicesNames() {
        return servicesNames;
    }

    /**
     * Sets grid service names.
     */
    public final void setServicesNames(Properties servicesNames) {
        this.servicesNames = servicesNames;
    }


    //=======================================
    //          TasksProcessorService
    //=======================================

    /**
     * Gets tasks processor service from application grid.
     */
    public final TasksProcessorService tasksProcessorService(String serviceName) {
        if (this.grid != null)
            return this.grid.services().serviceProxy(serviceName, TasksProcessorService.class, false);
        return null;
    }

    /**
     * Gets application tasks processor service from application grid.
     */
    public final TasksProcessorService getTasksProcessorService() {
        return tasksProcessorService(servicesNames.getProperty(TASKS_PROCESSOR_SERVICE_NAME_PROPERTY));
    }

    /**
     * Gets tasks processor service from custom grid.
     */
    public final TasksProcessorService tasksProcessorService(Ignite customGrid, String serviceName) {
        if (customGrid != null)
            return customGrid.services().serviceProxy(serviceName, TasksProcessorService.class, false);
        return null;
    }


    //=======================================
    //          DatabaseProcessorService
    //=======================================

    /**
     * Gets database processor service from application grid.
     */
    public final DatabaseProcessorService databaseProcessorService(String serviceName) {
        if (this.grid != null)
            return this.grid.services().serviceProxy(serviceName, DatabaseProcessorService.class, false);
        return null;
    }

    /**
     * Gets database processor service from application grid.
     */
    public final DatabaseProcessorService getDatabaseProcessorService() {
        return this.databaseProcessorService(servicesNames.getProperty(DATABASE_PROCESSOR_SERVICE_NAME_PROPERTY));
    }

    /**
     * Gets database processor service from custom grid.
     */
    public final DatabaseProcessorService databaseProcessorService(Ignite customGrid, String serviceName) {
        if (customGrid != null)
            return customGrid.services().serviceProxy(serviceName, DatabaseProcessorService.class, false);
        return null;
    }


    //=======================================
    //          MailSenderService
    //=======================================

    /**
     * Gets mail sender service from application grid.
     */
    public final MailSenderService mailSenderService(String serviceName) {
        if (this.grid != null)
            return this.grid.services().serviceProxy(serviceName, MailSenderService.class, false);
        return null;
    }

    /**
     * Gets mail sender service from application grid.
     */
    public final MailSenderService getMailSenderService() {
        return this.mailSenderService(servicesNames.getProperty(MAIL_PROCESSOR_SERVICE_NAME_PROPERTY));
    }

    /**
     * Gets mail sender service from custom grid.
     */
    public final MailSenderService mailSenderService(Ignite customGrid, String serviceName) {
        if (customGrid != null)
            return customGrid.services().serviceProxy(serviceName, MailSenderService.class, false);
        return null;
    }

    //=======================================
    //             Extensions
    //=======================================

    /**
     * Gets extensionsManagerProperties
     */
    public final Properties getExtensionsManagerProperties() {
        return extensionsManagerProperties;
    }

    /**
     * Gets extensions
     */
    public final List<Extension> getExtensions() {
        return extensions;
    }

    /**
     * Gets extension by extension id
     */
    public final Extension getExtension(String extensionId) {
        if (extensionId != null && extensions != null && extensions.size() > 0)
            for (int i = 0; i < extensions.size(); i++) {
                Extension extension = extensions.get(i);
                if (extensionId.equalsIgnoreCase(extension.getExtensionProperties().getProperty(APPCORE_EXTENSION_ID)))
                    return extension;

            }
        return null;
    }

    //=======================================
    //=======================================
    //=======================================

    /**
     * Gets cache from application grid.
     */
    public final IgniteCache cache(String cacheName) {
        if (this.grid != null)
            return this.grid.cache(cacheName);
        return null;
    }

    /**
     * Gets entry from properties cache.
     */
    public final String property(Object id) {
        if (this.propertyValue(id) != null)
            return this.propertyValue(id).getValueString();
        return null;
    }

    /**
     * Gets entry from properties cache.
     */
    public final GenericBean propertyValue(Object id) {
        if (this.propertiesCache != null)
            return (GenericBean) this.propertiesCache.get(id);
        return null;
    }

    /**
     * Gets entry from properties cache.
     */
    public final ArrayList<GenericBean> propertyValues(Object id) {
        if (this.propertiesCache != null)
            return (ArrayList<GenericBean>) this.propertiesCache.get(id);
        return null;
    }

    /**
     * Gets entry from dictionary cache or return id
     */
    public final String dictionary(Object id) {
        if (this.dictionaryValue(id) != null)
            return dictionaryValue(id).getValueString();
        return null;
    }

    /**
     * Gets entry from dictionary cache.
     */
    public final GenericBean dictionaryValue(Object id) {
        if (this.dictionaryCache != null)
            return (GenericBean) this.dictionaryCache.get(id);
        return null;
    }

    /**
     * Gets entry from dictionary cache.
     */
    public final ArrayList<GenericBean> dictionaryValues(Object id) {
        if (this.dictionaryCache != null)
            return (ArrayList<GenericBean>) this.dictionaryCache.get(id);
        return null;
    }

    /**
     * Gets entry from database cache.
     */
    public final String database(Object id) {
        if (this.databaseValue(id) != null)
            return this.databaseValue(id).getValueString();
        return null;
    }

    /**
     * Gets entry from database cache.
     */
    public final GenericBean databaseValue(Object id) {
        if (this.databaseCache != null)
            return (GenericBean) this.databaseCache.get(id);
        return null;
    }

    /**
     * Gets properties cache.
     */
    public final IgniteCache getPropertiesCache() {
        return propertiesCache;
    }

    /**
     * Sets properties cache.
     */
    public final void setPropertiesCache(IgniteCache propertiesCache) {
        this.propertiesCache = propertiesCache;
    }

    /**
     * Gets dictionary cache.
     */
    public final IgniteCache getDictionaryCache() {
        return dictionaryCache;
    }

    /**
     * Sets dictionary cache.
     */
    public final void setDictionaryCache(IgniteCache dictionaryCache) {
        this.dictionaryCache = dictionaryCache;
    }

    /**
     * Gets database cache.
     */
    public final IgniteCache getDatabaseCache() {
        return databaseCache;
    }

    /**
     * Sets database cache.
     */
    public final void setDatabaseCache(IgniteCache databaseCache) {
        this.databaseCache = databaseCache;
    }

    /**
     * Gets data cache.
     */
    public final IgniteCache getDataCache() {
        return dataCache;
    }

    /**
     * Sets data cache.
     */
    public final void setDataCache(IgniteCache dataCache) {
        this.dataCache = dataCache;
    }

    /**
     * Gets web content cache.
     */
    public final IgniteCache getWebContentCache() {
        return webContentCache;
    }

    /**
     * Sets web content cache.
     */
    public final void setWebContentCache(IgniteCache webContentCache) {
        this.webContentCache = webContentCache;
    }

    /**
     * Gets web sessions cache.
     */
    public final IgniteCache getWebSessionsCache() {
        return webSessionsCache;
    }

    /**
     * Sets web sessions cache.
     */
    public final void setWebSessionsCache(IgniteCache webSessionsCache) {
        this.webSessionsCache = webSessionsCache;
    }

    /**
     * Gets configurations.
     */
    public final List<Configuration> getConfigurations() {
        return configurations;
    }

    /**
     * Sets configurations.
     */
    public final void setConfigurations(List<Configuration> configurations) {
        this.configurations = configurations;
    }

    /**
     * Gets configuration files.
     */
    public final List<File> getConfigurationFiles() {
        return configurationFiles;
    }

    /**
     * Sets configuration files.
     */
    public final void setConfigurationFiles(List<File> configurationFiles) throws Exception {
        this.configurationFiles = configurationFiles;
    }

    /**
     * Gets errorFilters.
     */
    public final List<ApplicationFilterError> getErrorFilters() {
        return errorFilters;
    }

    /**
     * Gets logFilters.
     */
    public final List<ApplicationFilterLog> getLogFilters() {
        return logFilters;
    }

    /**
     * Gets securityFilters.
     */
    public final List<ApplicationFilterSecurity> getSecurityFilters() {
        return securityFilters;
    }

    /**
     * Gets cacheFilters.
     */
    public final List<ApplicationFilterCache> getCacheFilters() {
        return cacheFilters;
    }

    /**
     * Gets rewriteFilters.
     */
    public final List<ApplicationFilterRewrite> getRewriteFilters() {
        return rewriteFilters;
    }

    /**
     * Gets packages
     */
    public final List<ApplicationPackage> getPackages() {
        return packages;
    }

    /**
     * Gets mimeTypes
     */
    public final GenericMap<String, String> getMimeTypes() {
        return mimeTypes;
    }

}
