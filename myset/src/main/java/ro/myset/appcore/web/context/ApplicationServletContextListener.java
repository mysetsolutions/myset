/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.ignite.Ignite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import ro.myset.appcore.common.Constants;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.ServletRegistration;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Properties;

/**
 * ApplicationServletContextListener class. Application bootstrap servlet listener.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class ApplicationServletContextListener implements ServletContextListener {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationServletContextListener.class);

    // constants
    private static final String APPLICATION_ENV = "MYSET";
    private static final String APPLICATION_GRID_CONFIGURATION = "conf/application-grid.xml";
    private static final String APPLICATION_CONFIGURATION = "conf/application.xml";

    // variables
    private ServletContext servletContext = null;
    private File applicationHomeFile = null;
    private File applicationGridFile = null;
    private File applicationStartFile = null;

    private XmlWebApplicationContext applicationGridContext = null;
    private XmlWebApplicationContext applicationContext = null;

    public ApplicationServletContextListener() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void contextInitialized(ServletContextEvent servletContextEvent) {
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            servletContext = servletContextEvent.getServletContext();
            String contextName = servletContext.getServletContextName();
            String propertyName = contextName.toLowerCase() + ".home";
            String applicationHome = System.getProperty(propertyName);
            if (applicationHome == null)
                throw new NullPointerException("Java property " + propertyName + " was not found");
            LOG.debug("Java property %s='%s' found.",propertyName,applicationHome);

            // application home
            applicationHomeFile = new File(applicationHome);
            if (!applicationHomeFile.exists() || !applicationHomeFile.isDirectory())
                throw new FileNotFoundException(applicationHomeFile.getAbsolutePath());

            // application grid
            applicationGridFile = new File(applicationHomeFile, APPLICATION_GRID_CONFIGURATION);
            if (!applicationGridFile.exists() || !applicationGridFile.isFile())
                throw new FileNotFoundException(applicationGridFile.getAbsolutePath());

            // application start
            applicationStartFile = new File(applicationHomeFile, APPLICATION_CONFIGURATION);
            if (!applicationStartFile.exists() || !applicationStartFile.isFile())
                throw new FileNotFoundException(applicationStartFile.getAbsolutePath());

            servletContext.setAttribute(Constants.APP_SERVLET, this);
            this.start(servletContextEvent);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            this.contextDestroyed(servletContextEvent);
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void contextDestroyed(ServletContextEvent servletContextEvent) {
        LOG.debug(Constants.EVENT_INVOKED);
        this.stop(servletContextEvent);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Starts application contexts {@link org.springframework.web.context.WebApplicationContext}
     */
    public final void start(ServletContextEvent servletContextEvent) {
        long bt = System.currentTimeMillis();
        LOG.info(Constants.EVENT_INVOKED);
        this.startApplicationGridContext();
        this.startApplicationWebContext();
        long et = System.currentTimeMillis();
        LOG.info(Constants.EVENT_COMPLETED + " in %s milliseconds", et - bt);
    }

    private final void startApplicationGridContext() {
        long bt = System.currentTimeMillis();
        LOG.info(Constants.EVENT_INVOKED);
        applicationGridContext = new XmlWebApplicationContext();
        applicationGridContext.setServletContext(servletContext);
        applicationGridContext.setConfigLocation("file:" + applicationGridFile.getAbsolutePath());

        // startup
        applicationGridContext.refresh();
        applicationGridContext.start();
        long et = System.currentTimeMillis();
        LOG.info(Constants.EVENT_COMPLETED + " in %s milliseconds", et - bt);

    }

    private final void startApplicationWebContext() {
        long bt = System.currentTimeMillis();
        LOG.info(Constants.EVENT_INVOKED);
        applicationContext = new XmlWebApplicationContext();
        // !!!! Very Important !!!!
        servletContext.setAttribute(Constants.APP_CONTEXT, applicationContext);
        applicationContext.setServletContext(servletContext);
        applicationContext.setConfigLocation("file:" + applicationStartFile.getAbsolutePath());
        applicationContext.setParent(applicationGridContext);

        // Add spring DispatcherServlet
        DispatcherServlet springDispatcherServlet = new DispatcherServlet(applicationContext);
        servletContext.setAttribute(Constants.APP_SPRING_DISPATCHER_SERVLET, springDispatcherServlet);
        ServletRegistration.Dynamic springDispatcherServletRegistration = servletContext.addServlet(Constants.APP_SPRING_DISPATCHER_SERVLET_NAME, springDispatcherServlet);
        springDispatcherServletRegistration.setAsyncSupported(true);
        springDispatcherServletRegistration.setLoadOnStartup(1);

        // startup
        applicationContext.refresh();
        applicationContext.start();
        long et = System.currentTimeMillis();
        LOG.info(Constants.EVENT_COMPLETED + " in %s milliseconds", et - bt);

    }


    /**
     * Close application contexts{@link org.springframework.web.context.WebApplicationContext}
     */
    public final void stop(ServletContextEvent servletContextEvent) {
        long bt = System.currentTimeMillis();
        LOG.info(Constants.EVENT_INVOKED);
        if (applicationContext != null) {
            applicationContext.close();
        }
        if (applicationGridContext != null) {
            applicationGridContext.close();
        }
        long et = System.currentTimeMillis();
        LOG.info(Constants.EVENT_COMPLETED + " in %s milliseconds", et - bt);
    }

}
