/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.packages;

import java.io.Serializable;

/**
 * ApplicationPackage object class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationPackage implements Serializable {
    private static final long serialVersionUID = 1L;

    private String group;
    private String name;
    private String version;
    private String licenseName;
    private String licenseLink;

    public final String getGroup() {
        return group;
    }

    public final void setGroup(String group) {
        this.group = group;
    }

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final String getVersion() {
        return version;
    }

    public final void setVersion(String version) {
        this.version = version;
    }

    public final String getLicenseName() {
        return licenseName;
    }

    public final void setLicenseName(String licenseName) {
        this.licenseName = licenseName;
    }

    public final String getLicenseLink() {
        return licenseLink;
    }

    public final void setLicenseLink(String licenseLink) {
        this.licenseLink = licenseLink;
    }
}
