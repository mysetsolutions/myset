/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.CommonUtils;

import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

/**
 * Generic validator class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@FacesValidator(value = "appcore_Validator")

public class Validator implements javax.faces.validator.Validator {
    public static final String INFO = "INFO";
    public static final String WARN = "WARN";
    public static final String ERROR = "ERROR";
    public static final String FATAL = "FATAL";
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(Validator.class);
    private static final String DEFAULT_MESSAGE = "{0}: Value is not valid";


    @Override
    /**
     * Check multiple validation using attributes N=1..n
     * validationN          - Boolean attribute
     * validationN-type     - Validation type: INFO, WARN, ERROR, FATAL
     *                        Default is ERROR
     * validationN-message  - Validation message (using MessageFormat).
     *                        Implicit objects:
     *                          {0} - Component label attribute or clientId
     *                          {1} - Component submitted value
     */
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null)
            return;
        boolean evaluate = true;
        int i = 1;
        String postfix = null;
        while (evaluate) {
            postfix = String.valueOf(i);
            Object validationObject = component.getAttributes().get("validation".concat(postfix));
            if (validationObject != null) {
                boolean validation = Boolean.parseBoolean(String.valueOf(validationObject));
                Object messageTypeObject = component.getAttributes().get("validation".concat(postfix).concat("-type"));
                String messageType = String.valueOf(messageTypeObject);
                if (messageTypeObject == null)
                    messageType = ERROR;
                Object[] messageObjects = new Object[2];
                messageObjects[0] = component.getAttributes().get("label");
                if (messageObjects[0] == null)
                    messageObjects[0] = component.getClientId();
                messageObjects[1] = value;
                Object messageTextObject = component.getAttributes().get("validation".concat(postfix).concat("-message"));
                String messageText = String.valueOf(component.getAttributes().get("validation".concat(postfix).concat("-message")));
                try {
                    if (messageTextObject != null)
                        messageText = CommonUtils.format(messageText, messageObjects);
                    else
                        messageText = CommonUtils.format(DEFAULT_MESSAGE, messageObjects);
                    ;
                } catch (Exception e) {
                    FacesMessage facesMessage = new FacesMessage(e.toString());
                    facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(facesMessage);
                }
                if (!validation) {
                    LOG.debug("%s,%s,%s", "validation".concat(postfix), messageType, messageText);
                    FacesMessage facesMessage = new FacesMessage(messageText);
                    if (messageType.equalsIgnoreCase(INFO)) {
                        facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    }
                    if (messageType.equalsIgnoreCase(WARN)) {
                        facesMessage.setSeverity(FacesMessage.SEVERITY_WARN);
                        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    }
                    if (messageType.equalsIgnoreCase(ERROR)) {
                        facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                        throw new ValidatorException(facesMessage);
                    }
                    if (messageType.equalsIgnoreCase(FATAL)) {
                        facesMessage.setSeverity(FacesMessage.SEVERITY_FATAL);
                        throw new ValidatorException(facesMessage);
                    }
                }
                i++;
            } else
                evaluate = false;
        }
    }
}

