/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import java.io.Serializable;

/**
 * ApplicationFilterCache object class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationFilterCache implements Serializable {
    private static final long serialVersionUID = 1L;

    private String urlPattern = null;
    private String cacheControl = null;
    private long cacheMaxAge = -1;
    private long cacheMaxAgeMillis = -1;

    public final String getUrlPattern() {
        return urlPattern;
    }

    public final void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public final String getCacheControl() {
        return cacheControl;
    }

    public final void setCacheControl(String cacheControl) {
        this.cacheControl = cacheControl;
    }

    public final long getCacheMaxAge() {
        return cacheMaxAge;
    }

    public final void setCacheMaxAge(long cacheMaxAge) {
        this.cacheMaxAge = cacheMaxAge;
        this.cacheMaxAgeMillis = this.cacheMaxAge * 1000;
    }

    public final long getCacheMaxAgeMillis() {
        return cacheMaxAgeMillis;
    }
}
