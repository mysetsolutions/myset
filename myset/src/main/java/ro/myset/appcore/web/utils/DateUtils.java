/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Date utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_DateUtils")
@ApplicationScoped
@Scope("singleton")

public class DateUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(DateUtils.class);

    public DateUtils() {
        LOG.debug(this);
    }

    /**
     * Gets date
     */
    public final Date getCurrentDate() {
        return new Date();
    }


    /**
     * Gets date
     */
    public final Date getDate(String source, String pattern) throws Exception {
        Date result = null;
        if (source != null && pattern != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            result = sdf.parse(source);
        }
        return result;
    }

    /**
     * Gets date
     */
    public final Date getDate(String year, String month, String day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day), 0, 0, 0);
        return calendar.getTime();
    }

    /**
     * Gets date
     */
    public final Date getDateTime(String year, String month, String day, String hour, String minutes, String seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minutes), Integer.parseInt(seconds));
        return calendar.getTime();
    }

    /**
     * Date format.
     *
     * @param source
     */
    public final String format(Date source, String pattern) {
        String result = null;
        if (source != null && pattern != null && (source instanceof java.util.Date || source instanceof java.sql.Date)) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            result = sdf.format(source);
        }
        return result;
    }

    /**
     * Create JavaScript ISO8601 date from java Date (yyyy-MM-dd'T'00:00:00.000)
     */
    public final String createJavaScriptDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_ISO8601_DATE);
        return sdf.format(date);
    }

    /**
     * Create JavaScript ISO8601 date from java Date (yyyy-MM-dd'T'HH:mm:ss.SSSXXX)
     */
    public final String createJavaScriptDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_ISO8601_FULL);
        return sdf.format(date);
    }


    /**
     * Create JavaScript array of ISO8601 string dates from java ArrayList<GenericBean> from fieldId
     */
    public final String createJavaScriptArrayOfDates(ArrayList<GenericBean> arrayList, String fieldId) {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i = 0; i < arrayList.size(); i++) {
            sb.append("\"").append(createJavaScriptDate(((Date) arrayList.get(i).getFields().get(fieldId)))).append("\"");
            if (i + 1 < arrayList.size()) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

}
