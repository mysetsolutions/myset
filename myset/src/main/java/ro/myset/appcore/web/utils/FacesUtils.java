/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.support.XmlWebApplicationContext;
import ro.myset.appcore.common.Constants;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.*;
import java.math.BigDecimal;


/**
 * FacesUtils class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_FacesUtils")
@ApplicationScoped
@Scope("singleton")
public class FacesUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(FacesUtils.class);

    /**
     * Faces message without any assignment.
     */
    public final void message(Severity messageSeverity, String messageText) {
        if (messageSeverity.equals(FacesMessage.SEVERITY_FATAL)) {
            LOG.fatal("%s", messageText);
        } else if (messageSeverity.equals(FacesMessage.SEVERITY_ERROR)) {
            LOG.error("%s", messageText);
        } else if (messageSeverity.equals(FacesMessage.SEVERITY_WARN)) {
            LOG.warn("%s", messageText);
        } else if (messageSeverity.equals(FacesMessage.SEVERITY_INFO)) {
            LOG.info("%s", messageText);
        } else {
            LOG.debug("%s", messageText);
        }
        FacesMessage message = new FacesMessage(messageText);
        message.setSeverity(messageSeverity);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Faces message for a component.
     */
    public final void messageFor(String messageFor, Severity messageSeverity, String messageText) {
        if (messageSeverity.equals(FacesMessage.SEVERITY_FATAL)) {
            LOG.fatal("[%s] %s", messageFor, messageText);
        } else if (messageSeverity.equals(FacesMessage.SEVERITY_ERROR)) {
            LOG.error("[%s] %s", messageFor, messageText);
        } else if (messageSeverity.equals(FacesMessage.SEVERITY_WARN)) {
            LOG.warn("[%s] %s", messageFor, messageText);
        } else if (messageSeverity.equals(FacesMessage.SEVERITY_INFO)) {
            LOG.info("[%s] %s", messageFor, messageText);
        } else {
            LOG.debug("[%s] %s", messageFor, messageText);
        }
        FacesMessage message = new FacesMessage(messageText);
        message.setSeverity(messageSeverity);
        FacesContext.getCurrentInstance().addMessage(messageFor, message);
    }

    /**
     * Faces info message for a component
     */
    public final void infoMessageFor(String messageFor, String messageText) {
        messageFor(messageFor, FacesMessage.SEVERITY_INFO, messageText);
    }

    /**
     * Faces info message
     */
    public final void infoMessage(String messageText) {
        message(FacesMessage.SEVERITY_INFO, messageText);
    }

    /**
     * Faces warn message for a component
     */
    public final void warnMessageFor(String messageFor, String messageText) {
        messageFor(messageFor, FacesMessage.SEVERITY_WARN, messageText);
    }

    /**
     * Faces warn message
     */
    public final void warnMessage(String messageText) {
        message(FacesMessage.SEVERITY_WARN, messageText);
    }

    /**
     * Faces error message for a component
     */
    public final void errorMessageFor(String messageFor, String messageText) {
        messageFor(messageFor, FacesMessage.SEVERITY_WARN, messageText);
    }

    /**
     * Faces error message
     */
    public final void errorMessage(String messageText) {
        message(FacesMessage.SEVERITY_ERROR, messageText);
    }

    /**
     * Faces fatal message for a component
     */
    public final void fatalMessageFor(String messageFor, String messageText) {
        messageFor(messageFor, FacesMessage.SEVERITY_FATAL, messageText);
    }

    /**
     * Faces error message
     */
    public final void fatalMessage(String messageText) {
        message(FacesMessage.SEVERITY_FATAL, messageText);
    }

    /**
     * Writes stack trace of an exception into a string.
     */
    public final String printStackTrace(Throwable e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter, true));
        return stringWriter.toString();
    }

    /**
     * Writes faces message with all stack trace of an exception
     */
    public final void message(Throwable e) {
        LOG.error(Constants.EMPTY, e);
        this.message(FacesMessage.SEVERITY_ERROR, this.printStackTrace(e));
    }

    /**
     * Gets spring web application context
     */
    public final XmlWebApplicationContext getApplicationContext() {
        return ((XmlWebApplicationContext) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get(Constants.APP_CONTEXT));
    }

    /**
     * Check Object
     */
    public final void checkObject(Object t) {
        LOG.debug(t);
    }

    /**
     * Check String
     */
    public final void checkString(String t) {
        LOG.debug(t);
    }

    /**
     * Check double
     */
    public final void checkDouble(double t) {
        LOG.debug(t);
    }

    /**
     * Check BigDecimal
     */
    public final void checkBigDecimal(BigDecimal t) {
        LOG.debug(t);
    }


}
