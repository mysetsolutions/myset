/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.configuration;

import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.utils.DatabaseUtils;
import ro.myset.appcore.web.utils.Utils;
import ro.myset.utils.generic.GenericBean;

import java.sql.Clob;
import java.util.ArrayList;


/**
 * Preferences configurations loader
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class Preferences implements Configuration {
    private static final Logger LOG = LogManager.getFormatterLogger(Preferences.class);

    // Variables
    private DatabaseProcessorService databaseProcessorService = null;
    private IgniteCache databaseConfigurationCache = null;
    private IgniteCache cache = null;
    private boolean loadToCache = false;

    @Autowired
    private Utils appcore_Utils;


    public Preferences() throws Exception {
        LOG.debug(this);
    }

    /**
     * Refresh objects from table
     */
    public final void refresh() throws Exception {
        LOG.info(Constants.EVENT_INVOKED);

        // Debug
        LOG.debug("databaseProcessorService: %s", databaseProcessorService);
        LOG.debug("databaseConfigurationCache: %s", databaseConfigurationCache);
        LOG.debug("cache: %s", cache);

        // Get all preferences
        DatabaseCommand databaseCommand = new DatabaseCommand();
        databaseCommand.setSqlCommand(((GenericBean) databaseConfigurationCache.get("users_preferences.sql01")).getValueString());
        databaseCommand.setLog("users_preferences.sql01");
        ArrayList<GenericBean> results = databaseProcessorService.executeQuery(databaseCommand);

        // Get single value keys
        databaseCommand = new DatabaseCommand();
        databaseCommand.setSqlCommand(((GenericBean) databaseConfigurationCache.get("users_preferences.sql02")).getValueString());
        databaseCommand.setLog("users_preferences.sql02");
        ArrayList<GenericBean> resultsSingleValue = databaseProcessorService.executeQuery(databaseCommand);

        // Get multiple values keys
        databaseCommand = new DatabaseCommand();
        databaseCommand.setSqlCommand(((GenericBean) databaseConfigurationCache.get("users_preferences.sql03")).getValueString());
        databaseCommand.setLog("users_preferences.sql03");
        ArrayList<GenericBean> resultsMultipleValues = databaseProcessorService.executeQuery(databaseCommand);

        // Put single values into cache
        for (int i = 0; i < resultsSingleValue.size(); i++) {
            GenericBean genericBean = appcore_Utils.find(results, "cacheKey", resultsSingleValue.get(i).getFields().get("cacheKey"));
            //LOG.debug("%s:%s",genericBean.getFields().get("cacheKey"),genericBean.getFields().get("cacheValue"));
            if (genericBean.getFields().get("cacheValue") instanceof Clob)
                cache.put(genericBean.getFields().get("cacheKey"), DatabaseUtils.clobReader((Clob) genericBean.getFields().get("cacheValue")));
            else
                cache.put(genericBean.getFields().get("cacheKey"), genericBean.getFields().get("cacheValue"));
        }

        // Put multiple values into cache
        for (int i = 0; i < resultsMultipleValues.size(); i++) {
            ArrayList<GenericBean> genericBeans = appcore_Utils.filter(results, "cacheKey", String.valueOf(resultsMultipleValues.get(i).getFields().get("cacheKey")));
            //LOG.debug("%s=%s", String.valueOf(resultsMultipleValues.get(i).getFields().get("cacheKey")), genericBeans);
            cache.put(resultsMultipleValues.get(i).getFields().get("cacheKey"), genericBeans);
        }

        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Gets databaseProcessorService
     */
    public final DatabaseProcessorService getDatabaseProcessorService() {
        return databaseProcessorService;
    }

    /**
     * Sets databaseProcessorService
     */
    public final void setDatabaseProcessorService(DatabaseProcessorService databaseProcessorService) {
        this.databaseProcessorService = databaseProcessorService;
    }

    /**
     * Gets databaseConfigurationCache
     */
    public final IgniteCache getDatabaseConfigurationCache() {
        return databaseConfigurationCache;
    }

    /**
     * Sets databaseConfigurationCache
     */
    public final void setDatabaseConfigurationCache(IgniteCache databaseConfigurationCache) {
        this.databaseConfigurationCache = databaseConfigurationCache;
    }

    /**
     * Gets cache
     */
    public IgniteCache getCache() {
        return cache;
    }

    /**
     * Sets cache
     */
    public void setCache(IgniteCache cache) {
        this.cache = cache;
    }

    /**
     * Gets load to cache
     */
    public boolean isLoadToCache() {
        return loadToCache;
    }

    /**
     * Sets load to cache
     */
    public void setLoadToCache(boolean loadToCache) throws Exception {
        this.loadToCache = loadToCache;
        if (loadToCache)
            this.refresh();
    }


}

