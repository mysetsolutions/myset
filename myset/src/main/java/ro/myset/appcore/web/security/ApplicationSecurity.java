/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Application security class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ApplicationSecurity")
@SessionScoped
@Scope("session")
public class ApplicationSecurity implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationSecurity.class);

    private static int HTTP_200 = 200;
    private static int HTTP_401 = 401;
    private static String HTTP_401_MESSAGE = "Not authenticated";
    private static int HTTP_403 = 403;
    private static String HTTP_403_MESSAGE = "Not authorized";

    @Autowired
    private Application appcore_Application;

    // security
    private Security security = null;


    public ApplicationSecurity() throws Exception {
        LOG.debug(this);
    }

    public final Security getSecurity() {
        return security;
    }


    //================================
    //          Security
    //================================

    /**
     * Login method.
     *
     * @param userName              User name
     * @param userGenericBean       User bean
     * @param userRolesGenericBeans User roles beans
     */
    public final boolean login(String userName, GenericBean userGenericBean, ArrayList<GenericBean> userRolesGenericBeans) {
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        security = new Security();
        security.login(userGenericBean, userRolesGenericBeans);
        if (security.isAuthenticated()) {
            httpSession.setAttribute("user", userName);
            httpSession.setMaxInactiveInterval(Integer.parseInt(appcore_Application.property("application.session.timeout.authenticated")));
            LOG.info("Session:%s, MaxInactiveInterval:%s, User:%s", httpSession.getId(), httpSession.getMaxInactiveInterval(), userName);
            return true;
        } else {
            LOG.info("Session:%s, User:%s", httpSession.getId(), userName);
            return false;
        }
    }

    /**
     * Logout method.
     */
    public final void logout() {
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (security != null) {
            LOG.info("Session:%s, User:%s", httpSession.getId(), this.security.getUser().getUserName());
            security.logout();
            security = null;
        } else {
            LOG.info("Session:%s", httpSession.getId());
        }
        httpSession.invalidate();
    }

    /**
     * Security check authentication and authorization and throw standard http error codes.
     *
     * @param sourceURL Source url
     * @param roles     Roles regexp
     */
    public final void securityCheck(String sourceURL, String roles) {
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletResponse httpResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            if (httpResponse.getStatus() == HTTP_200) {
                // authentication check
                if (this.security == null || !this.security.authenticated) {
                    LOG.error("%s, %s %s, Session:%s", HTTP_401, HTTP_401_MESSAGE, sourceURL, httpSession.getId());
                    if (!httpResponse.isCommitted())
                        externalContext.responseSendError(HTTP_401, HTTP_401_MESSAGE);
                    return;
                }
                // authorization check
                if (!this.security.ifAnyRoles(roles)) {
                    LOG.error("%s, %s %s, Session:%s, User:%s, Roles:%s", sourceURL, HTTP_403, HTTP_403_MESSAGE, httpSession.getId(), this.security.getUser().getUserName(), roles);
                    if (!httpResponse.isCommitted())
                        externalContext.responseSendError(HTTP_403, HTTP_403_MESSAGE);
                    return;
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


}
