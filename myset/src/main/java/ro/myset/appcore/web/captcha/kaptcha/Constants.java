/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.captcha.kaptcha;

public class Constants {
    public final static String KAPTCHA_SESSION_KEY = "KAPTCHA_SESSION_KEY";

    public final static String KAPTCHA_SESSION_DATE = "KAPTCHA_SESSION_DATE";

    public final static String KAPTCHA_SESSION_CONFIG_KEY = "kaptcha.session.key";

    public final static String KAPTCHA_SESSION_CONFIG_DATE = "kaptcha.session.date";

    public final static String KAPTCHA_BORDER = "kaptcha.border";

    public final static String KAPTCHA_BORDER_COLOR = "kaptcha.border.color";

    public final static String KAPTCHA_BORDER_THICKNESS = "kaptcha.border.thickness";

    public final static String KAPTCHA_NOISE_COLOR = "kaptcha.noise.color";

    public final static String KAPTCHA_NOISE_IMPL = "kaptcha.noise.impl";

    public final static String KAPTCHA_OBSCURIFICATOR_IMPL = "kaptcha.obscurificator.impl";

    public final static String KAPTCHA_PRODUCER_IMPL = "kaptcha.producer.impl";

    public final static String KAPTCHA_TEXTPRODUCER_IMPL = "kaptcha.textproducer.impl";

    public final static String KAPTCHA_TEXTPRODUCER_CHAR_STRING = "kaptcha.textproducer.char.string";

    public final static String KAPTCHA_TEXTPRODUCER_CHAR_LENGTH = "kaptcha.textproducer.char.length";

    public final static String KAPTCHA_TEXTPRODUCER_FONT_NAMES = "kaptcha.textproducer.font.names";

    public final static String KAPTCHA_TEXTPRODUCER_FONT_COLOR = "kaptcha.textproducer.font.color";

    public final static String KAPTCHA_TEXTPRODUCER_FONT_SIZE = "kaptcha.textproducer.font.size";

    public final static String KAPTCHA_TEXTPRODUCER_CHAR_SPACE = "kaptcha.textproducer.char.space";

    public final static String KAPTCHA_WORDRENDERER_IMPL = "kaptcha.word.impl";

    public final static String KAPTCHA_BACKGROUND_IMPL = "kaptcha.background.impl";

    public static final String KAPTCHA_BACKGROUND_CLR_FROM = "kaptcha.background.clear.from";

    public static final String KAPTCHA_BACKGROUND_CLR_TO = "kaptcha.background.clear.to";

    public static final String KAPTCHA_IMAGE_WIDTH = "kaptcha.image.width";

    public static final String KAPTCHA_IMAGE_HEIGHT = "kaptcha.image.height";
}
