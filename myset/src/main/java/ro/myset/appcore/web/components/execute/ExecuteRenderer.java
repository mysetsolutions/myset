package ro.myset.appcore.web.components.execute;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Execute renderer class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

@FacesRenderer(componentFamily = ExecuteComponent.COMPONENT_FAMILY, rendererType = ExecuteComponent.RENDERER_TYPE)

public class ExecuteRenderer extends Renderer {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ExecuteRenderer.class);

    public ExecuteRenderer() {
        super();
        LOG.debug(this);
    }

    @Override
    public final void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        LOG.debug(this);
        if (component.isRendered()) {
            ResponseWriter writer = context.getResponseWriter();
            writer.startElement("div", null);
            writer.write(String.valueOf(component.getAttributes().get("value")));
            super.encodeBegin(context, component);
        }
    }

    @Override
    public final void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        LOG.debug(this);
        if (component.isRendered()) {
            ResponseWriter writer = context.getResponseWriter();
            writer.endElement("div");
            super.encodeEnd(context, component);
        }
    }


    @Override
    public final boolean getRendersChildren() {
        return ExecuteComponent.RENDERS_CHILDREN;
    }

    @Override
    public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
        LOG.debug(this);
        if (component.isRendered())
            super.encodeChildren(context, component);
    }


}
