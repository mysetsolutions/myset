/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.database;

import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.DatabaseProcessorService;
import ro.myset.appcore.grid.services.DatabaseProcessorServiceCommand;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.appcore.web.utils.FacesUtils;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import java.util.ArrayList;

/**
 * DatabaseProcessor class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_DatabaseProcessor")
@ApplicationScoped
@Scope("singleton")

public class DatabaseProcessor {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabaseProcessor.class);

    @Autowired
    private FacesUtils appcore_FacesUtils;

    private DatabaseProcessorService databaseProcessorService = null;



    public DatabaseProcessor() {
        LOG.debug(this);
    }

    public final ArrayList<GenericBean> executeQuery(DatabaseProcessorService databaseProcessorService, DatabaseProcessorServiceCommand databaseProcessorServiceCommand, String infoMessage, String warnMessage, String errorMessage) {
        ArrayList<GenericBean> results = null;
        try {
            results = databaseProcessorService.executeQuery(databaseProcessorServiceCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        } finally {
            return results;
        }
    }

    public final void executeQueryAsync(DatabaseProcessorService databaseProcessorService, DatabaseProcessorServiceCommand databaseProcessorServiceCommand, String infoMessage, String warnMessage, String errorMessage, IgniteCache cache, Object cacheKey) {
        try {
            databaseProcessorService.executeQueryAsync(databaseProcessorServiceCommand, cache, cacheKey);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void executeCommand(DatabaseProcessorService databaseProcessorService, DatabaseProcessorServiceCommand databaseProcessorServiceCommand, String infoMessage, String warnMessage, String errorMessage) {
        try {
            databaseProcessorService.executeCommand(databaseProcessorServiceCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void executeCommandAsync(DatabaseProcessorService databaseProcessorService, DatabaseProcessorServiceCommand databaseProcessorServiceCommand, String infoMessage, String warnMessage, String errorMessage) {
        try {
            databaseProcessorService.executeCommandAsync(databaseProcessorServiceCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void executeCallable(DatabaseProcessorService databaseProcessorService, DatabaseProcessorServiceCommand databaseProcessorServiceCommand, String infoMessage, String warnMessage, String errorMessage) {
        ArrayList<Object> results = null;
        try {
            databaseProcessorService.executeCallable(databaseProcessorServiceCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void executeCallableAsync(DatabaseProcessorService databaseProcessorService, DatabaseProcessorServiceCommand databaseProcessorServiceCommand, String infoMessage, String warnMessage, String errorMessage) {
        try {
            databaseProcessorService.executeCallableAsync(databaseProcessorServiceCommand);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void executeCommands(DatabaseProcessorService databaseProcessorService, ArrayList<DatabaseProcessorServiceCommand> databaseProcessorServiceCommands, String infoMessage, String warnMessage, String errorMessage) {
        try {
            databaseProcessorService.executeCommands(databaseProcessorServiceCommands);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void executeCommandsAsync(DatabaseProcessorService databaseProcessorService, ArrayList<DatabaseProcessorServiceCommand> databaseProcessorServiceCommands, String infoMessage, String warnMessage, String errorMessage) {
        try {
            databaseProcessorService.executeCommandsAsync(databaseProcessorServiceCommands);
            if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
            if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

}
