/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import java.io.Serializable;
import java.util.Date;

/**
 * User object class. This class contains details of one individual system user.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public final class User implements Serializable {
    private String userId = null;
    private String userName = null;
    private String userFirstName = null;
    private String userGivenName = null;
    private String userLastName = null;
    private Date userBirthDate = null;
    private String userAddress = null;
    private String userEmail = null;
    private String userOrganization = null;
    private String userOrganizationUnit = null;
    private String userDescription = null;
    private String userPhone = null;
    private String userMobile = null;
    private String userWorkPhone = null;
    private String userKey = null;
    private String userProtected = null;
    private Date userCreateDate = null;
    private Date userLastUpdate = null;
    private Date userLastLogon = null;


    public final String getUserId() {
        return userId;
    }

    public final void setUserId(String userId) {
        this.userId = userId;
    }

    public final String getUserName() {
        return userName;
    }

    public final void setUserName(String userName) {
        this.userName = userName;
    }

    public final String getUserFirstName() {
        return userFirstName;
    }

    public final void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public final String getUserGivenName() {
        return userGivenName;
    }

    public final void setUserGivenName(String userGivenName) {
        this.userGivenName = userGivenName;
    }

    public final String getUserLastName() {
        return userLastName;
    }

    public final void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public final Date getUserBirthDate() {
        return userBirthDate;
    }

    public final void setUserBirthDate(Date userBirthDate) {
        this.userBirthDate = userBirthDate;
    }

    public final String getUserAddress() {
        return userAddress;
    }

    public final void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public final String getUserEmail() {
        return userEmail;
    }

    public final void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public final String getUserOrganization() {
        return userOrganization;
    }

    public final void setUserOrganization(String userOrganization) {
        this.userOrganization = userOrganization;
    }

    public final String getUserOrganizationUnit() {
        return userOrganizationUnit;
    }

    public final void setUserOrganizationUnit(String userOrganizationUnit) {
        this.userOrganizationUnit = userOrganizationUnit;
    }

    public final String getUserDescription() {
        return userDescription;
    }

    public final void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public final String getUserPhone() {
        return userPhone;
    }

    public final void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public final String getUserMobile() {
        return userMobile;
    }

    public final void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public final String getUserWorkPhone() {
        return userWorkPhone;
    }

    public final void setUserWorkPhone(String userWorkPhone) {
        this.userWorkPhone = userWorkPhone;
    }

    public final String getUserKey() {
        return userKey;
    }

    public final void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public final String getUserProtected() {
        return userProtected;
    }

    public final void setUserProtected(String userProtected) {
        this.userProtected = userProtected;
    }

    public final Date getUserCreateDate() {
        return userCreateDate;
    }

    public final void setUserCreateDate(Date userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    public final Date getUserLastUpdate() {
        return userLastUpdate;
    }

    public final void setUserLastUpdate(Date userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    public Date getUserLastLogon() {
        return userLastLogon;
    }

    public void setUserLastLogon(Date userLastLogon) {
        this.userLastLogon = userLastLogon;
    }
}
