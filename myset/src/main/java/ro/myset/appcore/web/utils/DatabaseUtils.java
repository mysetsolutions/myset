package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.File;
import java.io.FileWriter;
import java.io.Reader;
import java.nio.CharBuffer;
import java.sql.Clob;
import java.sql.ParameterMetaData;

/**
 * Database utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_DatabaseUtils")
@ApplicationScoped
@Scope("singleton")
public class DatabaseUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabaseUtils.class);

    /*
     * Reads Clob or String into String
     */
    public static final String clobReader(Object obj) {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        String result = null;
        try {
            if (obj != null && obj instanceof Clob) {
                StringBuffer clobStringBuffer = new StringBuffer();
                int bufferSize = 1024;
                char[] buffer = new char[bufferSize];
                Reader clobReader = ((Clob) obj).getCharacterStream();
                int length = 0;
                while ((length = clobReader.read(buffer)) != -1) {
                    clobStringBuffer.append(buffer, 0, length);
                }
                result = clobStringBuffer.toString();
            }
            if (obj != null && obj instanceof String) {
                result = obj.toString();
            }
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
        } finally {
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
            return result;
        }
    }

    /*
     * Reads Clob or String into a limited String
     */
    public static final String clobReaderLimited(Object obj, int limit) {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        String result = null;
        try {
            if (obj != null && obj instanceof Clob) {
                StringBuffer clobStringBuffer = new StringBuffer();
                int bufferSize = 1024;
                int reads = 0;
                char[] buffer = new char[bufferSize];
                Reader clobReader = ((Clob) obj).getCharacterStream();
                int length = 0;
                while ((length = clobReader.read(buffer)) != -1) {
                    reads = reads + length;
                    if (reads < limit) {
                        clobStringBuffer.append(buffer, 0, length);
                    }
                }
                result = clobStringBuffer.toString();
            }
            if (obj != null && obj instanceof String) {
                if ((((String) obj).length()) > limit)
                    result = ((String) obj).substring(0, limit);
                else
                    result = (String) obj;
            }
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
        } finally {
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
            return result;
        }

    }

    /*
     * Write Clob into a file
     */
    public static final void clobToFile(Clob clobObj, File fileObj) {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        FileWriter fileWritter = null;
        try {
            if (clobObj != null
                    && clobObj instanceof Clob
                    && fileObj != null
                    && fileObj instanceof File) {
                LOG.info("[%s] clobLength:%s, file:%s", uuid, clobObj.length(), fileObj);
                fileWritter = new FileWriter(fileObj);
                int bufferSize = 1024;
                char[] buffer = new char[bufferSize];
                Reader clobReader = ((Clob) clobObj).getCharacterStream();
                int length = 0;
                while ((length = clobReader.read(buffer)) != -1) {
                    fileWritter.append(CharBuffer.wrap(buffer), 0, length);
                    fileWritter.flush();
                }
                fileWritter.close();
            }
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
        } finally {
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
        }
    }


}
