package ro.myset.appcore.web.components.eval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.FacesException;
import javax.faces.component.ContextCallback;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ListenerFor;
import javax.faces.event.PostAddToViewEvent;
import java.io.IOException;
import java.util.Map;

/**
 * Eval component class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

@FacesComponent(value = "ro.myset.appcore.web.components.EvalComponent")
@ListenerFor(systemEventClass = PostAddToViewEvent.class)

public class EvalComponent extends UIComponentBase {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(EvalComponent.class);

    public static final String COMPONENT_FAMILY = "ro.myset.appcore.web.components";
    public static final String RENDERER_TYPE = "ro.myset.appcore.web.components.eval.EvalRenderer";
    public static final boolean RENDERS_CHILDREN = false;

    public EvalComponent() {
        super();
        //LOG.debug(this);
        setRendererType(RENDERER_TYPE);
    }

    @Override
    public final String getFamily() {
        return COMPONENT_FAMILY;
    }

    @Override
    public void processEvent(ComponentSystemEvent event) throws AbortProcessingException {
        //LOG.debug("%s,%s", this, event);
        if (isRendered()) {
            // PostAddToViewEvent
            if (event instanceof PostAddToViewEvent) {
                // evaluate value expression
                this.getAttributes().get("value");
            }
        }
        super.processEvent(event);
    }

    @Override
    public final void encodeBegin(FacesContext context) throws IOException {
        //LOG.debug(this);
        if (isRendered()) {
            //this.getAttributes().get("value");
            super.encodeBegin(context);
        }
    }

    @Override
    public final void encodeEnd(FacesContext context) throws IOException {
        //LOG.debug(this);
        if (isRendered()) {
            super.encodeEnd(context);
        }
    }

    @Override
    public final boolean getRendersChildren() {
        return RENDERS_CHILDREN;
    }

    @Override
    public final void encodeChildren(FacesContext context) throws IOException {
        //LOG.debug(this);
        if (isRendered()) {
            super.encodeChildren(context);
        }
    }

}
