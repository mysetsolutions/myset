/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;

import javax.el.ValueExpression;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ELUtils class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ELUtils")
@ApplicationScoped
@Scope("singleton")
public class ELUtils {
    private final static String elRegex = ".*#\\{(.*)\\}.*";
    private final static String elStrictRegex = "#\\{(\\S+)\\}";
    private final static Class[] ca = new Class[0];
    private final static Object[] oa = new Object[0];


    public final static String emptyString(String value) {
        if (Constants.EMPTY.equals(value))
            return null;
        return value;
    }


    public static boolean isEL(String exp) {
        try {
            if (exp == null)
                return false;
            Matcher m = Pattern.compile(ELUtils.elRegex).matcher(exp);
            if (m.matches()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static Object evalExp(String exp) {
        try {
            if (exp == null)
                return exp;
            Matcher m = Pattern.compile(ELUtils.elRegex).matcher(exp);
            if (m.matches()) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                return facesContext.getApplication().evaluateExpressionGet(facesContext, exp, Object.class);
            }
            return exp;
        } catch (Exception e) {
            return exp;
        }
    }

    public static String evalExpToString(String exp) {
        Object o = ELUtils.evalExp(exp);
        if (o != null)
            return o.toString();
        return null;
    }

    public static boolean evalExpToBoolean(String exp) {
        Object o = ELUtils.evalExp(exp);
        if (o != null)
            return Boolean.valueOf(o.toString()).booleanValue();
        else
            throw new NullPointerException(exp + " value is null.");
    }

    public static int evalExpToInt(String exp) {
        Object o = ELUtils.evalExp(exp);
        if (o != null)
            return Integer.parseInt(o.toString());
        else
            throw new NullPointerException(exp + " value is null.");
    }

    public static long evalExpToLong(String exp) {
        Object o = ELUtils.evalExp(exp);
        if (o != null)
            return Long.parseLong(o.toString());
        else
            throw new NullPointerException(exp + " value is null.");
    }

    public static double evalExpToDouble(String exp) {
        Object o = ELUtils.evalExp(exp);
        if (o != null)
            return Double.parseDouble(o.toString());
        else
            throw new NullPointerException(exp + " value is null.");
    }

    public static BigDecimal evalExpToBigDecimal(String exp) {
        Object o = ELUtils.evalExp(exp);
        if (o != null)
            return new BigDecimal(o.toString());
        return null;
    }

    public static Object readFieldValue(Object o, String fieldName) throws Exception {
        if (o == null)
            return null;
        String readMethodName = new PropertyDescriptor(fieldName, o.getClass()).getReadMethod().getName();
        return o.getClass().getMethod(readMethodName, ELUtils.ca).invoke(o, ELUtils.oa);
    }

    public static void setProperty(UIComponent component, String propName, Object value) {
        if (component == null || propName == null)
            throw new NullPointerException();
        if (value == null)
            return;
        Matcher m = Pattern.compile(ELUtils.elRegex).matcher(value.toString());
        if (m.matches()) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ValueExpression ve = facesContext.getApplication().getExpressionFactory()
                    .createValueExpression(facesContext.getELContext(), value.toString(), value.getClass());
            component.setValueExpression(propName, ve);
        } else
            component.getAttributes().put(propName, value);

    }

    public static void setPropertyValueOnly(UIComponent component, String propName, String value) {
        if (component == null || propName == null || value == null)
            throw new NullPointerException();
        if (value == null)
            return;
        component.getAttributes().put(propName, value);
    }

    public static Object getProperty(UIComponent component, String propName) {
        if (component == null || propName == null)
            throw new NullPointerException();
        return component.getAttributes().get(propName);
    }

    public static String getPropertyToString(UIComponent component, String propName) {
        Object o = ELUtils.getProperty(component, propName);
        if (o != null)
            return o.toString();
        return null;
    }

    public static boolean getPropertyToBoolean(UIComponent component, String propName) {
        Object o = ELUtils.getProperty(component, propName);
        if (o != null)
            return Boolean.valueOf(o.toString()).booleanValue();
        else
            throw new NullPointerException(propName + " value is null.");
    }

    public static int getPropertyToInt(UIComponent component, String propName) {
        Object o = ELUtils.getProperty(component, propName);
        if (o != null)
            return Integer.parseInt(o.toString());
        else
            throw new NullPointerException(propName + " value is null.");
    }

    public static long getPropertyToLong(UIComponent component, String propName) {
        Object o = ELUtils.getProperty(component, propName);
        if (o != null)
            return Long.parseLong(o.toString());
        else
            throw new NullPointerException(propName + " value is null.");
    }

    public static double getPropertyToDouble(UIComponent component, String propName) {
        Object o = ELUtils.getProperty(component, propName);
        if (o != null)
            return Double.parseDouble(o.toString());
        else
            throw new NullPointerException(propName + " value is null.");
    }

    public static BigDecimal getPropertyToBigDecimal(UIComponent component, String propName) {
        Object o = ELUtils.getProperty(component, propName);
        if (o != null)
            return new BigDecimal(o.toString());
        return null;
    }


}
