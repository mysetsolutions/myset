/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import ro.myset.appcore.common.Constants;

import java.io.File;

/**
 * Configuration parser Database class for database.xml
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class Logger implements Configuration {
    private static final long serialVersionUID = 1L;
    private static final org.apache.logging.log4j.Logger LOG = LogManager.getFormatterLogger(Logger.class);

    private File configurationFile = null;

    public Logger() throws Exception {
        LOG.debug(this);
    }


    /**
     * Refresh configuration
     */
    public final void refresh() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.info(configurationFile.getAbsolutePath());
        Configurator.initialize(null, configurationFile.getAbsolutePath());
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Gets configuration file
     */
    public File getConfigurationFile() {
        return configurationFile;
    }

    /**
     * Sets configuration file
     */
    public void setConfigurationFile(File configurationFile) {
        this.configurationFile = configurationFile;
    }

}
