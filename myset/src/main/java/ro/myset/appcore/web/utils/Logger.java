/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.springframework.context.annotation.Scope;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * Logger ManagedBean class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_Logger")
@ApplicationScoped
@Scope("singleton")
public class Logger {
    private static final org.apache.logging.log4j.Logger LOG = LogManager.getFormatterLogger(Logger.class);

    public Logger() {
        LOG.debug(this);
    }

    /**
     * Gets current logger
     */
    public final org.apache.logging.log4j.Logger getLogger() {
        return LOG;
    }

}
