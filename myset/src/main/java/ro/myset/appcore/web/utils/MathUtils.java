/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Math utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_MathUtils")
@ApplicationScoped
@Scope("singleton")

public class MathUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(MathUtils.class);

    public MathUtils() {
        LOG.debug(this);
    }

    /**
     * Gets rounding modes array
     */
    public final RoundingMode[] roundingModes() {
        return RoundingMode.values();
    }

    /**
     * Gets rounding mode
     *
     * @param roundingModeName Rounding mode name
     */
    public final RoundingMode roundingMode(String roundingModeName) {
        return RoundingMode.valueOf(roundingModeName);
    }


    /**
     * Gets math context
     *
     * @param roundingModeName Rounding mode name
     */
    public final MathContext mathContext(int precision, String roundingModeName) {
        return new MathContext(precision, RoundingMode.valueOf(roundingModeName));
    }


    /**
     * Gets a BigDecimal number using String
     *
     * @param source Source number
     */
    public final BigDecimal number(String source) {
        if (source == null)
            return null;
        return new BigDecimal(source);
    }


    /**
     * Gets a BigDecimal number using String
     *
     * @param source           Source number
     * @param scale            Decimal scale using a positive integer
     * @param roundingModeName Rounding mode name
     */
    public final BigDecimal number(String source, int scale, String roundingModeName) {
        if (source == null)
            return null;
        return new BigDecimal(source).setScale(scale, RoundingMode.valueOf(roundingModeName));
    }


}
