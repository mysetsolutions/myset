/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.generic.GenericBean;

import java.util.ArrayList;
import java.util.List;

/**
 * FiltrableGenericBeanList class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class FilterableGenericBeanList {
    private static final Logger LOG = LogManager.getFormatterLogger(FilterableGenericBeanList.class);

    private List<GenericBean> source = null;
    private String fieldName = null;

    public FilterableGenericBeanList() {
        //LOG.debug(this);
    }

    public final List<GenericBean> filter(String filterText) {
        List<GenericBean> sourceFiltered = new ArrayList<GenericBean>();
        GenericBean currentGenericBean = null;
        String value = null;
        for (int i = 0; i < source.size(); i++) {
            currentGenericBean = source.get(i);
            try {
                if (fieldName != null)
                    value = currentGenericBean.getFields().get(fieldName).toString();
                else
                    value = currentGenericBean.getValue().toString();

                // check value
                //LOG.debug(value);
                if (value != null && value.toLowerCase().contains(filterText.toLowerCase()))
                    sourceFiltered.add(currentGenericBean);

            } catch (Exception e) {
                // nothing to do
            }
        }
        return sourceFiltered;
    }

    public final List<GenericBean> getSource() {
        return source;
    }

    public final void setSource(List<GenericBean> source) {
        this.source = source;
    }

    public final String getFieldName() {
        return fieldName;
    }

    public final void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
