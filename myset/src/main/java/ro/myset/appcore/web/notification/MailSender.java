/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.notification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.grid.services.MailSenderService;
import ro.myset.appcore.web.utils.FacesUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

/**
 * MailProcessor class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_MailSender")
@ApplicationScoped
@Scope("singleton")

public class MailSender {
    private static final Logger LOG = LogManager.getFormatterLogger(MailSender.class);

    private MailSenderService mailSenderService = null;

    @Autowired
    private FacesUtils appcore_FacesUtils;

    public MailSender() {
        LOG.debug(this);
    }

    public final void send(MailSenderService mailSenderService,
                           String messageType,
                           String messageEncoding,
                           String from,
                           String to,
                           String cc,
                           String bcc,
                           String attachments,
                           String subject,
                           String content,
                           String infoMessage,
                           String warnMessage,
                           String errorMessage) {
        try {
            LOG.info("%s,%s,%s,%s,%s,%s,%s,%s,%s", mailSenderService.getMailPoolServiceName(), messageType, messageEncoding, from, to, cc, bcc, attachments, subject);
            if (mailSenderService != null) {
                mailSenderService.send(messageType, messageEncoding, from, to, cc, bcc, attachments, subject, content);
                if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
                if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
            }

        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void sendAsync(MailSenderService mailSenderService,
                                String messageType,
                                String messageEncoding,
                                String from,
                                String to,
                                String cc,
                                String bcc,
                                String attachments,
                                String subject,
                                String content,
                                String infoMessage,
                                String warnMessage,
                                String errorMessage) {
        try {
            LOG.info("%s,%s,%s,%s,%s,%s,%s,%s,%s", mailSenderService.getMailPoolServiceName(), messageType, messageEncoding, from, to, cc, bcc, attachments, subject);
            if (mailSenderService != null) {
                mailSenderService.sendAsync(messageType, messageEncoding, from, to, cc, bcc, attachments, subject, content);
                if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
                if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
            }

        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }


    public final void sendBatch(MailSenderService mailSenderService,
                                String messageType,
                                String messageEncoding,
                                String from,
                                String bcc,
                                String attachments,
                                String subject,
                                String content,
                                String infoMessage,
                                String warnMessage,
                                String errorMessage) {
        try {
            LOG.info("%s,%s,%s,%s,%s,%s,%s", mailSenderService.getMailPoolServiceName(), messageType, messageEncoding, from, bcc, attachments, subject);
            if (mailSenderService != null) {
                mailSenderService.sendBatch(messageType, messageEncoding, from, bcc, attachments, subject, content);
                if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
                if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
            }

        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

    public final void sendBatchAsync(MailSenderService mailSenderService,
                                     String messageType,
                                     String messageEncoding,
                                     String from,
                                     String bcc,
                                     String attachments,
                                     String subject,
                                     String content,
                                     String infoMessage,
                                     String warnMessage,
                                     String errorMessage) {
        try {
            LOG.info("%s,%s,%s,%s,%s,%s,%s", mailSenderService.getMailPoolServiceName(), messageType, messageEncoding, from, bcc, attachments, subject);
            if (mailSenderService != null) {
                mailSenderService.sendBatchAsync(messageType, messageEncoding, from, bcc, attachments, subject, content);
                if (infoMessage != null && !Constants.EMPTY.equals(infoMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_INFO, infoMessage);
                if (warnMessage != null && !Constants.EMPTY.equals(warnMessage))
                    appcore_FacesUtils.message(FacesMessage.SEVERITY_WARN, warnMessage);
            }

        } catch (Exception e) {
            if (errorMessage != null && !Constants.EMPTY.equals(errorMessage))
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, errorMessage);
            else
                appcore_FacesUtils.message(FacesMessage.SEVERITY_ERROR, e.toString());
        }
    }

}
