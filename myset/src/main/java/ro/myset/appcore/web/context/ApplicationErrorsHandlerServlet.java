/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ApplicationServletErrorHandler class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationErrorsHandlerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationErrorsHandlerServlet.class);

    private static String HTTP_HEADER_REFERER = "Referer";
    private static String REGEXP_ERRORS = ".*/errors/(.*)";
    private static String ERROR_PAGE_DEFAULT = "exception.xhtml";

    @Override
    protected final void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException, IOException {
        this.forward(httpRequest, httpResponse);
    }

    @Override
    protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    private final void forward(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException, IOException {
        LOG.debug(Constants.EVENT_INVOKED);
        ApplicationContext springContext = RequestContextUtils.findWebApplicationContext(httpRequest);
        Application appcore_Application = springContext.getBean("appcore_Application", Application.class);
        String requestSource = (String) httpRequest.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
        if (requestSource == null)
            requestSource = httpRequest.getHeader(HTTP_HEADER_REFERER);
        if (requestSource == null)
            requestSource = httpRequest.getRequestURI();
        String errorURI = httpRequest.getRequestURI();
        LOG.debug("requestSource: %s, errorURI: %s", requestSource, errorURI);
        StringBuffer forwardTo = null;
        List<ApplicationFilterError> errorFilters = appcore_Application.getErrorFilters();
        if (errorFilters != null && requestSource != null) {
            for (int i = 0; i < errorFilters.size(); i++) {
                ApplicationFilterError errorFilter = errorFilters.get(i);
                Matcher m = Pattern.compile(errorFilter.getUrlPattern()).matcher(requestSource);
                if (m.matches()) {
                    LOG.debug("%s MATCH %s", requestSource, errorFilter.getUrlPattern());
                    forwardTo = new StringBuffer();
                    forwardTo.append(errorFilter.getForwardErrorsPath());
                    break;
                }
            }
        }
        if (forwardTo != null) {
            forwardTo.append(Constants.WEB_SEPARATOR);
            Matcher m = Pattern.compile(REGEXP_ERRORS).matcher(errorURI);
            if (m.matches() && m.groupCount() >= 1) {
                forwardTo = forwardTo.append(m.group(1));
            } else {
                forwardTo = forwardTo.append(ERROR_PAGE_DEFAULT);
            }
            LOG.error("%s to %s", requestSource, forwardTo.toString());
            if (httpRequest != null && !httpResponse.isCommitted())
                httpRequest.getRequestDispatcher(forwardTo.toString()).forward(httpRequest, httpResponse);
        } else {
            throw new ServletException(requestSource);
        }


        LOG.debug(Constants.EVENT_COMPLETED);
    }


}
