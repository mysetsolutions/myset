/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.web.Request;
import ro.myset.appcore.web.Session;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.appcore.web.database.DatabaseProcessor;
import ro.myset.appcore.web.security.ApplicationSecurity;
import ro.myset.appcore.web.utils.Utils;

import javax.management.AttributeList;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ApplicationFilter class.
 */
public class ApplicationFilter implements Filter {
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationFilter.class);
    // constants
    private static int HTTP_200 = 200;
    private static int HTTP_301 = 301;
    private static int HTTP_401 = 401;
    private static String HTTP_401_MESSAGE = "Not authenticated";
    private static int HTTP_403 = 403;
    private static String HTTP_403_MESSAGE = "Not authorized";
    private final String LOGS_SQL = "logs.sql";
    private final String HTTP_DATE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss z";
    private final String HTTP_DATE_TIMEZONE = "GMT";
    private final String HTTP_HEADER_CACHE_CONTROL = "Cache-Control";
    private final String HTTP_HEADER_CACHE_CONTROL_MAXAGE = "public, must-revalidate, max-age=";
    private final String HTTP_HEADER_CACHE_CONTROL_NOCACHE = "no-cache, no-store, must-revalidate, max-age=-1";
    private final String HTTP_HEADER_EXPIRES = "Expires";
    private final String HTTP_HEADER_EXPIRES_NOCACHE = "-1";
    // filter condition types
    private final String CONDITION_TYPE_METHOD = "request-method";
    private final String CONDITION_TYPE_ATTRIBUTE = "request-attribute";
    private final String CONDITION_TYPE_PARAMETER = "request-parameter";
    private final String CONDITION_TYPE_HEADER = "request-header";
    private final String CONDITION_TYPE_APPCORE_REQUEST_ATTRIBUTE = "appcore-request-attribute";
    private final String CONDITION_TYPE_APPCORE_SESSION_ATTRIBUTE = "appcore-session-attribute";
    private ServletContext servletContext = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        servletContext = filterConfig.getServletContext();
        LOG.info(Constants.EVENT_COMPLETED);
    }

    @Override
    public void destroy() {
        LOG.info(Constants.EVENT_COMPLETED);
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession httpSession = null;

        try {
            // spring
            ApplicationContext springContext = RequestContextUtils.findWebApplicationContext(httpRequest);
            Application appcore_Application = springContext.getBean("appcore_Application", Application.class);
            Request appcore_Request = springContext.getBean("appcore_Request", Request.class);
            DatabaseProcessor appcore_DatabaseProcessor = springContext.getBean("appcore_DatabaseProcessor", DatabaseProcessor.class);
            Utils appcore_Utils = springContext.getBean("appcore_Utils", Utils.class);

            // session
            Session appcore_Session = null;
            ApplicationSecurity appcore_ApplicationSecurity = null;

            // sourceURL
            String sourceURL = this.decode(httpRequest);

            // only on valid sourceURL
            if (sourceURL != null && !httpResponse.isCommitted()) {
                //create session
                httpSession = httpRequest.getSession(false);
                if (httpSession != null) {
                    appcore_Session = springContext.getBean("appcore_Session", Session.class);
                    appcore_ApplicationSecurity = springContext.getBean("appcore_ApplicationSecurity", ApplicationSecurity.class);
                }

                // logFilters
                this.logFilter(httpRequest,
                        httpResponse,
                        httpSession,
                        springContext,
                        appcore_Application,
                        appcore_Request,
                        appcore_DatabaseProcessor,
                        appcore_Utils,
                        appcore_Session,
                        appcore_ApplicationSecurity,
                        sourceURL);

                // securityFilters
                this.securityFilter(httpRequest,
                        httpResponse,
                        httpSession,
                        springContext,
                        appcore_Application,
                        appcore_Request,
                        appcore_DatabaseProcessor,
                        appcore_Utils,
                        appcore_Session,
                        appcore_ApplicationSecurity,
                        sourceURL);

                // cacheFilters
                this.cacheFilter(httpRequest,
                        httpResponse,
                        httpSession,
                        springContext,
                        appcore_Application,
                        appcore_Request,
                        appcore_DatabaseProcessor,
                        appcore_Utils,
                        appcore_Session,
                        appcore_ApplicationSecurity,
                        sourceURL);

                // rewriteFilters
                this.rewriteFilter(httpRequest,
                        httpResponse,
                        httpSession,
                        springContext,
                        appcore_Application,
                        appcore_Request,
                        appcore_DatabaseProcessor,
                        appcore_Utils,
                        appcore_Session,
                        appcore_ApplicationSecurity,
                        sourceURL);

            }

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            // chain filters
            if (request != null && !httpResponse.isCommitted())
                chain.doFilter(request, response);
        }

    }

    /*
     * Check log filters
     */

    private final void logFilter(HttpServletRequest httpRequest,
                                              HttpServletResponse httpResponse,
                                              HttpSession httpSession,
                                              ApplicationContext springContext,
                                              Application appcore_Application,
                                              Request appcore_Request,
                                              DatabaseProcessor appcore_DatabaseProcessor,
                                              Utils appcore_Utils,
                                              Session appcore_Session,
                                              ApplicationSecurity appcore_ApplicationSecurity,
                                              String sourceURL) {
        //LOG.debug(Constants.EVENT_INVOKED);
        List<ApplicationFilterLog> logFilters = appcore_Application.getLogFilters();
        if (logFilters == null)
            return;
        for (int i = 0; i < logFilters.size(); i++) {
            ApplicationFilterLog logFilter = logFilters.get(i);
            if (logFilter.getUrlPattern() != null) {
                Matcher m = Pattern.compile(logFilter.getUrlPattern()).matcher(sourceURL);
                if (m.matches()) {
                    LOG.debug("%s MATCH %s", sourceURL, logFilter.getUrlPattern());
                    String logUser = null;
                    if (appcore_ApplicationSecurity != null
                            && appcore_ApplicationSecurity.getSecurity() != null
                            && appcore_ApplicationSecurity.getSecurity().isAuthenticated())
                        logUser = appcore_ApplicationSecurity.getSecurity().getUser().getUserName();

                    // log to console
                    LOG.log(logFilter.getLoggerLevel(), "%s %s, Session:%s, User:%s, Method:%s",
                            sourceURL,
                            logFilter.getMessage(),
                            httpRequest.getRequestedSessionId(),
                            logUser,
                            httpRequest.getMethod());

                    // log to database
                    if (Boolean.parseBoolean(appcore_Application.property("log.enable")) && logFilter.getDatabaseProcessorService() != null) {
                        DatabaseCommand databaseCommand = springContext.getBean("appcore_DatabaseCommand", DatabaseCommand.class);
                        ArrayList logSqlParams = new AttributeList();
                        logSqlParams.add(appcore_Utils.truncate(logUser, 500));
                        logSqlParams.add(appcore_Utils.truncate(httpRequest.getRemoteAddr(), 500));
                        logSqlParams.add(appcore_Utils.truncate(sourceURL, 500));
                        logSqlParams.add(appcore_Utils.requestHeaders());
                        logSqlParams.add(logFilter.getLevel());
                        logSqlParams.add(logFilter.getMessage());
                        databaseCommand.setSqlCommand(appcore_Utils.format(appcore_Application.database(LOGS_SQL), logFilter.getName()));
                        databaseCommand.setLog(LOGS_SQL);
                        databaseCommand.setParameters(logSqlParams);
                        appcore_DatabaseProcessor.executeCommandAsync(logFilter.getDatabaseProcessorService(),
                                databaseCommand,
                                null, null, null);
                    }
                }
            }
        }
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    /*
     * Check security filters
     */
    private final void securityFilter(HttpServletRequest httpRequest,
                                                   HttpServletResponse httpResponse,
                                                   HttpSession httpSession,
                                                   ApplicationContext springContext,
                                                   Application appcore_Application,
                                                   Request appcore_Request,
                                                   DatabaseProcessor appcore_DatabaseProcessor,
                                                   Utils appcore_Utils,
                                                   Session appcore_Session,
                                                   ApplicationSecurity appcore_ApplicationSecurity,
                                                   String sourceURL) throws IOException {
        //LOG.debug(Constants.EVENT_INVOKED);
        List<ApplicationFilterSecurity> securityFilters = appcore_Application.getSecurityFilters();
        if (securityFilters == null)
            return;
        for (int i = 0; i < securityFilters.size(); i++) {
            ApplicationFilterSecurity securityFilter = securityFilters.get(i);
            if (securityFilter.getUrlPattern() != null) {
                Matcher m = Pattern.compile(securityFilter.getUrlPattern()).matcher(sourceURL);
                if (m.matches()) {
                    LOG.debug("%s MATCH %s", sourceURL, securityFilter.getUrlPattern());

                    // authentication and authorization check
                    if (appcore_ApplicationSecurity == null || appcore_ApplicationSecurity.getSecurity() == null) {
                        LOG.error("%s, %s %s", sourceURL, HTTP_401, HTTP_401_MESSAGE);
                        if (!httpResponse.isCommitted())
                            httpResponse.sendError(HTTP_401, HTTP_401_MESSAGE);
                    } else if (!appcore_ApplicationSecurity.getSecurity().isAuthenticated()) {
                        LOG.error("%s, %s %s", sourceURL, HTTP_401, HTTP_401_MESSAGE);
                        if (!httpResponse.isCommitted())
                            httpResponse.sendError(HTTP_401, HTTP_401_MESSAGE);
                    } else if (securityFilter.getRoles() != null
                            && !appcore_ApplicationSecurity.getSecurity().ifAnyRoles(securityFilter.getRoles())) {
                        LOG.error("%s, %s %s, User:%s, Roles:%s", sourceURL, HTTP_403, HTTP_403_MESSAGE, appcore_ApplicationSecurity.getSecurity().getUser().getUserName(), securityFilter.getRoles());
                        if (!httpResponse.isCommitted())
                            httpResponse.sendError(HTTP_403, HTTP_403_MESSAGE);
                    }
                }
            }
        }
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    /*
     * Check cache filters
     */
    private final void cacheFilter(HttpServletRequest httpRequest,
                                                HttpServletResponse httpResponse,
                                                HttpSession httpSession,
                                                ApplicationContext springContext,
                                                Application appcore_Application,
                                                Request appcore_Request,
                                                DatabaseProcessor appcore_DatabaseProcessor,
                                                Utils appcore_Utils,
                                                Session appcore_Session,
                                                ApplicationSecurity appcore_ApplicationSecurity,
                                                String sourceURL) {
        //LOG.debug(Constants.EVENT_INVOKED);
        List<ApplicationFilterCache> cacheFilters = appcore_Application.getCacheFilters();
        if (cacheFilters == null)
            return;
        for (int i = 0; i < cacheFilters.size(); i++) {
            ApplicationFilterCache cacheFilter = cacheFilters.get(i);
            if (cacheFilter.getUrlPattern() != null) {
                Matcher m = Pattern.compile(cacheFilter.getUrlPattern()).matcher(sourceURL);
                if (m.matches()) {
                    LOG.debug("%s MATCH %s", sourceURL, cacheFilter.getUrlPattern());
                    if (cacheFilter.getCacheMaxAgeMillis() == -1) {
                        LOG.debug("%s , max-age:%s", sourceURL, HTTP_HEADER_EXPIRES_NOCACHE);
                        httpResponse.setHeader(HTTP_HEADER_CACHE_CONTROL, HTTP_HEADER_CACHE_CONTROL_NOCACHE);
                        httpResponse.setHeader(HTTP_HEADER_EXPIRES, HTTP_HEADER_EXPIRES_NOCACHE);
                    }
                    if (cacheFilter.getCacheMaxAgeMillis() >= 0) {
                        Date expireDate = new Date(System.currentTimeMillis() + cacheFilter.getCacheMaxAgeMillis());
                        LOG.debug("%s , max-age:%s", sourceURL, cacheFilter.getCacheMaxAge());
                        httpResponse.setHeader(HTTP_HEADER_CACHE_CONTROL, HTTP_HEADER_CACHE_CONTROL_MAXAGE + cacheFilter.getCacheMaxAge());
                        httpResponse.setHeader(HTTP_HEADER_EXPIRES, getHttpDate(expireDate));
                    }
                    //exit
                    return;
                }
            }
        }
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    /*
     * Check rewrite filters
     */
    private final void rewriteFilter(HttpServletRequest httpRequest,
                                                  HttpServletResponse httpResponse,
                                                  HttpSession httpSession,
                                                  ApplicationContext springContext,
                                                  Application appcore_Application,
                                                  Request appcore_Request,
                                                  DatabaseProcessor appcore_DatabaseProcessor,
                                                  Utils appcore_Utils,
                                                  Session appcore_Session,
                                                  ApplicationSecurity appcore_ApplicationSecurity,
                                                  String sourceURL) throws IOException, ServletException {
        //LOG.debug(Constants.EVENT_INVOKED);
        List<ApplicationFilterRewrite> rewriteFilters = appcore_Application.getRewriteFilters();
        if (rewriteFilters == null)
            return;
        for (int i = 0; i < rewriteFilters.size(); i++) {
            ApplicationFilterRewrite rewriteFilter = rewriteFilters.get(i);
            if (rewriteFilter.getUrlPattern() != null) {
                Matcher m = Pattern.compile(rewriteFilter.getUrlPattern()).matcher(sourceURL);
                if (m.matches()) {
                    String destination = null;
                    if (rewriteFilter.getRewriteWith() != null)
                        destination = m.replaceAll(rewriteFilter.getRewriteWith());
                    LOG.debug("%s MATCH %s - destination:%s",
                            sourceURL,
                            rewriteFilter.getUrlPattern(),
                            destination);
                    // check rewrite conditions
                    boolean matchConditions = true;
                    if (destination != null && rewriteFilter.getRewriteConditions() != null) {
                        ApplicationFilterRewriteCondition rewriteCondition = null;
                        Matcher mc = null;
                        String value = null;
                        for (int j = 0; j < rewriteFilter.getRewriteConditions().size(); j++) {
                            rewriteCondition = rewriteFilter.getRewriteConditions().get(j);
                            value = null;
                            if (rewriteCondition.getConditionType() != null && rewriteCondition.getValuePattern() != null) {
                                if (CONDITION_TYPE_METHOD.equalsIgnoreCase(rewriteCondition.getConditionType()))
                                    value = httpRequest.getMethod();
                                else if (rewriteCondition.getConditionFor() != null) {
                                    if (CONDITION_TYPE_ATTRIBUTE.equalsIgnoreCase(rewriteCondition.getConditionType()))
                                        value = httpRequest.getAttribute(rewriteCondition.getConditionFor()).toString();
                                    else if (CONDITION_TYPE_PARAMETER.equalsIgnoreCase(rewriteCondition.getConditionType()))
                                        value = httpRequest.getParameter(rewriteCondition.getConditionFor());
                                    else if (CONDITION_TYPE_HEADER.equalsIgnoreCase(rewriteCondition.getConditionType()))
                                        value = httpRequest.getHeader(rewriteCondition.getConditionFor());
                                    else if (CONDITION_TYPE_APPCORE_REQUEST_ATTRIBUTE.equalsIgnoreCase(rewriteCondition.getConditionType()))
                                        value = appcore_Request.getAttributes().getString(rewriteCondition.getConditionFor());
                                    else if (CONDITION_TYPE_APPCORE_SESSION_ATTRIBUTE.equalsIgnoreCase(rewriteCondition.getConditionType())
                                            && appcore_Session != null)
                                        value = appcore_Session.getAttributes().getString(rewriteCondition.getConditionFor());
                                }
                                if (value != null) {
                                    mc = Pattern.compile(rewriteFilter.getUrlPattern()).matcher(sourceURL);
                                    if (m.matches()) {
                                        LOG.debug("%s MATCH %s - conditionType: %s, conditionFor: %s, valuePattern: %s,   ",
                                                sourceURL,
                                                rewriteFilter.getUrlPattern(),
                                                rewriteCondition.getConditionType(),
                                                rewriteCondition.getConditionFor(),
                                                rewriteCondition.getValuePattern());
                                    } else {
                                        matchConditions = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    // redirect or forward
                    if (!httpResponse.isCommitted() && matchConditions && destination != null) {
                        if (rewriteFilter.isRedirect()) {
                            httpResponse.setStatus(HTTP_301);
                            httpResponse.sendRedirect(destination);
                        } else
                            httpRequest.getRequestDispatcher(destination).forward(httpRequest, httpResponse);
                    }
                    //exit
                    return;
                }
            }
        }
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    private final String getHttpDate(Date source) {
        DateFormat df = new SimpleDateFormat(HTTP_DATE_PATTERN);
        df.setTimeZone(TimeZone.getTimeZone(HTTP_DATE_TIMEZONE));
        return df.format(source);
    }

    private final String decode(HttpServletRequest httpRequest) throws UnsupportedEncodingException {
        if (httpRequest == null)
            return null;
        if (httpRequest.getCharacterEncoding() != null)
            return URLDecoder.decode(httpRequest.getRequestURL().toString(), httpRequest.getCharacterEncoding());
        else
            return URLDecoder.decode(httpRequest.getRequestURL().toString(), System.getProperty("file.encoding"));

    }
}