/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.util.SerializableSupplier;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * PrimeFacesUtils class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.1
 */
@Named("appcore_PrimeFacesUtils")
@ApplicationScoped
@Scope("singleton")
public class PrimeFacesUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(PrimeFacesUtils.class);


    /**
     * Gets PrimeFaces current instance
     */
    public final PrimeFaces getCurrent(){
        return PrimeFaces.current();
    }


    /**
     * Execute client side code
     */
    public static final void execute(String code) {
        if (code != null)
            PrimeFaces.current().executeScript(code);
    }


    /**
     * Primefaces dialog framework
     * modal - 0 Boolean Controls modality of the dialog.
     * closable true Boolean Whether the dialog can be closed or not.
     * resizable 1 Boolean When enabled, makes dialog resizable.
     * draggable 1 Boolean When enabled, makes dialog draggable.
     * width auto Integer Width of the dialog.
     * height auto Integer Height of the dialog.
     * contentWidth 640 Integer Width of the dialog content.
     * contentHeight auto Integer Height of the dialog content.
     * includeViewParams false Boolean When enabled, includes the view parameters.
     */
    private final Map<String, Object> parseOptions(String optionsString) {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", false);
        //options.put("position","center top");
        options.put("includeViewParams", false);
        if (optionsString != null) {
            String[] optionsArray = optionsString.split(Constants.SEMICOLONS);
            for (int i = 0; i < optionsArray.length; i++) {
                String[] optionArray = optionsArray[i].split(Constants.COLON);
                if (optionArray != null && optionArray.length == 2)
                    options.put(optionArray[0], optionArray[1]);
            }
        }
        LOG.debug(options);
        return options;
    }

    /**
     * Open dialog using primefaces dialog framework
     */
    public final void openDialog(String location, String optionsString) {
        PrimeFaces.current().dialog().openDynamic(location, this.parseOptions(optionsString), null);
    }

    /**
     * Open dialog using primefaces dialog framework
     */
    public final void openDialog(String location) {
        PrimeFaces.current().dialog().openDynamic(location, this.parseOptions(null), null);
    }

    /**
     * Close dialog using primefaces dialog framework
     */
    public final void closeDialog(Object object) {
        PrimeFaces.current().dialog().closeDynamic(object);
    }


    /**
     * Gets dual list model
     */
    public static final DualListModel<GenericBean> getDualListModel(ArrayList<GenericBean> source, ArrayList<GenericBean> target) {
        return new DualListModel(source, target);
    }

    /**
     * Download file.
     *
     * @param file Source file.
     * @throws Exception If the execution fail.
     */
    public final StreamedContent download(File file) throws Exception {
        if (file != null && file.isFile()) {
            InputStream fis = new FileInputStream(file);
            //return new DefaultStreamedContent.(fis, URLConnection.guessContentTypeFromStream(fis), file.getName());
            return DefaultStreamedContent.builder().contentType(URLConnection.guessContentTypeFromStream(fis)).name(file.getName()).stream(() -> {return fis;}).build();
        }
        return null;
    }


}
