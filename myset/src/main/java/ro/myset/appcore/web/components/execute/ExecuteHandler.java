package ro.myset.appcore.web.components.execute;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.view.facelets.ComponentConfig;
import javax.faces.view.facelets.ComponentHandler;

/**
 * Execute handler class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class ExecuteHandler extends ComponentHandler {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ExecuteHandler.class);

    public ExecuteHandler(ComponentConfig config) {
        super(config);
        LOG.debug(this);
    }


}

