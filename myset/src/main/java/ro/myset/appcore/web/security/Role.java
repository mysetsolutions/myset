/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import java.io.Serializable;

/**
 * Role object class. This class contains details of one individual role.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class Role implements Serializable {
    private String roleId = null;
    private String roleName = null;
    private String roleDescription = null;
    private String roleProtected = null;

    public final String getRoleId() {
        return roleId;
    }

    public final void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public final String getRoleName() {
        return roleName;
    }

    public final void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public final String getRoleDescription() {
        return roleDescription;
    }

    public final void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public final String getRoleProtected() {
        return roleProtected;
    }

    public final void setRoleProtected(String roleProtected) {
        this.roleProtected = roleProtected;
    }
}
