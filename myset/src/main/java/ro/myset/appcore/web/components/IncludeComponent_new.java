/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.components;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Include component class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@FacesComponent(value = "components.include", namespace = "http://www.myset.ro/components", tagName = "include_new", createTag = true)
public class IncludeComponent_new extends UIComponentBase {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(IncludeComponent_new.class);
    private static final String COMPONENT_FAMILY = "ro.myset.components";

    public IncludeComponent_new() {
        //LOG.debug(this);
    }

    @Override
    public String getFamily() {
        return COMPONENT_FAMILY;
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        LOG.debug(this);
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            if (isRendered()) {
                ExternalContext externalContext = context.getExternalContext();
                HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
                HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

                RequestDispatcher requestDispatcher = request.getRequestDispatcher((String) getAttributes().get("src"));
                CharResponseWrapper charResponseWrapper = new CharResponseWrapper(response);
                requestDispatcher.include(request, charResponseWrapper);
                context.getResponseWriter().write(charResponseWrapper.toString());
            }
            LOG.debug(Constants.EVENT_COMPLETED);

        } catch (ServletException e) {
            throw new IOException();
        }
    }

    @Override
    public List<UIComponent> getChildren() {
        LOG.debug(this);
        if (isRendered())
            return super.getChildren();
        return new ArrayList<UIComponent>();
    }

    @Override
    public void encodeChildren(FacesContext context) throws IOException {
        //LOG.debug(this);
    }

    @Override
    public void encodeEnd(FacesContext context) throws IOException {
        //LOG.debug(this);
    }

}


class CharResponseWrapper extends HttpServletResponseWrapper {
    private static final Logger LOG = LogManager.getFormatterLogger(CharResponseWrapper.class);
    private HttpServletResponse response;
    private CharArrayWriter output;

    @Override
    public String toString() {
        return output.toString();
    }

    public CharResponseWrapper(HttpServletResponse response) {
        super(response);
        response = response;
        output = new CharArrayWriter();
    }

    public CharArrayWriter getCharWriter() {
        return output;
    }

    @Override
    public PrintWriter getWriter() {
        return new PrintWriter(output);
    }

    @Override
    public ServletOutputStream getOutputStream() {
        try {
            return new CharOutputStream(response, output);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(Constants.EMPTY, e);
            return null;
        }
    }

    public InputStream getInputStream() {
        return new ByteArrayInputStream(toString().getBytes());
    }
}

class CharOutputStream extends ServletOutputStream {
    private ServletOutputStream servletOutputStream;
    private Writer output;

    public CharOutputStream(HttpServletResponse response, Writer writer) throws Exception {
        this.servletOutputStream = response.getOutputStream();
        this.output = writer;
    }

    @Override
    public void write(int b) throws IOException {
        output.write(b);
    }

    @Override
    public boolean isReady() {
        return servletOutputStream.isReady();
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {
        servletOutputStream.setWriteListener(writeListener);
    }


}
