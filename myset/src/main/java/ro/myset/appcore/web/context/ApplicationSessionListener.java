/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * ApplicationSessionListener class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationSessionListener implements HttpSessionListener {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationSessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        LOG.debug(Constants.EVENT_INVOKED);
        ApplicationContext springContext = (ApplicationContext) event.getSession().getServletContext().getAttribute(Constants.APP_CONTEXT);
        Application appcore_Application = springContext.getBean("appcore_Application", Application.class);
        if (appcore_Application.property("application.session.timeout") != null)
            event.getSession().setMaxInactiveInterval(Integer.parseInt(appcore_Application.property("application.session.timeout")));
        LOG.info("Session:%s, MaxInactiveInterval:%s", event.getSession().getId(), event.getSession().getMaxInactiveInterval());
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.info("Session:%s", event.getSession().getId());
        LOG.debug(Constants.EVENT_COMPLETED);
    }
}
