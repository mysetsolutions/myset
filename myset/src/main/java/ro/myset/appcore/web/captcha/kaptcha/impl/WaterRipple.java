/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.captcha.kaptcha.impl;

import ro.myset.appcore.web.captcha.jhlabs.image.RippleFilter;
import ro.myset.appcore.web.captcha.jhlabs.image.TransformFilter;
import ro.myset.appcore.web.captcha.jhlabs.image.WaterFilter;
import ro.myset.appcore.web.captcha.kaptcha.GimpyEngine;
import ro.myset.appcore.web.captcha.kaptcha.NoiseProducer;
import ro.myset.appcore.web.captcha.kaptcha.util.Configurable;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * {@link WaterRipple} adds water ripple effect to an image.
 */
public class WaterRipple extends Configurable implements GimpyEngine {
    /**
     * Applies distortion by adding water ripple effect.
     *
     * @param baseImage the base image
     * @return the distorted image
     */
    public BufferedImage getDistortedImage(BufferedImage baseImage) {
        NoiseProducer noiseProducer = getConfig().getNoiseImpl();
        BufferedImage distortedImage = new BufferedImage(baseImage.getWidth(),
                baseImage.getHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D graphics = (Graphics2D) distortedImage.getGraphics();

        RippleFilter rippleFilter = new RippleFilter();
        rippleFilter.setWaveType(RippleFilter.SINE);
        rippleFilter.setXAmplitude(2.6f);
        rippleFilter.setYAmplitude(1.7f);
        rippleFilter.setXWavelength(15);
        rippleFilter.setYWavelength(5);
        rippleFilter.setEdgeAction(TransformFilter.NEAREST_NEIGHBOUR);

        WaterFilter waterFilter = new WaterFilter();
        waterFilter.setAmplitude(1.5f);
        waterFilter.setPhase(10);
        waterFilter.setWavelength(2);

        BufferedImage effectImage = waterFilter.filter(baseImage, null);
        effectImage = rippleFilter.filter(effectImage, null);

        graphics.drawImage(effectImage, 0, 0, null, null);

        graphics.dispose();

        noiseProducer.makeNoise(distortedImage, .1f, .1f, .25f, .25f);
        noiseProducer.makeNoise(distortedImage, .1f, .25f, .5f, .9f);
        return distortedImage;
    }
}
