package ro.myset.appcore.web.components.eval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * Eval renderer class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

@FacesRenderer(componentFamily = EvalComponent.COMPONENT_FAMILY, rendererType = EvalComponent.RENDERER_TYPE)

public class EvalRenderer extends Renderer {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(EvalRenderer.class);

    public EvalRenderer() {
        super();
        //LOG.debug(this);
    }


    @Override
    public void decode(FacesContext context, UIComponent component) {
        //LOG.debug(this);
        if (component.isRendered()) {
            super.decode(context, component);
        }
    }

    @Override
    public final void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        //LOG.debug(this);
        if (component.isRendered()) {
            super.encodeBegin(context, component);
        }
    }

    @Override
    public final void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        //LOG.debug(this);
        if (component.isRendered()) {
            super.encodeEnd(context, component);
        }
    }


    @Override
    public final boolean getRendersChildren() {
        //LOG.debug(this);
        return EvalComponent.RENDERS_CHILDREN;
    }

    @Override
    public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
        //LOG.debug(this);
        if (component.isRendered())
            super.encodeChildren(context, component);
    }


}
