/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import java.io.Serializable;

/**
 * ApplicationFilterRewriteCondition object class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class ApplicationFilterRewriteCondition implements Serializable {
    private static final long serialVersionUID = 1L;

    private String conditionType = null;

    private String conditionFor = null;

    private String valuePattern = null;

    public final String getConditionType() {
        return conditionType;
    }

    public final void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public final String getConditionFor() {
        return conditionFor;
    }

    public final void setConditionFor(String conditionFor) {
        this.conditionFor = conditionFor;
    }

    public final String getValuePattern() {
        return valuePattern;
    }

    public final void setValuePattern(String valuePattern) {
        this.valuePattern = valuePattern;
    }
}
