/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import java.io.Serializable;

/**
 * PhaseTracker utilities class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class PhaseTracker implements PhaseListener, Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(PhaseTracker.class);

    private PhaseId myphase;
    private PhaseId phaseId;

    public void setPhase(PhaseId phase) {
        LOG.debug(this);
        myphase = phase;
    }

    public PhaseId getPhaseId() {
        if (myphase == null)
            phaseId = PhaseId.ANY_PHASE;
        else
            phaseId = myphase;
        return phaseId;
    }

    public void beforePhase(PhaseEvent e) {
        this.phaseId = e.getPhaseId();
        LOG.debug(phaseId);
    }

    public void afterPhase(PhaseEvent e) {
        this.phaseId = e.getPhaseId();
        LOG.debug(phaseId);
    }
}
