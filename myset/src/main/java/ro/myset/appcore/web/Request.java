/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.utils.FilterableGenericBeanList;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;

/**
 * Global request application bean.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_Request")
@RequestScoped
@Scope("request")
public class Request implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;

    // constants
    private static final Logger LOG = LogManager.getFormatterLogger(Request.class);

    // attributes
    private OutputStream responseOutputStream = null;

    private GenericMap<String, Object> attributes = null;

    private ActionEvent actionEvent = null;

    private AjaxBehaviorEvent event = null;

    public Request() throws Exception {
        //LOG.debug(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    private void initialize() throws Exception {
        //LOG.debug(Constants.EVENT_INVOKED);

        // attributes
        this.attributes = new GenericMap<String, Object>();

        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Refresh method
     */
    public final void refresh() throws Exception {
        //LOG.debug(Constants.EVENT_INVOKED);
        //LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Gets responseOutputStream
     */
    public final OutputStream getResponseOutputStream() {
        return responseOutputStream;
    }

    /**
     * Gets attributes
     */
    public GenericMap<String, Object> getAttributes() {
        return attributes;
    }

    /**
     * Generic action event listener
     */
    public final void actionEventListener(ActionEvent actionEvent) {
        this.actionEvent = actionEvent;
    }

    /**
     * Gets current action event
     */
    public final ActionEvent getActionEvent() {
        return actionEvent;
    }

    /**
     * Action event check
     */
    public final boolean actionEventCheck() {
        return (actionEvent != null);
    }

    /**
     * Check if action event was raised by componentId
     *
     * @param componentId Id of component who raise the event.
     */
    public final boolean actionEventCheck(String componentId) {
        if (actionEventCheck()) {
            if (actionEvent.getComponent().getId().equals(componentId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if action event was raised by componentId within formId
     *
     * @param formId Id of form of component who raise the event.
     */
    public final boolean actionEventCheckForm(String formId) {
        if (actionEventCheck()) {
            if (this.getComponentFormId(actionEvent.getComponent()).equals(formId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if action event was raised by componentId within formId
     *
     * @param formId      Id of form of component who raise the event.
     * @param componentId Id of component who raise the event.
     */
    public final boolean actionEventCheckForm(String formId, String componentId) {
        if (actionEventCheck()) {
            if (this.getComponentFormId(actionEvent.getComponent()).equals(formId)
                    && actionEvent.getComponent().getId().equals(componentId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Generic event listener
     */
    public final void eventListener(AjaxBehaviorEvent event) {
        this.event = event;
    }

    /**
     * Gets current event
     */
    public final AjaxBehaviorEvent getEvent() {
        return event;
    }

    /**
     * Event check
     */
    public final boolean eventCheck() {
        return (event != null);
    }

    /**
     * Check if event was raised by componentId
     *
     * @param componentId Id of component who raise the event.
     */
    public final boolean eventCheck(String componentId) {
        if (eventCheck()) {
            if (event.getComponent().getId().equals(componentId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if event was raised by componentId within formId
     *
     * @param formId Id of form of component who raise the event.
     */
    public final boolean eventCheckForm(String formId) {
        if (eventCheck()) {
            if (this.getComponentFormId(event.getComponent()).equals(formId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if event was raised by componentId within formId
     *
     * @param formId      Id of form of component who raise the event.
     * @param componentId Id of component who raise the event.
     */
    public final boolean eventCheckForm(String formId, String componentId) {
        if (eventCheck()) {
            if (this.getComponentFormId(event.getComponent()).equals(formId)
                    && event.getComponent().getId().equals(componentId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if event was raised by componentId and if it is instance of eventClassName
     *
     * @param componentId    Id of component who raise the event.
     * @param eventClassName Event class name.
     */
    public final boolean eventCheck(String componentId, String eventClassName) {
        if (eventCheck(componentId)) {
            if (event.getClass().getName().equals(eventClassName)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if event was raised by componentId within formId and if it is instance of eventClassName
     *
     * @param formId         Id of form of component who raise the event.
     * @param componentId    Id of component who raise the event.
     * @param eventClassName Event class name.
     */
    public final boolean eventCheckForm(String formId, String componentId, String eventClassName) {
        if (eventCheckForm(formId, componentId)) {
            if (event.getClass().getName().equals(eventClassName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get component formId
     */
    private final String getComponentFormId(UIComponent component) {
        String result = null;
        try {
            if (component != null) {
                if (component.getParent() instanceof UIForm)
                    result = component.getParent().getId();
                else
                    result = this.getComponentFormId(component.getParent());
            }
        } catch (Exception e) {
            LOG.debug(Constants.EMPTY, e);
        } finally {
            return result;
        }

    }


    /**
     * Filter method for autocomplete using GenericBeanValue
     *
     * @param source Source list.
     */
    public final FilterableGenericBeanList filter(List<GenericBean> source) {
        FilterableGenericBeanList fgbl = new FilterableGenericBeanList();
        fgbl.setSource(source);
        return fgbl;
    }

    /**
     * Filter method for autocomplete using GenericBean fieldName
     *
     * @param source    Source list.
     * @param fieldName Filter string.
     */
    public final FilterableGenericBeanList filterField(List<GenericBean> source, String fieldName) {
        FilterableGenericBeanList fgbl = new FilterableGenericBeanList();
        fgbl.setSource(source);
        fgbl.setFieldName(fieldName);
        return fgbl;
    }

}
