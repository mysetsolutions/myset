/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

/**
 * ApplicationFilterRegistration class.
 */
public class ApplicationFilterRegistration implements FilterRegistration.Dynamic {
    private static final Logger LOG = LogManager.getFormatterLogger(ApplicationFilterRegistration.class);

    private FilterRegistration.Dynamic filterRegistration = null;

    public ApplicationFilterRegistration(FilterRegistration.Dynamic filterRegistration) {
        LOG.debug(filterRegistration);
        this.filterRegistration = filterRegistration;
    }

    @Override
    public void addMappingForServletNames(EnumSet<DispatcherType> enumSet, boolean b, String... strings) {
        filterRegistration.addMappingForServletNames(enumSet, b, strings);
    }

    @Override
    public Collection<String> getServletNameMappings() {
        return filterRegistration.getServletNameMappings();
    }

    @Override
    public void addMappingForUrlPatterns(EnumSet<DispatcherType> enumSet, boolean b, String... strings) {
        filterRegistration.addMappingForUrlPatterns(enumSet, b, strings);
    }

    @Override
    public Collection<String> getUrlPatternMappings() {
        return filterRegistration.getUrlPatternMappings();
    }

    @Override
    public void setAsyncSupported(boolean b) {
        filterRegistration.setAsyncSupported(b);
    }

    @Override
    public String getName() {
        return filterRegistration.getName();
    }

    @Override
    public String getClassName() {
        return filterRegistration.getClassName();
    }

    @Override
    public boolean setInitParameter(String s, String s1) {
        return filterRegistration.setInitParameter(s, s1);
    }

    @Override
    public String getInitParameter(String s) {
        return filterRegistration.getInitParameter(s);
    }

    @Override
    public Set<String> setInitParameters(Map<String, String> map) {
        return filterRegistration.setInitParameters(map);
    }

    @Override
    public Map<String, String> getInitParameters() {
        return filterRegistration.getInitParameters();
    }
}
