/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.cache;

import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.generic.GenericBean;

import java.util.Map;

/**
 * GridCacheLoader class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class GridCacheLoader {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(GridCacheLoader.class);

    private String source = null;
    private Map<String, Object> content;
    private IgniteCache cache = null;
    private boolean loadToCache = false;

    public GridCacheLoader() throws Exception {
        LOG.debug(this);
    }

    /**
     * Load into cache
     */
    public final void load() throws Exception {
        if (content != null && cache != null && !cache.isClosed()) {
            LOG.info("%s - %s, contentSize=%s, cacheName=%s, cacheSize=%s", source, Constants.EVENT_INVOKED, content.size(), cache.getName(), cache.size());
            for (Map.Entry<String, Object> entry : content.entrySet()) {
                cache.put(entry.getKey(), entry.getValue());
            }
            LOG.info("%s - %s, contentSize=%s, cacheName=%s, cacheSize=%s", source, Constants.EVENT_COMPLETED, content.size(), cache.getName(), cache.size());
        } else
            LOG.error(Constants.EVENT_FAILED);
    }

    /**
     * Gets source
     */
    public final String getSource() {
        return source;
    }

    /**
     * Sets source
     */
    public final void setSource(String source) {
        this.source = source;
    }

    /**
     * Gets content
     */
    public final Map<String, Object> getContent() {
        return content;
    }

    /**
     * Sets content
     */
    public final void setContent(Map<String, Object> content) {
        this.content = content;
    }

    /**
     * Gets cache
     */
    public final IgniteCache getCache() {
        return cache;
    }

    /**
     * Sets cache
     */
    public final void setCache(IgniteCache cache) {
        this.cache = cache;
    }

    /**
     * Gets load to cache
     */
    public final boolean isLoadToCache() {
        return loadToCache;
    }

    /**
     * Sets load to cache
     */
    public final void setLoadToCache(boolean loadToCache) throws Exception {
        this.loadToCache = loadToCache;
        if (loadToCache)
            this.load();
    }

}
