package ro.myset.appcore.web.database;
/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.grid.services.DatabaseProcessorServiceCommandParameter;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 * DatabaseCommandParameter class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_DatabaseCommandParameter")
@Dependent
@Scope("prototype")

public class DatabaseCommandParameter extends DatabaseProcessorServiceCommandParameter {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabaseCommandParameter.class);

    public DatabaseCommandParameter() {
        LOG.debug(this);
    }

}
