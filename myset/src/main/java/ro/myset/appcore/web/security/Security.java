/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.web.utils.DatabaseUtils;
import ro.myset.utils.common.Constants;
import ro.myset.utils.generic.GenericBean;

import java.io.Serializable;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Security checker abstract class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */

public class Security implements Serializable {
    private static final Logger LOG = LogManager.getFormatterLogger(Security.class);
    protected boolean authenticated = false;
    protected User user = null;
    protected Role[] userRoles = null;

    /**
     * Login action
     *
     * @param userGenericBean       User generic bean
     * @param userRolesGenericBeans User roles generic beans
     */
    public final void login(GenericBean userGenericBean, ArrayList<GenericBean> userRolesGenericBeans) {
        user = null;
        userRoles = null;
        authenticated = false;
        if (userGenericBean != null && userGenericBean.getFields().size() > 0) {
            // user attributes
            user = new User();
            Map<String, Object> userMap = userGenericBean.getFields();
            LOG.debug(userMap);
            user.setUserId(String.valueOf(userMap.get("userId")));
            user.setUserName((String) userMap.get("userName"));
            user.setUserFirstName((String) userMap.get("userFirstName"));
            user.setUserGivenName((String) userMap.get("userGivenName"));
            user.setUserLastName((String) userMap.get("userLastName"));
            user.setUserBirthDate((Date) userMap.get("userBirthDate"));
            user.setUserAddress((String) userMap.get("userAddress"));
            user.setUserEmail((String) userMap.get("userEmail"));
            user.setUserOrganization((String) userMap.get("userOrganization"));
            user.setUserOrganizationUnit((String) userMap.get("userOrganizationUnit"));
            user.setUserDescription((String) userMap.get("userDescription"));
            user.setUserPhone((String) userMap.get("userPhone"));
            user.setUserMobile((String) userMap.get("userMobile"));
            user.setUserWorkPhone((String) userMap.get("userWorkPhone"));
            if (userMap.get("userKey") instanceof Clob)
                user.setUserKey(DatabaseUtils.clobReader((Clob) userMap.get("userKey")));
            else
                user.setUserKey((String) userMap.get("userKey"));
            user.setUserProtected((String) userMap.get("userProtected"));
            user.setUserCreateDate((Date) userMap.get("userCreateDate"));
            user.setUserLastUpdate((Date) userMap.get("userLastUpdate"));
            user.setUserLastLogon((Date) userMap.get("userLastLogon"));

            // user roles
            StringBuffer roles = new StringBuffer();
            if (userRolesGenericBeans != null && userRolesGenericBeans.size() > 0) {
                userRoles = new Role[userRolesGenericBeans.size()];

                for (int i = 0; i < userRolesGenericBeans.size(); i++) {
                    Map<String, String> userRoleMap = userRolesGenericBeans.get(i).getFields();
                    LOG.debug(userRoleMap);
                    Role role = new Role();
                    role.setRoleId(String.valueOf(userRoleMap.get("roleId")));
                    role.setRoleName(userRoleMap.get("roleName"));
                    role.setRoleDescription(userRoleMap.get("roleDescription"));
                    role.setRoleProtected(userRoleMap.get("roleProtected"));
                    roles.append(role.getRoleName());
                    if (i < userRolesGenericBeans.size() - 1)
                        roles.append(Constants.COMMA);
                    userRoles[i] = role;
                }
            }
            LOG.debug("User=%s,Roles=%s", user.getUserName(), roles);
            this.authenticated = true;
        }
    }

    /*
     * Logout action
     */
    public final void logout() {
        user = null;
        userRoles = null;
        authenticated = false;
    }

    /*
     * Check if user is authenticated
     */
    public final boolean isAuthenticated() {
        return authenticated;
    }

    /**
     * Gets user object. User object contains all user attributes
     */
    public final User getUser() {
        return this.user;
    }

    /**
     * Gets user roles.
     */
    public final Role[] getUserRoles() {
        return userRoles;
    }


    /**
     * Check if user has no roles.
     */
    public final boolean ifNoneRoles() {
        if (this.getUserRoles() == null)
            return true;
        else {
            if (userRoles.length == 0)
                return true;
        }
        return false;
    }

    /**
     * Check if any user lowercase roles names match roles regexp.
     *
     * @param roles Roles regular expression.
     */
    public final boolean ifAnyRoles(String roles) {
        //LOG.debug(roles);
        if (roles != null && !this.ifNoneRoles()) {
            Matcher m = null;
            for (int i = 0; i < userRoles.length; i++) {
                m = Pattern.compile(roles).matcher(userRoles[i].getRoleName().toLowerCase());
                if (m.matches())
                    return true;
            }
        }
        return false;
    }

}
