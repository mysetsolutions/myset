/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.apache.ignite.IgniteCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.generic.GenericBean;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_Utils")
@ApplicationScoped
@Scope("singleton")

public class Utils {
    private static final Logger LOG = LogManager.getFormatterLogger(Utils.class);
    private ELUtils eLUtils = null;

    @Autowired
    private HttpServletRequest request;

    public Utils() {
        LOG.debug(this);
        this.eLUtils = new ELUtils();
    }

    /**
     * Gets object identity hash code
     */
    public static final String objectId(Object object) {
        if (object != null)
            return object.getClass().getCanonicalName() + "@" + Integer.toHexString(System.identityHashCode(object));
        return null;
    }

    /**
     * Gets CR
     */
    public static final String CR() {
        return "\r";
    }

    /**
     * Gets LF
     */
    public static final String LF() {
        return "\n";
    }

    /**
     * Gets CRLF
     */
    public static final String CRLF() {
        return "\r\n";
    }

    /**
     * Create JavaScript array from java ArrayList
     */
    public static String createJavaScriptArray(ArrayList<Object> arrayList) {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i = 0; i < arrayList.size(); i++) {
            sb.append("\"").append(arrayList.get(i)).append("\"");
            if (i + 1 < arrayList.size()) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Print stack trace into String
     */
    public final String printStackTrace(Exception e) {
        StringWriter stackTrace = new StringWriter();
        e.printStackTrace(new PrintWriter(stackTrace));
        return stackTrace.toString();
    }

    /**
     * Get system properties
     */
    public final Properties getSystemProperties() {
        return System.getProperties();
    }

    /**
     * Get system environment variables map
     */
    public final Map<String, String> getSystemEnvs() {
        return System.getenv();
    }

    /**
     * Class for name.
     */
    public final Class<?> classForName(String fullyQualifiedClassName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        return Class.forName(fullyQualifiedClassName);
    }

    /**
     * Check if bean is instance of fullyQualifiedClassName
     */
    public final boolean isInstance(Object bean, String fullyQualifiedClassName) {
        try {
            if (bean != null && fullyQualifiedClassName != null)
                return Class.forName(fullyQualifiedClassName).isInstance(bean);
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check fullyQualifiedClassName exists in classloader
     */
    public final boolean checkClass(String fullyQualifiedClassName) {
        try {
            if (fullyQualifiedClassName != null)
                return Class.forName(fullyQualifiedClassName)!=null;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Creates new instance.
     */
    public final Object newInstance(String className) throws Exception {
        LOG.debug(className);
        Object newInstance = Class.forName(className).newInstance();
        return newInstance;
    }

    /**
     * Creates new instance using constructor with params
     */
    public final Object newInstance(String className, ArrayList params) throws Exception {
        LOG.debug("%s,%s", className, params);
        Class[] paramsTypes = null;
        Object[] paramsValues = null;
        if (params != null && params.size() > 0) {
            paramsTypes = new Class[params.size()];
            paramsValues = new Object[params.size()];
            for (int i = 0; i < params.size(); i++) {
                paramsTypes[i] = params.get(i).getClass();
                paramsValues[i] = params.get(i);
            }
            return Class.forName(className).getConstructor(paramsTypes).newInstance(paramsValues);
        }
        return Class.forName(className).newInstance();
    }

    /**
     * Check regexp pattern
     *
     * @param value   String value
     * @param pattern Regexp pattern
     */
    public final boolean checkRegexp(String value, String pattern) {
        try {
            if (value == null || pattern == null) {
                LOG.debug("%s,%s", value, pattern);
                return false;
            }
            Matcher m = Pattern.compile(pattern).matcher(value);
            LOG.debug("%s,%s,%s", value, pattern, m.matches());
            if (m.matches()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Get regexp matcher.
     *
     * @param value   String value
     * @param pattern Regexp pattern
     */
    public final Matcher regexpMatcher(String value, String pattern) {
        LOG.debug("%s,%s", value, pattern);
        return Pattern.compile(pattern).matcher(value);
    }

    /**
     * MessageFormat source using object array params.
     */
    public final String formatObj(String source, Object[] params) {
        try {
            MessageFormat mf = new MessageFormat(source);
            return mf.format(params);
        } catch (Exception e) {
            return source;
        }
    }

    /**
     * MessageFormat source using a string with comma separated params values.
     */
    public final String format(String source, String params) {
        try {
            MessageFormat mf = new MessageFormat(source);
            return mf.format(params.split(Constants.COMMA));
        } catch (Exception e) {
            return source;
        }
    }


    /**
     * Gets String
     */
    public final String getString(String value) {
        return String.valueOf(value);
    }

    /**
     * Gets Integer
     */
    public final Integer getInt(String value) {
        return Integer.parseInt(value);
    }

    /**
     * Gets Double
     */
    public final Double getDouble(String value) {
        return Double.parseDouble(value);
    }

    /**
     * Gets Long
     */
    public final Long getLong(String value) {
        return Long.parseLong(value);
    }

    /**
     * Gets BigDecimal
     */
    public final BigDecimal getBigDecimal(String value) {
        return new BigDecimal(value);
    }

    /**
     * Gets java.sql.Date
     */
    public final java.sql.Date getSqlDate(Date value) {
        return new java.sql.Date(value.getTime());
    }

    /**
     * Gets java.sql.Timestamp
     */
    public final java.sql.Timestamp getSqlTimestamp(Date value) {
        return new java.sql.Timestamp(value.getTime());
    }

    /**
     * Strip tags
     */
    public final String stripTags(String source) {
        if (source != null)
            return source.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
        return source;
    }

    /**
     * Truncate string if exceed length
     */
    public final String truncate(String source, int maxLength) {
        if (source != null)
            return source.length() > maxLength ? source.substring(0, maxLength) : source;
        return source;
    }

    /**
     * Create single line text from array of Object values
     */
    public final String createText(Object[] values, String separator, String prefix, String suffix) {
        String result = null;
        if (values != null) {
            StringBuffer resultBuff = new StringBuffer();
            for (int i = 0; i < values.length; i++) {
                if (i > 0) {
                    if (prefix != null)
                        resultBuff.append(prefix);
                    resultBuff.append(separator);
                    if (suffix != null)
                        resultBuff.append(suffix);
                }
                resultBuff.append(values[i]);
            }
            result = resultBuff.toString();
        }
        return result;
    }

    /**
     * Create Object array using value of fieldName from a GenericBean array
     */
    public final Object[] createArray(GenericBean[] values, String fieldName) {
        Object[] result = null;
        if (values != null && fieldName != null) {
            result = new Object[values.length];
            for (int i = 0; i < values.length; i++) {
                result[i] = values[i].getFields().get(fieldName);
            }
        }
        return result;
    }

    /**
     * Create ArrayList from a Object array
     */
    public final ArrayList<Object> createArrayList(Object[] values) {
        ArrayList<Object> result = null;
        if (values != null) {
            result = new ArrayList<Object>(Arrays.asList(values));
        }
        return result;
    }

    /**
     * Gets request headers
     */
    public final String requestHeaders() {
        StringBuffer sb = new StringBuffer();
        if (request != null) {
            for (Enumeration e = request.getHeaderNames(); e.hasMoreElements(); ) {
                String key = (String) e.nextElement();
                sb.append(key);
                sb.append(Constants.COLON);
                sb.append(request.getHeader(key));
                sb.append(Constants.CRLF);
            }
        }
        return sb.toString();
    }

    /**
     * Find GenericBean into source by fieldName and fieldValue
     */
    public final GenericBean find(ArrayList<GenericBean> source, String fieldName, Object fieldValue) {
        GenericBean result = null;
        try {
            if (source != null && fieldName != null && fieldValue != null) {
                GenericBean bean = null;
                for (int i = 0; i < source.size(); i++) {
                    bean = source.get(i);
                    //LOG.debug("%s[%s][%s],%s[%s][%s] - %s", Utils.objectId(bean.getFields().get(fieldName)), bean.getFields().get(fieldName), String.valueOf(bean.getFields().get(fieldName)), Utils.objectId(fieldValue), fieldValue, String.valueOf(fieldValue), String.valueOf(fieldValue).equals(String.valueOf(bean.getFields().get(fieldName))));
                    if (bean != null
                            && bean instanceof GenericBean
                            && bean.getFields().get(fieldName) != null
                            && String.valueOf(fieldValue).equals(String.valueOf(bean.getFields().get(fieldName)))) {
                        result = bean;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return result;
        }
    }


    /**
     * Filter source by fieldName and regexp fieldValue
     */
    public final ArrayList<GenericBean> filter(ArrayList<GenericBean> source, String fieldName, String fieldValueRegexp) {
        ArrayList<GenericBean> results = null;
        try {
            if (source != null && fieldName != null && fieldValueRegexp != null) {
                GenericBean bean = null;
                for (int i = 0; i < source.size(); i++) {
                    bean = source.get(i);
                    if (bean != null
                            && bean instanceof GenericBean
                            && bean.getFields().get(fieldName) != null
                            && this.checkRegexp(String.valueOf(bean.getFields().get(fieldName)),fieldValueRegexp)) {
                        if (results == null)
                            results = new ArrayList<GenericBean>();
                        results.add(bean);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return results;
        }
    }


    /**
     * Put each GenericBean into cache using source by keyFieldName formatted with keyMessageFormat
     */
    public final void updateCache(IgniteCache cache, ArrayList<GenericBean> source, String keyFieldName, String keyMessageFormat) {
        LOG.debug(Constants.EVENT_INVOKED);
        if (source != null && keyFieldName != null && keyMessageFormat != null) {
            GenericBean genericBean = null;
            String key = null;
            Object[] params = new Object[1];
            for (int i = 0; i < source.size(); i++) {
                genericBean = source.get(i);
                if (genericBean instanceof GenericBean && genericBean.getFields().get(keyFieldName) != null) {
                    params[0] = genericBean.getFields().get(keyFieldName).toString();
                    key = formatObj(keyMessageFormat, params);
                    LOG.debug("%s,%s", key, genericBean);
                    cache.put(key, genericBean);
                }
            }
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }


    /**
     * Sleep and return object
     */
    public final Object sleep(long sleep, Object obj) {
        try {
            Thread.sleep(sleep);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            return obj;
        }
    }


}
