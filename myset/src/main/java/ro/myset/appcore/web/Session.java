/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.common.CommonUtils;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Global session application bean.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_Session")
@SessionScoped
@Scope("session")
public class Session implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;

    // constants
    private static final Logger LOG = LogManager.getFormatterLogger(Session.class);

    @Autowired
    private Application appcore_Application;

    // attributes
    private GenericMap<String, Object> attributes = null;

    // preferences
    private ArrayList<GenericBean> themes = null;
    private String theme = null;
    private ArrayList<GenericBean> locales = null;
    private String locale = null;
    private Locale localeObj = null;
    private ArrayList<GenericBean> timezones = null;
    private String timezone = null;
    private TimeZone timezoneObj = null;
    private DecimalFormatSymbols decimalFormatSymbols = null;
    private DateFormatSymbols dateFormatSymbols = null;


    private HttpSession httpSession = null;

    public Session() throws Exception {
        LOG.debug(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    private void initialize() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        // attributes
        this.attributes = new GenericMap<String, Object>();

        // session default preferences
        theme = appcore_Application.property("preference.theme");
        locale = appcore_Application.property("preference.locale");
        timezone = appcore_Application.property("preference.timezone");

        this.refresh();
        LOG.debug(Constants.EVENT_COMPLETED);
    }


    public final void refresh() throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);

        // httpSession
        httpSession = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getSession(true);
        LOG.info("Session:%s, Theme:%s, Locale:%s, Timezone:%s", httpSession.getId(), theme, locale, timezone);

        // theme
        themes = appcore_Application.propertyValues("preference.themes");

        // locale
        locales = appcore_Application.dictionaryValues(locale + ".locales");
        if (locale != null) {
            String[] localeArray = locale.split("_");
            localeObj = new Locale(localeArray[0], localeArray[1]);
        }

        // servlet session locale
        if (httpSession != null)
            httpSession.setAttribute("locale", localeObj);

        // faces locale
        if (FacesContext.getCurrentInstance().getViewRoot() != null)
            FacesContext.getCurrentInstance().getViewRoot().setLocale(localeObj);

        // timezone
        timezones = appcore_Application.dictionaryValues(locale + ".timezones");
        timezoneObj = TimeZone.getTimeZone(timezone);

        // format symbols
        decimalFormatSymbols = new DecimalFormatSymbols(localeObj);
        dateFormatSymbols = new DateFormatSymbols(localeObj);

        LOG.debug(Constants.EVENT_COMPLETED);
    }

    //================================
    // Common
    //================================
    public final String dictionary(String id) {
        StringBuffer sb = new StringBuffer();
        sb.append(locale);
        sb.append(ro.myset.utils.common.Constants.DOT);
        sb.append(id);
        return appcore_Application.dictionary(sb.toString());
    }

    public final String dictionary(String id, ArrayList<Object> objects) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append(locale);
        sb.append(ro.myset.utils.common.Constants.DOT);
        sb.append(id);
        return CommonUtils.format(appcore_Application.dictionary(sb.toString()), objects.toArray());
    }


    public final GenericBean dictionaryValue(String id) {
        StringBuffer sb = new StringBuffer();
        sb.append(locale);
        sb.append(ro.myset.utils.common.Constants.DOT);
        sb.append(id);
        return appcore_Application.dictionaryValue(sb.toString());
    }

    public final ArrayList<GenericBean> dictionaryValues(String id) {
        StringBuffer sb = new StringBuffer();
        sb.append(locale);
        sb.append(ro.myset.utils.common.Constants.DOT);
        sb.append(id);
        return appcore_Application.dictionaryValues(sb.toString());
    }

    public final void themeSwitcherListener(AjaxBehaviorEvent event) {
        LOG.debug(event.getSource());
    }


    //================================
    // Fields
    //================================

    public HttpSession getHttpSession() {
        return httpSession;
    }

    public GenericMap<String, Object> getAttributes() {
        return attributes;
    }

    public ArrayList<GenericBean> getLocales() {
        return locales;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) throws Exception {
        this.locale = locale;
        this.refresh();
    }

    public Locale getLocaleObj() {
        return localeObj;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public ArrayList<GenericBean> getTimezones() {
        return timezones;
    }

    public TimeZone getTimezoneObj() {
        return timezoneObj;
    }

    public DecimalFormatSymbols getDecimalFormatSymbols() {
        return decimalFormatSymbols;
    }

    public DateFormatSymbols getDateFormatSymbols() {
        return dateFormatSymbols;
    }

    public ArrayList<GenericBean> getThemes() {
        return themes;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

}
