/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.context;

/**
 * Test
 */
public class ApplicationELResolver extends org.springframework.web.jsf.el.SpringBeanFacesELResolver {

    // org.springframework.web.jsf.el.SpringBeanFacesELResolver


//    @Override
//    public Object convertToType(ELContext context, Object base, Class<?> targetType) {
//        //System.out.println("[convertToType] - " + context + "," + base + "," + targetType);
//        if (base == null && String.class.equals(targetType)) {
//            context.setPropertyResolved(true);
//        }
//        return base;
//    }

}
