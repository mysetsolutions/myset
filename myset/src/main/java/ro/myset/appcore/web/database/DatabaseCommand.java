/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.grid.services.DatabaseProcessorServiceCommand;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 * DatabaseCommand class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_DatabaseCommand")
@Dependent
@Scope("prototype")

public class DatabaseCommand extends DatabaseProcessorServiceCommand {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabaseCommand.class);

    public DatabaseCommand() {
        LOG.debug(this);
    }
}
