/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.extensions;
/**
 * Extension configuration class
 *
 * <b><u>You must follow the following rules:</u></b>
 * <li/><b>RULE 1</b> - Extension files should have different names than deployment files.
 * <li/><b>RULE 2</b> - Extension install doesn't overwrite deployment files.
 * <li/><b>RULE 3</b> - Extension uninstall clean all deployment empty folders.
 * <li/><b>RULE 4</b> - Extension install or uninstall should be followed by application restart.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;
import ro.myset.appcore.grid.ant.AntRunner;

import java.io.File;
import java.io.Serializable;
import java.util.Properties;


public final class Extension implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(Extension.class);

    // constants
    private static final String EXTENSIONS_MANAGER = "myset.extensions.manager";
    private static final String EXTENSIONS_AUTO = "myset.extensions.auto";
    private static final String EXTENSION_ID = "myset.extension.id";
    private static final String EXTENSION_MANAGER = "myset.extension.manager";
    private static final String EXTENSION_AUTO = "myset.extension.auto";

    // fields
    private Properties extensionProperties = null;
    private AntRunner extensionAntRunner = null;


    public Extension() {
        LOG.debug(this);
    }

    /**
     * Initialize extension.
     */
    public final void initialize(Application application) {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            // properties
            Properties allProperties = new Properties();
            allProperties.putAll(application.getExtensionsManagerProperties());
            allProperties.putAll(extensionProperties);

            // ant runner
            extensionAntRunner = new AntRunner();
            extensionAntRunner.setProperties(allProperties);
            File extensionManagerFile = new File(application.getExtensionsManagerProperties().getProperty(EXTENSIONS_MANAGER));
            if (extensionProperties.getProperty(EXTENSION_MANAGER) != null) {
                extensionManagerFile = new File(extensionProperties.getProperty(EXTENSION_MANAGER));
            }
            extensionAntRunner.setFile(extensionManagerFile);
            extensionAntRunner.initialize();
            String autoTarget = allProperties.getProperty(EXTENSIONS_AUTO);
            if (extensionProperties.getProperty(EXTENSION_AUTO) != null)
                autoTarget = extensionProperties.getProperty(EXTENSION_AUTO);
            LOG.info("%s - [%s] %s", extensionProperties.getProperty(EXTENSION_ID), autoTarget, extensionManagerFile.getAbsolutePath());
            extensionAntRunner.runTarget(autoTarget);
            LOG.debug(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    /**
     * Gets extensionProperties.
     */
    public final Properties getExtensionProperties() {
        return extensionProperties;
    }

    /**
     * Sets extensionProperties.
     */
    public final void setExtensionProperties(Properties extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    /**
     * Gets extensionAntRunner.
     */
    public final AntRunner getExtensionAntRunner() {
        return extensionAntRunner;
    }

}
