/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.cache.CacheProvider;
import org.springframework.beans.factory.annotation.Autowired;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.Application;

import java.io.Serializable;

/**
 * Grid cache provider class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class GridCacheProvider implements CacheProvider, Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(GridCacheProvider.class);

    @Autowired
    private Application appcore_Application;

    public GridCacheProvider() {
        LOG.debug(this);
        LOG.debug(appcore_Application);
    }

    @Override
    public Object get(String cacheName, String key) {
        LOG.debug("%s,%s", cacheName, key);
        return appcore_Application.getGrid().cache(cacheName).get(key);
    }

    @Override
    public void put(String cacheName, String key, Object value) {
        LOG.debug("%s,%s", cacheName, key);
        appcore_Application.getGrid().cache(cacheName).put(key, value);
    }

    @Override
    public void remove(String cacheName, String key) {
        LOG.debug("%s,%s", cacheName, key);
        appcore_Application.getGrid().cache(cacheName).remove(key);
    }

    @Override
    public void clear() {
        LOG.debug(Constants.EMPTY);
    }
}
