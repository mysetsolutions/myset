/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.web.utils;

import org.springframework.context.annotation.Scope;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * ExceptionHandlerTest class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
@Named("appcore_ExceptionHandlerTest")
@ApplicationScoped
@Scope("singleton")

public class ExceptionHandlerTest {

    public void throwNullPointerException() {
        throw new NullPointerException("This is a NullPointerException");
    }

    public void throwWrappedIllegalStateException() {
        Throwable t = new IllegalStateException("This is a IllegalStateException");
        throw new FacesException(t);
    }

    public void throwViewExpiredException() {
        throw new ViewExpiredException("This is a ViewExpiredException",
                FacesContext.getCurrentInstance().getViewRoot().getViewId());
    }
}
