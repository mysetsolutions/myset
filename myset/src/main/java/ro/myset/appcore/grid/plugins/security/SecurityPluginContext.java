/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.plugins.security;

import org.apache.ignite.internal.processors.security.SecurityContext;
import org.apache.ignite.plugin.security.SecurityPermission;
import org.apache.ignite.plugin.security.SecuritySubject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;

/**
 * Created by myset on 07/05/15.
 */
public class SecurityPluginContext implements SecurityContext {
    private static final Logger LOG = LogManager.getFormatterLogger(SecurityPluginContext.class);

    public SecurityPluginContext() {
        LOG.debug(this);
    }

    @Override
    public SecuritySubject subject() {
        LOG.debug(Constants.EMPTY);
        return new SecurityPluginSubject();
    }

    @Override
    public boolean cacheOperationAllowed(String s, SecurityPermission gridSecurityPermission) {
        LOG.debug(Constants.EMPTY);
        return true;
    }

    @Override
    public boolean serviceOperationAllowed(String s, SecurityPermission securityPermission) {
        return false;
    }

    @Override
    public boolean taskOperationAllowed(String s, SecurityPermission gridSecurityPermission) {
        LOG.debug(Constants.EMPTY);
        return true;
    }

    @Override
    public boolean systemOperationAllowed(SecurityPermission gridSecurityPermission) {
        LOG.debug(Constants.EMPTY);
        return true;
    }
}
