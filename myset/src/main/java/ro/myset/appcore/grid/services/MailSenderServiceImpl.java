/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.mail.MailSender;
import ro.myset.utils.security.UUIDUtils;

import javax.mail.Session;
import java.util.Arrays;
import java.util.regex.Pattern;


/**
 * MailSenderServiceImpl class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class MailSenderServiceImpl extends MailSender implements Service, MailSenderService {
    private static final Logger LOG = LogManager.getFormatterLogger(MailSenderServiceImpl.class);

    private String mailPoolServiceName = null;
    private MailPoolService mailPoolService = null;
    private String EMAIL_REGEXP_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    @IgniteInstanceResource
    private Ignite ignite;

    public MailSenderServiceImpl() {
        LOG.debug(this);
    }

    @Override
    public void cancel(ServiceContext serviceContext) {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void init(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        if (mailPoolService == null)
            mailPoolService = ignite.services().serviceProxy(mailPoolServiceName, MailPoolService.class, false);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void execute(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public String getMailPoolServiceName() {
        return this.mailPoolServiceName;
    }

    @Override
    public void setMailPoolServiceName(String mailPoolServiceName) {
        this.mailPoolServiceName = mailPoolServiceName;
    }

    @Override
    public MailPoolService getMailPoolService() {
        return this.mailPoolService;
    }

    @Override
    public void send(String messageType, String messageEncoding, String from, String to, String cc, String bcc, String attachments, String subject, String content) throws Exception {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] ".concat(Constants.EVENT_INVOKED), uuid);
        Session session = null;
        try {
            session = (Session) mailPoolService.doBorrow();
            LOG.debug("%s,%s", mailPoolServiceName, session);
            this.send(session, messageType, messageEncoding, from, to, cc, bcc, attachments, subject, content);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw e;
        } finally {
            mailPoolService.close(session);
            mailPoolService.doReturn(session);
            LOG.debug("[%s] ".concat(Constants.EVENT_COMPLETED), uuid);
        }
    }

    @Override
    public void sendAsync(String messageType, String messageEncoding, String from, String to, String cc, String bcc, String attachments, String subject, String content) throws Exception {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    send(messageType, messageEncoding, from, to, cc, bcc, attachments, subject, content);
                } catch (Exception e) {
                    LOG.error(Constants.EMPTY, e);
                }
            }
        });
        t.setName(MailSenderServiceImpl.class.getName().concat("-sendAsync"));
        t.start();
    }

    @Override
    public void sendBatch(String messageType, String messageEncoding, String from, String bcc, String attachments, String subject, String content) throws Exception {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] ".concat(Constants.EVENT_INVOKED), uuid);
        Session session = null;
        try {
            session = (Session) mailPoolService.doBorrow();
            LOG.debug("%s,%s", mailPoolServiceName, session);
            if (bcc != null) {
                String[] bccRecipientsArray = bcc.split(Constants.COMMA);
                // remove duplicates
                String[] bccRecipientsArrayUniques = Arrays.stream(bccRecipientsArray).distinct().toArray(String[]::new);

                int count = 0;
                StringBuffer bccRecipients = new StringBuffer();
                for (int i = 0; i < bccRecipientsArrayUniques.length; i++) {
                    if (checkEmailAddress(bccRecipientsArrayUniques[i])) {
                        count++;
                        if (count > 1)
                            bccRecipients.append(Constants.COMMA);
                        bccRecipients.append(bccRecipientsArrayUniques[i]);
                    }
                    // Send email
                    if (i == bccRecipientsArrayUniques.length - 1 || count == mailPoolService.getMaxRecipients()) {
                        LOG.debug(bccRecipients.toString());
                        try {
                            if (bccRecipients.toString().length() > 0)
                                // Send recipients
                                this.send(session, messageType, messageEncoding, from, null, null, bccRecipients.toString(), attachments, subject, content);
                        } catch (Exception e1) {
                            //LOG.error("%s %s",bccRecipients.toString(), e1.getLocalizedMessage());
                            String[] bccRecipientsArraySingle = bccRecipients.toString().split(Constants.COMMA);
                            for (int j = 0; j < bccRecipientsArraySingle.length; j++) {
                                try {
                                    // Send single recipient in order to avoid issues
                                    this.send(session, messageType, messageEncoding, from, null, null, bccRecipientsArraySingle[j], attachments, subject, content);
                                } catch (Exception e2) {
                                    LOG.error("%s %s", bccRecipientsArraySingle[j], e2.getLocalizedMessage());
                                }
                            }

                        }
                        // reset
                        count = 0;
                        bccRecipients = new StringBuffer();
                    }
                }

            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw e;
        } finally {
            mailPoolService.close(session);
            mailPoolService.doReturn(session);
            LOG.debug("[%s] ".concat(Constants.EVENT_COMPLETED), uuid);
        }
    }

    @Override
    public void sendBatchAsync(String messageType, String messageEncoding, String from, String bcc, String attachments, String subject, String content) throws Exception {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sendBatch(messageType, messageEncoding, from, bcc, attachments, subject, content);
                } catch (Exception e) {
                    LOG.error(Constants.EMPTY, e);
                }
            }
        });
        t.setName(MailSenderServiceImpl.class.getName().concat("-sendBatchAsync"));
        t.start();
    }

    private final boolean checkEmailAddress(String emailAddress) {
        boolean result = false;
        try {
            if (emailAddress != null)
                result = Pattern.compile(EMAIL_REGEXP_PATTERN).matcher(emailAddress).matches();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        } finally {
            if (!result) {
                LOG.debug("Invalid email address: %s", emailAddress);
            }
            return result;
        }
    }


}
