/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * DatabaseProcessorServiceCommand class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class DatabaseProcessorServiceCommand implements Serializable {
    private static final long serialVersionUID = 1L;

    private String sqlCommand = null;
    private ArrayList<Object> parameters = null;
    private String log = null;
    private boolean errors = false;
    private boolean readClob = false;
    private int rowsLimit = 0;

    public DatabaseProcessorServiceCommand() {
    }

    public final String getSqlCommand() {
        return sqlCommand;
    }

    public final void setSqlCommand(String sqlCommand) {
        this.sqlCommand = sqlCommand;
    }

    public final ArrayList<Object> getParameters() {
        return parameters;
    }

    public final void setParameters(ArrayList<Object> parameters) {
        this.parameters = parameters;
    }

    public final String getLog() {
        return log;
    }

    public final void setLog(String log) {
        this.log = log;
    }

    public final boolean isErrors() {
        return errors;
    }

    public final void setErrors(boolean errors) {
        this.errors = errors;
    }

    public final boolean isReadClob() {
        return readClob;
    }

    public final void setReadClob(boolean readClob) {
        this.readClob = readClob;
    }

    public final int getRowsLimit() {
        return rowsLimit;
    }

    public final void setRowsLimit(int rowsLimit) {
        if (rowsLimit > 0)
            this.rowsLimit = rowsLimit;
    }
}
