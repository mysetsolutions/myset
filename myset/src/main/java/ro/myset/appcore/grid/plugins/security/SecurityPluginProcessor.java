/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.plugins.security;

import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteException;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.internal.IgniteInternalFuture;
import org.apache.ignite.internal.processors.security.GridSecurityProcessor;
import org.apache.ignite.internal.processors.security.SecurityContext;
import org.apache.ignite.lang.IgniteFuture;
import org.apache.ignite.plugin.IgnitePlugin;
import org.apache.ignite.plugin.security.*;
import org.apache.ignite.plugin.security.SecurityException;
import org.apache.ignite.spi.IgniteNodeValidationResult;
import org.apache.ignite.spi.discovery.DiscoveryDataBag;
import org.apache.ignite.spi.discovery.DiscoverySpiNodeAuthenticator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

/**
 * Created by myset on 06/05/15.
 */
public class SecurityPluginProcessor implements IgnitePlugin, DiscoverySpiNodeAuthenticator, GridSecurityProcessor {
    private static final Logger LOG = LogManager.getFormatterLogger(SecurityPluginProcessor.class);
    private ArrayList<String> whitelistKeys = null;
    private SecurityPluginContext securityPluginContext = null;

    public SecurityPluginProcessor() {
        LOG.debug(this);
        securityPluginContext = new SecurityPluginContext();
    }


    @Override
    public void start() throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void stop(boolean b) throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void onKernalStart(boolean b) throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void onKernalStop(boolean b) {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void collectJoiningNodeData(DiscoveryDataBag discoveryDataBag) {

    }

    @Override
    public void collectGridNodeData(DiscoveryDataBag discoveryDataBag) {

    }

    @Override
    public void onGridDataReceived(DiscoveryDataBag.GridDiscoveryData gridDiscoveryData) {

    }

    @Override
    public void onJoiningNodeDataReceived(DiscoveryDataBag.JoiningNodeDiscoveryData joiningNodeDiscoveryData) {

    }

    @Override
    public void printMemoryStats() {
        LOG.debug(Constants.EMPTY);
    }

    //----------------------------------------
    // Important: Validate new node
    //----------------------------------------
    @Override
    public IgniteNodeValidationResult validateNode(ClusterNode clusterNode) {
        LOG.debug(Constants.EMPTY);
        //if (!clusterNode.attribute("KEY").equals("secret"))
        //    return new IgniteSpiNodeValidationResult(clusterNode.id(), "Access denied", "Access denied");
        return null;

    }

    @Override
    public IgniteNodeValidationResult validateNode(ClusterNode clusterNode, DiscoveryDataBag.JoiningNodeDiscoveryData joiningNodeDiscoveryData) {
        return null;
    }

    @Override
    public DiscoveryDataExchangeType discoveryDataType() {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public SecurityContext authenticateNode(ClusterNode clusterNode, SecurityCredentials securityCredentials) throws IgniteException {
        LOG.debug(clusterNode);
        LOG.debug(securityCredentials);
        //LOG.debug("KEY=%s", clusterNode.attribute("KEY"));
        //LOG.debug("ROLE=%s", clusterNode.attribute("ROLE"));
        return securityPluginContext;
    }

    //----------------------------------------
    // Important: Allow anyone to do auth
    //----------------------------------------
    @Override
    public boolean isGlobalNodeAuthentication() {
        LOG.debug(Constants.EMPTY);
        return true;
    }

    //----------------------------------------
    // Important: Authenticate check
    //----------------------------------------
    @Override
    public SecurityContext authenticate(AuthenticationContext authenticationContext) throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
        return securityPluginContext;
    }

    @Override
    public Collection<SecuritySubject> authenticatedSubjects() throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
        return Collections.emptyList();
    }

    @Override
    public SecuritySubject authenticatedSubject(UUID uuid) throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public void onDisconnected(IgniteFuture<?> igniteFuture) throws IgniteCheckedException {
        // TO-DO
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public IgniteInternalFuture<?> onReconnected(boolean b) throws IgniteCheckedException {
        // TO-DO
        LOG.debug(Constants.EMPTY);
        return null;
    }

    //----------------------------------------
    // Important: Authorize check
    //----------------------------------------
    @Override
    public void authorize(String s, SecurityPermission gridSecurityPermission, org.apache.ignite.internal.processors.security.SecurityContext securityContext) throws SecurityException {
        // TO-DO
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void onSessionExpired(UUID uuid) {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public boolean enabled() {
        // TO-DO
        //LOG.debug(Constants.EMPTY);
        return true;
    }


    public ArrayList<String> getWhitelistKeys() {
        return whitelistKeys;
    }

    public void setWhitelistKeys(ArrayList<String> whitelistKeys) {
        this.whitelistKeys = whitelistKeys;
    }
}