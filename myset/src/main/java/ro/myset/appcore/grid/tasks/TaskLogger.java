/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.security.UUIDUtils;

/**
 * TaskLogger class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class TaskLogger extends Thread {
    private static final Logger LOG = LogManager.getFormatterLogger(TaskLogger.class);

    private String message = null;
    private int sleep = 0;

    @Override
    public void run() {
        try {
            String uuid = UUIDUtils.generate();
            LOG.debug("[%s] - %s", uuid, Constants.EVENT_INVOKED);
            this.sleep(sleep);
            LOG.info("[%s] - %s, %s", uuid, this.getName(), message);
            LOG.debug("[%s] - %s", uuid, Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(this.getName(), e);
        }
    }

    /*
     * Gets message
     */
    public final String getMessage() {
        return message;
    }

    /*
     * Sets message
     */
    public final void setMessage(String message) {
        this.message = message;
    }

    /*
     * Sets sleep
     */
    public final int getSleep() {
        return sleep;
    }

    /*
     * Gets sleep
     */
    public final void setSleep(int sleep) {
        this.sleep = sleep;
    }
}
