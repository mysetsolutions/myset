/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import ro.myset.appcore.common.Constants;

import java.io.Serializable;

/**
 * TasksProcessorServiceImpl interface.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class TasksProcessorServiceImpl implements Service, TasksProcessorService {

    private static final Logger LOG = LogManager.getFormatterLogger(TasksProcessorServiceImpl.class);

    private String serviceName = null;
    private static ThreadPoolTaskExecutor threadPoolTaskExecutor = null;
    private static ThreadPoolTaskScheduler threadPoolTaskScheduler = null;

    private TasksProcessorService taskProcessorService = null;

    @IgniteInstanceResource
    private Ignite ignite;

    public TasksProcessorServiceImpl() {
        LOG.debug(this);
    }

    @Override
    public void cancel(ServiceContext serviceContext) {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        if (threadPoolTaskExecutor != null)
            threadPoolTaskExecutor.shutdown();
        if (threadPoolTaskScheduler != null)
            threadPoolTaskScheduler.shutdown();
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void init(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        if (taskProcessorService == null)
            taskProcessorService = ignite.services().serviceProxy(serviceName, TasksProcessorService.class, false);
        if (threadPoolTaskExecutor != null)
            threadPoolTaskExecutor.initialize();
        if (threadPoolTaskScheduler != null)
            threadPoolTaskScheduler.initialize();
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void execute(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        LOG.debug(serviceName);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /**
     * Gets service name
     */
    public String getServiceName() {
        return this.serviceName;
    }

    /**
     * Sets service name
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Gets ThreadPoolTaskExecutor
     */
    public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
        return this.threadPoolTaskExecutor;
    }

    /**
     * Sets ThreadPoolTaskExecutor
     */
    public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.threadPoolTaskExecutor = threadPoolTaskExecutor;
    }

    /**
     * Gets ThreadPoolTaskScheduler
     */
    public ThreadPoolTaskScheduler getThreadPoolTaskScheduler() {
        return this.threadPoolTaskScheduler;
    }

    /**
     * Sets ThreadPoolTaskExecutor
     */
    public void setThreadPoolTaskScheduler(ThreadPoolTaskScheduler threadPoolTaskScheduler) {
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
    }


}
