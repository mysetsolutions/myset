/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.ant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Properties;

/**
 * AntRunnerFile class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class AntRunnerFile {
    private static final Logger LOG = LogManager.getFormatterLogger(AntRunnerFile.class);
    private File file = null;
    private Properties properties = null;
    private Project project = null;
    private int messageOutputLevel = Project.MSG_INFO;
    private File outputFile = null;

    public AntRunnerFile() {
        LOG.debug(this);
    }

    /**
     * Initialize ant runner
     */
    public final void initialize() {
        try {
            if (file != null && file.isFile()) {
                LOG.debug("%s", file);
                project = new Project();

                // project properties
                project.setSystemProperties();
                project.setProperty("ant.file", file.getAbsolutePath());
                if (properties != null && !properties.isEmpty()) {
                    Enumeration propertyNames = properties.propertyNames();
                    while (propertyNames.hasMoreElements()) {
                        String key = (String) propertyNames.nextElement();
                        project.setProperty(key, properties.getProperty(key));
                    }
                }

                // project console
                ProjectHelper.configureProject(project, file);
                DefaultLogger consoleLogger = new DefaultLogger();
                FileOutputStream fos = new FileOutputStream(outputFile);
                PrintStream ps = new PrintStream(fos);
                consoleLogger.setErrorPrintStream(ps);
                consoleLogger.setOutputPrintStream(ps);
                consoleLogger.setMessageOutputLevel(messageOutputLevel);
                project.addBuildListener(consoleLogger);

                // init
                project.init();
            } else {
                LOG.error("Invalid configuration file - %s", file);
            }
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    /**
     * Run ant target.
     */
    public final void runTarget(String target) {
        LOG.debug("[%s] - %s", target, project.getProperties());
        if (project != null && target != null)
            project.executeTarget(target);
        else if (project != null)
            project.executeTarget(project.getDefaultTarget());
    }

    /**
     * Gets ant configuration file.
     */
    public final File getFile() {
        return file;
    }

    /**
     * Sets ant configuration file.
     */
    public final void setFile(File file) {
        this.file = file;
    }

    /**
     * Gets ant message output level.
     */
    public int getMessageOutputLevel() {
        return messageOutputLevel;
    }

    /**
     * Sets ant message output level.
     */
    public final void setMessageOutputLevel(int messageOutputLevel) {
        this.messageOutputLevel = messageOutputLevel;
    }

    /**
     * Gets ant project properties.
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Sets ant project properties.
     */
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * Gets outputFile
     */
    public final File getOutputFile() {
        return outputFile;
    }

    /**
     * Sets outputFile
     */
    public final void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    /**
     * Gets ant runner project.
     */
    public final Project getProject() {
        return project;
    }


}
