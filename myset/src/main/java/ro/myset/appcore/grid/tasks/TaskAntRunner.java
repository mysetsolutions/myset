/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.grid.ant.AntRunner;

/**
 * TaskAntRunner class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class TaskAntRunner extends Thread {
    private static final Logger LOG = LogManager.getFormatterLogger(TaskAntRunner.class);
    private AntRunner antRunner = null;
    private String antTarget = null;
    private String antOutput = null;

    @Override
    public void run() {
        if (antRunner != null) {
            antRunner.initialize();
            antOutput = antRunner.runTarget(antTarget);
        }
    }

    /*
     * Gets antRunner
     */
    public final AntRunner getAntRunner() {
        return antRunner;
    }

    /*
     * Sets antRunner
     */
    public final void setAntRunner(AntRunner antRunner) {
        this.antRunner = antRunner;
    }

    /*
     * Gets antTarget
     */
    public final String getAntTarget() {
        return antTarget;
    }

    /*
     * Sets antTarget
     */
    public final void setAntTarget(String antTarget) {
        this.antTarget = antTarget;
    }

    /*
     * Gets antOutput
     */
    public final String getAntOutput() {
        return antOutput;
    }
}
