/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.plugins.security;

import org.apache.ignite.plugin.PluginConfiguration;

/**
 * Created by myset on 06/05/15.
 */
public class SecurityPluginConfiguration implements PluginConfiguration {
    public SecurityPluginConfiguration() {
    }
}
