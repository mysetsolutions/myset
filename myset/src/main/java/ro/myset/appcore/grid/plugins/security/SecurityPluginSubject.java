/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.plugins.security;

import org.apache.ignite.plugin.security.SecurityPermissionSet;
import org.apache.ignite.plugin.security.SecuritySubject;
import org.apache.ignite.plugin.security.SecuritySubjectType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * Created by myset on 07/05/15.
 */
public class SecurityPluginSubject implements SecuritySubject {
    private static final Logger LOG = LogManager.getFormatterLogger(SecurityPluginProcessor.class);

    public SecurityPluginSubject() {
        LOG.debug(this);
    }

    @Override
    public UUID id() {
        LOG.debug(Constants.EMPTY);
        return UUID.randomUUID();
    }

    @Override
    public SecuritySubjectType type() {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public Object login() {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public InetSocketAddress address() {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public SecurityPermissionSet permissions() {
        LOG.debug(Constants.EMPTY);
        return null;
    }
}
