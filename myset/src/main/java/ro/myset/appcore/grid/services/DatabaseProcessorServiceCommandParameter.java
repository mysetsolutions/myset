/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import java.io.Serializable;
import java.sql.ParameterMetaData;
import java.util.ArrayList;

/**
 * DatabaseProcessorServiceCommandParameter class.
 * <li/>parameterMode - java.sql.ParameterMetaData.*
 * <li/>parameterType - java.sql.Types.*
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class DatabaseProcessorServiceCommandParameter implements Serializable {
    private static final long serialVersionUID = 1L;

    private int parameterMode = 0;
    private int parameterType = 0;
    private Object parameterInValue = null;
    private Object parameterOutValue = null;

    public DatabaseProcessorServiceCommandParameter() {
    }

    public final DatabaseProcessorServiceCommandParameter init(int parameterMode, int parameterType) {
        this.parameterMode = parameterMode;
        this.parameterType = parameterType;
        return this;
    }

    public final DatabaseProcessorServiceCommandParameter init(int parameterMode, int parameterType, Object parameterInValue) {
        this.parameterMode = parameterMode;
        this.parameterType = parameterType;
        this.parameterInValue = parameterInValue;
        return this;
    }

    public final int getParameterMode() {
        return parameterMode;
    }

    public final void setParameterMode(int parameterMode) {
        this.parameterMode = parameterMode;
    }

    public final int getParameterType() {
        return parameterType;
    }

    public final void setParameterType(int parameterType) {
        this.parameterType = parameterType;
    }

    public final Object getParameterInValue() {
        return parameterInValue;
    }

    public final void setParameterInValue(Object parameterInValue) {
        this.parameterInValue = parameterInValue;
    }

    public final Object getParameterOutValue() {
        return parameterOutValue;
    }

    public final void setParameterOutValue(Object parameterOutValue) {
        this.parameterOutValue = parameterOutValue;
    }
}
