/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import ro.myset.utils.pool.PoolException;
import ro.myset.utils.pool.PoolMetrics;

import java.util.Hashtable;
import java.util.Properties;

/**
 * DatabasePoolService interface.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public interface DatabasePoolService extends PoolService {
    /*
     * Gets driver class
     */
    //public String getDriver();

    /*
     * Sets driver class
     */
    //public void setDriver(String driver);

    /*
     * Gets connection string url
     */
    public String getConnStr();

    /*
     * Sets connection string url
     */
    public void setConnStr(String connStr);

    /*
     * Gets info properties
     */
    public Properties getInfo();

    /*
     * Sets info properties
     */
    public void setInfo(Properties info);

}
