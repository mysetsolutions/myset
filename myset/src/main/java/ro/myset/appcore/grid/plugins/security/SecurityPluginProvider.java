/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.plugins.security;

import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.internal.processors.security.GridSecurityProcessor;
import org.apache.ignite.plugin.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by myset on 06/05/15.
 */
public class SecurityPluginProvider implements PluginProvider<SecurityPluginConfiguration> {
    private static final Logger LOG = LogManager.getFormatterLogger(SecurityPluginProvider.class);

    private SecurityPluginProcessor securityPluginProcessor = null;

    public SecurityPluginProvider() {
        LOG.debug(this);
        securityPluginProcessor = new SecurityPluginProcessor();
    }

    @Override
    public String name() {
        return "SecurityPlugin";
    }

    @Override
    public String version() {
        return "1.0";
    }

    @Override
    public String copyright() {
        return "MYSET SOLUTIONS S.R.L.";
    }

    @Override
    public void initExtensions(PluginContext pluginContext, ExtensionRegistry extensionRegistry) {
        LOG.debug(Constants.EMPTY);
        LOG.debug("%s,%s", pluginContext, extensionRegistry);
    }

    @Override
    public CachePluginProvider createCacheProvider(CachePluginContext cachePluginContext) {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public void start(PluginContext pluginContext) throws IgniteCheckedException {
        LOG.debug(pluginContext);
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void stop(boolean b) throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void onIgniteStart() throws IgniteCheckedException {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void onIgniteStop(boolean b) {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public Serializable provideDiscoveryData(UUID uuid) {
        LOG.debug(Constants.EMPTY);
        return null;
    }

    @Override
    public void receiveDiscoveryData(UUID uuid, Serializable serializable) {
        LOG.debug(Constants.EMPTY);
    }

    @Override
    public void validateNewNode(ClusterNode clusterNode) throws PluginValidationException {
        LOG.debug(Constants.EMPTY);
    }


    @Override
    public Object createComponent(PluginContext ctx, Class cls) {
        // very important
        LOG.info("%s", cls);
        if (cls.isAssignableFrom(GridSecurityProcessor.class)) {
            return securityPluginProcessor;
        } else {
            return null;
        }
    }

    @Override
    public IgnitePlugin plugin() {
        LOG.debug(Constants.EMPTY);
        return securityPluginProcessor;
    }

}
