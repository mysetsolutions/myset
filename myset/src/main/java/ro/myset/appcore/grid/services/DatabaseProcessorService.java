/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import org.apache.ignite.IgniteCache;
import ro.myset.utils.generic.GenericBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 * DatabaseService interface.
 *
 * @author Cristian Gheorghe Florescu
 * @version 3.0
 */
public interface DatabaseProcessorService {
    /**
     * Gets database pool service name
     */
    public String getDatabasePoolServiceName();

    /**
     * Sets database pool service name
     */
    public void setDatabasePoolServiceName(String databasePoolServiceName);

    /**
     * Gets database pool service
     */
    public DatabasePoolService getDatabasePoolService();

    /**
     * Execute database query.
     *
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     */

    public ArrayList<GenericBean> executeQuery(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception;


    /**
     * Execute database query asynchronously and put ArrayList<GenericBean> results into a cache single key.
     *
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     * @param cache                           IgniteCache object
     * @param cacheKey                        Cache key
     */
    public void executeQueryAsync(DatabaseProcessorServiceCommand databaseProcessorServiceCommand, IgniteCache cache, Object cacheKey) throws Exception;


    /**
     * Execute database command.
     *
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     */
    public void executeCommand(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception;


    /**
     * Execute database command asynchronously.
     *
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     */
    public void executeCommandAsync(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception;


    /**
     * Execute database callable statement command.
     *
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     */
    public void executeCallable(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception;


    /**
     * Execute database callable statement command asynchronously.
     *
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     */
    public void executeCallableAsync(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception;


    /**
     * Execute database commands. All database commands will be executed within the same transaction.
     *
     * @param databaseProcessorServiceCommands DatabaseServiceCommand ArrayList object.
     */
    public void executeCommands(ArrayList<DatabaseProcessorServiceCommand> databaseProcessorServiceCommands) throws Exception;


    /**
     * Execute database commands asynchronously. All database commands will be executed within the same transaction.
     *
     * @param databaseProcessorServiceCommands DatabaseServiceCommand ArrayList object.
     */
    public void executeCommandsAsync(ArrayList<DatabaseProcessorServiceCommand> databaseProcessorServiceCommands) throws Exception;


    /**
     * Transaction open database connection
     *
     * @return Transaction database connection
     */
    public Connection transactionOpenConnection() throws Exception;


    /**
     * Transaction execute database command
     *
     * @param databaseConnection Transaction database connection
     * @param databaseProcessorServiceCommand DatabaseServiceCommand object
     */
    public void transactionExecuteCommand(Connection databaseConnection,
                                          DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception;


    /**
     * Transaction add batch database commands
     *
     * @param databaseConnection Transaction database connection
     * @param preparedStatement  PreparedStatement object
     * @param parameters         PreparedStatement parameters
     */
    public void transactionAddBatch(Connection databaseConnection,
                                                           PreparedStatement preparedStatement,
                                                           ArrayList<Object> parameters) throws Exception;

    /**
     * Transaction execute batch database commands
     *
     * @param databaseConnection Transaction database connection
     * @param preparedStatement  PreparedStatement object
     */
    public void transactionExecuteBatch(Connection databaseConnection,
                                               PreparedStatement preparedStatement) throws Exception;

    /**
     * Transaction commit or rollback on error and reset database connection
     *
     * @param databaseConnection Transaction database connection
     */
    public void transactionCommit(Connection databaseConnection) throws Exception;


    /**
     * Transaction rollback and reset database connection
     *
     * @param databaseConnection Transaction database connection
     */
    public void transactionRollback(Connection databaseConnection) throws Exception;


    /**
     * Close and reset database connection
     *
     * @param databaseConnection Transaction database connection
     */
    public void transactionCloseConnection(Connection databaseConnection) throws Exception;


}
