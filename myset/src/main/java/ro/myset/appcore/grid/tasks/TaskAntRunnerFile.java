/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.grid.ant.AntRunnerFile;

import java.io.File;

/**
 * TaskAntRunnerFile class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class TaskAntRunnerFile extends Thread {
    private static final Logger LOG = LogManager.getFormatterLogger(TaskAntRunnerFile.class);
    private AntRunnerFile antRunnerFile = null;
    private String antTarget = null;
    private File antOutputFile = null;

    @Override
    public void run() {
        if (antRunnerFile != null) {
            antRunnerFile.initialize();
            antRunnerFile.runTarget(antTarget);
        }
    }

    /*
     * Gets antRunnerFile
     */
    public final AntRunnerFile getAntRunnerFile() {
        return antRunnerFile;
    }

    /*
     * Sets antRunnerFile
     */
    public final void setAntRunnerFile(AntRunnerFile antRunnerFile) {
        this.antRunnerFile = antRunnerFile;
    }

    /*
     * Gets antTarget
     */
    public final String getAntTarget() {
        return antTarget;
    }

    /*
     * Sets antTarget
     */
    public final void setAntTarget(String antTarget) {
        this.antTarget = antTarget;
    }

    /*
     * Gets antOutputFile
     */
    public File getAntOutputFile() {
        return antOutputFile;
    }

    /*
     * Sets antOutputFile
     */
    public void setAntOutputFile(File antOutputFile) {
        this.antOutputFile = antOutputFile;
    }

}
