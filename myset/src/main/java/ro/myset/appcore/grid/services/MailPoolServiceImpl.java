/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.mail.MailPool;

/**
 * MailPoolServiceImpl interface.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class MailPoolServiceImpl extends MailPool implements Service, MailPoolService {
    private static final Logger LOG = LogManager.getFormatterLogger(MailPoolServiceImpl.class);
    private Integer maxRecipients = Integer.MAX_VALUE;

    public MailPoolServiceImpl() {
        LOG.debug(this);
    }

    @Override
    public Integer getMaxRecipients() {
        return maxRecipients;
    }

    @Override
    public void setMaxRecipients(Integer maxRecipients) {
        this.maxRecipients = maxRecipients;
    }

    @Override
    public void cancel(ServiceContext serviceContext) {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            LOG.debug(this);
            this.shutdown();
            LOG.debug(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    @Override
    public void init(ServiceContext serviceContext) throws Exception {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            LOG.debug(this);
            this.startup();
            LOG.debug(Constants.EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    @Override
    public void execute(ServiceContext serviceContext) throws Exception {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            LOG.debug(this);
            LOG.debug(this.getPoolName());
            while (!serviceContext.isCancelled()) {
                if (this.isStarted())
                    this.checkpoint();
                else
                    Thread.sleep(this.getPoolingInterval() * 1000);
            }
            LOG.debug(Constants.EVENT_COMPLETED);
        } catch (InterruptedException e) {
            //nothing to do
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }
}

