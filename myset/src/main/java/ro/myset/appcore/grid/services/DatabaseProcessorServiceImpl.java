/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.appcore.common.Constants;
import ro.myset.utils.database.DatabaseUtils;
import ro.myset.utils.database.DatabaseUtilsException;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;
import ro.myset.utils.security.UUIDUtils;

import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;


/**
 * DatabaseProcessorServiceImpl class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 4.1
 */
public class DatabaseProcessorServiceImpl implements Service, DatabaseProcessorService {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabaseProcessorServiceImpl.class);
    private static final String UPDATE_COUNT_MESSAGE = "Update count is 0";

    private String databasePoolServiceName = null;
    private DatabasePoolService databasePoolService = null;

    @IgniteInstanceResource
    private Ignite ignite;

    public DatabaseProcessorServiceImpl() {
        LOG.debug(this);
    }

    @Override
    public void cancel(ServiceContext serviceContext) {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void init(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        if (databasePoolService == null)
            databasePoolService = ignite.services().serviceProxy(databasePoolServiceName, DatabasePoolService.class, false);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public void execute(ServiceContext serviceContext) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(this);
        LOG.debug(databasePoolServiceName);
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    @Override
    public final String getDatabasePoolServiceName() {
        return this.databasePoolServiceName;
    }

    @Override
    public final void setDatabasePoolServiceName(String databasePoolServiceName) {
        this.databasePoolServiceName = databasePoolServiceName;
    }

    @Override
    public final DatabasePoolService getDatabasePoolService() {
        return this.databasePoolService;
    }

    @Override
    public final ArrayList<GenericBean> executeQuery(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        ArrayList<GenericBean> output = new ArrayList<GenericBean>();
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = (Connection) databasePoolService.doBorrow();
            LOG.debug("[%s] %s,%s,params=%s", uuid, databasePoolServiceName, databaseProcessorServiceCommand.getLog(), databaseProcessorServiceCommand.getParameters());
            ps = cn.prepareStatement(databaseProcessorServiceCommand.getSqlCommand());
            if (databaseProcessorServiceCommand.getParameters() != null) {
                for (int i = 0; i < databaseProcessorServiceCommand.getParameters().size(); i++) {
                    Object paramValue = databaseProcessorServiceCommand.getParameters().get(i);
                    // cast java.util.Date
                    if (paramValue instanceof java.util.Date)
                        paramValue = new java.sql.Timestamp(((java.util.Date) paramValue).getTime());

                    //LOG.debug("param[%s] = %s", i + 1, paramValue);
                    ps.setObject(i + 1, paramValue);
                }
            }
            // query rows limit
            if (databaseProcessorServiceCommand.getRowsLimit() > 0)
                ps.setMaxRows(databaseProcessorServiceCommand.getRowsLimit());
            rs = ps.executeQuery();
            // get query column names
            ArrayList<String> fieldsNames = new ArrayList<String>();
            ResultSetMetaData rsm = rs.getMetaData();
            for (int i = 1; i <= rsm.getColumnCount(); i++) {
                fieldsNames.add(rsm.getColumnLabel(i));
            }
            Object fieldValue = null;
            while (rs.next()) {
                GenericBean genericBean = new GenericBean();
                genericBean.setFieldsNames(fieldsNames);
                genericBean.setFields(new GenericMap<>());
                for (int i = 1; i <= rsm.getColumnCount(); i++) {
                    //LOG.debug("result['%s'] = %s", rsm.getColumnLabel(i), rs.getObject(i));
                    fieldValue = rs.getObject(i);

                    // java.sql.Clob field
                    if (fieldValue != null && fieldValue instanceof Clob && databaseProcessorServiceCommand.isReadClob())
                        fieldValue = clobReader((Clob) rs.getObject(i));

                    // add GenericBean field
                    genericBean.getFields().put(rsm.getColumnLabel(i), fieldValue);

                    // set GenericBean value
                    if (i == 1)
                        genericBean.setValue(fieldValue);
                }
                output.add(genericBean);
            }
            return output;
        } catch (Exception e) {
            databaseProcessorServiceCommand.setErrors(true);
            LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            DatabaseUtils.close(rs);
            DatabaseUtils.close(ps);
            databasePoolService.doReturn(cn);
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
        }
    }

    @Override
    public final void executeQueryAsync(final DatabaseProcessorServiceCommand databaseProcessorServiceCommand, final IgniteCache cache, final Object cacheKey) throws Exception {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<GenericBean> cacheValue = executeQuery(databaseProcessorServiceCommand);
                    cache.put(cacheKey, cacheValue);
                } catch (Exception e) {
                    LOG.error(Constants.EMPTY, e);
                    throw new RuntimeException(e);
                }
            }
        });
        t.setName(DatabaseProcessorServiceImpl.class.getName().concat("-executeQueryAsync"));
        t.start();
    }

    @Override
    public final void executeCommand(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = (Connection) databasePoolService.doBorrow();
            LOG.debug("[%s] %s,%s,params=%s", uuid, databasePoolServiceName, databaseProcessorServiceCommand.getLog(), databaseProcessorServiceCommand.getParameters());
            cn.setAutoCommit(true);
            ps = cn.prepareStatement(databaseProcessorServiceCommand.getSqlCommand());
            if (databaseProcessorServiceCommand.getParameters() != null) {
                for (int i = 0; i < databaseProcessorServiceCommand.getParameters().size(); i++) {
                    Object paramValue = paramValue = databaseProcessorServiceCommand.getParameters().get(i);
                    // cast java.util.Date
                    if (paramValue instanceof java.util.Date)
                        paramValue = new java.sql.Timestamp(((java.util.Date) paramValue).getTime());

                    //LOG.debug("param[%s] = %s", i + 1, paramValue);
                    ps.setObject(i + 1, paramValue);
                }
            }
            ps.execute();
            if (ps.getUpdateCount() <= 0)
                throw new ServiceException(UPDATE_COUNT_MESSAGE);
        } catch (Exception e) {
            databaseProcessorServiceCommand.setErrors(true);
            LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            DatabaseUtils.close(ps);
            databasePoolService.doReturn(cn);
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);

        }
    }

    @Override
    public final void executeCommandAsync(final DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    executeCommand(databaseProcessorServiceCommand);
                } catch (Exception e) {
                    LOG.error(Constants.EMPTY, e);
                    throw new RuntimeException(e);
                }
            }
        });
        t.setName(DatabaseProcessorServiceImpl.class.getName().concat("-executeCommandAsync"));
        t.start();
    }

    @Override
    public final void executeCallable(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        Connection cn = null;
        CallableStatement cs = null;
        try {
            cn = (Connection) databasePoolService.doBorrow();
            LOG.debug("[%s] %s,%s,params=%s", uuid, databasePoolServiceName, databaseProcessorServiceCommand.getLog(), databaseProcessorServiceCommand.getParameters());
            cs = cn.prepareCall(databaseProcessorServiceCommand.getSqlCommand());
            if (databaseProcessorServiceCommand.getParameters() != null) {
                for (int i = 0; i < databaseProcessorServiceCommand.getParameters().size(); i++) {
                    Object paramValue = paramValue = databaseProcessorServiceCommand.getParameters().get(i);
                    if (!(paramValue instanceof DatabaseProcessorServiceCommandParameter)) {
                        // cast java.util.Date
                        if (paramValue instanceof java.util.Date)
                            paramValue = new java.sql.Timestamp(((java.util.Date) paramValue).getTime());

                        //LOG.debug("param[%s] = %s", i + 1, paramValue);
                        cs.setObject(i + 1, paramValue);
                    }
                    if (paramValue instanceof DatabaseProcessorServiceCommandParameter) {
                        DatabaseProcessorServiceCommandParameter databaseProcessorServiceCommandParameter = (DatabaseProcessorServiceCommandParameter) paramValue;
                        /*
                        LOG.debug("[%s] param['%s'] - parameterMode:%s, parameterType:%s, parameterInValue:%s, parameterOutValue:%s", uuid, i + 1,
                                databaseProcessorServiceCommandParameter.getParameterMode(),
                                databaseProcessorServiceCommandParameter.getParameterType(),
                                databaseProcessorServiceCommandParameter.getParameterInValue(),
                                databaseProcessorServiceCommandParameter.getParameterOutValue());
                        */
                        if (databaseProcessorServiceCommandParameter.getParameterMode() == ParameterMetaData.parameterModeOut)
                            cs.registerOutParameter(i + 1, databaseProcessorServiceCommandParameter.getParameterType());
                        if (databaseProcessorServiceCommandParameter.getParameterMode() == ParameterMetaData.parameterModeIn ||
                                databaseProcessorServiceCommandParameter.getParameterMode() == ParameterMetaData.parameterModeInOut) {
                            if (databaseProcessorServiceCommandParameter.getParameterInValue() instanceof java.util.Date)
                                cs.setObject(i + 1, new java.sql.Timestamp(((java.util.Date) databaseProcessorServiceCommandParameter.getParameterInValue()).getTime()));
                            else
                                cs.setObject(i + 1, databaseProcessorServiceCommandParameter.getParameterInValue());
                        }
                    }
                }
            }

            // execute
            cs.execute();

            // update out parameters
            for (int i = 0; i < databaseProcessorServiceCommand.getParameters().size(); i++) {
                Object paramValue = paramValue = databaseProcessorServiceCommand.getParameters().get(i);
                if (paramValue instanceof DatabaseProcessorServiceCommandParameter) {
                    DatabaseProcessorServiceCommandParameter databaseProcessorServiceCommandParameter = (DatabaseProcessorServiceCommandParameter) paramValue;

                    if (databaseProcessorServiceCommandParameter.getParameterMode() == ParameterMetaData.parameterModeOut ||
                            databaseProcessorServiceCommandParameter.getParameterMode() == ParameterMetaData.parameterModeInOut)
                        databaseProcessorServiceCommandParameter.setParameterOutValue(cs.getObject(i + 1));
                    /*
                    LOG.debug("[%s] param['%s'] - parameterMode:%s, parameterType:%s, parameterInValue:%s, parameterOutValue:%s", uuid, i + 1,
                            databaseProcessorServiceCommandParameter.getParameterMode(),
                            databaseProcessorServiceCommandParameter.getParameterType(),
                            databaseProcessorServiceCommandParameter.getParameterInValue(),
                            databaseProcessorServiceCommandParameter.getParameterOutValue());
                    */
                }
            }

        } catch (Exception e) {
            databaseProcessorServiceCommand.setErrors(true);
            LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            DatabaseUtils.close(cs);
            databasePoolService.doReturn(cn);
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
        }
    }

    @Override
    public final void executeCallableAsync(DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    executeCallable(databaseProcessorServiceCommand);
                } catch (Exception e) {
                    LOG.error(Constants.EMPTY, e);
                    throw new RuntimeException(e);
                }
            }
        });
        t.setName(DatabaseProcessorServiceImpl.class.getName().concat("-executeCallableAsync"));
        t.start();
    }

    @Override
    public final void executeCommands(ArrayList<DatabaseProcessorServiceCommand> databaseProcessorServiceCommands) throws Exception {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        Connection cn = null;
        PreparedStatement ps = null;
        DatabaseProcessorServiceCommand databaseProcessorServiceCommand = null;
        String errorMessage = null;
        try {
            cn = (Connection) databasePoolService.doBorrow();
            cn.setAutoCommit(false);
            for (int k = 0; k < databaseProcessorServiceCommands.size(); k++) {
                databaseProcessorServiceCommand = (DatabaseProcessorServiceCommand) databaseProcessorServiceCommands.get(k);
                LOG.debug("[%s] %s,%s,params=%s", uuid, databasePoolServiceName, databaseProcessorServiceCommand.getLog(), databaseProcessorServiceCommand.getParameters());
                ps = cn.prepareStatement(databaseProcessorServiceCommand.getSqlCommand());
                if (databaseProcessorServiceCommand.getParameters() != null) {
                    for (int i = 0; i < databaseProcessorServiceCommand.getParameters().size(); i++) {
                        Object paramValue = databaseProcessorServiceCommand.getParameters().get(i);
                        // cast java.util.Date
                        if (paramValue instanceof java.util.Date)
                            paramValue = new java.sql.Timestamp(((java.util.Date) paramValue).getTime());

                        //LOG.debug("param[%s] = %s", i + 1, paramValue);
                        ps.setObject(i + 1, paramValue);
                    }
                }
                ps.execute();
            }
            cn.commit();
        } catch (Exception e) {
            databaseProcessorServiceCommand.setErrors(true);
            LOG.error("[%s]", uuid, e);
            try {
                cn.rollback();
                throw e;
            } catch (Exception re) {
                LOG.error("[%s]", uuid, re);
                throw re;
            }
        } finally {
            try {
                cn.setAutoCommit(true);
            } catch (Exception se) {
                LOG.error("[%s]", uuid, se);
                throw new DatabaseUtilsException(se);
            }
            DatabaseUtils.close(ps);
            databasePoolService.doReturn(cn);
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
        }

    }

    @Override
    public final void executeCommandsAsync(final ArrayList<DatabaseProcessorServiceCommand> databaseProcessorServiceCommands) throws Exception {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    executeCommands(databaseProcessorServiceCommands);
                } catch (Exception e) {
                    LOG.error(Constants.EMPTY, e);
                    throw new RuntimeException(e);
                }
            }
        });
        t.setName(DatabaseProcessorServiceImpl.class.getName().concat("-executeCommandsAsync"));
        t.start();
    }


    @Override
    public final Connection transactionOpenConnection() throws Exception {
        Connection cn = null;
        cn = (Connection) databasePoolService.doBorrow();
        cn.setAutoCommit(false);
        LOG.info("[%s]", UUIDUtils.generate(cn.toString()));
        return cn;
    }


    @Override
    public final void transactionExecuteCommand(Connection databaseConnection,
                                                DatabaseProcessorServiceCommand databaseProcessorServiceCommand) throws Exception {
        String uuid = UUIDUtils.generate(databaseConnection.toString());
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        PreparedStatement ps = null;
        try {
            LOG.debug("[%s] %s,%s,params=%s", uuid, databasePoolServiceName, databaseProcessorServiceCommand.getLog(), databaseProcessorServiceCommand.getParameters());
            ps = databaseConnection.prepareStatement(databaseProcessorServiceCommand.getSqlCommand());
            if (databaseProcessorServiceCommand.getParameters() != null) {
                for (int i = 0; i < databaseProcessorServiceCommand.getParameters().size(); i++) {
                    Object paramValue = paramValue = databaseProcessorServiceCommand.getParameters().get(i);
                    // cast java.util.Date
                    if (paramValue instanceof java.util.Date)
                        paramValue = new java.sql.Timestamp(((java.util.Date) paramValue).getTime());

                    //LOG.debug("param[%s] = %s", i + 1, paramValue);
                    ps.setObject(i + 1, paramValue);
                }
            }
            ps.execute();
            if (ps.getUpdateCount() <= 0)
                throw new ServiceException(UPDATE_COUNT_MESSAGE);
        } catch (Exception e) {
            databaseProcessorServiceCommand.setErrors(true);
            LOG.error("[%s]", uuid, e);
            try {
                databaseConnection.rollback();
                throw e;
            } catch (Exception re) {
                LOG.error("[%s]", uuid, re);
                throw re;
            }
        } finally {
            DatabaseUtils.close(ps);
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);

        }
    }

    @Override
    public final void transactionAddBatch(Connection databaseConnection,
                                          PreparedStatement preparedStatement,
                                          ArrayList<Object> parameters) throws Exception {
        String uuid = UUIDUtils.generate(databaseConnection.toString());
        //LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        try {
            //LOG.debug("[%s] %s,%s,params=%s", uuid, databasePoolServiceName, preparedStatement, parameters);
            if (preparedStatement != null && parameters != null) {
                // !!! - for multi-thread
                synchronized (preparedStatement) {
                    // set parameters
                    for (int i = 0; i < parameters.size(); i++) {
                        Object paramValue = parameters.get(i);
                        // cast java.util.Date
                        if (paramValue instanceof java.util.Date)
                            paramValue = new java.sql.Timestamp(((java.util.Date) paramValue).getTime());

                        //LOG.debug("param[%s] = %s", i + 1, paramValue);
                        preparedStatement.setObject(i + 1, paramValue);
                    }
                    // add batch
                    preparedStatement.addBatch();
                }
            }
        } catch (Exception e) {
            //LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            //LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
        }
    }

    @Override
    public final void transactionExecuteBatch(Connection databaseConnection,
                                              PreparedStatement preparedStatement) throws Exception {
        String uuid = UUIDUtils.generate(databaseConnection.toString());
        //LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        try {
            if (preparedStatement != null) {
                // !!! - for multi-thread
                synchronized (preparedStatement) {
                    int[] results = preparedStatement.executeBatch();
                    //LOG.debug("[%s] commands:%s", uuid, results.length);
                    for (int i = 0; i < results.length; i++) {
                        if (results[i] <= 0)
                            throw new ServiceException(UPDATE_COUNT_MESSAGE);
                    }
                }
            }

        } catch (Exception e) {
            //LOG.error("[%s]", uuid, e);
            throw e;
        } finally {
            //LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
        }
    }


    @Override
    public final void transactionCommit(Connection databaseConnection) throws Exception {
        String uuid = UUIDUtils.generate(databaseConnection.toString());
        try {
            databaseConnection.commit();
            LOG.info("[%s]", uuid);
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            try {
                databaseConnection.rollback();
                throw e;
            } catch (Exception re) {
                LOG.error("[%s]", uuid, re);
                throw re;
            }
        }
    }

    @Override
    public final void transactionRollback(Connection databaseConnection) throws Exception {
        String uuid = UUIDUtils.generate(databaseConnection.toString());
        try {
            databaseConnection.rollback();
        } catch (Exception re) {
            LOG.error("[%s]", uuid, re);
            throw re;
        }
    }

    @Override
    public final void transactionCloseConnection(Connection databaseConnection) throws Exception {
        String uuid = UUIDUtils.generate(databaseConnection.toString());
        databaseConnection.setAutoCommit(true);
        databasePoolService.doReturn(databaseConnection);
        LOG.info("[%s]", uuid);
    }


    /*
     * Reads Clob or String into String
     */
    private final String clobReader(Object obj) {
        String uuid = UUIDUtils.generate();
        LOG.debug("[%s] %s", uuid, Constants.EVENT_INVOKED);
        String result = null;
        try {
            if (obj != null && obj instanceof Clob) {
                StringBuffer clobStringBuffer = new StringBuffer();
                int bufferSize = 1024;
                char[] buffer = new char[bufferSize];
                Reader clobReader = ((Clob) obj).getCharacterStream();
                int length = 0;
                while ((length = clobReader.read(buffer)) != -1) {
                    clobStringBuffer.append(buffer, 0, length);
                }
                result = clobStringBuffer.toString();
            }
            if (obj != null && obj instanceof String) {
                result = obj.toString();
            }
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
        } finally {
            LOG.debug("[%s] %s", uuid, Constants.EVENT_COMPLETED);
            return result;
        }
    }

}
