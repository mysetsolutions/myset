/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.appcore.grid.services;

import ro.myset.utils.pool.PoolException;
import ro.myset.utils.pool.PoolMetrics;

import java.util.Hashtable;

/**
 * PoolService interface.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public interface PoolService {

    /*
     * Startup pool
     */
    public void startup() throws PoolException;

    /*
     * Pool checkpoint
     */
    public void checkpoint() throws Exception;

    /*
     * Shutting down pool
     */
    public void shutdown() throws PoolException;

    /*
     * Create object.
     */
    public Object create() throws PoolException;

    /*
     * Check if object is valid.
     */
    public boolean isValid(Object o);

    /*
     * Close object.
     */
    public void close(Object o) throws PoolException;

    /*
     * Borrow action. After this doBorrow is strongly recommended to use doReturn.
     *
     * @exception PoolException If the execution fail.
     */
    public Object doBorrow() throws PoolException;

    /*
     * Release and return the borrowed object.
     */
    public void doReturn(Object o);

    /*
     * Gets pool name.
     */
    public String getPoolName();

    /*
     * Sets pool name.
     */
    public void setPoolName(String poolName);

    /*
     * Check if pool is started
     *
     * @exception Exception If the execution fail.
     */
    public boolean isStarted();

    /*
     * Gets pooling interval in seconds.
     */
    public int getPoolingInterval();

    /*
     * Sets pooling interval in seconds.
     *
     * @exception PoolException If the execution fail.
     */
    public void setPoolingInterval(int poolingInterval) throws PoolException;

    /*
     * Gets validate before borrow.
     */
    public boolean isValidateBeforeBorrow();

    /*
     * Sets validate before borrow. Default is true.
     */
    public void setValidateBeforeBorrow(boolean validateBeforeBorrow);

    /*
     * Gets minimum number of instances.
     */
    public long getMinPoolSize();

    /*
     * Sets minimum number of instances.
     *
     * @exception PoolException If the execution fail.
     */

    public void setMinPoolSize(long minPoolSize) throws PoolException;

    /*
     * Gets maximum accepted number of instances.
     */
    public long getMaxPoolSize();

    /*
     * Sets maximum pool size.
     *
     * @exception PoolException If the execution fail.
     */
    public void setMaxPoolSize(long maxPoolSize) throws PoolException;

    /*
     * Gets pool metrics
     */
    public PoolMetrics getPoolMetrics();

    /*
     * Gets maximum number of attempts.
     */
    public int getMaxAttempts();

    /*
     * Sets maximum number of attempts.
     *
     * @exception PoolException If the execution fail.
     */
    public void setMaxAttempts(int maxAttempts) throws PoolException;

    /*
     * Gets retry interval in seconds to the instance creation.
     */
    public int getRetryInterval();

    /*
     * Sets retry interval in seconds to the instance creation.
     *
     * @exception PoolException If the execution fail.
     */
    public void setRetryInterval(int retryInterval) throws PoolException;

    /*
     * Gets inactivity timeout in seconds.
     */
    public int getInactivityTimeout();

    /*
     * Sets inactivity timeout in seconds.
     *
     * @exception PoolException If the execution fail.
     */
    public void setInactivityTimeout(int inactivityTimeout) throws PoolException;

    /*
     * Gets usage timeout in seconds.
     */
    public int getUsageTimeout();

    /*
     * Sets usage timeout for in use instances.
     *
     * @exception PoolException If the execution fail.
     */
    public void setUsageTimeout(int usageTimeout) throws PoolException;

    /*
     * Gets idle objects.
     */
    public Hashtable getIdleObjects();

    /*
     * Gets in use objects.
     */
    public Hashtable getInUseObjects();


}
