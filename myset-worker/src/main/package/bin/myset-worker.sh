#!/bin/bash
#==================================================================================
#==================================================================================
#                                  myset-worker
#==================================================================================
#==================================================================================
echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][INFO]["${0##*/}"] - Worker initializing"

# Home
WORKER_HOME=$( cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd )
echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][INFO]["${0##*/}"] - WORKER_HOME="$WORKER_HOME

# Check JAVA_HOME
if [ "$JAVA_HOME" = "" ]; then
    echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][ERROR]["${0##*/}"] - JAVA_HOME environment variable was not found"
    exit 1
fi
echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][INFO]["${0##*/}"] - JAVA_HOME="$JAVA_HOME

# Check java binary
JAVA=${JAVA_HOME}/bin/java
if [ ! -e "$JAVA" ]; then
    echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][ERROR]["${0##*/}"] - java binary was not found in JAVA_HOME/bin"
    exit 1
fi

# JVM options
if [ -z "$JVM_OPTS" ] ; then
    JVM_OPTS=""
fi
JVM_OPTS=$JVM_OPTS" -server"
JVM_OPTS=$JVM_OPTS" -Xms512M -Xmx512M"
JVM_OPTS=$JVM_OPTS" -Dfile.encoding=UTF-8"
JVM_OPTS=$JVM_OPTS" -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true"
#JVM_OPTS=$JVM_OPTS" -Djava.security.manager"
#JVM_OPTS=$JVM_OPTS" -Djava.security.debug=all"
#JVM_OPTS=$JVM_OPTS" -Djava.security.policy="$WORKER_HOME"/conf/catalina.policy"
JVM_OPTS=$JVM_OPTS" -Djava.io.tmpdir="$WORKER_HOME"/temp"
JVM_OPTS=$JVM_OPTS" -Dworker.home="$WORKER_HOME
#JVM_OPTS=$JVM_OPTS" --add-opens=java.base/java.lang=ALL-UNNAMED"
#JVM_OPTS=$JVM_OPTS" --add-opens=java.base/java.io=ALL-UNNAMED"
#JVM_OPTS=$JVM_OPTS" --add-opens=java.base/java.util=ALL-UNNAMED"
#JVM_OPTS=$JVM_OPTS" --add-opens=java.base/java.util.concurrent=ALL-UNNAMED"
#JVM_OPTS=$JVM_OPTS" --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED"


echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][INFO]["${0##*/}"] - JVM_OPTS="$JVM_OPTS

# Main class
MAIN_CLASS="ro.myset.worker.Worker"

# Classpath
for file in ${WORKER_HOME}/lib/*
do
    CP=${CP}":"${file}
done
echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][INFO]["${0##*/}"] - CLASSPATH="${CP}

# Command
cd ${WORKER_HOME}
echo "["$(date +"%Y-%m-%d %H:%M:%S,%3N")"][INFO]["${0##*/}"] - Worker initialized"
"$JAVA" ${JVM_OPTS} -classpath "${CP}" ${MAIN_CLASS} "$@"
