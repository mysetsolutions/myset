<%@ page import="java.util.*" %>
<%@ page import="java.net.InetAddress" %>
<html>
<head>
    <title><%= request.getLocalAddr()%>
    </title>
    <style>
        body {
            font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif;
            font-size: 80%;
        }

        table {
            font-size: 100%;
            border-collapse: collapse;
            border: 1px solid #aaaaaa;
        }

        table td {
            border: 1px solid #aaaaaa;
            padding-right: 4px;
        }

        .header {
            font-weight: bolder;
            font-size: 22px;
            text-decoration: underline;
        }

        .global {
            background-color: #efefef;
        }

        .label {
            font-weight: bold;
        }

    </style>
</head>
<body>
<span class="header"><%=InetAddress.getLocalHost().getHostName()%></span>
<table>
    <tr class="global">
        <td class="label">Server</td>
        <td><%= request.getLocalAddr()%>
        </td>
    </tr>
    <tr class="global">
        <td class="label">Host</td>
        <td><%= request.getRemoteHost()%>
        </td>
    </tr>
    <tr class="global">
        <td class="label">User</td>
        <td><%=(request.getUserPrincipal()!=null)?request.getUserPrincipal().getName():""%>
        </td>
    </tr>
    <tr class="global">
        <td class="label">Date</td>
        <td><%= new java.util.Date()%>
        </td>
    </tr>
    <%
        Enumeration headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String headerName = (String) headers.nextElement();
    %>
    <tr>
        <td class="label"><%= headerName%>
        </td>
        <td><%= request.getHeader(headerName) %>
        </td>
    </tr>
    <%}%>
</table>

</body>
</html>
