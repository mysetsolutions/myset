/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
package ro.myset.worker;
/**
 * Worker class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.*;
import java.lang.reflect.Method;


public class Worker {
    private static final long serialVersionUID = 1L;
    private Logger LOG = null;

    // System
    public static final String SYS_FILE_SEPARATOR = System.getProperty("file.separator");
    public static final String EMPTY = "";

    // Simple events
    public static final String EVENT_INVOKED = "Invoked";
    public static final String EVENT_COMPLETED = "Completed";

    // Worker
    public static final String WORKER_HOME = "worker.home";
    public static final String WORKER_CATALINA_BASE = "catalina.base";
    public static final String WORKER_CATALINA_HOME = "catalina.home";
    public static final String WORKER_CATALINA_TEMP = "java.io.tmpdir";


    public final void start() {
        try {
            LOG.debug(EVENT_INVOKED);

            LOG.info("%s: %s", WORKER_HOME, System.getProperty(WORKER_HOME));

            // system properties
            System.setProperty(WORKER_CATALINA_BASE, System.getProperty(WORKER_HOME));
            System.setProperty(WORKER_CATALINA_HOME, System.getProperty(WORKER_HOME));
            System.setProperty(WORKER_CATALINA_TEMP, System.getProperty(WORKER_HOME).concat(SYS_FILE_SEPARATOR).concat("temp"));

            // server init
            Class<?> bootstrap = Class.forName("org.apache.catalina.startup.Bootstrap");
            Method bootstrapMain = bootstrap.getMethod("main", String[].class);
            String[] args = {"start"};
            bootstrapMain.invoke(null, (Object) args);

            LOG.info(EVENT_COMPLETED);
        } catch (Exception e) {
            LOG.error(EMPTY, e);
        }
    }


    /**
     * Sets LOG
     */
    public final void setLOG(Logger LOG) {
        this.LOG = LOG;
    }


    /**
     * main
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        // Checks
        if (System.getProperty(WORKER_HOME) == null)
            throw new NullPointerException("System property 'worker.home' not found.");
        File workerHomeFolderFile = new File(System.getProperty(WORKER_HOME));
        if (!workerHomeFolderFile.exists() || !workerHomeFolderFile.isDirectory())
            throw new FileNotFoundException(workerHomeFolderFile.getAbsolutePath());
        File catalinaPropertiesFile = new File(workerHomeFolderFile, "conf/catalina.properties");
        if (!catalinaPropertiesFile.exists() || !catalinaPropertiesFile.isFile())
            throw new FileNotFoundException(catalinaPropertiesFile.getAbsolutePath());
        FileReader workerPropertiesFileReader = new FileReader(catalinaPropertiesFile);
        System.getProperties().load(workerPropertiesFileReader);

        // Logger
        Configurator.initialize(null, workerHomeFolderFile.getAbsolutePath().concat(SYS_FILE_SEPARATOR).concat("conf").concat(SYS_FILE_SEPARATOR).concat("logger.xml"));

        // Worker
        Worker worker = new Worker();
        worker.setLOG(LogManager.getFormatterLogger(Worker.class));
        worker.start();

    }

}
