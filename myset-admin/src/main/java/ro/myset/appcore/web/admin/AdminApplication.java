package ro.myset.appcore.web.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;
import ro.myset.appcore.web.database.DatabaseCommand;
import ro.myset.utils.security.UUIDUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * AdminApplication beans.
 *
 * @version 1.0
 */

@Named("appcore_AdminApplication")
@ApplicationScoped
@Scope("singleton")

public class AdminApplication implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(AdminApplication.class);

    @Autowired
    private ro.myset.appcore.web.Application appcore_Application;

    @Autowired
    private ro.myset.appcore.web.utils.PrimeFacesUtils appcore_PrimefacesUtils;


    public AdminApplication() {
        LOG.debug(this);
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    public final void initialize() throws Exception {
        try {
            LOG.debug(Constants.EVENT_INVOKED);

            // common
            this.refreshUsers();



        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw e;
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }


    //============================================================
    //============================================================
    //============================================================
    //============================================================

    /*
     * Refresh - myset.users
     */
    public final void refreshUsers() throws Exception {
        String uuid = UUIDUtils.generate();
        try {
            LOG.debug("[%s]", uuid);
            DatabaseCommand databaseCommand = new DatabaseCommand();
            String databaseCommandId = "users.sql20";
            databaseCommand.setSqlCommand(appcore_Application.database(databaseCommandId));
            databaseCommand.setLog(databaseCommandId);
            ArrayList<Object> databaseCommandParams = new ArrayList<Object>();
            databaseCommand.setParameters(databaseCommandParams);
            appcore_Application.getAttributes().put("myset.users", appcore_Application.getDatabaseProcessorService().executeQuery(databaseCommand));
        } catch (Exception e) {
            LOG.error("[%s]", uuid, e);
            throw e;
        }
    }





}
