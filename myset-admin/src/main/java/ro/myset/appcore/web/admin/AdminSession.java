package ro.myset.appcore.web.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ro.myset.appcore.common.Constants;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * AdminSession beans.
 *
 * @version 1.0
 */

@Named("appcore_AdminSession")
@SessionScoped
@Scope("session")

public class AdminSession implements Serializable, InitializingBean {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getFormatterLogger(AdminSession.class);

    @Autowired
    private ro.myset.appcore.web.Application appcore_Application;

    @Autowired
    private AdminApplication appcore_AdminApplication;


    public AdminSession() {
        LOG.debug(this);
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        initialize();
    }

    public final void initialize() throws Exception {
        try {
            LOG.debug(Constants.EVENT_INVOKED);

            // common
            //this.refresh();



        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw e;
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }


    //============================================================
    //============================================================
    //============================================================
    //============================================================









}
