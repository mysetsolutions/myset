/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;
import ro.myset.utils.pool.Pool;
import ro.myset.utils.pool.PoolException;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Properties;

/**
 * DatabasePool for JDBC connections.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class DatabasePool extends Pool implements Serializable {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabasePool.class);

    private String driver = null;
    private String connStr = null;
    private Properties info = null;

    public DatabasePool() {
        LOG.debug(this);
    }

    public DatabasePool(String driver, String connStr) {
        LOG.debug("%s,%s", driver, connStr);
        this.driver = driver;
        this.connStr = connStr;
    }

    public DatabasePool(String driver, String connStr, Properties info) {
        LOG.debug("%s,%s", driver, connStr);
        this.driver = driver;
        this.connStr = connStr;
        this.info = info;
    }

    public final Connection create() throws PoolException {
        long bt = System.currentTimeMillis();
        try {
            Connection connection = null;
            LOG.debug("%s,%s", driver, connStr);
            if (info != null && info instanceof Properties)
                connection = DatabaseUtils.getConnection(driver, connStr, info);
            else
                connection = DatabaseUtils.getConnection(driver, connStr);
            LOG.debug(connection);
            return connection;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new PoolException(e);
        } finally {
            this.getPoolMetrics().setCreateDuration(System.currentTimeMillis() - bt);
        }
    }

    public final boolean isValid(Object o) {
        long bt = System.currentTimeMillis();
        try {
            if (o != null && (o instanceof Connection)) {
                if (this.getUsageTimeout() >= 0 && ((Connection) o).isValid(this.getUsageTimeout())) {
                    LOG.debug("%s,%s", o, true);
                    return true;
                }
                if (!((Connection) o).isClosed()) {
                    LOG.debug("%s,%s", o, true);
                    return true;
                }
            }
            LOG.debug("%s,%s", o, false);
            return false;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            return false;
        } finally {
            this.getPoolMetrics().setValidateDuration(System.currentTimeMillis() - bt);
        }
    }

    public final void close(Object o) throws PoolException {
        long bt = System.currentTimeMillis();
        LOG.debug(o);
        try {
            if (o == null || !(o instanceof Connection))
                return;
            else {
                ((Connection) o).close();
                o = null;
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new PoolException(e);
        } finally {
            this.getPoolMetrics().setCloseDuration(System.currentTimeMillis() - bt);
        }
    }

    public final String getDriver() {
        return driver;
    }

    public final void setDriver(String driver) {
        this.driver = driver;
    }

    public final String getConnStr() {
        return connStr;
    }

    public final void setConnStr(String connStr) {
        this.connStr = connStr;
    }

    public final Properties getInfo() {
        return info;
    }

    public final void setInfo(Properties info) {
        this.info = info;
    }
}
