/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.sql.PooledConnection;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 * Database utilities class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.1
 */
public class DatabaseUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(DatabaseUtils.class);

    /**
     * Gets a new connection object.
     *
     * @param driver  :"oracle.jdbc.driver.OracleDriver"
     * @param connStr :"jdbc:oracle:thin:user/pass@host:port:SID"
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static Connection getConnection(String driver, String connStr) throws DatabaseUtilsException {
        try {
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(connStr);
            return conn;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        }
    }

    /**
     * Gets a new connection object.
     *
     * @param driver  :"oracle.jdbc.driver.OracleDriver"
     * @param connStr :"jdbc:oracle:thin:user/pass@host:port:SID"
     * @param info
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static Connection getConnection(String driver, String connStr, Properties info) throws DatabaseUtilsException {
        try {
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(connStr, info);
            return conn;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        }
    }

    /**
     * Gets a new connection object.
     *
     * @param driver  :"oracle.jdbc.driver.OracleDriver"
     * @param connStr :"jdbc:oracle:thin:user/pass@host:port:SID"
     * @param user    User name
     * @param pass    User password
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static Connection getConnection(String driver, String connStr, String user, String pass) throws DatabaseUtilsException {
        try {
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(connStr, user, pass);
            return conn;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        }
    }

    /**
     * Gets a new connection object.
     *
     * @param jndiPath :"jdbc/myConnection".
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static Connection getConnection(String jndiPath) throws DatabaseUtilsException {
        try {
            InitialContext ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup(jndiPath);
            Connection conn = ds.getConnection();
            return conn;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        }
    }

    /**
     * Gets a new PreparedStatement object.
     *
     * @param conn   Connection object.
     * @param sqlCmd SQL command.
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static PreparedStatement getPreparedStatement(Connection conn, String sqlCmd) throws DatabaseUtilsException {
        try {
            PreparedStatement pstmt = conn.prepareStatement(sqlCmd);
            return pstmt;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        }
    }

    /**
     * Gets a new CallableStatement object.
     *
     * @param conn   Connection object.
     * @param sqlCmd SQL command.
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static CallableStatement getCallableStatement(Connection conn, String sqlCmd) throws DatabaseUtilsException {
        try {
            CallableStatement cstmt = conn.prepareCall(sqlCmd);
            return cstmt;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        }
    }

    /**
     * Release jdbc resources (close jdbc objects).
     */
    public static void close(Object obj) {
        try {
            if (obj != null) {
                if (obj instanceof ResultSet) {
                    ((ResultSet) obj).close();
                    obj = null;
                    // LOG.debug("[INFO] ResultSet closed ");
                    return;
                }
                // statements
                if (obj instanceof PreparedStatement) {
                    ((PreparedStatement) obj).close();
                    obj = null;
                    // LOG.debug("[INFO] PreparedStatement closed ");
                    return;
                }
                if (obj instanceof CallableStatement) {
                    ((CallableStatement) obj).close();
                    obj = null;
                    // LOG.debug("[INFO] CallableStatement closed ");
                    return;
                }
                if (obj instanceof Statement) {
                    ((Statement) obj).close();
                    obj = null;
                    // LOG.debug("[INFO] Statement closed ");
                    return;
                }
                // connections
                if (obj instanceof PooledConnection) {
                    ((PooledConnection) obj).close();
                    obj = null;
                    // LOG.debug("[INFO] Connection closed ");
                    return;
                }
                if (obj instanceof Connection) {
                    ((Connection) obj).close();
                    obj = null;
                    // LOG.debug("[INFO] Connection closed ");
                    return;
                }

            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            System.out.println(e.toString());
        }
    }

    /**
     * Executes one DML or DDL sql command.
     *
     * @param driver  :"oracle.jdbc.driver.OracleDriver"
     * @param connStr :"jdbc:oracle:thin:user/pass@host:port:SID"
     * @param sqlCmd  SQL command.
     * @return the current result as an update count; <i>0</i> if there are no results or <i>n</i>
     * number of updates
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static int executeOperation(String driver, String connStr, String sqlCmd) throws DatabaseUtilsException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int i = 0;
        try {
            conn = getConnection(driver, connStr);
            conn.setAutoCommit(true);
            pstmt = getPreparedStatement(conn, sqlCmd);
            pstmt.execute();
            i = pstmt.getUpdateCount();
            if (i <= 0)
                i = 0;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        } finally {
            close(pstmt);
            close(conn);
        }
        return i;
    }

    /**
     * Executes one DML or DDL sql command.
     *
     * @param jndiPath :"jdbc/myConnection".
     * @param sqlCmd   SQL command.
     * @return the current result as an update count; <i>0</i> if there are no results or <i>n</i>
     * number of updates
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static int executeOperation(String jndiPath, String sqlCmd) throws DatabaseUtilsException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int i = -1;
        try {
            conn = getConnection(jndiPath);
            conn.setAutoCommit(true);
            pstmt = getPreparedStatement(conn, sqlCmd);
            pstmt.execute();
            i = pstmt.getUpdateCount();
            if (i <= 0)
                i = 0;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        } finally {
            close(pstmt);
            close(conn);
        }
        return i;
    }

    /**
     * Transactionaly executes multiple DML or DDL sql commands.
     *
     * @param driver  :"oracle.jdbc.driver.OracleDriver"
     * @param connStr :"jdbc:oracle:thin:user/pass@host:port:SID"
     * @param sqlCmds SQL commands List object.
     * @return the current result as an update count; <i>0</i> if there are no results or <i>n</i>
     * number of updates
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static int executeOperations(String driver, String connStr, List sqlCmds) throws DatabaseUtilsException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int i = 0;
        try {
            conn = getConnection(driver, connStr);
            conn.setAutoCommit(false);
            Iterator iter = sqlCmds.iterator();
            int j = 0;
            while (iter.hasNext()) {
                DatabaseUtils.close(pstmt);
                pstmt = getPreparedStatement(conn, (String) iter.next());
                pstmt.execute();
                j = pstmt.getUpdateCount();
                if (j > 0)
                    i += j;
            }
            conn.commit();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            try {
                conn.rollback();
            } catch (Exception re) {
                throw new DatabaseUtilsException(re);
            }
            throw new DatabaseUtilsException(e);
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Exception se) {
                throw new DatabaseUtilsException(se);
            }
            close(pstmt);
            close(conn);
        }
        return i;
    }

    /**
     * Transactionaly executes multiple DML or DDL sql commands.
     *
     * @param jndiPath :"jdbc/myConnection".
     * @param sqlCmds  SQL commands List object.
     * @return the current result as an update count; <i>0</i> if there are no results or <i>n</i>
     * number of updates
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static int executeOperations(String jndiPath, List sqlCmds) throws DatabaseUtilsException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        int i = 0;
        try {
            conn = getConnection(jndiPath);
            conn.setAutoCommit(false);
            Iterator iter = sqlCmds.iterator();
            int j = 0;
            while (iter.hasNext()) {
                DatabaseUtils.close(pstmt);
                pstmt = getPreparedStatement(conn, (String) iter.next());
                pstmt.execute();
                j = pstmt.getUpdateCount();
                if (j > 0)
                    i += j;
            }
            conn.commit();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            try {
                conn.rollback();
            } catch (Exception re) {
                throw new DatabaseUtilsException(re);
            }
            throw new DatabaseUtilsException(e);
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Exception se) {
                throw new DatabaseUtilsException(se);
            }
            close(pstmt);
            close(conn);
        }
        return i;
    }

    /**
     * Gets field values from a SELECT statement.
     *
     * @param conn   Connection object.
     * @param sqlCmd SQL command (only SELECT command).
     * @param field  Searched field name.
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String[] getFieldValues(Connection conn, String sqlCmd, String field) throws DatabaseUtilsException {
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        try {
            Vector vresults = new Vector();
            String[] results = null;
            pstmt = getPreparedStatement(conn, sqlCmd);
            pstmt.execute();
            rset = pstmt.getResultSet();
            while (rset.next()) {
                vresults.add(rset.getString(field));
            }
            if (vresults.size() > 0) {
                results = new String[vresults.size()];
                for (int i = 0; i < results.length; i++) {
                    results[i] = (String) vresults.get(i);
                    // System.out.println(results[i]);
                }
            }
            return results;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        } finally {
            close(rset);
            close(pstmt);
        }
    }

    /**
     * Gets field values from a SELECT statement.
     *
     * @param conn   Connection object.
     * @param sqlCmd SQL command (only SELECT command).
     * @param field  Searched field name.
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String[] getFieldValues(Connection conn, String sqlCmd, int field) throws DatabaseUtilsException {
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        try {
            Vector vresults = new Vector();
            String[] results = null;
            pstmt = getPreparedStatement(conn, sqlCmd);
            pstmt.execute();
            rset = pstmt.getResultSet();
            while (rset.next()) {
                vresults.add(rset.getString(field));
            }
            if (vresults.size() > 0) {
                results = new String[vresults.size()];
                for (int i = 0; i < results.length; i++) {
                    results[i] = (String) vresults.get(i);
                    // System.out.println(results[i]);
                }
            }
            return results;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        } finally {
            close(rset);
            close(pstmt);
        }
    }

    /**
     * Gets field value from a SELECT statement. If field has more than one value method will return
     * the first one.
     *
     * @param conn   Connection object.
     * @param sqlCmd SQL command (only SELECT command).
     * @param field  Searched field name.
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String getFieldValue(Connection conn, String sqlCmd, String field) throws DatabaseUtilsException {
        return getFieldValues(conn, sqlCmd, field)[0];
    }

    /**
     * Gets field value from a SELECT statement. If field has more than one value method will return
     * the first one.
     *
     * @param conn   Connection object.
     * @param sqlCmd SQL command (only SELECT command).
     * @param field  Searched field name.
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String getFieldValue(Connection conn, String sqlCmd, int field) throws DatabaseUtilsException {
        return getFieldValues(conn, sqlCmd, field)[0];
    }

    /**
     * Gets field values from a SELECT statement.
     *
     * @param conn      Connection object.
     * @param sqlCmd    SQL command (only SELECT command).
     * @param field     Searched field name.
     * @param parameter SQL command parameter
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String[] getFieldValues(Connection conn, String sqlCmd, String field, String parameter) throws DatabaseUtilsException {
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        try {
            Vector vresults = new Vector();
            String[] results = null;
            pstmt = getPreparedStatement(conn, sqlCmd);
            pstmt.setString(1, parameter);
            pstmt.execute();
            rset = pstmt.getResultSet();
            while (rset.next()) {
                vresults.add(rset.getString(field));
            }
            if (vresults.size() > 0) {
                results = new String[vresults.size()];
                for (int i = 0; i < results.length; i++) {
                    results[i] = (String) vresults.get(i);
                    // System.out.println(results[i]);
                }
            }
            return results;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        } finally {
            close(rset);
            close(pstmt);
        }
    }

    /**
     * Gets field values from a SELECT statement.
     *
     * @param conn      Connection object.
     * @param sqlCmd    SQL command (only SELECT command).
     * @param field     Searched field name.
     * @param parameter SQL command parameter
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String[] getFieldValues(Connection conn, String sqlCmd, int field, String parameter) throws DatabaseUtilsException {
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        try {
            Vector vresults = new Vector();
            String[] results = null;
            pstmt = getPreparedStatement(conn, sqlCmd);
            pstmt.setString(1, parameter);
            pstmt.execute();
            rset = pstmt.getResultSet();
            while (rset.next()) {
                vresults.add(rset.getString(field));
            }
            if (vresults.size() > 0) {
                results = new String[vresults.size()];
                for (int i = 0; i < results.length; i++) {
                    results[i] = (String) vresults.get(i);
                    // System.out.println(results[i]);
                }
            }
            return results;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DatabaseUtilsException(e);
        } finally {
            close(rset);
            close(pstmt);
        }
    }

    /**
     * Gets field value from a SELECT statement. If field has more than one value method will return
     * the first one.
     *
     * @param conn      Connection object.
     * @param sqlCmd    SQL command (only SELECT command).
     * @param field     Searched field name.
     * @param parameter SQL command parameter
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String getFieldValue(Connection conn, String sqlCmd, String field, String parameter) throws DatabaseUtilsException {
        return getFieldValues(conn, sqlCmd, field, parameter)[0];
    }

    /**
     * Gets field value from a SELECT statement. If field has more than one value method will return
     * the first one.
     *
     * @param conn      Connection object.
     * @param sqlCmd    SQL command (only SELECT command).
     * @param field     Searched field name.
     * @param parameter SQL command parameter
     * @throws DatabaseUtilsException If the execution fail.
     */
    public static String getFieldValue(Connection conn, String sqlCmd, int field, String parameter) throws DatabaseUtilsException {
        return getFieldValues(conn, sqlCmd, field, parameter)[0];
    }

}
