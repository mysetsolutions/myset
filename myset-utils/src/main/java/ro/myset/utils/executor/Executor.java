/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.executor;


import java.util.concurrent.*;


/**
 * Executor class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class Executor extends ThreadPoolExecutor {

    private int executeParallelism = 0;
    private boolean executeExceptions = false;
    private ExecuteExceptionHandler executeExceptionHandler = null;
    private boolean shutdownOnExceptions = false;


    public Executor(int executeParallelism) {
        super(executeParallelism, executeParallelism, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        this.executeParallelism = executeParallelism;
    }

    public Executor(int executeParallelism, BlockingQueue<Runnable> executeQueue) {
        super(executeParallelism, executeParallelism, 0L, TimeUnit.MILLISECONDS, executeQueue);
        this.executeParallelism = executeParallelism;
    }

    @Override
    protected final void afterExecute(final Runnable r, final Throwable t) {
        if (t != null) {
            executeExceptions = true;
            if (executeExceptionHandler != null)
                executeExceptionHandler.handle(r, t);
            if(shutdownOnExceptions) {
                this.getQueue().clear();
                this.shutdown();
            }
        }
    }

    public final int getExecuteParallelism() {
        return executeParallelism;
    }

    public final void setExecuteParallelism(int executeParallelism) {
        this.executeParallelism = executeParallelism;
        super.setCorePoolSize(executeParallelism);
        super.setMaximumPoolSize(executeParallelism);
    }

    public final ExecuteExceptionHandler getExecuteExceptionHandler() {
        return executeExceptionHandler;
    }

    public final void setExecuteExceptionHandler(ExecuteExceptionHandler executeExceptionHandler) {
        this.executeExceptionHandler = executeExceptionHandler;
    }

    public final boolean isExecuteExceptions() {
        return executeExceptions;
    }

    public final boolean isShutdownOnExceptions() {
        return shutdownOnExceptions;
    }

    public final void setShutdownOnExceptions(boolean shutdownOnExceptions) {
        this.shutdownOnExceptions = shutdownOnExceptions;
    }
}
