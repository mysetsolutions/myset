/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.pool;

/**
 * Class for handling Pool exceptions.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class PoolException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new Exception with the specified message.
     *
     * @param msg The text message
     */
    public PoolException(String msg) {
        super(msg);
    }

    /**
     * Constructs a new Exception using text message and StackTrace values of input Exception object.
     *
     * @param e The source exception
     */
    public PoolException(Exception e) {
        super(e.getLocalizedMessage());
        super.setStackTrace(e.getStackTrace());
    }
}
