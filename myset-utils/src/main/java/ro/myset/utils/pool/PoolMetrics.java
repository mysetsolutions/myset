/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Object Pool metrics.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class PoolMetrics {
    private static final Logger LOG = LogManager.getFormatterLogger(PoolMetrics.class);

    private int poolSize = 0;
    private int inUseObjectsSize = 0;
    private int idleObjectsSize = 0;
    private long checkpointDuration = 0;
    private long createDuration = 0;
    private long validateDuration = 0;
    private long closeDuration = 0;

    public PoolMetrics() {
        LOG.debug(this);
    }

    public void reset() {
        this.poolSize = 0;
        this.inUseObjectsSize = 0;
        this.idleObjectsSize = 0;
        this.checkpointDuration = 0;
        this.createDuration = 0;
        this.validateDuration = 0;
        this.closeDuration = 0;
    }

    public final int getPoolSize() {
        return poolSize;
    }

    public final void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public int getInUseObjectsSize() {
        return inUseObjectsSize;
    }

    public void setInUseObjectsSize(int inUseObjectsSize) {
        this.inUseObjectsSize = inUseObjectsSize;
    }

    public int getIdleObjectsSize() {
        return idleObjectsSize;
    }

    public void setIdleObjectsSize(int idleObjectsSize) {
        this.idleObjectsSize = idleObjectsSize;
    }

    public long getCheckpointDuration() {
        return checkpointDuration;
    }

    public void setCheckpointDuration(long checkpointDuration) {
        this.checkpointDuration = checkpointDuration;
    }

    public long getCreateDuration() {
        return createDuration;
    }

    public void setCreateDuration(long createDuration) {
        this.createDuration = createDuration;
    }

    public long getValidateDuration() {
        return validateDuration;
    }

    public void setValidateDuration(long validateDuration) {
        this.validateDuration = validateDuration;
    }

    public long getCloseDuration() {
        return closeDuration;
    }

    public void setCloseDuration(long closeDuration) {
        this.closeDuration = closeDuration;
    }
}
