/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Object Pool base abstract class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 4.0
 */

public abstract class Pool implements Serializable {
    private static final Logger LOG = LogManager.getFormatterLogger(Pool.class);

    private PoolMetrics poolMetrics = null;

    // commons
    private String poolName = null;
    private Hashtable<Object, Long> idleObjects = null;
    private Hashtable<Object, Long> inUseObjects = null;
    private boolean started = false;
    private int poolingInterval = 10;
    private int poolingIntervalMillis = 10000;
    private boolean validateBeforeBorrow = true;
    // creation
    private int maxAttempts = 3;
    private int retryInterval = 1;
    private int retryIntervalMillis = 1000;
    // sizing
    private long minPoolSize = 0;
    private long maxPoolSize = -1;
    // timeouts
    private int inactivityTimeout = 60;
    private int inactivityTimeoutMillis = 60000;
    private int usageTimeout = 300;
    private int usageTimeoutMillis = 300000;

    private final String MESSAGE_001 = "No valid objects available";


    public abstract Object create() throws PoolException;

    public abstract boolean isValid(Object o);

    public abstract void close(Object o) throws PoolException;

    /*
     * Startup pool
     */
    public final void startup() {
        LOG.debug(Constants.EVENT_INVOKED);
        this.started = true;
        this.idleObjects = new Hashtable();
        this.inUseObjects = new Hashtable();
        poolMetrics = new PoolMetrics();
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /*
     * Shutting down pool
     */
    public final void shutdown() throws PoolException {
        LOG.debug(Constants.EVENT_INVOKED);
        this.started = false;
        poolMetrics.reset();
        Enumeration e;
        Object o;
        // close all idle instances
        if (idleObjects != null && idleObjects.size() > 0) {
            e = idleObjects.keys();
            while (e.hasMoreElements()) {
                o = e.nextElement();
                synchronized (idleObjects) {
                    idleObjects.remove(o);
                }
                this.close(o);
            }
        }

        // close all inUse instances
        if (inUseObjects != null && inUseObjects.size() > 0) {
            e = inUseObjects.keys();
            while (e.hasMoreElements()) {
                o = e.nextElement();
                synchronized (inUseObjects) {
                    inUseObjects.remove(o);
                }
                this.close(o);
            }
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }


    /*
     * Checkpoint procedure
     */
    public final void checkpoint() throws Exception {
        long now = System.currentTimeMillis();
        Enumeration e;
        Object o;

        // check idleObjects
        if (inactivityTimeoutMillis >= 0 && this.idleObjects.size() > 0) {
            e = idleObjects.keys();
            while (e.hasMoreElements()) {
                o = e.nextElement();
                // check inactivityTimeoutMillis
                if (idleObjects.get(o) != null) {
                    if (((now - ((Long) idleObjects.get(o)).longValue()) > inactivityTimeoutMillis)) {
                        LOG.debug("inactivityTimeoutMillis: %s", o);
                        synchronized (idleObjects) {
                            idleObjects.remove(o);
                        }
                        this.close(o);
                    }
                }
            }
        }

        // check inUseObjects
        if (usageTimeoutMillis >= 0 && this.inUseObjects.size() > 0) {
            e = inUseObjects.keys();
            while (e.hasMoreElements()) {
                o = e.nextElement();
                // check usageTimeout
                if (inUseObjects.get(o) != null) {
                    if ((now - ((Long) inUseObjects.get(o)).longValue()) > usageTimeoutMillis) {
                        LOG.debug("usageTimeoutMillis: %s", o);
                        synchronized (inUseObjects) {
                            inUseObjects.remove(o);
                        }
                        synchronized (idleObjects) {
                            idleObjects.put(o, System.currentTimeMillis());
                        }
                    }
                }
            }
        }

        // check poolSize and call factory to create objects according to minSize but not more than maxSize
        if ((minPoolSize > 0) && (minPoolSize - (this.idleObjects.size() + this.inUseObjects.size())) > 0) {
            LOG.debug("Create %s instances to reach minPoolSize: %s",(minPoolSize - (this.idleObjects.size() + this.inUseObjects.size())),minPoolSize);
            while ((minPoolSize - (this.idleObjects.size() + this.inUseObjects.size())) > 0) {
                synchronized (idleObjects) {
                    idleObjects.put(this.create(), System.currentTimeMillis());
                }
            }
        }

        //pool metrics update
        poolMetrics.setPoolSize(this.idleObjects.size() + this.inUseObjects.size());
        poolMetrics.setInUseObjectsSize(this.inUseObjects.size());
        poolMetrics.setIdleObjectsSize(this.idleObjects.size());
        poolMetrics.setCheckpointDuration(System.currentTimeMillis() - now);

        // calculate sleep
        try {
            if ((System.currentTimeMillis() - now) < poolingIntervalMillis) {
                Thread.sleep(poolingIntervalMillis - (System.currentTimeMillis() - now));
            }
        } catch (InterruptedException ie) {
            LOG.debug(Constants.EMPTY, ie);
            this.shutdown();
        }
    }

    /*
     * Borrow action. After this doBorrow is strongly recommended to use doReturn.
     *
     * @exception PoolException If the execution fail.
     */
    public final synchronized Object doBorrow() throws PoolException {
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            Enumeration e = null;
            Object o = null;
            if (poolMetrics.getIdleObjectsSize() > 0)
                e = idleObjects.keys();
            for (int i = 1; i <= maxAttempts; i++) {
                LOG.debug("Attempt %s", i);
                if (e != null && e.hasMoreElements()) {
                    // get existing
                    o = e.nextElement();
                    synchronized (idleObjects) {
                        idleObjects.remove(o);
                    }
                } else {
                    // create new instance
                    if (this.getMaxPoolSize() == -1 || (poolMetrics.getPoolSize() < this.getMaxPoolSize()))
                        o = this.create();
                }
                // check validate before borrow
                if (validateBeforeBorrow) {
                    if (o != null && !this.isValid(o)) {
                        this.close(o);
                        o = null;
                    }
                }
                if (o != null)
                    i = maxAttempts;
                else if (i != maxAttempts)
                    Thread.sleep(retryIntervalMillis);
            }
            if (o != null) {
                LOG.debug(o);
                synchronized (inUseObjects) {
                    inUseObjects.put(o, System.currentTimeMillis());
                }
                return o;
            } else {
                throw new PoolException(MESSAGE_001);
            }
        } catch (InterruptedException ie) {
            LOG.debug(Constants.EMPTY, ie);
            this.shutdown();
            throw new PoolException(MESSAGE_001);
        } catch (PoolException pe) {
            throw pe;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new PoolException(e);
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }

    /*
     * Release and return the borrowed object.
     */
    public final synchronized void doReturn(Object o) {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(o);
        if (o != null) {
            synchronized (inUseObjects) {
                inUseObjects.remove(o);
            }
            synchronized (idleObjects) {
                idleObjects.put(o, System.currentTimeMillis());
            }
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }

    /*
     * Gets pool name.
     */
    public String getPoolName() {
        return poolName;
    }

    /*
     * Sets pool name.
     */
    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    /*
     * Check if pool is started
     */
    public final boolean isStarted() {
        return started;
    }

    /*
     * Gets pooling interval in seconds.
     */
    public final int getPoolingInterval() {
        return poolingInterval;
    }

    /*
     * Sets pooling interval in seconds.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setPoolingInterval(int poolingInterval) throws PoolException {
        if (poolingInterval < 1)
            throw new PoolException("Invalid poolingInterval parameter value " + poolingInterval);
        this.poolingInterval = poolingInterval;
        this.poolingIntervalMillis = poolingInterval * 1000;
    }

    /*
     * Gets validate before borrow.
     */
    public final boolean isValidateBeforeBorrow() {
        return validateBeforeBorrow;
    }

    /*
     * Sets validate before borrow. Default is true.
     */
    public final void setValidateBeforeBorrow(boolean validateBeforeBorrow) {
        this.validateBeforeBorrow = validateBeforeBorrow;
    }

    /*
     * Gets minimum number of instances.
     */
    public final long getMinPoolSize() {
        return minPoolSize;
    }

    /*
     * Sets minimum number of instances.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setMinPoolSize(long minPoolSize) throws PoolException {
        if (minPoolSize < 0)
            throw new PoolException("Invalid minPoolSize parameter value " + minPoolSize);
        if (maxPoolSize > 0) {
            if (minPoolSize > maxPoolSize)
                throw new PoolException("Invalid minPoolSize parameter value " + minPoolSize);
        }
        this.minPoolSize = minPoolSize;
    }

    /*
     * Gets maximum accepted number of instances.
     */
    public final long getMaxPoolSize() {
        return maxPoolSize;
    }

    /*
     * Sets maximum pool size.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setMaxPoolSize(long maxPoolSize) throws PoolException {
        if (maxPoolSize < -1 || maxPoolSize == 0)
            throw new PoolException("Invalid maxPoolSize parameter value " + maxPoolSize);
        this.maxPoolSize = maxPoolSize;
    }


    /*
     * Gets pool metrics.
     */
    public final PoolMetrics getPoolMetrics() {
        return this.poolMetrics;
    }

    /*
     * Gets maximum number of attempts.
     */
    public final int getMaxAttempts() {
        return maxAttempts;
    }

    /*
     * Sets maximum number of attempts.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setMaxAttempts(int maxAttempts) throws PoolException {
        if (maxAttempts < 1)
            throw new PoolException("Invalid maxAttempts parameter value " + maxAttempts);
        this.maxAttempts = maxAttempts;
    }

    /*
     * Gets retry interval in seconds to the instance creation.
     */
    public final int getRetryInterval() {
        return retryInterval;
    }

    /*
     * Sets retry interval in seconds to the instance creation.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setRetryInterval(int retryInterval) throws PoolException {
        if (retryInterval < 1)
            throw new PoolException("Invalid retryInterval parameter value " + retryInterval);
        this.retryInterval = retryInterval;
        this.retryIntervalMillis = retryInterval * 1000;
    }

    /*
     * Gets inactivity timeout in seconds.
     */
    public final int getInactivityTimeout() {
        return inactivityTimeout;
    }

    /*
     * Sets inactivity timeout in seconds.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setInactivityTimeout(int inactivityTimeout) throws PoolException {
        if (inactivityTimeout < -1)
            throw new PoolException("Invalid inactivityTimeout parameter value " + inactivityTimeout);
        this.inactivityTimeout = inactivityTimeout;
        this.inactivityTimeoutMillis = inactivityTimeout * 1000;
    }

    /*
     * Gets usage timeout in seconds.
     */
    public final int getUsageTimeout() {
        return usageTimeout;
    }

    /*
     * Sets usage timeout for in use instances.
     *
     * @exception PoolException If the execution fail.
     */
    public final void setUsageTimeout(int usageTimeout) throws PoolException {
        if (usageTimeout < -1)
            throw new PoolException("Invalid usageTimeout parameter value " + usageTimeout);
        this.usageTimeout = usageTimeout;
        this.usageTimeoutMillis = usageTimeout * 1000;
    }

    /*
     * Gets idle objects.
     */
    public final Hashtable getIdleObjects() {
        return idleObjects;
    }

    /*
     * Gets in use objects.
     */
    public final Hashtable getInUseObjects() {
        return inUseObjects;
    }

}
