/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.common;

/**
 * Constants class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public final class Constants {

    // System
    public static final String SYS_FILE_SEPARATOR = System.getProperty("file.separator");
    public static final String SYS_LINE_SEPARATOR = System.getProperty("line.separator");

    // Punctuation
    public static final String EMPTY = "";
    public static final String EQUAL = "=";
    public static final String SPACE = " ";
    public static final String COMMA = ",";
    public static final String DOT = ".";
    public static final String COLON = ":";
    public static final String SEMICOLONS = ";";
    public static final String SLASH = "/";
    public static final String BACKSLASH = "\\";
    public static final String UNDERLINE = "_";
    public static final String DASH = "-";

    // Other
    public static final String ELLIPSIS = "...";
    public static final String CR = "\r";
    public static final String CRLF = "\r\n";

    // Simple events
    public static final String EVENT_INVOKED = "Invoked";
    public static final String EVENT_COMPLETED = "Completed";
    public static final String EVENT_FAILED = "Failed";

    // Base numbers
    public static final String ZERO = "0";
    public static final String ONE = "1";

    // Logger
    public static final String LOGGER_FATAL = "FATAL";
    public static final String LOGGER_ERROR = "ERROR";
    public static final String LOGGER_WARN = "WARN";
    public static final String LOGGER_INFO = "INFO";
    public static final String LOGGER_DEBUG = "DEBUG";



}
