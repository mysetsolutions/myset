/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Various utilities methods for usage.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class CommonUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(CommonUtils.class);

    public CommonUtils() {
        LOG.debug(this);
    }

    /**
     * Formats a string according with pattern.
     *
     * @param src Source object (Date, Integer, Double, Long, BigDecimal ...).
     * @param p   dd-MM-yyyy ... or ###,###,###.###
     * @throws CommonUtilsException If the execution fail.
     */
    public static String format(Object src, String p) throws CommonUtilsException {
        try {
            if (src instanceof Date)
                return new SimpleDateFormat(p).format(src);
            if ((src instanceof Integer) || (src instanceof Long) || (src instanceof Double) || (src instanceof BigDecimal))
                return new DecimalFormat(p).format(src);
            else
                return null;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Formats a String acording with format using Objects array.
     *
     * @param src : My name is {0} and i am {1} years old.
     * @param oa  :Object o[0]="ABC";o[1]="30";
     * @throws CommonUtilsException If the execution fail.
     */
    public static String format(String src, Object[] oa) throws CommonUtilsException {
        try {
            MessageFormat mf = new MessageFormat(src);
            return mf.format(oa);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Formats a String array to a String ex: ('1','2','6') Return null if <i>a</i> is null.
     *
     * @param a         Source
     * @param prefix    Ex:"('"
     * @param suffix    Ex:"')"
     * @param separator Ex: "','"
     * @throws CommonUtilsException If the execution fail.
     */
    public static String format(String[] a, String prefix, String suffix, String separator) throws CommonUtilsException {
        try {
            if (a != null) {
                StringBuffer res = new StringBuffer();
                res.append(prefix);
                for (int i = 0; i < (a.length); i++) {
                    if (a[i] != null) {
                        res.append(a[i]);
                        if (i != a.length - 1)
                            res.append(separator);
                    }
                }
                res.append(suffix);
                return res.toString();
            } else
                return null;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Replaces amp, lt, gt and return null if
     * <i>s</i> is null.
     *
     * @param s Source
     * @throws CommonUtilsException If the execution fail.
     */
    public static String stripTags(String s) throws CommonUtilsException {
        try {
            if (s != null)
                return s.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            else
                return null;

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Replaces " with &quota; Return null if <i>s</i> is null.
     *
     * @param s Source
     * @throws CommonUtilsException If the execution fail.
     */
    public static String stripQuota(String s) throws CommonUtilsException {
        try {
            if (s != null)
                return s.replaceAll("\"", "&quota;");
            else
                return null;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Replaces " with &apos; Return null if <i>s</i> is null.
     *
     * @param s Source
     * @throws CommonUtilsException If the execution fail.
     */
    public static String stripApos(String s) throws CommonUtilsException {
        try {
            if (s != null)
                return s.replaceAll("'", "&apos;");
            else
                return null;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Validates a date from <i>src</i> according with format <i>p</i>.
     *
     * @param src Source
     * @param p   :"dd-MM-yyyy".
     */
    public static boolean isValidDate(String src, String p) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(p);
            sdf.setLenient(false);
            Date resdate = sdf.parse(src);
            if (src.equalsIgnoreCase(sdf.format(resdate))) {
                return true;
            } else
                return false;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            return false;
        }
    }

    /**
     * Returns a date from <i>src</i> according with format <i>p</i>.
     *
     * @param src Source date.
     * @param p   Pattern :"dd-MM-yyyy".
     * @throws CommonUtilsException If the execution fail.
     */
    public static Date getDate(String src, String p) throws CommonUtilsException {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(p);
            sdf.setLenient(false);
            Date resdate = sdf.parse(src);
            if (src.equalsIgnoreCase(sdf.format(resdate))) {
                return resdate;
            } else
                throw new CommonUtilsException("Source '" + src + "' do not match the pattern '" + p + "'");
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Validates a BigDecimal according with default locale settings of computer "separator . or ,"
     *
     * @param number Source
     * @return boolean
     */
    public static boolean isValidBigDecimal(String number) {
        try {
            new BigDecimal(number);
            return true;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            return false;
        }
    }

    /**
     * Returns a BigDecimal Number.
     *
     * @param src Source.
     */
    public static BigDecimal getBigDecimal(String src) throws CommonUtilsException {
        try {
            return new BigDecimal(src);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new CommonUtilsException(e);
        }
    }

    /**
     * Returns a string from an input String[].
     *
     * @param src       Source.
     * @param separator Separator.
     */
    public static String arrayToString(String[] src, String separator) {
        StringBuffer result = new StringBuffer();
        if (src.length > 0) {
            result.append(src[0]);
            for (int i = 1; i < src.length; i++) {
                result.append(separator);
                result.append(src[i]);
            }
        }
        return result.toString();
    }
}