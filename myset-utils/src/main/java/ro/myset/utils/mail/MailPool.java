/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;
import ro.myset.utils.pool.Pool;
import ro.myset.utils.pool.PoolException;

import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.Transport;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Properties;

/**
 * MailPool for mail connections.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class MailPool extends Pool {
    public static final String PROPS_AUTH = "mail.smtp.auth";
    public static final String PROPS_USER = "mail.user";
    public static final String PROPS_PASSWORD = "mail.password";
    public static final String PROPS_TRUE = "true";
    public static final String PROPS_FALSE = "false";
    private static final Logger LOG = LogManager.getFormatterLogger(MailPool.class);
    private String jndiPath = null;
    private Properties properties = null;


    public MailPool() {
        LOG.debug(this);
    }

    public MailPool(String jndiPath) {
        LOG.debug(jndiPath);
        this.jndiPath = jndiPath;
    }

    public MailPool(Properties properties) {
        LOG.debug(properties);
        this.properties = properties;
    }

    public final Session create() throws PoolException {
        long bt = System.currentTimeMillis();
        try {
            Session session = null;
            if (jndiPath != null) {
                Context ctx = new InitialContext();
                session = (Session) ctx.lookup(jndiPath);
            } else {
                if (properties.containsKey(PROPS_AUTH) && properties.getProperty(PROPS_AUTH).equalsIgnoreCase(PROPS_TRUE)) {
                    Authenticator authenticator = new SMTPAuthenticator(properties.getProperty(PROPS_USER),
                            properties.getProperty(PROPS_PASSWORD));
                    session = Session.getInstance(properties, authenticator);
                } else {
                    properties.put(PROPS_AUTH, PROPS_FALSE);
                    session = Session.getInstance(properties);
                }
                Transport transport = session.getTransport();
                transport.connect();
                transport.close();
            }
            LOG.debug(session);
            return session;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new PoolException(e);
        } finally {
            this.getPoolMetrics().setCreateDuration(System.currentTimeMillis() - bt);
        }
    }

    public final boolean isValid(Object o) {
        long bt = System.currentTimeMillis();
        LOG.debug(o);
        try {
            if (o == null || !(o instanceof Session))
                return false;
            else {
                Session session = (Session) o;
                Transport transport = session.getTransport();
                transport.connect();
                if (transport.isConnected()) {
                    transport.close();
                    return true;
                } else {
                    transport.close();
                    return false;
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            return false;
        } finally {
            this.getPoolMetrics().setValidateDuration(System.currentTimeMillis() - bt);
        }
    }

    public final void close(Object o) throws PoolException {
        long bt = System.currentTimeMillis();
        LOG.debug(o);
        try {
            if (o == null || !(o instanceof Session))
                return;
            else {
                Session session = (Session) o;
                Transport transport = session.getTransport();
                transport.connect();
                transport.close();
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new PoolException(e);
        } finally {
            this.getPoolMetrics().setCloseDuration(System.currentTimeMillis() - bt);
        }
    }

    /**
     * Gets jndiPath for mail session
     */
    public final String getJndiPath() {
        return jndiPath;
    }

    /**
     * Sets jndiPath for mail session
     */
    public final void setJndiPath(String jndiPath) {
        this.jndiPath = jndiPath;
    }

    /**
     * Gets connection properties
     */
    public final Properties getProperties() {
        return properties;
    }

    /**
     * Sets connection properties
     */
    public final void setProperties(Properties properties) {
        this.properties = properties;
    }

}
