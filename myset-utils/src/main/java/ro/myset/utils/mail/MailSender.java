/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.util.Properties;

/**
 * MailSender class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class MailSender {
    private static final Logger LOG = LogManager.getFormatterLogger(MailSender.class);
    public static final String HTML_MESSAGE = "text/html";
    public static final String TEXT_MESSAGE = "text/plain";
    public static final String PROPS_AUTH = "mail.smtp.auth";
    public static final String PROPS_USER = "mail.user";
    public static final String PROPS_PASSWORD = "mail.password";
    public static final String PROPS_TRUE = "true";
    public static final String PROPS_FALSE = "false";
    public static final String DEFAULT_MESSAGE_TYPE = "text/html";
    public static final String DEFAULT_MESSAGE_ENCODING = "UTF-8";

    public MailSender() {
        LOG.debug(this);
    }


    /**
     * Gets mail session using properties.
     */
    public final Session getSession(Properties properties) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        Session session = null;
        if (properties.containsKey(PROPS_AUTH) && properties.getProperty(PROPS_AUTH).equalsIgnoreCase(PROPS_TRUE)) {
            Authenticator authenticator = new SMTPAuthenticator(properties.getProperty(PROPS_USER),
                    properties.getProperty(PROPS_PASSWORD));
            session = Session.getInstance(properties, authenticator);
        } else {
            properties.put(PROPS_AUTH, PROPS_FALSE);
            session = Session.getInstance(properties);
        }
        // Check connection
        Transport transport = session.getTransport();
        transport.connect();
        transport.close();
        LOG.debug(Constants.EVENT_COMPLETED);
        return session;
    }

    /**
     * Gets mail session using jndiPath.
     */
    public final Session getSession(String jndiPath) throws Exception {
        LOG.debug(Constants.EVENT_INVOKED);
        LOG.debug(jndiPath);
        Session session = null;
        Context ctx = new InitialContext();
        session = (Session) ctx.lookup(jndiPath);
        LOG.debug(Constants.EVENT_COMPLETED);
        return session;
    }


    /**
     * Gets internet addresses array from comma separated string addresses.
     *
     * @param addresses Comma separated addresses
     * @throws Exception If the execution fail.
     */
    private final InternetAddress[] getInternetAddresses(String addresses) throws Exception {
        if (addresses != null && !Constants.EMPTY.equals(addresses.trim())) {
            String[] addressesArray = addresses.split(Constants.COMMA);
            InternetAddress[] internetAddresses = new InternetAddress[addressesArray.length];
            for (int i = 0; i < addressesArray.length; i++) {
                internetAddresses[i] = new InternetAddress(addressesArray[i]);
            }
            return internetAddresses;
        }
        return null;
    }


    /**
     * Attach file to mime mesage.
     *
     * @param mimeMultipart Message multipart object
     * @param file          File object
     * @throws MailSenderException If the execution fail.
     */
    private final void attachFile(MimeMultipart mimeMultipart, File file) throws MailSenderException {
        LOG.debug(file);
        try {
            if (file == null)
                throw new MailSenderException("File object is null");
            if (!file.exists() || !file.isFile()) {
                throw new MailSenderException("\"" + file.getCanonicalPath() + "\" - File not found");
            }
            MimeBodyPart attachmentPart = new MimeBodyPart();
            DataSource ds = new FileDataSource(file);
            attachmentPart.setDataHandler(new DataHandler(ds));
            attachmentPart.setFileName(ds.getName());
            mimeMultipart.addBodyPart(attachmentPart);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new MailSenderException(e);
        }
    }

    private final void attachFiles(MimeMultipart msgMultipart, String attachments) throws Exception {
        if (attachments != null && !Constants.EMPTY.equals(attachments.trim())) {
            String[] attachmentsArray = attachments.split(Constants.COMMA);
            for (int i = 0; i < attachmentsArray.length; i++) {
                attachFile(msgMultipart, new File(attachmentsArray[i]));
            }
        }
    }


    /**
     * Sends mail.
     *
     * @param messageType     Message type (text/plain or text/html). Default id 'text/plain'.
     * @param messageEncoding Message encoding. Default is 'UTF-8'.
     * @param from            Message from internet address.
     * @param to              Message to internet addresses. Comma separated internet addresses.
     * @param cc              Message cc internet addresses. Comma separated internet addresses.
     * @param bcc             Message bcc internet addresses. Comma separated internet addresses.
     * @param attachments     Message attachments. Comma separated file full paths.
     * @param subject         Message subject.
     * @param content         Message content.
     * @throws MailSenderException If the execution fail.
     */
    public final void send(Session session,
                           String messageType,
                           String messageEncoding,
                           String from,
                           String to,
                           String cc,
                           String bcc,
                           String attachments,
                           String subject,
                           String content
    ) throws MailSenderException {
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            LOG.debug("%s,%s,%s,%s,%s,%s,%s,%s,%s", session, messageType, messageEncoding, from, to, cc, bcc, attachments, subject);
            // Message
            Message message = new MimeMessage(session);
            if (messageType == null)
                messageType = DEFAULT_MESSAGE_TYPE;
            if (messageEncoding == null)
                messageEncoding = DEFAULT_MESSAGE_ENCODING;
            if (from != null)
                message.setFrom(new InternetAddress(from));
            if (to != null)
                message.setRecipients(Message.RecipientType.TO, getInternetAddresses(to));
            if (cc != null)
                message.setRecipients(Message.RecipientType.CC, getInternetAddresses(cc));
            if (bcc != null)
                message.setRecipients(Message.RecipientType.BCC, getInternetAddresses(bcc));
            if (subject != null)
                message.setSubject(subject);
            // message body
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            if (content != null)
                messageBodyPart.setText(content, messageEncoding);
            messageBodyPart.setHeader("Content-Type", "" + messageType + "; charset=\"" + messageEncoding + "\"");
            messageBodyPart.setHeader("Content-Transfer-Encoding", "quoted-printable");
            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(messageBodyPart);
            //message attachments
            if (attachments != null)
                attachFiles(mimeMultipart, attachments);
            // message all
            message.setContent(mimeMultipart);
            // message send
            session.getTransport().send(message);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new MailSenderException(e);
        }
        LOG.debug(Constants.EVENT_COMPLETED);
    }

}