/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.util.UUID;

/**
 * UUID Utilities See RFC 4122
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class UUIDUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(UUIDUtils.class);

    /**
     * Generate UUID Version 3 using a namespace.
     *
     * @param namespace Namespace string value.
     */
    public static final String generate(String namespace) {
        LOG.debug(namespace);
        return UUID.nameUUIDFromBytes(namespace.getBytes()).toString();
    }

    /**
     * Generate UUID Version 3 using namespace concatenated with "-" and System.nanoTime()
     *
     * @param namespace Namespace string value.
     */
    public static final String generateUsingTime(String namespace) {
        LOG.debug(namespace);
        return UUID.nameUUIDFromBytes((namespace + "-" + System.nanoTime()).getBytes()).toString();
    }

    /**
     * Generate UUID Version 4 randomly.
     */
    public static final String generate() {
        LOG.debug(Constants.EMPTY);
        return UUID.randomUUID().toString();
    }

    /**
     * Parse UUID string and return an UUID object.
     */
    public static final UUID parse(String uuidStringValue) {
        LOG.debug(uuidStringValue);
        return UUID.fromString(uuidStringValue);
    }

}
