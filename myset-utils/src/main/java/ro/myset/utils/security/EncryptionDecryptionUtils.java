/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 * EncryptionDecryption Utilities
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.1
 */
public class EncryptionDecryptionUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(EncryptionDecryptionUtils.class);

    /**
     * Encrypt text
     *
     * @param algorithm Algorithm name (according with SDK Cipher Algorithm Names).
     * @param key       Algorithm key.
     * @param text      Plain text.
     * @throws Exception If the execution fail.
     */
    public static String encrypt(String algorithm, String key, String text) throws Exception {
        Key algorithmKey = generateKey(algorithm, key);
        Cipher chiper = Cipher.getInstance(algorithm);
        chiper.init(Cipher.ENCRYPT_MODE, algorithmKey);
        byte[] encVal = chiper.doFinal(text.getBytes());
        String encryptedValue = Base64.getEncoder().encodeToString(encVal);
        return encryptedValue;
    }


    /**
     * Decrypt text
     *
     * @param algorithm Algorithm name (according with SDK Cipher Algorithm Names).
     * @param key       Algorithm key.
     * @param text      Encrypted text.
     * @throws Exception If the execution fail.
     */
    public static String decrypt(String algorithm, String key, String text) throws Exception {
        Key algorithmKey = generateKey(algorithm, key);
        Cipher chiper = Cipher.getInstance(algorithm);
        chiper.init(Cipher.DECRYPT_MODE, algorithmKey);
        byte[] decodedValue = Base64.getDecoder().decode(text);
        byte[] decValue = chiper.doFinal(decodedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }


    /**
     * Generate algorithm key object
     *
     * @param algorithm Algorithm name.
     * @param key       Algorithm key.
     * @throws Exception If the execution fail.
     */
    private static Key generateKey(String algorithm, String key) throws Exception {
        return new SecretKeySpec(key.getBytes(), algorithm);
    }

}
