/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.SecureRandom;

/**
 * Random Utilities
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class RandomUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(RandomUtils.class);


    /**
     * Generate random string
     *
     * @param len Output string length
     * @throws Exception If the execution fail.
     */
    public static final String randomString(int len) {
        StringBuilder result = new StringBuilder(len);
        String source = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom secureRandom = new SecureRandom();
        for (int i = 0; i < len; i++)
            result.append(source.charAt(secureRandom.nextInt(source.length())));
        return result.toString();
    }

    /**
     * Generate random string
     *
     * @param source Source string
     * @param len    Output string length
     * @throws Exception If the execution fail.
     */
    public static final String randomString(String source, int len) {
        StringBuilder result = new StringBuilder(len);
        SecureRandom secureRandom = new SecureRandom();
        for (int i = 0; i < len; i++)
            result.append(source.charAt(secureRandom.nextInt(source.length())));
        return result.toString();
    }
}
