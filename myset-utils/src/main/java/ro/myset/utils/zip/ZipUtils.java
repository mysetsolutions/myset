/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.zip;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;
import ro.myset.utils.disk.DiskUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Zip utilities class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.2
 */
public class ZipUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(ZipUtils.class);

    private static final int BUFFER_SIZE = 2048;
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final String EMPTY_STRING = "";
    private static final String DOT = ".";
    private static final String SLASH = "/";
    private static final String BACKSLASH = "\\";
    private static final String BACKSLASH_REGEXP = "\\\\";

    /**
     * Gets archive entries from source.
     *
     * @param source Source archive file.
     * @throws ZipUtilsException If the execution fail.
     */
    public static final ArrayList getZipEntries(File source) throws ZipUtilsException {
        ZipFile zipFile = null;
        try {
            if (source == null || source.isDirectory())
                throw new ZipUtilsException("Invalid source file");
            ArrayList entries = new ArrayList();
            zipFile = new ZipFile(source);
            ZipEntry zipEntry = null;
            Enumeration e = zipFile.entries();
            while (e.hasMoreElements()) {
                zipEntry = (ZipEntry) e.nextElement();
                entries.add(zipEntry);
            }
            return entries;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new ZipUtilsException(e);
        } finally {
            try {
                zipFile.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
        }
    }

    /**
     * Extract archive entry from source to destination.
     *
     * @param source      Source archive file.
     * @param destination Destination directory.
     * @param overwrite   Overwrite file or directory if exists.
     * @throws ZipUtilsException If the execution fail.
     */
    public static final void extractEntry(File source, ZipEntry zipEntry, File destination, boolean overwrite) throws ZipUtilsException {
        ZipFile zipFile = null;
        BufferedInputStream bis = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            // check parameters
            if (source == null || source.isDirectory())
                throw new ZipUtilsException("Invalid source file");
            if (zipEntry == null)
                throw new ZipUtilsException("Invalid entry");
            if (destination == null || destination.isFile())
                throw new ZipUtilsException("Invalid destination directory");
            // ZipUtils.dumpEntry(zipEntry);
            // ZipUtils.dumpEntry(zipEntry);
            // read archive content
            zipFile = new ZipFile(source);
            // extract entry
            if (zipEntry.isDirectory()) {
                // make folder to destination
                // LOG.debug("Make folder %s", destination.getCanonicalPath() + FILE_SEPARATOR + zipEntry.getName());
                DiskUtils.mkdir(destination.getCanonicalPath() + FILE_SEPARATOR + zipEntry.getName(), overwrite);
            } else {
                // write file to destination
                bis = new BufferedInputStream(zipFile.getInputStream(zipEntry));
                //LOG.debug("Make file %s", destination.getCanonicalPath() + FILE_SEPARATOR + zipEntry.getName());
                File destFile = new File(destination.getCanonicalPath() + FILE_SEPARATOR + zipEntry.getName());
                if (!destFile.getParentFile().exists()) {
                    //LOG.debug("Make folder %s", destFile.getParentFile().getCanonicalPath());
                    DiskUtils.mkdir(destFile.getParentFile().getCanonicalPath(), overwrite);
                }
                fos = new FileOutputStream(destFile);
                bos = new BufferedOutputStream(fos);
                byte[] buff = new byte[8096];
                int bufferUsed = 0;
                while ((bufferUsed = bis.read(buff)) != -1) {
                    bos.write(buff, 0, bufferUsed);
                    bos.flush();
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new ZipUtilsException(e);
        } finally {
            // LOG.debug("finally");
            try {
                if (zipFile != null)
                    zipFile.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
            try {
                if (bis != null)
                    bis.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
            try {
                if (fos != null)
                    fos.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
            try {
                if (bos != null)
                    bos.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
        }

    }

    /**
     * Extract archive entry from source to destination.
     *
     * @param source      Source archive file.
     * @param destination Destination file.
     * @param overwrite   Overwrite file or directory if exists.
     * @throws ZipUtilsException If the execution fail.
     */
    public static final void extractEntryFile(File source, ZipEntry zipEntry, File destination, boolean overwrite) throws ZipUtilsException {
        ZipFile zipFile = null;
        BufferedInputStream bis = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            // check parameters
            if (source == null || source.isDirectory())
                throw new ZipUtilsException("Invalid source file");
            if (zipEntry == null || zipEntry.isDirectory())
                throw new ZipUtilsException("Invalid entry");
            // ZipUtils.dumpEntry(zipEntry);
            // read archive content
            zipFile = new ZipFile(source);
            // extract entry
            if (!zipEntry.isDirectory()) {
                // write file to destination
                bis = new BufferedInputStream(zipFile.getInputStream(zipEntry));
                //LOG.debug("Make file %s", destination.getCanonicalPath());
                fos = new FileOutputStream(destination);
                bos = new BufferedOutputStream(fos);
                byte[] buff = new byte[8096];
                int bufferUsed = 0;
                while ((bufferUsed = bis.read(buff)) != -1) {
                    bos.write(buff, 0, bufferUsed);
                    bos.flush();
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new ZipUtilsException(e);
        } finally {
            // LOG.debug("finally");
            try {
                if (zipFile != null)
                    zipFile.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
            try {
                if (bis != null)
                    bis.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
            try {
                if (fos != null)
                    fos.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
            try {
                if (bos != null)
                    bos.close();
            } catch (Exception e) {
                LOG.error(Constants.EMPTY, e);
            }
        }

    }


    /**
     * Extract all archive entries from source to destination.
     *
     * @param source      Source archive file.
     * @param destination Destination directory.
     * @param overwrite   Overwrite file or directory if exists.
     * @throws ZipUtilsException If the execution fail.
     */
    public static void extractAll(File source, File destination, boolean overwrite) throws ZipUtilsException {
        try {
            if (source == null || source.isDirectory())
                throw new ZipUtilsException("Invalid source file");
            ArrayList entries = ZipUtils.getZipEntries(source);
            ZipEntry zipEntry = null;
            for (int i = 0; i < entries.size(); i++) {
                zipEntry = (ZipEntry) entries.get(i);
                ZipUtils.extractEntry(source, zipEntry, destination, overwrite);
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new ZipUtilsException(e);
        }
    }

    /**
     * Dump on STDOUT information about zip entry (debugging purposes)
     *
     * @throws ZipUtilsException If the execution fail.
     */
    public static void dumpEntry(ZipEntry zipEntry) {
        if (zipEntry != null && zipEntry instanceof ZipEntry) {
            LOG.debug("===========================");
            LOG.debug("isDirectory:" + zipEntry.isDirectory());
            LOG.debug("getName:" + zipEntry.getName());
            LOG.debug("getComment:" + zipEntry.getComment());
            LOG.debug("getCompressedSize:" + zipEntry.getCompressedSize());
            LOG.debug("getSize:" + zipEntry.getSize());
            LOG.debug("getTime:" + zipEntry.getTime());
            LOG.debug("getCrc:" + zipEntry.getCrc());
            LOG.debug("===========================");
        } else
            LOG.debug("Invalid zipEntry " + zipEntry);
    }

    /**
     * Create archive from multiple source files or folders.
     *
     * @param files     Source files or folders.
     * @param dest      Destination archive file.
     * @param overwrite Overwrite or not.
     * @throws ZipUtilsException If the execution fail.
     */
    public final void createArchive(String[] files, String dest, boolean overwrite) throws ZipUtilsException {
        try {
            if (files == null)
                throw new ZipUtilsException("Invalid source files");
            if (dest != null && new File(dest).exists() && !overwrite)
                throw new ZipUtilsException(dest + " - Already exist and can not be overwritten.");
            File destFile = new File(dest);
            //LOG.debug("%s,%s",dest,zipRoot);
            FileOutputStream fos = new FileOutputStream(dest);
            ZipOutputStream zos = new ZipOutputStream(fos);
            // add files into archive
            File f = null;
            for (int i = 0; i < files.length; i++) {
                f = new File(files[i]);
                addFileToArchive(zos, f, f);
            }
            zos.close();

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new ZipUtilsException(e);
        }
    }

    /**
     * Adds file into archive
     *
     * @throws ZipUtilsException If the execution fail.
     */
    private final void addFileToArchive(ZipOutputStream zos, File path, File src) throws ZipUtilsException {
        try {
            if (zos == null || src == null)
                return;
            ZipEntry zipEntry = null;
            //LOG.debug("path:%s, src:%s", path, src);
            if (src.isDirectory()) {
                // add entry
                zipEntry = new ZipEntry(getZipEntryPath(path, src) + SLASH);
                zos.putNextEntry(zipEntry);
                //dumpEntry(zipEntry);
                // list folder files
                String[] files = src.list();
                File f = null;
                for (int i = 0; i < files.length; i++) {
                    f = new File(src.getCanonicalPath() + FILE_SEPARATOR + files[i]);
                    // recurse on folders
                    addFileToArchive(zos, path, f);
                }
            }
            if (src.isFile()) {
                // read file
                byte data[] = new byte[BUFFER_SIZE];
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(src), BUFFER_SIZE);
                // add entry
                zipEntry = new ZipEntry(getZipEntryPath(path, src));
                zos.putNextEntry(zipEntry);
                //dumpEntry(zipEntry);
                int l;
                while ((l = bis.read(data, 0, BUFFER_SIZE)) > 0) {
                    zos.write(data, 0, l);
                }
                bis.close();
            }
            zos.closeEntry();

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new ZipUtilsException(e);
        }

    }


    /**
     * Calculate zip entry path
     *
     * @throws ZipUtilsException If the execution fail.
     */
    private final String getZipEntryPath(File path, File src) throws Exception {
        //LOG.debug("path:%s, src:%s", path, src);
        String zipEntryPath = null;
        if (path != null && src != null) {
            String npath = path.getCanonicalPath().replace(BACKSLASH, SLASH);
            String nsrc = src.getCanonicalPath().replace(BACKSLASH, SLASH);
            Matcher m = Pattern.compile("(" + npath + ")(.*)").matcher(nsrc);
            if (m.matches()) {
                int lastIndexOfPathSeparator = path.getCanonicalPath().lastIndexOf(SLASH);
                zipEntryPath = npath.substring(lastIndexOfPathSeparator + 1) + m.group(2);
            }
        }
        LOG.debug(zipEntryPath);
        return zipEntryPath;
    }

}
