/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.disk;

/**
 * Class for handling DiskUtils exceptions.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class DiskUtilsException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with the specified message.
     *
     * @param msg The text message
     */
    public DiskUtilsException(String msg) {
        super(msg);
    }

    /**
     * Constructs a new exception using text message and StackTrace values of
     * input Exception object.
     *
     * @param e The source exception
     */
    public DiskUtilsException(Exception e) {
        super(e.getLocalizedMessage());
        super.setStackTrace(e.getStackTrace());
    }
}
