/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.disk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.CommonUtils;
import ro.myset.utils.common.Constants;

import java.io.*;
import java.util.Date;

/**
 * Disk utilities class. This read, write, delete, copy, move or rename files and folders.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class DiskUtils {
    protected static final String FILE_SEPARATOR = System.getProperty("file.separator");
    protected static final String LINE_SEPARATOR = System.getProperty("line.separator");
    protected static final String DUPLICATE_DATE_PATTERN = "yyyyMMddHHmmssSSS";
    private static final Logger LOG = LogManager.getFormatterLogger(DiskUtils.class);

    /**
     * Checks if directory exists and return an File object.
     *
     * @param path /myfolder/
     * @throws DiskUtilsException If the execution fail.
     */
    public static File getDirectory(String path) throws DiskUtilsException {
        if (path == null) {
            throw new DiskUtilsException("\"" + path + "\" - Directory not found");
        } else {
            File f = new File(path);
            if (!f.exists() || !f.isDirectory()) {
                throw new DiskUtilsException("\"" + path + "\" - Directory not found");
            }
            return f;
        }

    }

    /**
     * Checks if file exists and return an File object.
     *
     * @param path /myfolder/myfile.txt
     * @throws DiskUtilsException If the execution fail.
     */
    public static File getFile(String path) throws DiskUtilsException {
        if (path == null) {
            throw new DiskUtilsException("\"" + path + "\" - File not found");
        } else {
            File f = new File(path);
            if (!f.exists() || !f.isFile()) {
                throw new DiskUtilsException("\"" + path + "\" - File not found");
            }
            return f;
        }

    }

    /**
     * Gets file extension
     *
     * @param fileName File name
     * @throws DiskUtilsException If the execution fail.
     */
    public static String getFileExtension(String fileName) throws DiskUtilsException {
        if (fileName == null) {
            throw new DiskUtilsException("\"" + fileName + "\" - Invalid file name");
        } else {
            int dotIndex = fileName.lastIndexOf(".");
            return (dotIndex >= 0 && dotIndex < fileName.length()) ? fileName.substring(dotIndex + 1) : null;
        }

    }

    /**
     * Parse path and return the absolute path like String object. The last character within result
     * path is file separator loaded from System.getProperty("file.separator") object. <br>
     *
     * @param path Source path.
     * @throws DiskUtilsException If the execution fail.
     */
    public static String parsePath(String path) throws DiskUtilsException {
        try {
            File p = getDirectory(path);
            path = p.getCanonicalPath();
            String lc = path.substring(path.length() - 1);
            if (!lc.equals("/") && !lc.equals("\\"))
                path += FILE_SEPARATOR;
            return path;
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Writes text into file. If file was not found will be created a new one.
     *
     * @param filefullpath /mypath/myfile.txt
     * @param text         Inserted text.
     * @param append       Append into file
     * @throws DiskUtilsException If the execution fail.
     */
    public static void writeIntoFile(String filefullpath, String text, Boolean append) throws DiskUtilsException {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(filefullpath, append));
            bw.write(text);
            bw.close();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Append text into file. If file was not found will be created a new one.
     *
     * @param filefullpath /mypath/myfile.txt
     * @param text         Inserted text.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void writeIntoFile(String filefullpath, String text) throws DiskUtilsException {
        writeIntoFile(filefullpath, text, true);
    }

    /**
     * Append line into file. Line is delimited by System.getProperty("line.separator"). Create new
     * file if file was not found.
     *
     * @param filefullpath /mypath/myfile.txt
     * @param text         Line text.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void writeLineIntoFile(String filefullpath, String text) throws DiskUtilsException {
        writeIntoFile(filefullpath, text + LINE_SEPARATOR);
    }

    /**
     * Reads file content as ByteArrayOutputStream.
     *
     * @param filefullpath /myfolder/myfile.txt
     * @throws DiskUtilsException If the execution fail.
     */
    public static ByteArrayOutputStream readFileContent(String filefullpath) throws DiskUtilsException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileInputStream fileStream = new FileInputStream(getFile(filefullpath));
            final int BUFF_SIZE = 2048;
            byte[] buf = new byte[BUFF_SIZE];
            int l = 0;
            while ((l = fileStream.read(buf)) > 0) {
                baos.write(buf, 0, l);
            }
            return baos;
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Read file content as ByteArrayOutputStream from offset.
     *
     * @param filefullpath /myfolder/myfile.txt
     * @param fromOffset   From bytes offset
     * @throws DiskUtilsException If the execution fail.
     */
    public static ByteArrayOutputStream readFileContent(String filefullpath, long fromOffset) throws DiskUtilsException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileInputStream fileStream = new FileInputStream(getFile(filefullpath));
            LOG.info(fileStream.available());
            if (fromOffset >= 0)
                fileStream.skip(fromOffset);
            final int BUFF_SIZE = 2048;
            byte[] buf = new byte[BUFF_SIZE];
            int l = 0;
            while ((l = fileStream.read(buf)) > 0) {
                baos.write(buf, 0, l);
            }
            return baos;
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }


    /**
     * Makes directory.
     *
     * @param path      /myfolder/
     * @param overwrite Overwrite if exists without clear content.
     * @throws DiskUtilsException If the execution fail.
     */
    public static File mkdir(String path, boolean overwrite) throws DiskUtilsException {
        try {
            File dir = new File(path);
            if (!dir.mkdirs() && !overwrite)
                throw new DiskUtilsException("\"" + path + "\" - Already exist and can not be overwritten.");
            return dir;
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Lists directory content as File[].
     *
     * @param path /myfolder/
     * @throws DiskUtilsException If the execution fail.
     */
    public static File[] list(String path) throws DiskUtilsException {
        try {
            File dir = getDirectory(path);
            return dir.listFiles();
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }


    /**
     * Copy file from source to destination.
     *
     * @param src       Source file.
     * @param dest      Destination file.
     * @param overwrite Overwrite file if exists.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void copyFile(File src, File dest, boolean overwrite) throws DiskUtilsException {
        try {
            //LOG.debug("%s,%s", src, dest);
            if ((src == null) || !(src instanceof File) || src.isDirectory())
                throw new DiskUtilsException("\"" + src.getCanonicalPath() + "\" - Invalid file source file.");
            if ((dest == null) || !(dest instanceof File) || dest.isDirectory())
                throw new DiskUtilsException("\"" + dest.getCanonicalPath() + "\" - Invalid destination file.");
            BufferedInputStream fis = new BufferedInputStream(new FileInputStream(src));
            BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(dest));
            int c;
            while ((c = fis.read()) >= 0)
                fos.write(c);
            fis.close();
            fos.close();
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }


    /**
     * Copy recursive from source to destination.
     *
     * @param src       Source file or directory.
     * @param dest      Destination directory.
     * @param overwrite Overwrite file or directory if exists.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void copy(File src, File dest, boolean overwrite) throws DiskUtilsException {
        try {
            //LOG.debug("%s,%s", src, dest);
            if (!dest.isDirectory())
                throw new DiskUtilsException("\"" + dest.getCanonicalPath() + "\" - Invalid destination.");
            if (src.isDirectory()) {
                File newDest = DiskUtils.mkdir(dest.getCanonicalPath() + FILE_SEPARATOR + src.getName(), overwrite);
                if (newDest.exists() && overwrite && src.getParentFile().getCanonicalPath().equals(dest.getCanonicalPath())) {
                    newDest = DiskUtils.renameFileIfExists(newDest);
                    if (!newDest.exists())
                        DiskUtils.mkdir(newDest.getCanonicalPath(), overwrite);
                }
                String list[] = src.list();
                for (int i = 0; i < list.length; i++) {
                    File fsrc = new File(src.getPath() + FILE_SEPARATOR + list[i]);
                    copy(fsrc, newDest, overwrite);
                }
            } else {
                File newDest = new File(dest, src.getName());
                if (newDest.exists() && overwrite && src.getParentFile().getCanonicalPath().equals(dest.getCanonicalPath())) {
                    newDest = DiskUtils.renameFileIfExists(newDest);
                }
                if (newDest.exists() && !overwrite)
                    throw new DiskUtilsException("\"" + newDest.getCanonicalPath() + "\" - Already exist and can not be overwritten.");
                BufferedInputStream fis = new BufferedInputStream(new FileInputStream(src));
                BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(newDest));
                int c;
                while ((c = fis.read()) >= 0)
                    fos.write(c);
                fis.close();
                fos.close();
            }
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }


    private static final File renameFileIfExists(File file) throws Exception {
        File checkFile = file;
        if (file.exists()) {
            checkFile = new File(file.getParentFile(), file.getName() + " (copy " + CommonUtils.format(new Date(), DUPLICATE_DATE_PATTERN) + ")");
            if (checkFile.exists())
                return DiskUtils.renameFileIfExists(file);
        }
        return checkFile;
    }


    /**
     * Delete files recursive from directory and subdirectories content. After completion it will
     * remain all subdirectories with empty contents.
     *
     * @param src Source directory.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void delete(File src) throws DiskUtilsException {
        try {
            if (src.isDirectory()) {
                String list[] = src.list();
                for (int i = 0; i < list.length; i++) {
                    File fsrc = new File(src.getPath() + FILE_SEPARATOR + list[i]);
                    delete(fsrc);
                }
            } else {
                if (!src.exists())
                    throw new DiskUtilsException("\"" + src.getCanonicalPath() + "\" - File not found.");
                if (!src.delete())
                    throw new DiskUtilsException("\"" + src.getCanonicalPath() + "\" - File coud not be deleted.");
            }
        } catch (DiskUtilsException due) {
            LOG.error(Constants.EMPTY, due);
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Delete recursive directory and subdirectories content including the directories its selfs.
     *
     * @param src Source directory.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void deleteDirectory(File src) throws DiskUtilsException {
        try {
            if (src.isDirectory()) {
                delete(src);
                String list[] = src.list();
                for (int i = 0; i < list.length; i++) {
                    File fsrc = new File(src.getPath() + FILE_SEPARATOR + list[i]);
                    deleteDirectory(fsrc);
                }
                src.delete();
            }
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Move content from source file object to destination file object.
     *
     * @param src       Source directory.
     * @param dest      Destination directory.
     * @param overwrite Overwrite file or directory if exists.
     * @throws DiskUtilsException If the execution fail.
     */
    public static void move(File src, File dest, boolean overwrite) throws DiskUtilsException {
        try {
            if (src.isFile()) {
                if (!src.getParentFile().getCanonicalPath().equals(dest.getCanonicalPath())) {
                    copy(src, dest, overwrite);
                    delete(src);
                }
            }
            if (src.isDirectory()) {
                if (!src.getCanonicalPath().equals(dest.getCanonicalPath())) {
                    copy(src, dest, overwrite);
                    deleteDirectory(src);
                }
            }
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Calculate folder size in bytes.
     *
     * @param src Source directory.
     * @throws DiskUtilsException If the execution fail.
     */
    public static final long getFolderSize(File src) throws DiskUtilsException {
        try {
            if (src == null)
                throw new DiskUtilsException("Directory is null");
            if (!src.exists() || !src.isDirectory())
                throw new DiskUtilsException("\"" + src.getCanonicalPath() + "\" - Directory not found");
            long size = src.length();
            File[] fa = src.listFiles();
            for (int i = 0; i < fa.length; i++) {
                if (fa[i].isFile())
                    size += fa[i].length();
                else
                    size += getFolderSize(fa[i]);
            }
            return size;
        } catch (DiskUtilsException due) {
            throw due;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new DiskUtilsException(e);
        }
    }

    /**
     * Calculate file size using sizeUnit (bits, Kilobytes, Megabytes, Gigabytes, Terrabytes)
     * and evaluating by 1000.
     *
     * @param src            Source file or folder.
     * @param sizeMultiplier Size multiplier (Ex. 1024 or 1000).
     * @throws DiskUtilsException If the execution fail.
     */
    public static final double fileSizeValue(File src, double sizeMultiplier) throws Exception {
        double fileSize = 0;
        if (!src.exists())
            return fileSize;
        if (src.isFile())
            fileSize = src.length();
        if (src.isDirectory())
            fileSize = DiskUtils.getFolderSize(src);
        //convert
        if (fileSize < 1000)
            // bits
            return fileSize;
        else if (fileSize < 1000 * sizeMultiplier)
            // Kilobytes
            return fileSize / 1024;
        else if (fileSize < 1000 * sizeMultiplier * sizeMultiplier)
            // Megabytes
            return fileSize / (sizeMultiplier * sizeMultiplier);
        else if (fileSize < 1000 * sizeMultiplier * sizeMultiplier * sizeMultiplier)
            // Gigabytes
            return fileSize / (sizeMultiplier * sizeMultiplier * sizeMultiplier);
        else
            // Terrabytes
            return fileSize / (sizeMultiplier * sizeMultiplier * sizeMultiplier * sizeMultiplier);
    }

    /**
     * Calculate file size unit using sizeUnit (bits, Kilobytes, Megabytes, Gigabytes, Terrabytes)
     * and evaluating by 1000.
     *
     * @param src            Source file or folder.
     * @param sizeMultiplier Size multiplier (Ex. 1024 or 1000).
     * @param format         Five comma separated text values (Ex. b,Kb,Mb,Gb,Tb)
     * @throws DiskUtilsException If the execution fail.
     */

    public static final String fileSizeUnit(File src, double sizeMultiplier, String format) throws Exception {
        String[] formats = format.split(Constants.COMMA);
        double fileSize = 0;
        if (!src.exists())
            return formats[0];
        if (src.isFile())
            fileSize = src.length();
        if (src.isDirectory())
            fileSize = DiskUtils.getFolderSize(src);
        //convert
        if (fileSize < 1000)
            // bits
            return formats[0];
        else if (fileSize < 1000 * sizeMultiplier)
            // Kilobytes
            return formats[1];
        else if (fileSize < 1000 * sizeMultiplier * sizeMultiplier)
            // Megabytes
            return formats[2];
        else if (fileSize < 1000 * sizeMultiplier * sizeMultiplier * sizeMultiplier)
            // Gigabytes
            return formats[3];
        else
            // Terrabytes
            return formats[4];
    }


}
