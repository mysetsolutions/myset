/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.classloader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;

/**
 * InternalClassLoader is an extender for URLClassLoader.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class InternalClassLoader extends URLClassLoader {
    private static final Logger LOG = LogManager.getFormatterLogger(InternalClassLoader.class);
    private static final String FILE_PROTOCOL = "file:///";
    private String name;

    /**
     * Create a new instance of InternalClassLoader using the default delegation parent ClassLoader
     *
     * @param name Classloader name
     */
    public InternalClassLoader(String name) {
        super(new URL[0]);
        LOG.debug("%s:%s", this, name);
        this.name = name;
    }

    /**
     * Create a new instance of InternalClassLoader using a specific delegation parent ClassLoader
     *
     * @param name   Classloader name
     * @param parent Delegation parent ClassLoader
     */
    public InternalClassLoader(String name, ClassLoader parent) {
        super(new URL[0], parent);
        LOG.debug("%s:%s:%s", this, parent, name);
        this.name = name;
    }

    /**
     * Create a new instance of InternalClassLoader using a specific delegation parent ClassLoader and
     * URLStreamHandlerFactory
     *
     * @param name    Classloader name
     * @param parent  Delegation parent ClassLoader
     * @param factory The URLStreamHandlerFactory to use when creating URLs
     */
    public InternalClassLoader(String name, ClassLoader parent, URLStreamHandlerFactory factory) {
        super(new URL[0], parent, factory);
        LOG.debug("%s:%s:%s:%s", this, parent, factory, name);
        this.name = name;
    }

    /**
     * Load (append) dynamically file from URL to InternalClassLoader instance
     *
     * @param file File to append
     * @throws InternalClassLoaderException If the execution fail.
     */
    public void loadFile(URL file) throws InternalClassLoaderException {
        LOG.debug(file);
        try {
            file.openConnection().connect();
            addURL(file);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new InternalClassLoaderException(e);
        }
    }

    /**
     * Load (append) dynamically directory content to InternalClassLoader instance
     *
     * @param f       Directory source
     * @param recurse Recurse on childs directories
     * @throws InternalClassLoaderException If the execution fail.
     */
    public void loadDirectory(File f, boolean recurse) throws InternalClassLoaderException {
        LOG.debug("%s,%s", f, recurse);
        try {
            // System.out.println(f.getCanonicalPath());
            if (f == null || !f.exists() || !f.isDirectory())
                throw new InternalClassLoaderException("Directory not found at " + f.getCanonicalPath());
            File[] fileArray = f.listFiles();
            File cf;
            for (int i = 0; i < fileArray.length; i++) {
                cf = fileArray[i];
                if (cf.isFile())
                    this.loadFile(cf);
                else if (recurse)
                    this.loadDirectory(cf, recurse);
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new InternalClassLoaderException(e);
        }
    }

    /**
     * Load (append) dynamically file to InternalClassLoader instance
     *
     * @param f File to append
     * @throws InternalClassLoaderException If the execution fail.
     */
    public void loadFile(File f) throws InternalClassLoaderException {
        try {
            // System.out.println(f.getCanonicalPath());
            if (f == null || !f.exists() || !f.isFile())
                throw new InternalClassLoaderException("File not found at " + f.getCanonicalPath());
            addURL(new URL(InternalClassLoader.FILE_PROTOCOL + f.getCanonicalPath()));
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new InternalClassLoaderException(e);
        }
    }

    /**
     * Gets the array of URLs associated with InternalClassLoader instance.
     */
    public URL[] listURLs() {
        return getURLs();
    }

    /**
     * Lists to console InternalClassLoader content.
     */
    public void listContent() {
        LOG.debug("ClassLoader %s (%s) content:", name, this);
        for (int i = 0; i < getURLs().length; i++) {
            LOG.debug("%s", name, getURLs()[i]);
        }
    }

    /**
     * Check if class is loaded into InternalClassLoader instance.
     *
     * @param name Class name
     */
    public boolean isLoaded(String name) {
        LOG.debug("%s", name);
        try {
            if (loadClass(name) != null)
                return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Gets classloader name.
     */
    public String getName() {
        return name;
    }
}
