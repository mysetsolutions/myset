/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerFactory;

/**
 * Common parameters class for SAX parser.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class XmlParameters {
    /**
     * Constants.
     */
    public static final int EVENT_SOURCE_OTHER = 0;
    public static final int EVENT_SOURCE_START_ELEMENT = 1;
    public static final int EVENT_SOURCE_END_ELEMENT = 2;

    /**
     * Default constructor.
     */
    public XmlParameters() {
    }

    /**
     * Creates a new instance of default JVM SAX parser
     *
     * @throws SAXException If the execution fail.
     */
    public static XMLReader getSAXParser() throws SAXException, ParserConfigurationException {
        // XMLReader parser =
        // XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        SAXParser sp = spf.newSAXParser();
        XMLReader sr = sp.getXMLReader();
        return sr;
    }

    /**
     * Creates a new instance of SAX parser for validating
     *
     * @throws SAXException If the execution fail.
     */
    public static XMLReader getSAXParserValidator() throws SAXException, ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        spf.setValidating(true);
        spf.setFeature("http://xml.org/sax/features/validation", true);
        spf.setFeature("http://apache.org/xml/features/validation/schema", true);
        spf.setFeature("http://apache.org/xml/features/validation/schema-full-checking", true);
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        SAXParser sp = spf.newSAXParser();
        XMLReader sr = sp.getXMLReader();
        return sr;

    }

    /**
     * Creates a new instance of TransformerFactory
     *
     * @throws Exception If the execution fail.
     */
    public static TransformerFactory getTransformerFactory() throws Exception {
        // System.setProperty("javax.xml.transform.TransformerFactory","org.apache.xalan.processor.TransformerFactoryImpl");
        TransformerFactory tf = TransformerFactory.newInstance();
        return tf;
    }

}
