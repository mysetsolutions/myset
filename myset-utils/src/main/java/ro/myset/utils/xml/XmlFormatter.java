/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;
import ro.myset.utils.common.Constants;

import java.io.*;
import java.util.ArrayList;


/**
 * Format XML files using SAX parser by passing stream through an intermediate
 * temporary file.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */

public class XmlFormatter extends DefaultHandler implements ContentHandler, LexicalHandler {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlFormatter.class);
    /**
     * The current value.
     */
    private String value;

    /**
     * Filters
     */
    private ArrayList<XmlFilterBean> filters;

    /**
     * The line separator.
     */
    private String lineSeparator = System.getProperty("line.separator");

    /**
     * The number of spaces to indent.
     */
    private int indent = 2;

    /**
     * The indentation result.
     */
    private String indentResult = "";

    /**
     * The element namesspaces.
     */
    private String namespaces = "";

    /**
     * The node flag. It is true only if endElement occurs twice.
     */
    private boolean nodeFlag = false;

    /**
     * Put attributes on new line.
     */
    private boolean attributeOnNewLine = false;

    /**
     * The attribute indent value.
     */
    private String attributeIndentValue;

    /**
     * The temporary file object.
     */
    private File temporaryFile;

    /**
     * The temporary path. Default is java.io.tmpdir
     */
    private String temporaryPath = System.getProperty("java.io.tmpdir");

    /**
     * The temporary output stream
     */
    private OutputStream temporaryStream;

    /**
     * Check well-formed before write.
     */
    private boolean checkWellFormed = true;

    /**
     * Allow comments flag
     */
    private boolean allowComments = true;

    /**
     * The source input stream
     */
    private InputStream fromStream;

    /**
     * The source file path
     */
    private String fromFilePath;

    /**
     * The output file path
     */
    private String toFilePath;

    /**
     * The formated XML output stream.
     */
    private OutputStream to;


    /**
     * The XmlFormatter instance.
     */
    public XmlFormatter() {
        LOG.debug(this);
        this.filters = new ArrayList<XmlFilterBean>();
    }

    /**
     * Handler for startDocument event
     */
    public void startDocument() {
        attributeIndentValue = attributeIndent();
    }

    /**
     * Handler for endDocument event
     */
    public void endDocument() {
    }

    /**
     * Handler for startPrefixMapping event
     */
    public void startPrefixMapping(String prefix, String uri) {
        if (!prefix.equals(""))
            prefix = ":" + prefix;
        if (attributeOnNewLine)
            namespaces = namespaces + lineSeparator + indentResult + attributeIndentValue + "xmlns" + prefix + "=\"" + uri + "\"";
        else
            namespaces = namespaces + " xmlns" + prefix + "=\"" + uri + "\"";
    }

    /**
     * Handler for endPrefixMapping event
     */
    public void endPrefixMapping(String prefix) {
    }

    /**
     * Handler for startElement event
     */
    public void startElement(String uri, String uri_name, String name, Attributes atts) throws SAXException {
        process();
        nodeFlag = false;
        if (indentResult.equals("")) {
            writeToStream(lineSeparator + "<" + name + namespaces);
        } else {
            writeToStream(lineSeparator + indentResult + "<" + name + namespaces);
        }
        if (atts != null) {
            for (int i = 0; i < atts.getLength(); i++) {
                if (attributeOnNewLine) {
                    writeToStream(lineSeparator + indentResult + attributeIndentValue);
                    writeToStream(atts.getQName(i) + "=\"" + atts.getValue(i) + "\"");
                } else
                    writeToStream(" " + atts.getQName(i) + "=\"" + atts.getValue(i) + "\"");
            }
        }
        writeToStream(">");
        newIndent();
        namespaces = "";
    }

    /**
     * Handler for endElement event
     */
    public void endElement(String uri, String uri_name, String name) throws SAXException {
        removeIndent();
        process();
        if (nodeFlag) {
            writeToStream(lineSeparator + indentResult + "</" + name + ">");
        } else
            writeToStream("</" + name + ">");
        nodeFlag = true;
    }

    /**
     * Handler for characters event
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        String val = new String(ch, start, length);
        if (value == null)
            value = val;
        else
            value += val;
        // System.out.print(val);
    }

    /**
     * Handler for values.
     */
    private void process() throws SAXException {
        if (value != null) {
            value = value.trim();
            writeToStream(value);
        }
        value = null;
    }

    /**
     * Handler for comments event
     */
    public void comment(char[] ch, int start, int length) throws SAXException {
        String commentText = new String(ch, start, length);
        writeToStream(lineSeparator + indentResult + "<!-- ");
        if (commentText != null)
            writeToStream(commentText.trim());
        writeToStream(" -->");

    }

    /**
     * Handler for startCDATA event
     */
    public void startCDATA() throws SAXException {
        process();
        writeToStream("<![CDATA[");
    }

    /**
     * Handler for endCDATA event
     */
    public void endCDATA() throws SAXException {
        process();
        writeToStream("]]>");
    }

    /**
     * Handler for startEntity event
     */
    public void startEntity(String name) throws SAXException {
    }

    /**
     * Handler for endEntity event
     */
    public void endEntity(String name) throws SAXException {
    }

    /**
     * Handler for startDTD event
     */
    public void startDTD(String name, String publicId, String systemId) throws SAXException {
    }

    /**
     * Handler for endDTD event
     */
    public void endDTD() throws SAXException {
    }

    /**
     * Gets attribute indentation event
     */
    private String attributeIndent() {
        String attributeIndent = "";
        for (int i = 1; i <= indent; i++) {
            attributeIndent += " ";
        }
        return attributeIndent;
    }

    /**
     * Writes to temporary output stream
     */
    private void writeToStream(String s) throws SAXException {
        try {
            if (temporaryStream == null)
                this.createTemporaryFile();
            if (s != null) {
                temporaryStream.write(s.getBytes());
                temporaryStream.flush();
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new SAXException(e);
        }
    }

    /**
     * Writes to temporary output stream
     */
    public void writeText(String s) throws XmlFormatterException {
        try {
            this.writeToStream(s);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        }
    }

    /**
     * Add new indent to the current indentation.
     */
    public void newIndent() {
        indentResult += attributeIndentValue;
    }

    /**
     * Remove indent from the current indentation.
     */
    public void removeIndent() {
        if (indentResult.length() > indent)
            indentResult = indentResult.substring(0, indentResult.length() - indent);
        else
            indentResult = "";
    }

    /**
     * Gets attributeOnNewLine value.
     */
    public boolean isAttributeOnNewLine() {
        return attributeOnNewLine;
    }

    /**
     * Sets attributeOnNewLine value.
     */
    public void setAttributeOnNewLine(boolean attributeOnNewLine) {
        this.attributeOnNewLine = attributeOnNewLine;
    }

    /**
     * Gets indent value.
     */
    public int getIndent() {
        return indent;
    }

    /**
     * Sets indent value.
     */
    public void setIndent(int indent) throws XmlFormatterException {
        if (indent > 0 && indent < 100)
            this.indent = indent;
        else
            throw new XmlFormatterException("The indent value is not between 1 and 99");
    }

    /**
     * Creates temporary file
     */
    private void createTemporaryFile() throws XmlFormatterException {
        try {
            temporaryFile = File.createTempFile("~file~", ".tmp", new File(temporaryPath));
            temporaryStream = new FileOutputStream(temporaryFile);
            LOG.debug("%s", temporaryFile.getCanonicalPath());
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        }
    }

    /**
     * Deletes temporary file
     */
    private void deleteTemporaryFile() throws XmlFormatterException {
        try {
            if (temporaryFile != null) {
                LOG.debug("Delete temporary file " + temporaryFile.getCanonicalPath());
                if (temporaryStream != null) {
                    temporaryStream.close();
                    temporaryStream = null;
                }
                if (temporaryFile.delete()) {
                    LOG.debug("Delete temporary file succeded");
                    temporaryFile = null;
                }
            }
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        }
    }

    /**
     * Fill output stream from temporary file
     */
    private void fillTo() throws XmlFormatterException {
        try {
            if (fromStream != null) {
                fromStream.close();
                fromStream = null;
            }
            // validate wellformed
            if (checkWellFormed) {
                LOG.debug("Validate temporary file");
                XmlValidator xmlv = new XmlValidator();
                xmlv.validate(XmlParameters.getSAXParser(), new FileInputStream(temporaryFile));
            }
            // read temporary file and write bytes to OutputStream
            LOG.debug("Write temporary file content to output");
            FileInputStream temporaryInputStream = new FileInputStream(temporaryFile);
            byte[] buffer = new byte[1024];
            int bufferUsed = 0;
            if (to == null && toFilePath != null)
                this.to = new FileOutputStream(toFilePath);
            while ((bufferUsed = (temporaryInputStream.read(buffer))) != -1) {
                to.write(buffer, 0, bufferUsed);
                to.flush();
            }
            temporaryInputStream.close();
            temporaryInputStream = null;
            to.close();
            to = null;
            this.deleteTemporaryFile();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        } finally {
            this.deleteTemporaryFile();
        }
    }

    /**
     * Format from xml object to outoput stream.
     */
    public void format() throws XmlFormatterException {
        try {
            XmlFilter xmlf = new XmlFilter();
            xmlf.setParent(XmlParameters.getSAXParser());
            xmlf.setFilters(filters);
            xmlf.setAllowComments(allowComments);
            this.format(xmlf);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        } finally {
            this.deleteTemporaryFile();
        }
    }

    /**
     * Format from xml object to output stream filtered by XmlFilter.
     */
    public void format(XmlFilter xmlf) throws XmlFormatterException {
        LOG.debug("%s", xmlf);
        try {
            if (xmlf == null)
                throw new SAXException("Invalid XmlFilter");
            if (to == null && toFilePath == null) {
                throw new XmlFormatterException("Invalid ouput source");
            }
            if (fromFilePath != null)
                this.fromStream = new FileInputStream(fromFilePath);
            if (this.fromStream == null)
                throw new XmlFormatterException("Invalid input source.");
            xmlf.setParent(XmlParameters.getSAXParser());
            xmlf.setContentHandler(this);
            xmlf.setLexicalHandler(this);
            xmlf.setAllowComments(allowComments);
            xmlf.parse(new InputSource(fromStream));
            this.fillTo();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        } finally {
            this.deleteTemporaryFile();
        }
    }

    /**
     * Sets source input stream
     */
    public void setFrom(InputStream is) {
        this.fromStream = is;
    }

    /**
     * Sets source file path.
     */
    public void setFrom(String file) throws XmlFormatterException {
        try {
            if (file == null)
                throw new XmlFormatterException("File path is null");
            File f = new File(file);
            if (!f.exists() || !f.isFile())
                throw new XmlFormatterException("File not found " + file);
            this.fromFilePath = f.getCanonicalPath();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlFormatterException(e);
        }
    }

    /**
     * Sets results output stream
     */
    public void setTo(OutputStream os) {
        this.to = os;
    }

    /**
     * Sets results output stream to file
     */
    public void setTo(String file) throws XmlFormatterException {
        this.toFilePath = file;
    }

    /**
     * Sets working line separator to Windows OS.
     */
    public void setLineSeparatorWindows() {
        this.lineSeparator = "\r\n";
    }

    /**
     * Sets working line separator to Unix OS.
     */
    public void setLineSeparatorUnix() {
        this.lineSeparator = "\n";
    }

    /**
     * Sets working line separator to Mac OS.
     */
    public void setLineSeparatorMac() {
        this.lineSeparator = "\r";
    }

    /**
     * Sets working line separator to system default
     */
    public void setLineSeparatorDefault() {
        this.lineSeparator = System.getProperty("line.separator");
    }

    /**
     * Gets line separator.
     */
    public String getLineSeparator() {
        return lineSeparator;
    }

    /**
     * Gets temporary path.
     */
    public String getTemporaryPath() {
        return temporaryPath;
    }

    /**
     * Sets temporary path.
     */
    public void setTemporaryPath(String temporaryPath) {
        this.temporaryPath = temporaryPath;
    }

    /**
     * Check well-formed status.
     */
    public boolean isCheckWellFormed() {
        return checkWellFormed;
    }

    /**
     * Sets well-formed checking. Default is true.
     */
    public void setCheckWellFormed(boolean checkWellFormed) {
        this.checkWellFormed = checkWellFormed;
    }

    /**
     * Gets allow comments
     */
    public boolean isAllowComments() {
        return allowComments;
    }

    /**
     * Sets allow comments
     */
    public void setAllowComments(boolean allowComments) {
        this.allowComments = allowComments;
    }

    /**
     * Gets filters
     */
    public ArrayList<XmlFilterBean> getFilters() {
        return filters;
    }

    /**
     * Sets filters
     */
    public void setFilters(ArrayList<XmlFilterBean> filters) {
        this.filters = filters;
    }


    /**
     * Add filter
     *
     * @param filter Nodes sequence (node1/node12/node121...)
     */
    public void addFilter(String filter) {
        LOG.debug("%s", filter);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        this.filters.add(xmlFilterBean);
    }


    /**
     * Add filter
     *
     * @param filter            Nodes sequence (node1/node12/node121...)
     * @param filterCheckParent Check parent before filter
     */
    public void addFilter(String filter, boolean filterCheckParent) {
        LOG.debug("%s", filter);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        xmlFilterBean.setFilterCheckParent(filterCheckParent);
        this.filters.add(xmlFilterBean);
    }

    /**
     * Add filter
     *
     * @param filter          Nodes sequence (node1/node12/node121...)
     * @param filterAttrName  Node attribute name
     * @param filterAttrValue Node attribute value
     */
    public void addFilter(String filter, String filterAttrName, String filterAttrValue) {
        LOG.debug("%s,%s,%s", filter, filterAttrName, filterAttrValue);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        xmlFilterBean.setFilterAttrName(filterAttrName);
        xmlFilterBean.setFilterAttrValue(filterAttrValue);
        this.filters.add(xmlFilterBean);
    }

    /**
     * Add filter
     *
     * @param filter            Nodes sequence (node1/node12/node121...)
     * @param filterAttrName    Node attribute name
     * @param filterAttrValue   Node attribute value
     * @param filterCheckParent Check parent before filter
     */
    public void addFilter(String filter, String filterAttrName, String filterAttrValue, boolean filterCheckParent) {
        LOG.debug("%s,%s,%s", filter, filterAttrName, filterAttrValue);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        xmlFilterBean.setFilterAttrName(filterAttrName);
        xmlFilterBean.setFilterAttrValue(filterAttrValue);
        xmlFilterBean.setFilterCheckParent(filterCheckParent);
        this.filters.add(xmlFilterBean);
    }

    /**
     * Resets filters
     */
    public void resetFilters() {
        LOG.debug(Constants.EMPTY);
        this.filters = new ArrayList<XmlFilterBean>();
    }

}
