/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.*;
import ro.myset.utils.common.Constants;

import java.io.InputStream;


/**
 * Validate xml streams versus xsd Schema file using SAX.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */
public class XmlValidator {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlValidator.class);

    /**
     * Creates a new instance.
     */
    public XmlValidator() {
        //LOG.debug(this);
    }


    /**
     * Validate xml well-formed stream.
     *
     * @param xmlReader XMLReader instance.
     * @param xmlSrc    Xml source stream
     * @throws XmlValidatorException If the execution fail.
     */
    public void validate(XMLReader xmlReader, InputStream xmlSrc) throws XmlValidatorException {
        if (xmlReader == null)
            throw new XmlValidatorException("XMLReader is null");
        if (xmlSrc == null)
            throw new XmlValidatorException("XML source is null");
        try {
            xmlReader.parse(new InputSource(xmlSrc));
        } catch (SAXParseException spe) {
            throw (new XmlValidatorException("Invalid well-formed at [line " +
                    spe.getLineNumber() + ", colum " + spe.getColumnNumber() + "] " + spe.getLocalizedMessage()));
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw (new XmlValidatorException(e));
        }
    }


    /**
     * Validate xml stream versus xsd schema using SAX.
     *
     * @param xmlReader      XMLReader instance.
     * @param xmlSrc         Xml source stream.
     * @param entityResolver EntityResolver instance.
     * @param errorHandler   ErrorHandler instance.
     * @throws XmlValidatorException If the execution fail.
     */
    public void validateSchema(XMLReader xmlReader, InputStream xmlSrc, EntityResolver entityResolver, ErrorHandler errorHandler) throws XmlValidatorException {
        if (xmlReader == null)
            throw new XmlValidatorException("XMLReader is null");
        if (xmlSrc == null)
            throw new XmlValidatorException("XML source is null");
        try {
            if (entityResolver != null)
                xmlReader.setEntityResolver(entityResolver);
            if (errorHandler != null)
                xmlReader.setErrorHandler(errorHandler);
            xmlReader.parse(new InputSource(xmlSrc));
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw (new XmlValidatorException(e));
        }
    }


}