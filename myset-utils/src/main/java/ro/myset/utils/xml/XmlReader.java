/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import ro.myset.utils.common.Constants;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.generic.GenericMap;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Read XML files using SAX parser for elements and atributes using simple templates like
 * (tag1/tag12/tag121/...).
 *
 * @author Cristian Gheorghe Florescu
 * @version 3.0
 */
public class XmlReader extends DefaultHandler implements ContentHandler {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlReader.class);

    /**
     * The input source object.
     */
    private InputSource source;

    /**
     * The source file path.
     */
    private String sourceFilePath;

    /**
     * The XMLReader.
     */
    private XMLReader parser;

    /**
     * The current element name.
     */
    private String field;

    /**
     * The current path hierarchy.
     */
    private String currentPath;

    /**
     * The current generic bean.
     */
    private GenericBean currentGenericBean;

    /**
     * The current value.
     */
    private String currentValue;


    /**
     * The searched element names hierarchy.
     */
    private String find;

    /**
     * Search attributes or nodes values. Default value is false
     */
    private boolean findAttributes = false;

    /**
     * Filters
     */
    private ArrayList<XmlFilterBean> filters;

    /**
     * Results fields names.
     */
    private ArrayList<String> fieldsNames;

    /**
     * Results generic bean.
     */
    private ArrayList<GenericBean> results;


    /**
     * The not found exception message.
     */
    private String nfxm;

    /**
     * Creates a new instance and get SAX parser <i>parser</i> object using a file path. <br>
     *
     * @param file Source file path
     * @throws XmlReaderException If the execution fail.
     */
    public XmlReader(String file) throws XmlReaderException {
        LOG.debug("File: %s", file);
        try {
            if (file == null)
                throw new XmlReaderException("File currentPath is null");
            File f = new File(file);
            if (!f.exists() || !f.isFile())
                throw new XmlReaderException("File not found " + file);
            this.sourceFilePath = f.getCanonicalPath();
            this.source = new InputSource(this.sourceFilePath);
            this.parser = XmlParameters.getSAXParser();
            this.filters = new ArrayList<XmlFilterBean>();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlReaderException(e);
        }
    }

    /**
     * Creates a new instance and get SAX parser <i>parser</i> object using a InputStream. <br>
     *
     * @param is Source input stream
     * @throws XmlReaderException If the execution fail.
     */
    public XmlReader(InputStream is) throws XmlReaderException {
        LOG.debug("Stream: %s", is);
        try {
            if (is == null)
                throw new XmlReaderException("Invalid source InputStream");
            this.sourceFilePath = null;
            this.source = new InputSource(is);
            this.parser = XmlParameters.getSAXParser();
            this.filters = new ArrayList<XmlFilterBean>();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlReaderException(e);
        }
    }

    /**
     * Creates a new instance and get SAX parser <i>parser</i> object using a Reader. <br>
     *
     * @param r Source reader
     * @throws XmlReaderException If the execution fail.
     */
    public XmlReader(Reader r) throws XmlReaderException {
        LOG.debug("Reader: %s", r);
        try {
            if (r == null)
                throw new XmlReaderException("Invalid source Reader");
            this.source = new InputSource(r);
            this.parser = XmlParameters.getSAXParser();
            this.filters = new ArrayList<XmlFilterBean>();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlReaderException(e);
        }
    }

    /**
     * Handler for startDocument
     */
    public void startDocument() {
        // LOG.debug(Constants.EMPTY);
    }

    /**
     * Handler for endDocument
     */
    public void endDocument() {
        // LOG.debug(Constants.EMPTY);
        nfxm = null;
        if (results.size() == 0)
            nfxm = (find + " was not found");
    }

    // Element section

    /**
     * Handler for startElement
     */
    public void startElement(String uri, String uri_name, String name, Attributes atts) {
        // LOG.debug("%s",name);
        field = name;
        if (currentPath == null)
            currentPath = field;
        else
            currentPath += "/" + field;
        this.process(XmlParameters.EVENT_SOURCE_START_ELEMENT, uri, uri_name, name, atts);
    }

    /**
     * Handler for endElement
     */
    public void endElement(String uri, String uri_name, String name) {
        // LOG.debug("%s",name);
        this.process(XmlParameters.EVENT_SOURCE_END_ELEMENT, uri, uri_name, name, null);
        if ((currentPath.lastIndexOf("/") != -1)) {
            currentPath = currentPath.substring(0, currentPath.lastIndexOf("/"));
        } else
            currentPath = name;
    }

    /**
     * Handler for characters
     */
    public void characters(char[] ch, int start, int length) {
        String val = new String(ch, start, length);
        if (currentValue == null)
            currentValue = val;
        else
            currentValue += val;
        //LOG.debug("%s", currentValue);
    }

    void process(int eventSource, String uri, String uri_name, String name, Attributes attrs) {
        LOG.debug("EVENT_SOURCE[%s] currentPath:%s", eventSource, currentPath);
        // START_ELEMENT
        if (eventSource == XmlParameters.EVENT_SOURCE_START_ELEMENT) {
            //reset current value
            currentValue = null;
            if (fieldsNames.size() > 0) {
                if (currentPath.equals(find)) {
                    currentGenericBean = new GenericBean();
                    currentGenericBean.setFieldsNames(fieldsNames);
                    currentGenericBean.setFields(new GenericMap<>());
                }
                for (int j = 0; j < fieldsNames.size(); j++) {
                    // process nodes
                    if (!findAttributes && currentPath.equals(find + Constants.SLASH + fieldsNames.get(j))) {
                        currentGenericBean = new GenericBean();
                        currentGenericBean.setFieldsNames(fieldsNames);
                        currentGenericBean.setFields(new GenericMap<>());
                        currentGenericBean.getFields().put(fieldsNames.get(j), currentValue);
                    }
                    // process attributes
                    if (findAttributes && currentPath.equals(find)) {
                        for (int i = 0; i < attrs.getLength(); i++) {
                            //LOG.debug("ATTRIBUTE %s=%s", attrs.getQName(i),attrs.getValue(i));
                            if (fieldsNames.get(j).equals(attrs.getQName(i))) {
                                currentGenericBean.getFields().put(fieldsNames.get(j), attrs.getValue(i));
                            }
                        }
                    }
                }
            }
        }
        // END_ELEMENT
        if (eventSource == XmlParameters.EVENT_SOURCE_END_ELEMENT) {
            if (currentValue != null)
                currentValue = currentValue.trim();
            if (!findAttributes) {
                for (int j = 0; j < fieldsNames.size(); j++) {
                    if (currentPath != null && currentPath.equals(find + Constants.SLASH + fieldsNames.get(j))) {
                        LOG.debug(currentGenericBean);
                        currentGenericBean.setValue(currentValue);
                        currentGenericBean.getFields().put(fieldsNames.get(j), currentValue);
                        results.add(currentGenericBean);
                    }
                }
            }
            if (findAttributes) {
                if (currentPath != null && currentPath.equals(find)) {
                    LOG.debug(currentGenericBean);
                    currentGenericBean.setValue(currentValue);
                    results.add(currentGenericBean);
                }
            }
        }

    }

    /**
     * Reads nodes values based on "find/<nodeName>".
     * If "find" or searched tag hierarchy is not found it will throw XmlReaderException.
     *
     * @param find       Parent nodes sequence (node1/node12/node121...)
     * @param nodesNames Child nodes names (find/<nodeName>)
     * @throws XmlReaderException If the execution fail.
     */
    public ArrayList<GenericBean> readNodes(String find, ArrayList<String> nodesNames) throws XmlReaderException {
        LOG.debug("%s, %s", find, nodesNames);
        try {
            this.results = new ArrayList<GenericBean>();
            this.find = find;
            this.fieldsNames = nodesNames;
            this.findAttributes = false;
            this.currentPath = null;
            this.field = null;
            XmlFilter xf = new XmlFilter(parser);
            xf.setFilters(find, filters);
            xf.setContentHandler(this);
            xf.parse(this.source);
            if (nfxm != null)
                throw new XmlReaderException(nfxm);
            return this.results;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlReaderException(e);
        }
    }

    /**
     * Reads nodes values based on "find/<fieldName>" first occurrence.
     * If "find" or searched tag hierarchy is not found it will throw XmlReaderException.
     *
     * @param find       Parent nodes sequence (node1/node12/node121...)
     * @param nodesNames Child nodes names (find/<nodeName>)
     * @throws XmlReaderException If the execution fail.
     */
    public GenericBean readNodesFirst(String find, ArrayList<String> nodesNames) throws XmlReaderException {
        LOG.debug("%s, %s", find, nodesNames);
        return (GenericBean) readNodes(find, nodesNames).get(0);
    }

    /**
     * Reads attributes values based on "find" node and attribute name using <attrName>".
     * If "find" or searched tag hierarchy is not found it will throw XmlReaderException.
     *
     * @param find       Parent nodes sequence (node1/node12/node121...)
     * @param attrsNames Node attributes names (find and attributeName using <attrName>)
     * @throws XmlReaderException If the execution fail.
     */
    public ArrayList<GenericBean> readAttributes(String find, ArrayList<String> attrsNames) throws XmlReaderException {
        LOG.debug("%s, %s", find, attrsNames);
        try {
            this.results = new ArrayList<GenericBean>();
            this.find = find;
            this.fieldsNames = attrsNames;
            this.findAttributes = true;
            this.currentPath = null;
            this.field = null;
            XmlFilter xf = new XmlFilter(parser);
            xf.setFilters(find, filters);
            xf.setContentHandler(this);
            xf.parse(this.source);
            if (nfxm != null)
                throw new XmlReaderException(nfxm);
            return this.results;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlReaderException(e);
        }
    }

    /**
     * Reads attributes values based on "find" node and attribute name using <attrName>" first occurrence..
     * If "find" or searched tag hierarchy is not found it will throw XmlReaderException.
     *
     * @param find       Parent nodes sequence (node1/node12/node121...)
     * @param attrsNames Node attributes names (find and attributeName using <attrName>)
     * @throws XmlReaderException If the execution fail.
     */
    public GenericBean readAttributesFirst(String find, ArrayList<String> attrsNames) throws XmlReaderException {
        LOG.debug("%s, %s", find, attrsNames);
        return (GenericBean) readAttributes(find, attrsNames).get(0);
    }

    /**
     * Gets source file path
     */
    public String getSourceFilePath() {
        return sourceFilePath;
    }


    /**
     * Gets filters
     */
    public ArrayList<XmlFilterBean> getFilters() {
        return filters;
    }

    /**
     * Sets filters
     */
    public void setFilters(ArrayList<XmlFilterBean> filters) {
        this.filters = filters;
    }

    /**
     * Add filter
     *
     * @param filter Nodes sequence (node1/node12/node121...)
     */
    public void addFilter(String filter) {
        LOG.debug("%s", filter);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        this.filters.add(xmlFilterBean);
    }


    /**
     * Add filter
     *
     * @param filter            Nodes sequence (node1/node12/node121...)
     * @param filterCheckParent Check parent before filter
     */
    public void addFilter(String filter, boolean filterCheckParent) {
        LOG.debug("%s", filter);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        xmlFilterBean.setFilterCheckParent(filterCheckParent);
        this.filters.add(xmlFilterBean);
    }

    /**
     * Add filter
     *
     * @param filter          Nodes sequence (node1/node12/node121...)
     * @param filterAttrName  Node attribute name
     * @param filterAttrValue Node attribute value
     */
    public void addFilter(String filter, String filterAttrName, String filterAttrValue) {
        LOG.debug("%s,%s,%s", filter, filterAttrName, filterAttrValue);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        xmlFilterBean.setFilterAttrName(filterAttrName);
        xmlFilterBean.setFilterAttrValue(filterAttrValue);
        this.filters.add(xmlFilterBean);
    }

    /**
     * Add filter
     *
     * @param filter            Nodes sequence (node1/node12/node121...)
     * @param filterAttrName    Node attribute name
     * @param filterAttrValue   Node attribute value
     * @param filterCheckParent Check parent before filter
     */
    public void addFilter(String filter, String filterAttrName, String filterAttrValue, boolean filterCheckParent) {
        LOG.debug("%s,%s,%s", filter, filterAttrName, filterAttrValue);
        XmlFilterBean xmlFilterBean = new XmlFilterBean();
        xmlFilterBean.setFilter(filter);
        xmlFilterBean.setFilterAttrName(filterAttrName);
        xmlFilterBean.setFilterAttrValue(filterAttrValue);
        xmlFilterBean.setFilterCheckParent(filterCheckParent);
        this.filters.add(xmlFilterBean);
    }

    /**
     * Resets filters
     */
    public void resetFilters() {
        LOG.debug(Constants.EMPTY);
        this.filters = new ArrayList<XmlFilterBean>();
    }


}
