/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.XMLFilterImpl;
import ro.myset.utils.common.Constants;
import ro.myset.utils.generic.GenericMap;

import java.util.ArrayList;

/**
 * Filter XML files using SAX parser using filters on elements hierarchy and
 * attribute that match specific values.
 *
 * @author Cristian Gheorghe Florescu
 * @version 2.0
 */

public class XmlFilter extends XMLFilterImpl implements LexicalHandler {

    private static final Logger LOG = LogManager.getFormatterLogger(XmlFilter.class);

    private ArrayList<XmlFilterBean> filters = null;
    private String find = null;
    private ArrayList<XmlFilterBean> feasibleFilters = null;

    private LexicalHandler lexicalHandler;
    private String currentPath;
    private GenericMap<String, Boolean> nodeFound = null;
    private boolean allowComments = false;

    private boolean allow = false;

    /**
     * Creates a new instance of filter
     */
    public XmlFilter() {
    }

    /**
     * Creates a new instance of filter and sets parent reader
     */
    public XmlFilter(XMLReader parent) {
        super(parent);
    }

    /**
     * Gets parent reader
     */
    public XMLReader getParent() {
        return super.getParent();
    }

    /**
     * Sets parent reader
     */
    public void setParent(XMLReader parent) {
        super.setParent(parent);
    }

    /**
     * Sets filters and calculate feasible filters based on find path.
     *
     * @param filters Array list of XmlFilterValues
     * @param find    Nodes sequence (node1/node12/node121...)
     */
    public void setFilters(String find, ArrayList<XmlFilterBean> filters) {
        this.nodeFound = new GenericMap<String, Boolean>();
        this.find = find;
        this.filters = filters;
        // Evaluate feasibleFilters
        feasibleFilters = new ArrayList<XmlFilterBean>();
        for (int i = 0; i < filters.size(); i++) {
            if (filters.get(i).getFilter().equals(find) || filters.get(i).getFilter().startsWith(find + Constants.SLASH) || find.startsWith(filters.get(i).getFilter() + Constants.SLASH)) {
                feasibleFilters.add(filters.get(i));
            }
        }
    }

    /**
     * Sets filters
     *
     * @param filters Array list of XmlFilterValues
     */
    public void setFilters(ArrayList<XmlFilterBean> filters) {
        this.nodeFound = new GenericMap<String, Boolean>();
        this.filters = filters;
        // Evaluate feasibleFilters
        feasibleFilters = filters;
    }

    /**
     * Overrides startElement of parent content handler.
     */
    public void startElement(String uri, String uri_name, String name, Attributes attrs) throws SAXException {
        if (currentPath == null)
            currentPath = name;
        else
            currentPath = currentPath + "/" + name;
        //evaluate after currentPath build
        this.process(XmlParameters.EVENT_SOURCE_START_ELEMENT, uri, uri_name, name, attrs);
        if (allow)
            super.startElement(uri, uri_name, name, attrs);

    }

    /**
     * Overrides endElement of parent content handler.
     */
    public void endElement(String uri, String uri_name, String name) throws SAXException {
        //evaluate before currentPath build
        this.process(XmlParameters.EVENT_SOURCE_END_ELEMENT, uri, uri_name, name, null);
        if ((currentPath.lastIndexOf("/") != -1)) {
            currentPath = currentPath.substring(0, currentPath.lastIndexOf("/"));
        } else
            currentPath = name;
        if (allow)
            super.endElement(uri, uri_name, name);
    }

    /**
     * Overrides characters of parent content handler.
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        this.process();
        if (allow)
            super.characters(ch, start, length);
    }

    /**
     * Gets the lexical event handler.
     */
    public LexicalHandler getLexicalHandler() {
        return lexicalHandler;
    }

    /**
     * Sets the lexical event handler.
     */

    public void setLexicalHandler(LexicalHandler handler) throws SAXException {
        lexicalHandler = handler;
        this.setProperty("http://xml.org/sax/properties/lexical-handler", this);
    }

    /**
     * Overrides startDTD of parent lexical handler.
     */
    public void startDTD(String name, String publicId, String systemId) throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            lexicalHandler.startDTD(name, publicId, systemId);
        }
    }

    /**
     * Overrides endDTD of parent lexical handler.
     */
    public void endDTD() throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            lexicalHandler.endDTD();
        }
    }

    /**
     * Overrides startEntity of parent lexical handler.
     */
    public void startEntity(String name) throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            lexicalHandler.startEntity(name);
        }
    }

    /**
     * Overrides endEntity of parent lexical handler.
     */
    public void endEntity(String name) throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            lexicalHandler.endEntity(name);
        }
    }

    /**
     * Overrides startCDATA of parent lexical handler.
     */
    public void startCDATA() throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            if (allow)
                lexicalHandler.startCDATA();
        }
    }

    /**
     * Overrides endCDATA of parent lexical handler.
     */
    public void endCDATA() throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            if (allow)
                lexicalHandler.endCDATA();
        }
    }

    /**
     * Overrides comment of parent lexical handler.
     */
    public void comment(char[] ch, int start, int length) throws SAXException {
        if (lexicalHandler != null) {
            this.process();
            if (allowComments)
                lexicalHandler.comment(ch, start, length);
        }
    }


    /**
     * Process events
     */
    private final void process(int eventSource, String uri, String uri_name, String name, Attributes attrs) {
        //LOG.debug("EVENT_SOURCE[%s] currentPath:%s,name:%s, allow:%s", eventSource, currentPath, name, allow);
        //no filters
        if (filters == null || currentPath == null || filters.size() == 0) {
            allow = true;
            return;
        }
        if (eventSource > XmlParameters.EVENT_SOURCE_OTHER) {
            // Evaluate find
            if (find != null && !(currentPath.startsWith(find) || find.startsWith(currentPath))) {
                return;
            }
            filtersLoop:
            for (int i = 0; i < feasibleFilters.size(); i++) {
                allow = false;
                // START_ELEMENT
                if (eventSource == XmlParameters.EVENT_SOURCE_START_ELEMENT && currentPath.equals(feasibleFilters.get(i).getFilter())) {
                    nodeFound.put(currentPath, false);
                    if (feasibleFilters.get(i).isFilterCheckParent()) {
                        if (!checkParentNodeFound(currentPath))
                            break filtersLoop;
                    }

                    // currentPath match filter with no attribute filter
                    if (feasibleFilters.get(i).getFilterAttrName() == null || feasibleFilters.get(i).getFilterAttrValue() == null) {
                        allow = true;
                        nodeFound.put(currentPath, true);
                        break filtersLoop;
                    }
                    // currentPath match filter with attribute filter
                    if (feasibleFilters.get(i).getFilterAttrName() != null && feasibleFilters.get(i).getFilterAttrValue() != null && attrs != null) {
                        for (int j = 0; j < attrs.getLength(); j++) {
                            if (attrs.getQName(j).equals(feasibleFilters.get(i).getFilterAttrName()) && attrs.getValue(j).equals(feasibleFilters.get(i).getFilterAttrValue())) {
                                allow = true;
                                nodeFound.put(currentPath, true);
                                break filtersLoop;
                            }
                            if (feasibleFilters.get(i).getFilterAttrName().equals(attrs.getQName(j)))
                                break;
                        }
                    }
                }
                // END_ELEMENT
                if (eventSource == XmlParameters.EVENT_SOURCE_END_ELEMENT && currentPath.equals(feasibleFilters.get(i).getFilter())) {
                    if (feasibleFilters.get(i).isFilterCheckParent()) {
                        if (!checkParentNodeFound(currentPath))
                            break filtersLoop;
                    }
                    if ((Boolean) nodeFound.get(currentPath)) {
                        if ((feasibleFilters.get(i).getFilterAttrName() == null || feasibleFilters.get(i).getFilterAttrValue() == null)) {
                            allow = true;
                            break filtersLoop;
                        }
                        if (feasibleFilters.get(i).getFilterAttrName() != null && feasibleFilters.get(i).getFilterAttrValue() != null) {
                            allow = true;
                            break filtersLoop;
                        }
                    }
                }
            }

        }
    }

    /**
     * Process OTHER events
     */
    private final void process() {
        process(XmlParameters.EVENT_SOURCE_OTHER, null, null, null, null);
    }

    private final boolean checkParentNodeFound(String path) {
        if (path != null && (path.lastIndexOf("/") != -1)) {
            path = path.substring(0, path.lastIndexOf("/"));
            if (nodeFound.get(path) != null) {
                if ((Boolean) nodeFound.get(path))
                    return true;
                else
                    checkParentNodeFound(path);
            }
        }
        return false;
    }

    /**
     * Gets allow comments
     */
    public final boolean isAllowComments() {
        return allowComments;
    }

    /**
     * Sets allow comments
     */
    public final void setAllowComments(boolean allowComments) {
        this.allowComments = allowComments;
    }
}
