/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


/**
 * Transform XML files XSL in various formats.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class XmlTransformer {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlTransformer.class);

    /**
     * Gets TransformerFactory
     */
    public TransformerFactory getTransformerFactory() throws XmlTransformerException {
        try {
            TransformerFactory tf = XmlParameters.getTransformerFactory();
            return tf;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlTransformerException(e);
        }
    }


    /**
     * Gets Transformer
     *
     * @param ssxsl XSL source
     * @throws XmlTransformerException If the execution fail.
     */
    public Transformer getTransformer(StreamSource ssxsl) throws XmlTransformerException {
        try {
            TransformerFactory tf = XmlParameters.getTransformerFactory();
            if (!tf.getFeature(StreamSource.FEATURE))
                throw new XmlTransformerException("Could not get StreamSource.FEATURE");
            if (!tf.getFeature(StreamResult.FEATURE))
                throw new XmlTransformerException("Could not get StreamResult.FEATURE");
            Transformer transformer = tf.newTransformer(ssxsl);
            return transformer;
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlTransformerException(e);
        }
    }


    /**
     * Transform XML StreamSource using XSL StreamSource into StreamResult
     *
     * @param ssxml XML source
     * @param ssxsl XSL source
     * @param sr    Output source
     * @throws XmlTransformerException If the execution fail.
     */
    public static void transform(StreamSource ssxml, StreamSource ssxsl, StreamResult sr) throws XmlTransformerException {
        try {
            TransformerFactory tf = XmlParameters.getTransformerFactory();
            if (!tf.getFeature(StreamSource.FEATURE))
                throw new XmlTransformerException("Could not get StreamSource.FEATURE");
            if (!tf.getFeature(StreamResult.FEATURE))
                throw new XmlTransformerException("Could not get StreamResult.FEATURE");
            Transformer transformer = tf.newTransformer(ssxsl);
            transformer.transform(ssxml, sr);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new XmlTransformerException(e);
        }
    }
    

}
