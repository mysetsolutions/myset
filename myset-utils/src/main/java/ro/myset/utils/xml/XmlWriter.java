/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import ro.myset.utils.common.Constants;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * XML writer processor. The xml objects (namespaces, elements, attributes) can
 * be renamed, updated, inserted, deleted using filters based on elements paths
 * or elements paths and attribute that match a specific value.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class XmlWriter extends XmlFilter {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlWriter.class);
    /**
     * The target XmlFormatter object.
     */
    XmlFormatter xmlFormatter;

    /**
     * The action type.
     */
    private int actionType = 0;

    /**
     * The action is done.
     */
    private boolean actionDone = false;

    /**
     * The old name.
     */
    private String oldName;

    /**
     * The new name.
     */
    private String newName;

    /**
     * The new value.
     */
    private String newValue;

    /**
     * The filter path.
     */
    private String filter;

    /**
     * The current path.
     */
    private String cfilter;

    /**
     * The filter attribute name.
     */
    private String filterAttrName;

    /**
     * The filter attribute value.
     */
    private String filterAttrValue;

    /**
     * The attribute and associated value was found.
     */
    private boolean attrFound = false;

    /**
     * The full filter was found.
     */
    private boolean filterFound = false;

    /**
     * Continue action flag.
     */
    private boolean continueAction = false;

    /**
     * Let events.
     */
    private boolean letit = true;

    /**
     * PrefixImp array list.
     */
    private ArrayList startprefixal;

    /**
     * PrefixImp array list iterator.
     */
    private Iterator startprefixi;

    /**
     * PrefixImp current object.
     */
    private PrefixImpl startprefixpi;

    public XmlWriter() {
        this.startprefixal = new ArrayList();
    }

    /**
     * Handler for startDocument
     */
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    /**
     * Handler for endDocument
     */
    public void endDocument() throws SAXException {
        super.endDocument();
        LOG.debug("\r\nAction done : " + actionDone);
    }

    /**
     * Handler for startPrefixMapping
     */
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        startprefixal.add(new PrefixImpl(prefix, uri));
    }

    /**
     * Handler for endPrefixMapping
     */
    public void endPrefixMapping(String prefix) throws SAXException {
    }

    /**
     * Handler for startElement
     */
    public void startElement(String uri, String uri_name, String name, Attributes atts) throws SAXException {
        letit = true;
        // format cfilter
        if (!actionDone) {
            if (cfilter == null)
                cfilter = name;
            else
                cfilter = cfilter + "/" + name;
            this.filterFound = false;
            this.attrFound = false;
            if (filter != null && filter.equals(cfilter)) {
                filterFound = true;
                if (filterAttrName != null && filterAttrValue != null && atts != null) {
                    filterFound = false;
                    for (int i = 0; i < atts.getLength(); i++) {
                        if (atts.getQName(i).equals(filterAttrName) && atts.getValue(i).equals(filterAttrValue)) {
                            attrFound = true;
                            i = atts.getLength();
                        }
                    }
                    if (attrFound)
                        filterFound = true;
                }
            }
            //LOG.debug("[" + actionType + "," + cfilter + "," +filterFound + "]");
            // rename namespace
            if (actionType == 1 && filterFound) {
                startprefixi = startprefixal.iterator();
                while (startprefixi.hasNext()) {
                    startprefixpi = (PrefixImpl) startprefixi.next();
                    if (startprefixpi.getPrefix().equals(this.oldName)) {
                        startprefixpi.setPrefix(newName);
                        actionDone = true;
                    }
                }
            }
            // insert namespace
            if (actionType == 2 && filterFound) {
                startprefixal.add(new PrefixImpl(this.newName, this.newValue));
                actionDone = true;
            }
            // update namespace
            if (actionType == 3 && filterFound) {
                startprefixi = startprefixal.iterator();
                while (startprefixi.hasNext()) {
                    startprefixpi = (PrefixImpl) startprefixi.next();
                    if (startprefixpi.getPrefix().equals(this.oldName)) {
                        startprefixpi.setUri(newValue);
                        actionDone = true;
                    }
                }
            }
            // delete namespace
            if (actionType == 4 && filterFound) {
                startprefixi = startprefixal.iterator();
                while (startprefixi.hasNext()) {
                    startprefixpi = (PrefixImpl) startprefixi.next();
                    if (startprefixpi.getPrefix().equals(this.oldName)) {
                        startprefixi.remove();
                        actionDone = true;
                    }
                }
            }
            // rename element
            if (actionType == 5 && filterFound) {
                continueAction = true;
                name = newValue;
                uri_name = "";
                int sp = name.lastIndexOf(":");
                if (sp > 0)
                    uri_name = newValue.substring(sp + 1);
            }
            // insert element
            if (actionType == 6 && filterFound) {
                continueAction = true;
            }
            // update element
            if (actionType == 7 && (filterFound || continueAction)) {
                if (!continueAction) {
                    startprefixi = startprefixal.iterator();
                    while (startprefixi.hasNext()) {
                        startprefixpi = (PrefixImpl) startprefixi.next();
                        super.startPrefixMapping(startprefixpi.getPrefix(), startprefixpi.getUri());
                    }
                    startprefixal.clear();
                    super.startElement(uri, uri_name, name, atts);
                    char[] vca = newValue.toCharArray();
                    super.characters(vca, 0, vca.length);
                    continueAction = true;
                    letit = false;
                    return;
                }
                continueAction = true;
                letit = false;
            }
            // delete element
            if (actionType == 8 && (filterFound || continueAction)) {
                continueAction = true;
                letit = false;
            }
            // rename attribute
            if (actionType == 9 && filterFound) {
                AttributesImpl attsnew;
                if (atts != null) {
                    attsnew = new AttributesImpl(atts);
                    for (int i = 0; i < attsnew.getLength(); i++) {
                        if (attsnew.getQName(i).equals(oldName)) {
                            attsnew.setQName(i, newName);
                            atts = attsnew;
                            actionDone = true;
                            i = attsnew.getLength();
                        }
                    }
                }

            }
            // insert attribute
            if (actionType == 10 && filterFound) {
                AttributesImpl attsnew;
                if (atts != null)
                    attsnew = new AttributesImpl(atts);
                else
                    attsnew = new AttributesImpl();
                attsnew.addAttribute(uri, uri_name, newName, newValue.getClass().getName(), newValue);
                atts = attsnew;
                actionDone = true;
            }
            // update attribute
            if (actionType == 11 && filterFound) {
                AttributesImpl attsnew;
                if (atts != null) {
                    attsnew = new AttributesImpl(atts);
                    for (int i = 0; i < attsnew.getLength(); i++) {
                        if (attsnew.getQName(i).equals(oldName)) {
                            attsnew.setValue(i, newValue);
                            atts = attsnew;
                            actionDone = true;
                            i = attsnew.getLength();
                        }
                    }
                }
            }
            // delete attribute
            if (actionType == 12 && filterFound) {
                AttributesImpl attsnew;
                if (atts != null) {
                    attsnew = new AttributesImpl(atts);
                    for (int i = 0; i < attsnew.getLength(); i++) {
                        if (attsnew.getQName(i).equals(oldName)) {
                            attsnew.removeAttribute(i);
                            atts = attsnew;
                            actionDone = true;
                            i = attsnew.getLength();
                        }
                    }
                }
            }

        }
        if (letit) {
            startprefixi = startprefixal.iterator();
            while (startprefixi.hasNext()) {
                startprefixpi = (PrefixImpl) startprefixi.next();
                super.startPrefixMapping(startprefixpi.getPrefix(), startprefixpi.getUri());
            }
            super.startElement(uri, uri_name, name, atts);
        }
        startprefixal.clear();
    }

    /**
     * Handler for endElement
     */
    public void endElement(String uri, String uri_name, String name) throws SAXException {
        letit = true;
        if (!actionDone) {
            if (cfilter.equals(filter)) {
                filterFound = true;
            }
            // rename element
            if (actionType == 5 && filterFound && continueAction) {
                continueAction = false;
                name = newValue;
                uri_name = "";
                int sp = name.lastIndexOf(":");
                if (sp > 0 && (sp + 1) < newValue.length())
                    uri_name = newValue.substring(sp + 1);
                actionDone = true;
            }
            // insert element
            if (actionType == 6 && filterFound && continueAction) {
                String uriName = "";
                int sp = newName.lastIndexOf(":");
                if (sp > 0 && (sp + 1) < newName.length())
                    uriName = newName.substring(sp + 1);
                super.startElement(uri, uriName, newName, null);
                char[] vca = newValue.toCharArray();
                super.characters(vca, 0, vca.length);
                super.endElement(uri, uriName, newName);
                actionDone = true;
            }
            // update element
            if (actionType == 7 && continueAction) {
                letit = false;
                if (filterFound) {
                    continueAction = false;
                    letit = true;
                    actionDone = true;
                }
            }
            // delete element
            if (actionType == 8 && continueAction) {
                letit = false;
                if (filterFound) {
                    continueAction = false;
                    actionDone = true;
                }
            }
        }
        // format cfilter
        if ((cfilter.lastIndexOf("/") != -1)) {
            cfilter = cfilter.substring(0, cfilter.lastIndexOf("/"));
        } else
            cfilter = name;
        if (letit)
            super.endElement(uri, uri_name, name);
    }

    /**
     * Handler for characters.
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (letit)
            super.characters(ch, start, length);
    }

    /**
     * Handler for comments.
     */
    public void comment(char[] ch, int start, int length) throws SAXException {
        if (letit)
            super.comment(ch, start, length);
    }

    /**
     * Handler for startCDATA.
     */
    public void startCDATA() throws SAXException {
        if (letit)
            super.startCDATA();
    }

    /**
     * Handler for endCDATA.
     */
    public void endCDATA() throws SAXException {
        if (letit)
            super.endCDATA();
    }

    /**
     * Handler for startEntity.
     */
    public void startEntity(String name) throws SAXException {
        super.startEntity(name);
    }

    /**
     * Handler for endEntity.
     */
    public void endEntity(String name) throws SAXException {
        if (letit)
            super.endEntity(name);
    }

    /**
     * Handler for startDTD.
     */
    public void startDTD(String name, String publicId, String systemId) throws SAXException {
        if (letit)
            super.startDTD(name, publicId, systemId);
    }

    /**
     * Handler for endDTD.
     */
    public void endDTD() throws SAXException {
        if (letit)
            super.endDTD();
    }

    /**
     * Gets xml formatter.
     */
    public XmlFormatter getXmlFormatter() {
        return xmlFormatter;
    }

    /**
     * Sets xml formatter.
     */
    public void setXmlFormatter(XmlFormatter xmlFormatter) {
        this.xmlFormatter = xmlFormatter;
    }

    /**
     * Sets filter
     *
     * @param filter Nodes sequence (node1/node12/node121...)
     */
    public void setFilter(String filter) {
        this.filter = filter;
        this.filterAttrName = null;
        this.filterAttrValue = null;
    }

    /**
     * Sets filter
     *
     * @param filter    Nodes sequence (node1/node12/node121...)
     * @param attrName  The last node attribute name
     * @param attrValue The last node attribute value
     */
    public void setFilter(String filter, String attrName, String attrValue) {
        this.filter = filter;
        this.filterAttrName = attrName;
        this.filterAttrValue = attrValue;
    }

    /**
     * Resets filter.
     */
    public void resetFilter() {
        this.filter = null;
        this.filterAttrName = null;
        this.filterAttrValue = null;
    }

    /**
     * Inits action
     */
    private void initAction() throws XmlWriterException {
        if (xmlFormatter == null)
            throw new XmlWriterException("Invalid XmlFormatter");
        this.actionDone = false;
        this.cfilter = null;
        this.filterFound = false;
        this.continueAction = false;
        this.attrFound = false;
        this.oldName = null;
        this.newName = null;
        this.newValue = null;
    }

    /**
     * Write text using xml formater.
     *
     * @param s Text
     */
    public void writeText(String s) throws XmlWriterException,XmlFormatterException {
        if (xmlFormatter == null)
            throw new XmlWriterException("Invalid XmlFormatter");
        xmlFormatter.writeText(s);
    }

    /**
     * Renames namespace of first element searched by filter.
     *
     * @param oldName The namespace prefix
     * @param newName The namespace new prefix
     * @throws XmlWriterException If execution fail
     */
    public void renameNamespace(String oldName, String newName) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 1;
            this.oldName = oldName;
            this.newName = newName;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Inserts namespace in the first element searched by filter.
     *
     * @param name  The namespace prefix
     * @param value The namespace URI value
     * @throws XmlWriterException If execution fail
     */
    public void insertNamespace(String name, String value) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 2;
            this.newName = name;
            this.newValue = value;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Updates namespace URI of first element searched by filter.
     *
     * @param name  The namespace prefix
     * @param value The namespace URI value
     * @throws XmlWriterException If execution fail
     */
    public void updateNamespace(String name, String value) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 3;
            this.oldName = name;
            this.newValue = value;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Deletes namespace of first element searched by filter.
     *
     * @param name The namespace prefix
     * @throws XmlWriterException If execution fail
     */
    public void deleteNamespace(String name) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 4;
            this.oldName = name;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Renames first element searched by filter.
     *
     * @param name The element name
     * @throws XmlWriterException If execution fail
     */
    public void renameElement(String name) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 5;
            this.newValue = name;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Insterts element in first element searched by filter. The new element
     * will be placed before the closing tag of filter element.
     *
     * @param name  The element name
     * @param value The element value
     * @throws XmlWriterException If execution fail
     */
    public void insertElement(String name, String value) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 6;
            this.newName = name;
            this.newValue = value;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Updates content of first element searched by filter.
     *
     * @param value The new element value
     * @throws XmlWriterException If execution fail
     */
    public void updateElement(String value) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 7;
            this.newValue = value;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Deletes first element searched by filter.
     *
     * @throws XmlWriterException If execution fail
     */
    public void deleteElement() throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 8;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Renames the attribute of first element searched by filter.
     *
     * @param oldName The attribute current name
     * @param newName The attribute new name
     * @throws XmlWriterException If execution fail
     */
    public void renameAttribute(String oldName, String newName) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 9;
            this.oldName = oldName;
            this.newName = newName;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Insterts attribute in first element searched by filter. The new attribute
     * will be placed after the last attribute.
     *
     * @param name  The attribute name
     * @param value The attribute value
     * @throws XmlWriterException If execution fail
     */
    public void insertAttribute(String name, String value) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 10;
            this.newName = name;
            this.newValue = value;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Updates attribute content of the first element searched by filter.
     *
     * @param name  The attribute name
     * @param value The new attribute value
     * @throws XmlWriterException If execution fail
     */
    public void updateAttribute(String name, String value) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 11;
            this.oldName = name;
            this.newValue = value;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Deletes attribute of the first element searched by filter.
     *
     * @param name The attribute name
     * @throws XmlWriterException If execution fail
     */
    public void deleteAttribute(String name) throws XmlWriterException {
        try {
            this.initAction();
            this.actionType = 12;
            this.oldName = name;
            xmlFormatter.format(this);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            actionDone = false;
            throw new XmlWriterException(e);
        }
    }

    /**
     * Check if the action was completed or not.
     */
    public boolean isActionDone() {
        return actionDone;
    }
}
