/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

/**
 * XmlFilterValue class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class XmlFilterBean {

    private String filter;
    private String filterAttrName;
    private String filterAttrValue;
    private boolean filterCheckParent = false;

    public XmlFilterBean() {
    }

    /*
     *  Gets filter
     */
    public String getFilter() {
        return filter;
    }

    /*
     *  Sets filter
     *   @param filter    Nodes sequence (node1/node12/node121...)
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /*
     *  Gets filter attribute name
     */
    public String getFilterAttrName() {
        return filterAttrName;
    }

    /*
     *  Sets filter attribute name
     *   @param filterAttrName   Attribute name
     */
    public void setFilterAttrName(String filterAttrName) {
        this.filterAttrName = filterAttrName;
    }

    /*
     *  Gets filter attribute value
     */
    public String getFilterAttrValue() {
        return filterAttrValue;
    }

    /*
     *  Sets filter attribute value
     */
    public void setFilterAttrValue(String filterAttrValue) {
        this.filterAttrValue = filterAttrValue;
    }


    public boolean isFilterCheckParent() {
        return filterCheckParent;
    }

    public void setFilterCheckParent(boolean filterCheckParent) {
        this.filterCheckParent = filterCheckParent;
    }
}
