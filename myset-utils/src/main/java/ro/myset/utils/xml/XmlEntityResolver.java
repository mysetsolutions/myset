/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.*;
import ro.myset.utils.common.Constants;


/**
 * XmlEntityResolver class. All systemId's on the same location with xsd schema.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class XmlEntityResolver implements EntityResolver {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlEntityResolver.class);

    private String xsdSrc = null;
    private String xsdPath = null;
    private boolean autoDiscover = true;


    public XmlEntityResolver(String xsdSrc) {
        try {
            LOG.debug("%s", xsdSrc);
            this.xsdSrc=xsdSrc;
            xsdSrc.lastIndexOf(Constants.SLASH);
            xsdPath = xsdSrc.substring(0, xsdSrc.lastIndexOf(Constants.SLASH) + 1);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }

    public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
        try {
            String newSystemId = xsdSrc;
            if(autoDiscover)
                newSystemId=xsdPath.concat(systemId.substring(systemId.lastIndexOf(Constants.SLASH) + 1, systemId.length()));
            LOG.debug("%s", newSystemId);
            return new InputSource(newSystemId);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new SAXException(e.getMessage());
        }
    }

    /*
     * Is autoDiscover
     */
    public boolean isAutoDiscover() {
        return autoDiscover;
    }

    /*
     * Set autoDiscover
     */
    public void setAutoDiscover(boolean autoDiscover) {
        this.autoDiscover = autoDiscover;
    }
}

