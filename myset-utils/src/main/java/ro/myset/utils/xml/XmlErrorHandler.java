/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.*;
import ro.myset.utils.common.Constants;


/**
 * XmlErrorHandler class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class XmlErrorHandler implements ErrorHandler {
    private static final Logger LOG = LogManager.getFormatterLogger(XmlErrorHandler.class);

    private boolean valid = true;

    public XmlErrorHandler() {
        //LOG.debug(this);
    }

    public final boolean isValid() {
        return valid;
    }

    public void warning(SAXParseException e) throws SAXException {
        LOG.warn("Line: %s, Column:%s - Message:%s",
                e.getLineNumber(), e.getColumnNumber(), e.getMessage());
    }

    public void error(SAXParseException e) throws SAXException {
        valid=false;
        LOG.error("Line: %s, Column:%s - Message:%s",
                e.getLineNumber(), e.getColumnNumber(), e.getMessage());
    }

    public void fatalError(SAXParseException e) throws SAXException {
        valid=false;
        LOG.fatal("Line: %s, Column:%s - Message:%s",
                e.getLineNumber(), e.getColumnNumber(), e.getMessage());
    }
}


