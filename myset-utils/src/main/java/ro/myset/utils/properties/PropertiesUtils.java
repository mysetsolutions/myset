/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * ApplicationProperties class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class PropertiesUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(PropertiesUtils.class);
    private File propertiesFile = null;
    private Properties properties = null;

    public PropertiesUtils() {
        LOG.debug(this);
    }

    /**
     * Class constructor.
     *
     * @param propertiesFile Properties file source.
     * @throws IOException If the execution fail.
     */
    public PropertiesUtils(File propertiesFile) throws IOException {
        LOG.debug(this);
        this.propertiesFile = propertiesFile;
        this.refresh();
    }

    /**
     * Refresh properties.
     *
     * @throws IOException If the execution fail.
     */
    public final void refresh() throws IOException {
        LOG.debug(Constants.EMPTY);
        FileInputStream fis = new FileInputStream(propertiesFile);
        properties.load(fis);
    }

    /**
     * Gets properties.
     */
    public final Properties getProperties() {
        return properties;
    }

    /**
     * Gets property by key.
     */
    public final String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * Sets property into property file.
     *
     * @param key   Properties key.
     * @param value Properties value.
     * @throws PropertiesUtilsException If the execution fail.
     */
    public final void setProperty(String key, String value) throws PropertiesUtilsException {
        LOG.debug("%s:%s", key, value);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(propertiesFile);
            properties.setProperty(key, value);
            properties.store(fos, null);
            fos.close();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            LOG.error(Constants.EMPTY, e);
            throw new PropertiesUtilsException(e);
        }
    }

    /**
     * Remove property from property file.
     *
     * @param key Properties key.
     * @throws PropertiesUtilsException If the execution fail.
     */
    public final void removeProperty(String key) throws PropertiesUtilsException {
        LOG.debug(key);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(propertiesFile);
            properties.remove(key);
            properties.store(fos, null);
            fos.close();
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            LOG.error(Constants.EMPTY, e);
            throw new PropertiesUtilsException(e);
        }
    }

    /**
     * Gets properties file source.
     */
    public final File getPropertiesFile() {
        return propertiesFile;
    }

    /**
     * Sets properties file. After that need to perform refresh.
     *
     * @param propertiesFile Properties file source.
     * @throws PropertiesUtilsException If the execution fail.
     */
    public void setPropertiesFile(File propertiesFile) {
        LOG.debug(propertiesFile);
        this.propertiesFile = propertiesFile;
    }

}
