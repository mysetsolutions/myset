/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.serialization;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

/**
 * Serialization/Deserialization utilities class
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */

public class SerializationUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(SerializationUtils.class);

    /**
     * Serialize object to OutputStream using ObjectOutputStream
     *
     * @param obj Serializable object.
     * @param os  OutputStream for serialized object.
     */
    public static final void serialize(Object obj, OutputStream os) throws IOException {
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            BufferedOutputStream bos = new BufferedOutputStream(os);
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            bos.flush();
            bos.close();
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }

    /**
     * Deserialize object from InputStream using ObjectInputStream
     *
     * @param is InputStream for serialized object.
     */
    public static final Object deserialize(InputStream is) throws IOException, ClassNotFoundException {
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            BufferedInputStream bis = new BufferedInputStream(is);
            ObjectInputStream ois = new ObjectInputStream(bis);
            Object obj = ois.readObject();
            ois.close();
            bis.close();
            return obj;
        } finally {
            LOG.debug(Constants.EVENT_COMPLETED);
        }
    }

    /**
     * Serialize object to OutputStream using XMLEncoder
     *
     * @param obj Serializable object.
     * @param os  OutputStream for serialized object.
     */
    public static final void serializeToXml(Object obj, OutputStream os) throws IOException {
        LOG.debug(Constants.EMPTY);
        BufferedOutputStream bos = new BufferedOutputStream(os);
        XMLEncoder encoder = new XMLEncoder(bos);
        encoder.writeObject(obj);
        encoder.close();
    }

    /**
     * Deserialize object from InputStream using XMLDecoder
     *
     * @param is InputStream for serialized object.
     */
    public static final Object deserializeFromXml(InputStream is) throws IOException, ClassNotFoundException {
        LOG.debug(Constants.EMPTY);
        BufferedInputStream bis = new BufferedInputStream(is);
        XMLDecoder decoder = new XMLDecoder(bis);
        Object obj = decoder.readObject();
        return obj;
    }

}
