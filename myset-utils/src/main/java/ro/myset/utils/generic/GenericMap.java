/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.generic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

/**
 * GenericMap class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class GenericMap<K, V> extends HashMap {
    private static final Logger LOG = LogManager.getFormatterLogger(GenericMap.class);

    public GenericMap() {
        //LOG.debug(this);
    }

    @Override
    public Object get(Object key) {
        //LOG.debug(key);
        if (key != null && super.containsKey(key))
            return super.get(key);
        else
            return null;
    }

    /**
     * Gets value
     */
    public final String getString(Object key) {
        //LOG.debug(key);
        if (key != null && super.containsKey(key))
            return super.get(key).toString();
        else
            return null;
    }

    /**
     * Gets value
     */
    public final GenericBean getGenericBean(Object key) {
        //LOG.debug(key);
        if (key != null && super.containsKey(key))
            return (GenericBean) super.get(key);
        else
            return null;
    }


    /*
     * Put an entry if key is not nulls.
     *
     * @see java.util.concurrent.HashMap#put(java.lang.Object, java.lang.Object)
     */
    @Override
    public Object put(Object key, Object value) {
        //LOG.debug("%s:%s", key, value);
        if (key != null)
            return super.put(key, value);
        return null;
    }

    /**
     * Remove map entry
     */
    @Override
    public Object remove(Object key) {
        //LOG.debug(key);
        if (key != null && super.containsKey(key))
            return super.remove(key);
        return null;
    }

}
