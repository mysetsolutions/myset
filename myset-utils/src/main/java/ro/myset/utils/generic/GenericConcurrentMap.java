/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.generic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentHashMap;

/**
 * GenericConcurrentMap class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class GenericConcurrentMap<K, V> extends ConcurrentHashMap {
    private static final Logger LOG = LogManager.getFormatterLogger(GenericConcurrentMap.class);

    public GenericConcurrentMap() {
        //LOG.debug(this);
    }

    @Override
    public Object get(Object key) {
        //LOG.debug(key);
        if (key != null)
            return super.get(key);
        else
            return null;
    }

    /**
     * Gets value
     */
    public final String getString(Object key) {
        //LOG.debug(key);
        if (key != null)
            return super.get(key).toString();
        else
            return null;
    }

    /**
     * Gets value
     */
    public final GenericBean getGenericBean(Object key) {
        //LOG.debug(key);
        if (key != null)
            return (GenericBean) super.get(key);
        else
            return null;
    }


    /*
     * Put an entry if key and value are not nulls.
     *
     * @see java.util.concurrent.ConcurrentHashMap#put(java.lang.Object, java.lang.Object)
     */
    @Override
    public Object put(Object key, Object value) {
        //LOG.debug("%s:%s", key, value);
        if (key != null && value != null)
            return super.put(key, value);
        return null;
    }

    /*
     * Put an entry only if key is absent and key and value are not nulls.
     */
    @Override
    public Object putIfAbsent(Object key, Object value) {
        //LOG.debug("%s:%s", key, value);
        if (key != null && value != null)
            return super.putIfAbsent(key, value);
        return null;
    }

    @Override
    public Object remove(Object key) {
        //LOG.debug(key);
        return super.remove(key);
    }

}
