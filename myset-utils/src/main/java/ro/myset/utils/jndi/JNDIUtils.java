/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.jndi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.common.Constants;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Hashtable;

/**
 * JNDI utilities class.
 *
 * @author Cristian Gheorghe Florescu
 * @version 1.0
 */
public class JNDIUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(JNDIUtils.class);

    /**
     * @param env  Environment custom parameters.
     * @return Context
     * @throws JNDIUtilsException If the execution fail.
     */
    public static Context getContext(Hashtable env) throws JNDIUtilsException {
        try {
            return new InitialContext(env);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new JNDIUtilsException(e);
        }
    }


    /**
     * @param icf  Initial context factory.
     * @param purl Provider url.
     * @param sp   User name.
     * @param sc   Password.
     * @param ssl  Use ssl sockets.
     * @return Context
     * @throws JNDIUtilsException If the execution fail.
     */
    public static Context getContext(String icf, String purl, String sp, String sc, boolean ssl) throws JNDIUtilsException {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, icf);
        env.put(Context.PROVIDER_URL, purl);
        env.put(Context.SECURITY_PRINCIPAL, sp);
        env.put(Context.SECURITY_CREDENTIALS, sc);
        if (ssl)
            env.put(Context.SECURITY_PROTOCOL, "ssl");
        try {
            return new InitialContext(env);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new JNDIUtilsException(e);
        }
    }

    /**
     * Lookups for a JNDI remote object.
     *
     * @param ctx      Context object.
     * @param jndiPath Ex:jndi/myObject.
     * @throws JNDIUtilsException If the execution fail.
     */
    public static Object jndiLookup(Context ctx, String jndiPath) throws JNDIUtilsException {
        try {
            return ctx.lookup(jndiPath);
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
            throw new JNDIUtilsException(e);
        }
    }
}
