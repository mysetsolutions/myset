<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:x="http://www.myset.ro">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <root>
            <xsl:for-each select="x:root/x:tests">
                <tests>
                    <xsl:attribute name="id">
                        <xsl:text>t</xsl:text><xsl:value-of select="position()"/>
                    </xsl:attribute>
                    <xsl:for-each select="x:test">
                        <test>
                            <xsl:attribute name="id">
                                <xsl:value-of select="@id"/>
                            </xsl:attribute>
                            <xsl:attribute name="description">
                                <xsl:value-of select="@description"/>
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </test>
                    </xsl:for-each>
                </tests>
            </xsl:for-each>
        </root>
    </xsl:template>
</xsl:stylesheet>
