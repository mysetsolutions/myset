/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.common.CommonUtils;
import ro.myset.utils.common.Constants;

import java.util.Date;
import java.util.Locale;

public class TestCommonUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestCommonUtils.class);

    public TestCommonUtils() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            Locale.setDefault(new Locale("en","US"));
            //Locale.setDefault(new Locale("ro","RO"));
            LOG.debug(Locale.getDefault());
            LOG.debug(Locale.getDefault().getLanguage());
            LOG.debug(Locale.getDefault().getCountry());
            LOG.debug(Locale.getDefault().getDisplayCountry());
            for (int i = 0; i < Locale.getAvailableLocales().length; i++) {
                //LOG.debug(Locale.getAvailableLocales()[i] + " " + Locale.getAvailableLocales()[i].getDisplayCountry());
            }
            Object[] o = {"One", 1, new Date()};
            LOG.debug(CommonUtils.format("{0} {1} {2}", o));
            LOG.debug(CommonUtils.format (new Date(),"dd-MM-yyyy HH:mm:ss"));
            String[] sa = {"a", "b", "c"};
            LOG.debug(CommonUtils.format(sa, "IN ('", "') ", "','"));
            LOG.debug(CommonUtils.stripQuota("text \" and other text"));
            LOG.debug(CommonUtils.stripTags("<a href = 'cccc' > ss & s < / a > "));
            LOG.debug(CommonUtils.isValidDate("30-01-2000 18:39:49", "dd-MM-yyyy HH:mm:ss"));
            LOG.debug(CommonUtils.getDate("30-01-2000 17:49", "dd-MM-yyyy HH:mm"));
            LOG.debug(CommonUtils.isValidBigDecimal("123456789012456.123456789"));
            LOG.debug(CommonUtils.getBigDecimal("123456789012456.123456789"));
            LOG.debug(CommonUtils.format(CommonUtils.getBigDecimal("12345678901234567890123456789.1234567890"), "###,###.##"));
        } catch (Exception e) {
            LOG.error(Constants.EMPTY,e);
        }
    }

}
