/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Test;
import ro.myset.utils.security.MD5Utils;

import java.io.File;

public class TestMD5Utils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestMD5Utils.class);

    public TestMD5Utils(){
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            // initialization
            String text = "abcd1234";
            LOG.debug("Original text: %s",text);
            LOG.debug("MD5 encrypted: %s",MD5Utils.encrypt(text));
            LOG.debug("MD5 generated: %s", MD5Utils.generate());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
