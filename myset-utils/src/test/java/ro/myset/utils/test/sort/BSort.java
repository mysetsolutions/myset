/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.sort;

/*
 * Generates n random strings and then sorts them using bubble sort.
 */
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.test.common.TestCommonUtils;

import java.util.Date;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;

public class BSort {
    private static final Logger LOG = LogManager.getFormatterLogger(BSort.class);

    public BSort() {
        //LOG.debug(this);
    }
    static int num = 1000;  // number of strings to sort
    static int ssize = 100;  // size of each string.
    static char[] chars = new char[] {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };

    static void bsort(List array){
        int n = array.size();
        for (int i = n - 1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (((String)array.get(j)).compareTo(((String)array.get(j+1))) < 0){
                    Object t = array.get(j);
                    array.set(j, array.get(j+1));
                    array.set(j+1, t);
                }
            }
        }
    }

    static void printa(List array){
        for (int i = 0; i < array.size(); i++){
            LOG.debug("[" + i + "] " + array.get(i));
        }
    }

    static void populate(List array, int num){
        Random ran = new java.util.Random();
        for (int i = 0; i < num; i++){
            String s = "";
            for (int j = 0; j < ssize; j++){
                int r = ran.nextInt(chars.length);
                s += chars[r];
            }
            array.add(s);
        }
    }

    @Test
    public void test(){
        List array = new ArrayList(); // Do not specify the size on purpose

        LOG.debug("Populating the array with " + num + " Strings, each of " + ssize + " characters ...");
        populate(array, num);
        LOG.debug("Populating done");

        LOG.debug("Original Array::"); printa(array);

        LOG.debug("Sorting the array ...");
        bsort(array);
        //array.sort();
        LOG.debug("Sorting done");

        LOG.debug("Sorted Array::"); printa(array);
    }
}