/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Test;
import ro.myset.utils.security.EncryptionDecryptionUtils;

public class TestEncryptDecryptUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestEncryptDecryptUtils.class);

    public TestEncryptDecryptUtils(){
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            // initialization
            String algorithmName = null;
            String algorithmKey = null;
            String text = null;
            String encryptedText = null;
            String decryptedText = null;


            LOG.debug("=========================================");
            algorithmName = "AES";
            algorithmKey = "MysetDefault-Key";
            text = "This is a text ...";
            LOG.debug("Algorithm name: %s", algorithmName);
            LOG.debug("Algorithm key: %s", algorithmKey);
            LOG.debug("Text: %s", text);
            LOG.debug("-----------------------------------------");
            encryptedText = EncryptionDecryptionUtils.encrypt(algorithmName, algorithmKey, text);
            LOG.debug("Encrypted Text: %s", encryptedText);
            decryptedText = EncryptionDecryptionUtils.decrypt(algorithmName, algorithmKey, encryptedText);
            LOG.debug("Decrypted Text: %s", decryptedText);

            LOG.debug("=========================================");
            algorithmName = "Blowfish";
            algorithmKey = "EncryptionKey123";
            text = "This is a text ...";
            LOG.debug("Algorithm name: %s", algorithmName);
            LOG.debug("Algorithm key: %s", algorithmKey);
            LOG.debug("Text: %s", text);
            LOG.debug("-----------------------------------------");
            encryptedText = EncryptionDecryptionUtils.encrypt(algorithmName, algorithmKey, text);
            LOG.debug("Encrypted Text: %s", encryptedText);
            decryptedText = EncryptionDecryptionUtils.decrypt(algorithmName, algorithmKey, encryptedText);
            LOG.debug("Decrypted Text: %s", decryptedText);

            LOG.debug("=========================================");
            algorithmName = "RC2";
            algorithmKey = "EncryptionKey123";
            text = "This is a text ...";
            LOG.debug("Algorithm name: %s", algorithmName);
            LOG.debug("Algorithm key: %s", algorithmKey);
            LOG.debug("Text: %s", text);
            LOG.debug("-----------------------------------------");
            encryptedText = EncryptionDecryptionUtils.encrypt(algorithmName, algorithmKey, text);
            LOG.debug("Encrypted Text: %s", encryptedText);
            decryptedText = EncryptionDecryptionUtils.decrypt(algorithmName, algorithmKey, encryptedText);
            LOG.debug("Decrypted Text: %s", decryptedText);


            LOG.debug("=========================================");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
