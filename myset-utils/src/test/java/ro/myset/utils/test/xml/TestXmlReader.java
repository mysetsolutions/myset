/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.xml;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.common.Constants;
import ro.myset.utils.generic.GenericBean;
import ro.myset.utils.xml.XmlReader;

import java.util.ArrayList;

public class TestXmlReader {
    private static final Logger LOG = LogManager.getFormatterLogger(TestXmlReader.class);

    public TestXmlReader() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        long mm = Runtime.getRuntime().maxMemory() / (1024 * 1024);
        long um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
        LOG.debug("Memory usage: %s of %s Mb", um, mm);
        try {
            XmlReader xr = new XmlReader(System.getProperty("myset.utils.home").concat("/files/test.xml"));
//            // level
//            xr.addFilter("root");
//            xr.addFilter("root/t");
//            xr.addFilter("root/t1");
//            xr.addFilter("root/t", "id", "1");
//            xr.addFilter("root/t", "id", "2");
//            xr.addFilter("root/t", "id", "3");
//
//            // level
//            xr.addFilter("root/tests", "id", "t1");
//            xr.addFilter("root/tests", "id", "t2");
//            xr.addFilter("root/tests", "id", "t3");
//            xr.addFilter("root/tests/test", "id", "1");
//            xr.addFilter("root/tests/test", "id", "2", true);
//            xr.addFilter("root/tests/test", "id", "3", true);
//
//            // level
//            xr.addFilter("root/tests-group");
//            xr.addFilter("root/tests-group/tests", "id", "t1");
//            xr.addFilter("root/tests-group/tests", "id", "t2");
//            xr.addFilter("root/tests-group/tests", "id", "t3");
//            xr.addFilter("root/tests-group/tests/tests","id","t1",true);
//            xr.addFilter("root/tests-group/tests/tests", "id", "t2", true);
//            xr.addFilter("root/tests-group/tests/tests", "id", "t3", true);
//            xr.addFilter("root/tests-group/tests/tests/test", "id", "1", true);
//            xr.addFilter("root/tests-group/tests/tests/test", "id", "2", true);
//            xr.addFilter("root/tests-group/tests/tests/test", "id", "3", true);

            ArrayList<GenericBean> beans;


            // read nodes values
            LOG.debug("-----------------------readNodes----------------------------");
            ArrayList<String> nodesNames = new ArrayList<String>();
            nodesNames.add("t");
            //nodesNames.add("t1");
            //nodesNames.add("t2");
            //nodesNames.add("t3");
            beans = xr.readNodes("root", nodesNames);
            for (int i = 0; i < beans.size(); i++) {
                for (int j = 0; j < beans.get(i).getFieldsNames().size(); j++) {
                    LOG.debug("%s=%s", beans.get(i).getFieldsNames().get(j), beans.get(i).getFields().get(beans.get(i).getFieldsNames().get(j)));
                }
                LOG.info(beans.get(i).getValue());
            }


            // read attributes values
            LOG.debug("-----------------------readAttributes----------------------------");
            ArrayList<String> attrsNames = new ArrayList<String>();
            attrsNames.add("id");
            attrsNames.add("description");
            beans = xr.readAttributes("root/t1", attrsNames);
            for (int i = 0; i < beans.size(); i++) {
                for (int j = 0; j < beans.get(i).getFieldsNames().size(); j++) {
                    LOG.debug("%s=%s", beans.get(i).getFieldsNames().get(j), beans.get(i).getFields().get(beans.get(i).getFieldsNames().get(j)));
                }
                LOG.info(beans.get(i).getValue());
            }


            // System.gc();
            um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
            LOG.debug("Memory usage: %s of %s Mb", um, mm);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }

    }


}
