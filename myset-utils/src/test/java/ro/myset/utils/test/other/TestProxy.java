/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.other;

import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.net.*;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;


public class TestProxy {


    public static void main(String[] args) {
        try {
            //========================================================================
            //===================        SSL TRUST MANAGER     =======================
            //========================================================================

            SSLContext sslContext = SSLContext.getInstance("SSL");

            // set up a TrustManager that trusts everything
            sslContext.init(null, new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    System.out.println("getAcceptedIssuers =============");
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs,
                                               String authType) {
                    System.out.println("checkClientTrusted =============");
                }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                    System.out.println("checkServerTrusted =============");
                }
            }}, new SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(
                    sslContext.getSocketFactory());

            HttpsURLConnection
                    .setDefaultHostnameVerifier(new HostnameVerifier() {
                        public boolean verify(String arg0, SSLSession arg1) {
                            System.out.println("hostnameVerifier =============");
                            return true;
                        }
                    });

            HttpsURLConnection.setDefaultSSLSocketFactory(
                    sslContext.getSocketFactory());

            HttpsURLConnection
                    .setDefaultHostnameVerifier(new HostnameVerifier() {
                        public boolean verify(String arg0, SSLSession arg1) {
                            System.out.println("hostnameVerifier =============");
                            return true;
                        }
                    });

            //========================================================================
            //========================================================================
            //========================================================================

            //PROXY
            SocketAddress proxyAddress = new InetSocketAddress("10.1.1.4", 9050);
            Proxy proxy = new Proxy(Proxy.Type.SOCKS, proxyAddress);

            // TARGET

            URL url = new URL("https://www.myset.ro");
            //URL url = new URL("https://api.ipify.org/?format=text");
            //URL url = new URL("https://www.whatismybrowser.com");

            //URL url = new URL("http://web-myset.rhcloud.com");
            //URL url = new URL("http://portal-myset.rhcloud.com/snoop.jsp");

            // REQUEST HEADERS
            HttpURLConnection cn = (HttpURLConnection) url.openConnection();
            //HttpURLConnection cn = (HttpURLConnection) url.openConnection(proxy);
            cn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0");
            //cn.setRequestProperty("User-Agent","Mozilla/5.0 (Android 6.0; Mobile; rv:45.0) Gecko/20100101 Firefox/45.0");
            cn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            cn.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
            cn.setRequestProperty("Accept-Encoding", "gzip, deflate");

            // RESPONSE HEADERS
            System.out.println("-----------------------------------");
            System.out.println(cn.getHeaderFields());

            // RESPONSE CONTENT
            System.out.println("-----------------------------------");
            BufferedInputStream bis = null;
            if ("gzip".equals(cn.getContentEncoding()))
                bis = new BufferedInputStream(new GZIPInputStream(cn.getInputStream()));
            else
                bis = new BufferedInputStream(cn.getInputStream());

            byte[] buff = new byte[2048];
            int rb = 0;
            while ((rb = bis.read(buff)) > 0) {
                System.out.print(new String(Arrays.copyOf(buff, rb)));
            }
            System.out.println();
            System.out.println("-----------------------------------");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
