package ro.myset.utils.test.executor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.executor.ExecuteExceptionHandler;

public class TestExecuteExceptionHandler implements ExecuteExceptionHandler {
    private static final Logger LOG = LogManager.getFormatterLogger(TestExecuteExceptionHandler.class);

    @Override
    public void handle(Runnable r, Throwable t) {
        LOG.error(r,t);
    }
}
