/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xerces.parsers.SAXParser;
import org.junit.Test;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.XMLReader;
import ro.myset.utils.common.Constants;
import ro.myset.utils.xml.XmlEntityResolver;
import ro.myset.utils.xml.XmlErrorHandler;
import ro.myset.utils.xml.XmlParameters;
import ro.myset.utils.xml.XmlValidator;

import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;

public class TestXmlValidator {
    private static final Logger LOG = LogManager.getFormatterLogger(TestXmlValidator.class);

    public TestXmlValidator() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        long mm = Runtime.getRuntime().maxMemory() / (1024 * 1024);
        long um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
        LOG.debug("Memory usage: %s of %s Mb", um, mm);
        try {
            String xmlSrc = System.getProperty("myset.utils.home").concat("/files/test.xml");
            String xsdSrc = System.getProperty("myset.utils.home").concat("/files/test.xsd");

            // initialize
            XmlValidator xv = new XmlValidator();

            //well-formed validation
            XMLReader xmlReader= XmlParameters.getSAXParser();
            xv.validate(xmlReader,new FileInputStream(xmlSrc));
            LOG.debug("Done well formed validation");

            //schema xsd validation
            XMLReader xmlReaderValidator=XmlParameters.getSAXParserValidator();
            XmlEntityResolver entityResolver=new XmlEntityResolver(xsdSrc);
            ErrorHandler errorHandler=new XmlErrorHandler();
            xv.validateSchema(xmlReaderValidator,new FileInputStream(xmlSrc),entityResolver,errorHandler);
            LOG.debug("Done schema xsd validation");


            um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
            LOG.debug("Memory usage: %s of %s Mb", um, mm);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
    }


}
