/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.xml.XmlFormatter;
import ro.myset.utils.xml.XmlWriter;

public class TestXmlWriter {
    private static final Logger LOG = LogManager.getFormatterLogger(TestXmlWriter.class);

    public TestXmlWriter() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        long mm = Runtime.getRuntime().maxMemory() / (1024 * 1024);
        long um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
        LOG.debug("Memory usage: %s of %s Mb", um, mm);
        try {
            XmlWriter xmlw = new XmlWriter();
            XmlFormatter xf = new XmlFormatter();
            xf.setTemporaryPath(System.getProperty("myset.utils.home").concat("/temp"));
            xf.setFrom(System.getProperty("myset.utils.home").concat("/files/test.xml"));
            xf.setTo(System.getProperty("myset.utils.home").concat("/temp/test-write.xml"));
            xf.setCheckWellFormed(false);
            xf.setAllowComments(false);
            xf.setLineSeparatorDefault();
            xf.setIndent(4);
            xf.setAttributeOnNewLine(false);
            xmlw.setXmlFormatter(xf);

            // XML first line
            xmlw.writeText("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

            // FILTERS - only one line at time
            //xmlw.setFilter("root");
            xmlw.setFilter("root/tests", "id", "t2");

            // NAMESPACES - only one line at time
            //xmlw.insertNamespace("x", "http://www.x.org");
            //xmlw.renameNamespace("xsi","x");
            //xmlw.updateNamespace("xsi","http://www.xsi.org");
            //xmlw.deleteNamespace("xsi");

            // ELEMENTS - only one line at time
            //xmlw.renameElement("e");
            xmlw.insertElement("e", "value");
            //xmlw.updateElement("new value");
            //xmlw.deleteElement();

            // ATTRIBUTES - only one line at time
            //xmlw.renameAttribute("id","uuid");
            //xmlw.updateAttribute("id", "new value");
            //xmlw.deleteAttribute("id");


            um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
            LOG.debug("Memory usage: %s of %s Mb", um, mm);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
