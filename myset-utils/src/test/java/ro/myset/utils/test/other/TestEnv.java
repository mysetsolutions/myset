package ro.myset.utils.test.other;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.*;

public class TestEnv {
    private static final Logger LOG = LogManager.getFormatterLogger(TestEnv.class);

    @Test
    public void testEnv() {
        try {
            Map<String, String> env = System.getenv();
            LOG.debug("================ System environment ===================");
            for (String envName : env.keySet()) {
                LOG.debug("%s=%s", envName, env.get(envName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testJvmProperties() {
        try {
            Properties p = System.getProperties();
            Enumeration e = p.keys();
            Object key;
            LOG.debug("================ JVM Properties ===================");
            while (e.hasMoreElements()) {
                key = e.nextElement();
                LOG.debug("%s=%s", key, p.getProperty(key.toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
