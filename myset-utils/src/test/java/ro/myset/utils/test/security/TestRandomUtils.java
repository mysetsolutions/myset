/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.security.RandomUtils;

public class TestRandomUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestRandomUtils.class);

    public TestRandomUtils() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            int i = 10;
            LOG.debug("randomString(%s): %s", i, RandomUtils.randomString(i));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
