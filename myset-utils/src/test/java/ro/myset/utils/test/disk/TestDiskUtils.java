/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.disk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.common.Constants;
import ro.myset.utils.disk.DiskUtils;

import java.io.File;

public class TestDiskUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestDiskUtils.class);

    public TestDiskUtils() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            File homePath = DiskUtils.getDirectory(System.getProperty("myset.utils.home"));
            LOG.debug(homePath);
            LOG.debug(DiskUtils.getFolderSize(homePath));
            DiskUtils.mkdir(System.getProperty("myset.utils.home").concat("/temp/test"), true);
            DiskUtils.writeIntoFile(System.getProperty("myset.utils.home").concat("/temp/test/test1.txt"), "Test...", false);
            LOG.debug(DiskUtils.getFileExtension(System.getProperty("myset.utils.home").concat("/temp/test/test1.txt")));
            byte[] ba = DiskUtils.readFileContent(System.getProperty("myset.utils.home").concat("/temp/test/test1.txt")).toByteArray();
            LOG.debug(new String(ba));
            byte[] bat = DiskUtils.readFileContent(System.getProperty("myset.utils.home").concat("/temp/test/test1.txt"),2).toByteArray();
            LOG.debug(new String(bat));
            DiskUtils.copyFile(DiskUtils.getFile(System.getProperty("myset.utils.home").concat("/temp/test/test1.txt")),
                    new File(System.getProperty("myset.utils.home").concat("/temp/test/test2.txt")),false);
            DiskUtils.mkdir(System.getProperty("myset.utils.home").concat("/temp/test1"), true);
            DiskUtils.copy(DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test")),
                    DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test1")), true);
            DiskUtils.mkdir(System.getProperty("myset.utils.home").concat("/temp/test2"), true);
            DiskUtils.copy(DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test")),
                    DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test2")), true);
            DiskUtils.delete(DiskUtils.getFile(System.getProperty("myset.utils.home").concat("/temp/test/test2.txt")));
            DiskUtils.deleteDirectory(DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test2")));
            LOG.debug(DiskUtils.getFolderSize(DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp"))));
            DiskUtils.deleteDirectory(DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test")));
            DiskUtils.deleteDirectory(DiskUtils.getDirectory(System.getProperty("myset.utils.home").concat("/temp/test1")));

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }

    }


}