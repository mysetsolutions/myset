/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.other;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class TestArrayList {
    private static final Logger LOG = LogManager.getFormatterLogger(TestArrayList.class);

    @Test
    public void test() {
        String names[] = {"Ana-Maria", "George", "Ioana", "Ion", "Marius", "Radu", "Claudiu", "Vlad", "Nico", "Andrei", "Ana", "Ion", "Ana"};

        // Convert array to list
        ArrayList list = new ArrayList(Arrays.asList(names));
        LOG.debug("Converted list: [length: %s ] ", list.size());
        LOG.debug(list);

        // Sort list
        Collections.sort(list);
        Iterator i = list.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            LOG.debug(list.indexOf(o) + " " + o);
        }
        LOG.debug("Sorted list: [length: %s ] ", list.size());
        LOG.debug(list);

        // Search element
        String searchText="Ana";
        LOG.debug("'%s' is at index %s" ,searchText ,Collections.binarySearch(list, searchText) );

        // Search for element not in list
        searchText="Maria";
        LOG.debug("'%s' is at index %s" ,searchText ,Collections.binarySearch(list, searchText) );

    }
}