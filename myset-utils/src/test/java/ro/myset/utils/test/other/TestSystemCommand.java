/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.other;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.test.security.TestEncryptDecryptUtils;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class TestSystemCommand {
    private static final Logger LOG = LogManager.getFormatterLogger(TestSystemCommand.class);

    //@Test
    public void test() {
        try {
            executeCommand("date");
            executeCommand("ping -c 5 localhost");
            executeCommand("date");
            //t.executeCommand("xterm");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void executeCommand(String command) throws Exception {
        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        process = runtime.exec(command);
        InputStream stderr = process.getErrorStream();
        InputStreamReader isrErr = new InputStreamReader(stderr);
        BufferedReader brErr = new BufferedReader(isrErr);

        InputStream stdout = process.getInputStream();
        InputStreamReader isrStd = new InputStreamReader(stdout);
        BufferedReader brStd = new BufferedReader(isrStd);

        String val = null;
        while ((val = brStd.readLine()) != null) {
            LOG.debug(val);
        }

        while ((val = brErr.readLine()) != null) {
            LOG.debug(val);
        }
        int exitVal = process.waitFor();

    }


}
