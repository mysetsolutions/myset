/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.other;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class TestMath {
    private static final Logger LOG = LogManager.getFormatterLogger(TestMath.class);

    @Test
    public void testMath(){
        BigDecimal a=new BigDecimal("2100");
        BigDecimal b=new BigDecimal("201");
        LOG.debug("%s*%s=%s",a, b, a.multiply(b));
        LOG.debug("%s/%s=%s",a, b,a.divide(b,100,RoundingMode.HALF_EVEN));
    }

}
