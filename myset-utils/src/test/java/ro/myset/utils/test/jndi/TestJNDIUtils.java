/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.jndi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.common.Constants;
import ro.myset.utils.jndi.JNDIUtils;

import javax.naming.Context;

public class TestJNDIUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestJNDIUtils.class);

    //@Test
    public void test() {
        try {
            Context ctx = JNDIUtils.getContext(
                    "org.jboss.naming.remote.client.InitialContextFactory",
                    "remote://localhost:4447",
                    "admin",
                    "admin",
                    false);
            LOG.debug("Lookup context");
            JNDIUtils.jndiLookup(ctx, "test/resource");
            LOG.debug("Lookup done");
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }

    }

}