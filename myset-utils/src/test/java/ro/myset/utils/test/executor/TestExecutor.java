package ro.myset.utils.test.executor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.executor.Executor;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;


public class TestExecutor {
    private static final Logger LOG = LogManager.getFormatterLogger(TestExecutor.class);


    //@Test
    public void test() throws Exception {

        int tasks = 100000;
        List<Integer> executedTasks = new ArrayList<Integer>();

        // parameters
        int executeParallelism = 100;
        int executeMaxQueueCount = 10000;
        int executePoolingInterval = 1000;

        // ThreadPoolExecutor
        Executor executor = new Executor(executeParallelism);
        executor.setExecuteExceptionHandler(new TestExecuteExceptionHandler());
        executor.setShutdownOnExceptions(true);

        LOG.debug("*** START %s tasks submission", tasks);
        int i = 0;
        for (i = 1; i <= tasks; i++) {
            // Check queue
            if (i % executeMaxQueueCount == 0)
                LOG.debug("*** tasks submitted %s", i);
            while (executor.getQueue().size() >= executeMaxQueueCount) {
                LOG.debug("Wait queueSize:%s", executor.getQueue().size());
                Thread.sleep(executePoolingInterval);
            }

            // execute
            executor.execute(testAsync(i, executedTasks));

        }
        LOG.debug("*** END %s tasks submission", i - 1);
        executor.shutdown();
        while (!executor.isTerminated()) {
            Thread.sleep(executePoolingInterval);
        }

        LOG.debug("executedTasks:%s", executedTasks.size());
        //LOG.debug("executedTasks:%s", executedTasks);

        // check duplicates
        Set<Integer> executedTasksSet = new HashSet<Integer>();
        for (int j = 0; j < executedTasks.size(); j++) {
            if (executedTasksSet.add(executedTasks.get(j)) == false) {
                LOG.error("Duplicate found - %s", executedTasks.get(j));
            }
        }

        LOG.debug("*** DONE %s tasks", i - 1);

    }


    private Thread testAsync(int i, List<Integer> executedTasks) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // simulate a random processing wait
                    Thread.sleep(ThreadLocalRandom.current().nextInt(0, 10));

                    // test exception
                    if (i == -1)
                        throw new RuntimeException("Thread exception");

                    // log execution
                    logExecution(i, executedTasks);

                } catch (Exception e) {
                    LOG.error(e);
                    throw new RuntimeException(e);
                }
            }
        });
        return t;
    }


    private synchronized void logExecution(int i, List<Integer> executedTasks) {
        executedTasks.add(i);
    }


}
