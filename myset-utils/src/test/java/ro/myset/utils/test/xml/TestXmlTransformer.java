/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.common.Constants;
import ro.myset.utils.xml.XmlTransformer;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileOutputStream;

public class TestXmlTransformer {
    private static final Logger LOG = LogManager.getFormatterLogger(TestXmlTransformer.class);

    public TestXmlTransformer() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        long mm = Runtime.getRuntime().maxMemory() / (1024 * 1024);
        long um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
        LOG.debug("Memory usage: %s of %s Mb", um, mm);
        LOG.debug(Constants.EVENT_INVOKED);
        try {
            XmlTransformer.transform(new StreamSource(System.getProperty("myset.utils.home").concat("/files/test.xml")),
                    new StreamSource(System.getProperty("myset.utils.home").concat("/files/test.xsl")),
                    new StreamResult(new FileOutputStream(System.getProperty("myset.utils.home").concat("/temp/test-transformed.xml"))));
        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }
        LOG.debug(Constants.EVENT_COMPLETED);
        um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
        LOG.debug("Memory usage: %s of %s Mb", um, mm);

    }

}
