/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.security.UUIDUtils;

import java.util.UUID;

public class TestUUIDUtils {
    private static final Logger LOG = LogManager.getFormatterLogger(TestUUIDUtils.class);

    public TestUUIDUtils() {
        //LOG.debug(this);
    }

    private static void printUUID(UUID uuidObj) {
        LOG.debug(uuidObj.toString());
        LOG.debug("Version: %s", uuidObj.version());
        LOG.debug("Variant: %s", uuidObj.variant());
        LOG.debug("HashCode: %s", uuidObj.hashCode());
        LOG.debug("MostSignificantBits: %s", uuidObj.getMostSignificantBits());
        LOG.debug("LeastSignificantBits: %s", uuidObj.getLeastSignificantBits());
        if (uuidObj.version() == 1) {
            LOG.debug("Node: %s", uuidObj.node());
            LOG.debug("ClockSequence: %s", uuidObj.clockSequence());
            LOG.debug("Timestamp: %s", uuidObj.timestamp());
        }
    }

    @Test
    public void test() {
        LOG.debug(UUIDUtils.generate("m1"));
        LOG.debug(UUIDUtils.generate("m1"));
        LOG.debug(UUIDUtils.generateUsingTime("m1"));
        LOG.debug(UUIDUtils.generateUsingTime("m1"));
        LOG.debug(UUIDUtils.generate("m2"));
        LOG.debug(UUIDUtils.generate());
        LOG.debug(UUIDUtils.generate());
        String uuidValue = UUIDUtils.generate("test");
        // v3
        UUID uuidObj = UUIDUtils.parse(uuidValue);
        TestUUIDUtils.printUUID(uuidObj);

        // v4
        TestUUIDUtils.printUUID(UUIDUtils.parse(UUIDUtils.generate()));
    }

}
