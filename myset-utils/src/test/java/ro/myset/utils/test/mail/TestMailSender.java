/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ro.myset.utils.mail.MailSender;

import java.util.Properties;

public class TestMailSender {
    public static final String HTML_MESSAGE = "text/html";
    public static final String TEXT_MESSAGE = "text/plain";
    private static final Logger LOG = LogManager.getFormatterLogger(TestMailSender.class);
    private MailSender mailSender = null;


    public TestMailSender() throws Exception {
        //LOG.debug(this);
    }

    //@Test
    public final void localhostTest() throws Exception {
        Properties properties = new Properties();
        //properties.put("mail.debug", "true");
        properties.put("mail.smtp.host", "localhost");
        properties.put("mail.smtp.port", "25");
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", "true");

        // credentials
        properties.put("mail.user", "myuser");
        properties.put("mail.password", "mypassword");

        // MailSender
        MailSender mailSender = new MailSender();
        mailSender.send(mailSender.getSession(properties),
                "text/html",
                "UTF-8",
                "TEST<no-reply@myset.ro>",
                "otheruser@gmail.com",
                null,
                null,
                null,
                "Test message",
                "<h1>Message content ...</h1>"
        );

    }

    //@Test
    public final void googleTest() throws Exception {
        Properties properties = new Properties();
        //properties.put("mail.debug", "true");
        properties.put("mail.smtp.host", "smtp.googlemail.com");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        // credentials
        properties.put("mail.user", "googleuser");
        properties.put("mail.password", "googlepassword");

        // MailSender
        MailSender mailSender = new MailSender();
        mailSender.send(mailSender.getSession(properties),
                "text/html",
                "UTF-8",
                "TEST <googleuser@gmail.com>",
                "otheruser@gmail.com",
                null,
                null,
                null,
                "Test message",
                "<h1>Message content ...</h1>"
        );

    }
}
