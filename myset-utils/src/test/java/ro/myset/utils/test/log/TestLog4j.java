/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Date;

public class TestLog4j {
    private static final Logger LOG = LogManager.getFormatterLogger(TestLog4j.class);


    public TestLog4j() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            LOG.error("Error event");
            LOG.warn("Warn event");
            LOG.info("Info event");
            LOG.debug("Debug event");
            LOG.trace("Trace event");
            throw new NullPointerException("Test exception");
        } catch (Exception e) {
            LOG.error("", e);
        } finally {
            LOG.info("All done");
        }

    }

}
