/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.common.Constants;
import ro.myset.utils.xml.XmlFormatter;

/**
 * Created by myset
 */
public class TestXmlFormatter {
    private static final Logger LOG = LogManager.getFormatterLogger(TestXmlFormatter.class);

    public TestXmlFormatter() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        long mm = Runtime.getRuntime().maxMemory() / (1024 * 1024);
        long um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
        LOG.debug("Memory usage: %s of %s Mb", um, mm);
        try {
            XmlFormatter xf = new XmlFormatter();

            xf.setFrom(System.getProperty("myset.utils.home").concat("/files/test.xml"));
            xf.setTo(System.getProperty("myset.utils.home").concat("/temp/test-formated.xml"));
            xf.setTemporaryPath(System.getProperty("myset.utils.home").concat("/temp"));
            xf.setCheckWellFormed(false);
            xf.setLineSeparatorDefault();
            xf.setIndent(4);
            xf.setAttributeOnNewLine(false);
            xf.setAllowComments(false);
            xf.writeText("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

            // level
            xf.addFilter("root");

            // level
            xf.addFilter("root/t", "id", "1");
            xf.addFilter("root/t", "id", "2");
            xf.addFilter("root/t", "id", "3");

            //xf.addFilter("root/tests", "id", "t1");
            xf.addFilter("root/tests", "id", "t2");
            xf.addFilter("root/tests/test", true);
            //xf.addFilter("root/tests", "id", "t3");
            //xf.addFilter("root/tests/test", "id", "1");
            //xf.addFilter("root/tests/test", "id", "2");
            //xf.addFilter("root/tests/test", "id", "3", true);

            // level
//            xf.addFilter("root/tests-group");
//            //xf.addFilter("root/tests-group/tests", "id", "t1");
//            xf.addFilter("root/tests-group/tests", "id", "t2");
//            //xf.addFilter("root/tests-group/tests", "id", "t3");
//            //xf.addFilter("root/tests-group/tests/tests","id","t1",true);
//            xf.addFilter("root/tests-group/tests/tests", "id", "t2", true);
//            xf.addFilter("root/tests-group/tests/tests", "id", "t3", true);
//            //xf.addFilter("root/tests-group/tests/tests/test", "id", "1", true);
//            xf.addFilter("root/tests-group/tests/tests/test", "id", "2", true);
//            xf.addFilter("root/tests-group/tests/tests/test", "id", "3", true);


            xf.format();


            // System.gc();
            um = (Runtime.getRuntime().totalMemory() / (1024 * 1024) - Runtime.getRuntime().freeMemory() / (1024 * 1024));
            LOG.debug("Memory usage: %s of %s Mb", um, mm);

        } catch (Exception e) {
            LOG.error(Constants.EMPTY, e);
        }

    }


}
