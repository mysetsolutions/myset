/**
 * Copyright 2008 - MYSET SOLUTIONS
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ro.myset.utils.test.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Test;
import ro.myset.utils.common.Constants;
import ro.myset.utils.pool.Pool;

public class TestPool extends Pool {
    private static final Logger LOG = LogManager.getFormatterLogger(TestPool.class);

    public TestPool() {
        //LOG.debug(this);
    }

    //@Test
    public void test() {
        try {
            // logger initialization
            TestPool pool = new TestPool();
            pool.setPoolName("TestPool");
            pool.setPoolingInterval(1);
            pool.setMaxAttempts(3);
            pool.setRetryInterval(1);
            pool.setMinPoolSize(5);
            pool.setMaxPoolSize(-1);
            pool.setInactivityTimeout(10);
            pool.setUsageTimeout(30);
            pool.startup();
            while (pool.isStarted()) {
                pool.checkpoint();
                for (int i = 0; i < 4; i++) {
                    try {
                        Thread.sleep(new Double(Math.random() * 4000).longValue());
                        Object o = pool.doBorrow();
                        LOG.debug(o);
                        pool.doReturn(o);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Thread.sleep(5000);
                pool.shutdown();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object create() {
        try {
            LOG.debug(Constants.EVENT_INVOKED);
            Object o = new Object();
            Thread.sleep(3000);
            return o;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isValid(Object o) {
        if (o != null && o instanceof Object) {
            LOG.debug("%s,%s", o, true);
            return true;
        }
        LOG.debug("%s,%s", o, false);
        return false;
    }

    public void close(Object o) {
        try {
            LOG.debug(o);
            Thread.sleep(2000);
            o = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            this.startup();
            while (isStarted()) {
                this.checkpoint();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
