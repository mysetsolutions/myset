/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import ro.myset.utils.classloader.InternalClassLoader;
import ro.myset.utils.common.Constants;

import java.io.File;
import java.net.URL;

public class TestInternalClassLoader {
    private static final Logger LOG = LogManager.getFormatterLogger(TestInternalClassLoader.class);

    public TestInternalClassLoader() {
        LOG.debug(this);
    }

    @Test
    public void test() {
        try {
            File libsPath=new File(System.getProperty("myset.utils.home").concat("/files"));
            InternalClassLoader icl = new InternalClassLoader("MyClassLoader");
            icl.loadFile(new File(libsPath,"test.jar"));
            URL[] la = icl.listURLs();
            LOG.debug("ClassLoader %s",icl.getName());
            for (int i = 0; i < la.length; i++) {
                LOG.debug(la[i]);
            }
            LOG.debug(icl.isLoaded("mypackage.MyClass"));

        } catch (Exception e) {
            LOG.error(Constants.EMPTY,e);
        }

    }

}
