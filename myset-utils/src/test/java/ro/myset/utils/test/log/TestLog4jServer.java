/*
  Copyright 2008 - MYSET SOLUTIONS

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package ro.myset.utils.test.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.layout.SyslogLayout;
import org.apache.logging.log4j.core.net.DatagramSocketManager;
import org.junit.Test;

import java.io.File;

public class TestLog4jServer {
    private Logger LOG = LogManager.getFormatterLogger(TestLog4jServer.class);

    public TestLog4jServer() {
        //LOG.debug(this);
    }

    @Test
    public void test() {
        TestLog4jServer t = new TestLog4jServer();
        t.startServer("127.0.0.1",9000,2048);
    }

    public void startServer(String serverHost,int serverPort,int serverBufferSize) {
        try {
            LOG.debug("Logging server %s:%d starting",serverHost , serverPort);
            Layout logLayout= SyslogLayout.newBuilder().build();
            DatagramSocketManager datagramSocketManager = DatagramSocketManager.getSocketManager(serverHost,serverPort,logLayout,serverBufferSize);
            LOG.debug("Logging server %s:%d started",serverHost , serverPort);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        finally {
            LOG.debug("Logging server %s:%d stopped",serverHost , serverPort);
        }


    }

}
